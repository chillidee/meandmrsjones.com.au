<?php

class warehouse_database_class {

    private function warehouse_db_check_db() {
        $feed_products_db = $this->warehouse_db_connect();
        return ($feed_products_db) ? true : false;
    }

    private function warehouse_db_connect() {
        $connection_details = $this->warehouse_db_get_connections_details();
        return mysqli_connect($connection_details['db_host'], $connection_details['db_username'], $connection_details['db_password'], $connection_details['db_name']);
    }

    private function warehouse_db_get_connections_details() {
        $returnArray = array();
        $returnArray['db_name'] = get_option('warehouse_feed_products_db_name');
        $returnArray['db_username'] = get_option('warehouse_feed_products_db_usrname');
        $returnArray['db_password'] = get_option('warehouse_feed_products_db_psw');
        $returnArray['db_host'] = get_option('warehouse_feed_products_db_host');
        $returnArray['db_notification'] = get_option('warehouse_feed_products_db_notification');
        return $returnArray;
    }

    private function warehouse_db_close_db($db) {
        mysqli_close($db);
    }

    public function warehouse_db_do_query($queryString) {
        $con = $this->warehouse_db_connect();
        $result = mysqli_query($con, $queryString);
        $this->warehouse_db_close_db($con);
        return $result;
    }

    private function warehouse_check_if_table_exists($table) {
        $result = $this->warehouse_db_do_query('SELECT * FROM `' . $table . '`');
        return ($result) ? true : false;
    }

    public function warehouse_setup_feed_products_database() {
        $returnString = '';
        if ($this->warehouse_db_check_db()):
            if (!$this->warehouse_check_if_table_exists('feed_products')):
                $result = $this->warehouse_db_do_query('CREATE TABLE `feed_products` (`id` int(11) NOT NULL,`feed_id` int(11) NOT NULL,`Code` text NOT NULL,`Title` text NOT NULL,`Short Description` text NOT NULL, `Long Description` text NOT NULL,`Size` text NOT NULL,`Colour` text NOT NULL,`Brand` text NOT NULL,`Wholesale Price` text NOT NULL,`Category` text NOT NULL,`Image` text NOT NULL,`Barcode` text NOT NULL,`AvailableStock` text NOT NULL,`Weight` text NOT NULL,`imported` tinyint(1) NOT NULL,`new` text NOT NULL) ENGINE=InnoDB');
                $result = $this->warehouse_db_do_query("ALTER TABLE `feed_products`ADD PRIMARY KEY (`id`)");
                $result = $this->warehouse_db_do_query("ALTER TABLE `feed_products`MODIFY `id` int(11) NOT NULL AUTO_INCREMENT");
                $returnString = ($result) ? 'New table created in database' : 'Error creating table in database';
            else:
                $returnString = 'Database products table exists nothing to setup';
            endif;
            if (!$this->warehouse_check_if_table_exists('notifications')):
                $result = $this->warehouse_db_do_query("CREATE TABLE `notifications` (`id` int(11) NOT NULL,`feed_id` int(11) NOT NULL,`note` text NOT NULL,`product_code` text NOT NULL,`product_barcode` text NOT NULL,`product_id` int(11) NOT NULL,`product_title` text NOT NULL) ENGINE=InnoDB");
                $result = $this->warehouse_db_do_query("ALTER TABLE `notifications` ADD PRIMARY KEY (`id`)");
                $result = $this->warehouse_db_do_query("ALTER TABLE `notifications` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;");
                $returnString = ($result) ? 'New table created in database' : 'Error creating table in database';
            endif;
        else:
            $returnString = 'Error connecting to database';
        endif;
        return $returnString;
    }

    public function get_notification_email_list() {
        $db_notification = get_option('warehouse_feed_products_db_notification');
        if($db_notification) {
            return explode (", ", $db_notification); 
        }
        return null;
    }

}
