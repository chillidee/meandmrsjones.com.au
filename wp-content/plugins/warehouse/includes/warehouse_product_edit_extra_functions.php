<?php

require_once 'ajax_functions.php';

class warehouse_product_edit_extra_functions {

    public static function add_featured_meta_box() {
        add_meta_box('featured_products_meta', 'Featured Products', array('warehouse_product_edit_extra_functions', 'output_featured_products_meta'), 'page', 'normal', 'high');
        wp_enqueue_style('admin_featured_products', WAREHOUSE_PLUGIN_URL . '/includes/css/admin_featured_products.css');
        wp_enqueue_style('fontawesome', st_load_script_from_cdn('https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', get_template_directory_uri() . '/css/font-awesome.min.css'));
        wp_enqueue_script('admin_featured_products', WAREHOUSE_PLUGIN_URL . '/includes/js/admin_featured_products.js');
    }

    public static function output_featured_products_meta() {
        global $post;
        echo "<input id='post_id' type='hidden' value='$post->ID' />";
        echo self::featuredProductsAllProducts();
    }

    public static function featuredProductsAllProducts() {
        global $post;
        $featured_products = (array) json_decode(get_post_meta($post->ID, 'featured_products', true));
        $grid = '<div class="featured_products_grid_container_new"><div class="loading-overlay hidden"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div><input id="ajax_url" type="hidden" value="' . admin_url('admin-ajax.php') . '"/>';
        foreach (st_get_all_products_for_template($post->post_name, -1)[0] as $id):
            $_post = get_post($id);
            $grid .= "<div id='$id' class='featured_products_grid'>";
            $grid .= "<p class='gridNumber' data-product_id='$id'>";
            $grid .= (in_array($id, $featured_products)) ? array_search($id, $featured_products) : '';
            $grid .= "</p>";
            $grid .= "<input type='hidden' value='$id' name='product_id'/>";
            $grid .= "<img id='$id' src='" . self::get_image_src($id) . "'/>";
            $grid .= "<p class='product_title'>$_post->post_title</p>";
            $grid .= self::createGridNumbers($id, $featured_products);
            $grid .= "</div>";
        endforeach;
        $grid .= '</div>';
        $grid .= self::createShortcutbuttons($featured_products);
        $grid .= '<button class="button button-primary button-large" type="button" name="save_featured_products">Save</button>';
        return $grid;
    }

    public static function createShortcutbuttons($featured_products) {
        $featured_products = array_values($featured_products);
        $returnString = "<div class='shortcut-buttons-container'>";
        for ($i = 1; $i < 16; $i++):
            $returnString .= "<input name='sortcut-button' style='margin-right:10px;' type='button' class='button button-primary button-large ";
            $returnString .= (isset($featured_products[$i - 1]) && !empty($featured_products[$i - 1])) ? 'grid-button-pselected' : '';
            $returnString .= "' name='gridButtonNumber' value='$i'/>";
        endfor;
        return $returnString;
    }

    public static function createGridNumbers($id, $featured_products) {
        $featured_products = array_values($featured_products);
        $grid = "<div class='gridNumbersOverlay hidden'><i class='fa fa-times close-grid-overlay' aria-hidden='true'></i>";
        for ($i = 1; $i < 16; $i++):
            $grid .= "<button data-productNumber='$id' type='button' class='button button-primary button-large ";
            $grid .= ($featured_products[$i - 1] == $id) ? 'grid-button-selected' : '';
            $grid .= "' name='gridButtonNumber' value='$i'>$i</button>";
        endfor;
        $grid .= "</div>";
        return $grid;
    }

    public static function save_featured_products($post_id, $gridArray) {
        return update_post_meta($post_id, 'featured_products', $gridArray);
    }

    private function get_image_src($_product_id) {
        $image = wp_get_attachment_image_src(get_post_thumbnail_id($_product_id));
        if ($image):
            $image = $image[0];
        else:
            $image = get_post_meta($_product_id, 'main_image_link', true);
        endif;
        return $image;
    }

}
