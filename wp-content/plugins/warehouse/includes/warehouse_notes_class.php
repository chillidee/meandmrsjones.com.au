<?php

class warehouse_notes_class {

    public function __construct() {

        $destFolder = WAREHOUSE_PLUGIN_PATH . '/temp/';

        $this->warehouse_createDestFolder($destFolder);
    }

    public function warehouse_get_notes_json_file() {
        try {
            return $this->warehouse_create_notes_json();
        } catch (Exception $e) {
            wp_mail('boni@chillidee.com.au', 'MMJ Error - Notes Json', $e->getMessage(), 'From: MMJ <info@meandmrsjones.com.au>' . "\r\n");
        }
        return '';
    }

    private function warehouse_createDestFolder($destFolder) {
        if (!file_exists($destFolder)) {
            mkdir($destFolder, 0775, true);
        }
    }

    private function warehouse_write_notes_json($notes) {
        if (isset($notes) && !empty($notes)):
            $destFolder = WAREHOUSE_PLUGIN_PATH . '/temp/';
            $this->warehouse_createDestFolder($destFolder);
            $destFolder = WAREHOUSE_PLUGIN_PATH . 'temp/notes.json';            
            if (file_put_contents($destFolder, $this->warehouse_notes_create_json_array($notes)) > 0):
                $result = WAREHOUSE_PLUGIN_URL . 'temp/notes.json';
            endif;
        endif;        
        return (isset($result) && !empty($result)) ? $result : $destFolder;
    }

    private function warehouse_get_notes_from_db() {
        require_once 'warehouse_db_class.php';
        $warehouse_db = new warehouse_database_class();
        return $warehouse_db->warehouse_db_do_query("SELECT * FROM `notifications` ORDER BY `notifications`.`id` DESC");
    }

    public function warehouse_delete_all_notes() {
        require_once 'warehouse_db_class.php';
        $warehouse_db = new warehouse_database_class();
        $warehouse_db->warehouse_db_do_query("TRUNCATE notifications");
        wp_redirect($_SERVER['HTTP_REFERER']);
    }

    private function warehouse_create_notes_json() {
        return $this->warehouse_write_notes_json($this->warehouse_get_notes_from_db());
    }

    private function warehouse_notes_create_json_array($notes) {
        require_once 'vendors_classes.php';
        $vendor_class = new vendor_functions();
        $returnArray['data'] = array();
        if ($notes):
            while ($row = mysqli_fetch_assoc($notes)):
                $tempArray = array(($vendor_class->vendor_get_vendor_name($row['feed_id'])) ? $vendor_class->vendor_get_vendor_name($row['feed_id']) : ' ', ($row['product_title']) ? $row['product_title'] : ' ', ($row['product_code']) ? $row['product_code'] : ' ', ($row['product_barcode']) ? $row['product_barcode'] : ' ', ($row['note']) ? $row['note'] : ' ', $this->warehouse_get_note_actions($row['id'], $row["product_id"], $row["product_code"], $row['product_barcode']));
                array_push($returnArray['data'], $tempArray);
            endwhile;
        endif;
        return json_encode($returnArray);
    }

    private function warehouse_get_note_actions($id, $product_id, $product_code, $product_barcode) {
        $returnString = '';
        $returnString .= "<input type='hidden' name='note_id' value='$id'/>";

        $returnString .= '<div class="col-md-12">';
        $returnString .= '  <div class="col-xs-6">';
        $variation = wc_get_product($product_id);
        if($variation->get_parent_id() > 0) {
            $returnString .= "  <a class='btn btn-info glyphicon glyphicon-pencil' style='color:white;min-width:40px;margin-right:15px;' href=' " . get_site_url() . "/wp-admin/post.php?post=" . $variation->get_parent_id() . "&action=edit&variation=$product_id' target='_blank'></a>";
        } else {
            $returnString .= "  <a class='btn btn-info glyphicon glyphicon-pencil' style='color:white;min-width:40px;margin-right:15px;' href=' " . get_site_url() . "/wp-admin/post.php?post=$product_id&action=edit' target='_blank' style='min-width:40px'></a>";
        }        
        /*$returnString .= "<form method='POST' target='_blank' action='" . get_site_url() . "/wp-admin/admin.php?page=warehouse-feeds'>";
        $returnString .= "<input type='hidden' name='product_code' value='$product_code'/>";
        $returnString .= "<input type='hidden' name='product_barcode' value='$product_barcode'/>";
        $returnString .= "<button name='search_feed' class='btn btn-info' value='true'>View Results</button>";
        $returnString .= "</form>";*/
        $returnString .= '  </div>';
        $returnString .= '  <div class="col-xs-6">';
        $returnString .= "      <button type='button' class='warehouse-delete-note btn btn-danger glyphicon glyphicon-trash' style='min-width:40px'></button>";
        $returnString .= '  </div>';
        $returnString .= '</div>';
        return $returnString;
    }

}
