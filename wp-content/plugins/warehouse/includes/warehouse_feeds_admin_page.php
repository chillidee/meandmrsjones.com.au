<?php
require_once 'vendors_classes.php';
$vendor_functions = new vendor_functions();
$current_vendors = $vendor_functions->vendor_get_vendors();
require_once 'warehouse_feed_class.php';
$feed_functions = new warehouse_feed_class();
?>
<div class="ajax_result_message message-container col-lg-12">

</div>
<?php
if ($_POST['vendor_meta']):
    ?>
    <div class="col-lg-12">
        <h1>Feed Settings</h1>
        <form method="POST">
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="product-code">Code</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="product-code" value="<?php echo get_post_meta($_POST['vendor_id'], '_feed_meta_code', true) ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="product-code">Title</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="product-title" value="<?php echo get_post_meta($_POST['vendor_id'], '_feed_meta_title', true) ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="product-s-desc">Short Description</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="product-s-desc" value="<?php echo get_post_meta($_POST['vendor_id'], '_feed_meta_sdesc', true) ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="product-l-desc">Long Description</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="product-l-desc" value="<?php echo get_post_meta($_POST['vendor_id'], '_feed_meta_ldesc', true) ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="product-size">Size</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="product-size" value="<?php echo get_post_meta($_POST['vendor_id'], '_feed_meta_size', true) ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="product-colour">Colour</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="product-colour" value="<?php echo get_post_meta($_POST['vendor_id'], '_feed_meta_colour', true) ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="product-brand">Brand</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="product-brand" value="<?php echo get_post_meta($_POST['vendor_id'], '_feed_meta_brand', true) ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="product-price">Price</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="product-price" value="<?php echo get_post_meta($_POST['vendor_id'], '_feed_meta_price', true) ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="product-cat">Category</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="product-cat" value="<?php echo get_post_meta($_POST['vendor_id'], '_feed_meta_cat', true) ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="product-image">Image</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="product-image" value="<?php echo get_post_meta($_POST['vendor_id'], '_feed_meta_image', true) ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="product-barcode">Barcode</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="product-barcode" value="<?php echo get_post_meta($_POST['vendor_id'], '_feed_meta_barcode', true) ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="product-qty">Qty</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="product-qty" value="<?php echo get_post_meta($_POST['vendor_id'], '_feed_meta_qty', true) ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="product-qty">New Product</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="_new_product" value="<?php echo get_post_meta($_POST['vendor_id'], '_feed_new_product', true) ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <button name="feed-meta-save" class="btn btn-success" value="<?php echo $_POST['vendor_id'] ?>">Save</button>
                    <button class="btn btn-danger">Cancel</button>
                </div>
            </div>
        </form>
    </div>
    <?php
else:
    if (isset($_POST['feed-meta-save'])):
        $vendor_id = (int) $_POST['feed-meta-save'];
        update_post_meta($vendor_id, '_feed_meta_code', $_POST['product-code']);
        update_post_meta($vendor_id, '_feed_meta_title', $_POST['product-title']);
        update_post_meta($vendor_id, '_feed_meta_sdesc', $_POST['product-s-desc']);
        update_post_meta($vendor_id, '_feed_meta_ldesc', $_POST['product-l-desc']);
        update_post_meta($vendor_id, '_feed_meta_size', $_POST['product-size']);
        update_post_meta($vendor_id, '_feed_meta_colour', $_POST['product-colour']);
        update_post_meta($vendor_id, '_feed_meta_brand', $_POST['product-brand']);
        update_post_meta($vendor_id, '_feed_meta_price', $_POST['product-price']);
        update_post_meta($vendor_id, '_feed_meta_cat', $_POST['product-cat']);
        update_post_meta($vendor_id, '_feed_meta_image', $_POST['product-image']);
        update_post_meta($vendor_id, '_feed_meta_barcode', $_POST['product-barcode']);
        update_post_meta($vendor_id, '_feed_meta_qty', $_POST['product-qty']);
        update_post_meta($vendor_id, '_feed_new_product', $_POST['_new_product']);
    endif;
    if (isset($_POST['vendor_download'])):
        $result = $feed_functions->warehouse_download_products($_POST['vendor_id']);
        ?>
        <div class="col-lg-12 message-container <?php echo ($result) ? 'successfull' : 'failure' ?>_message">
            <?php if (isset($result['success'])): ?>
                <p>Products found in feeds: <?php echo $result['success'] ?></p>
                <?php if (isset($result['failure'])): ?>
                    <p>Products not found in feeds: <?php echo $result['failure'] ?></p>
                <?php endif; ?>
            <?php endif; ?>
            <?php if (isset($result['error'])): ?>
                <p><?php echo $result['error'] ?></p>
            <?php endif; ?>
        </div>
        <?php
    endif;
    if (isset($_POST['vendor_test'])):
        $result = $feed_functions->warehouse_test_feed($_POST['vendor_id']);
        ?>
        <div class="col-lg-12 message-container <?php echo ($result) ? 'successfull' : 'failure' ?>_message">
            <p>Connection result <?php echo ($result) ? 'successfull' : 'failure' ?></p>
        </div>
        <?php
    endif;
    if (isset($_POST['compare_products'])):
        require_once 'warehouse_live_products_class.php';
        $live_products = new warehouse_live_products_class();
        $live_products->warehouse_live_get_all_products((int) $_POST['vendor_id']);
        ?>
        <div class="col-lg-12 message-container <?php echo ($result) ? 'successfull' : 'failure' ?>_message">
            <p>Products updated check notifications for details</p>
        </div>
        <?php
    endif;
    if (!isset($_POST['search_feed'])):
        ?>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <h1>Warehouse Feeds</h1>
            <div class="alert alert-warning alert-dismissible show" role="alert" style="margin-right: 20px">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>               
                <p>Step by step to update products in MMJ via feeds:</p>                     
                <p> </p>
                <p>1. <strong>Test Connection</strong> - If it is OK proceed to step 2, otherwise check their connection settings <a href="<?php echo get_site_url() . '/wp-admin/admin.php?page=warehouse-vendors/' ?>">here</a></p>
                <p>2. <strong>Download Product Feeds</strong> - Let it run until show you either successful notification or failure</p>
                <p>3. <strong>Update Live Products</strong> - After downloading product feeds, it requires your confirmation to update these products LIVE</p>
                <p>4. <strong>Review Changes</strong> - Go to Warehouse by clicking <a href="<?php echo get_site_url() . '/wp-admin/admin.php?page=warehouse%2Fincludes%2Fwarehouse-admin.php/' ?>">here</a>, then follow directions from any notes</p>         
            </div>
            <table>
                <thead>
                    <tr>
                        <td><strong>Feed Name</strong></td>
                        <td><strong>Actions</strong></td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($current_vendors as $vendor): ?>
                        <tr>
                            <td>
                                <?php echo $vendor->post_title ?>
                            </td>
                            <td>
                                <form method="POST">
                                    <input name="vendor_id" value="<?php echo $vendor->ID ?>" type="hidden"/>
                                    <input type="submit" name="vendor_meta" value="Settings" class="btn-info btn"/>
                                    <input type="button" name="vendor_test" value="1. Test Connection" class="btn-info btn"/>
                                    <input type="submit" name="vendor_download" value="2. Download Product Feeds" class="btn-info btn"/>
                                    <input type="submit" name="compare_products" value="3. Update Live Products" class="btn-info btn"/>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach;
                    ?>
                </tbody>
            </table>
        </div>
        <?php
    else:
        require_once 'warehouse_db_class.php';
        $warehouse_db = new warehouse_database_class();
        $product_code = htmlentities($_POST['product_code']);
        $product_barcode = htmlentities($_POST['product_barcode']);
        $query = "SELECT * "
                . "FROM `feed_products` "
                . "WHERE Code =  '$product_code' "
                . "OR Barcode = '$product_barcode'";
        $results = $warehouse_db->warehouse_db_do_query($query);
        if ($results->num_rows > 0):
            ?>
            <h1 style="margin: 20px auto">Results</h1>
            <table>
                <thead>
                    <tr>
                        <td>Code</td>
                        <td>Title</td>
                        <td>Short Description</td>
                        <td>Size</td>
                        <td>Colour</td>
                        <td>Wholesale Price</td>
                        <td>Barcode</td>
                    </tr>
                </thead>
                <tbody>
                    <?php while ($row = mysqli_fetch_assoc($results)): ?>
                        <tr>
                            <td><?php echo $row['Code'] ?></td>
                            <td><?php echo $row['Title'] ?></td>
                            <td><?php echo $row['Short Description'] ?></td>
                            <td><?php echo $row['Size'] ?></td>
                            <td><?php echo $row['Colour'] ?></td>
                            <td><?php echo $row['Wholesale Price'] ?></td>
                            <td><?php echo $row['Barcode'] ?></td>
                        </tr>
                    <?php endwhile; ?>
                </tbody>
            </table>
        <?php else:
            ?>
            <h1 style="margin: 20px auto">No Results Found</h1>
        <?php
        endif;
    endif;
endif;
