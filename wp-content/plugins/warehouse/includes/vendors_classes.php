<?php

class vendor_functions {

    public function vendor_get_vendors() {
        $query = new WP_Query(array('post_type' => 'vendors', 'posts_per_page' => -1));
        return $query->posts;
    }

    public function vendor_get_vendor_name($id) {
        $returnID = false;
        foreach ($this->vendor_get_vendors() as $post):
            if ($post->ID == (int) $id):
                $returnID = $post->post_title;
            endif;
        endforeach;
        return $returnID;
    }

    public function warehouse_vendors_get_download_type($id) {
        $vendorType = false;
        foreach ($this->vendor_get_vendors() as $post):
            if ($post->ID == (int) $id):
                $vendorType = get_post_meta($id, '_feed_type', true);
            endif;
        endforeach;
        return $vendorType;
    }

    public static function getVendorIdFromName($vendorName) {
        $query = new WP_Query(array('post_type' => 'vendors', 'posts_per_page' => -1));
        foreach ($query->posts as $vendor):
            if (strtolower($vendor->post_title) === strtolower($vendorName)):
                return $vendor->ID;
            endif;
        endforeach;
    }

}
