<?php
require_once 'warehouse_maintenance_class.php';
$customer_details = new warehouse_maintenance();
$url = $customer_details->warehouse_create_customer_csv();
if (isset($_POST['clear_cache'])):
    st_cache_pages();
endif;
if (isset($_POST['set_markups'])):
    $customer_details->warehouse_set_markups();
endif;
if (isset($_POST['set_regular_prices'])):
    $customer_details->warehouse_set_regular_prices();
endif;
if (isset($_POST['set_retail_prices'])):
    $customer_details->warehouse_set_retail_prices();
endif;
if (isset($_POST['remove_retail_prices'])):
    $customer_details->warehouse_remove_retail_prices();
endif;
?>

<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
    <h1>Warehouse Maintenance</h1>
    <div class="alert alert-warning alert-dismissible show" role="alert" style="margin-right: 20px">
        Manage carefully your feeds with these tools available below. 
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <div class="col-lg-12">
        <p class="ajax_result_message"></p>
    </div>

    <h2>Customers</h2>
    <a class="btn btn-info" href="<?php echo $url ?>" download>Download List</a>

    <h2>Products</h2>
    <form method="POST">
        <input type="button" value="Round Prices" class="btn btn-info" title="Round prices too .00" name="warehouse_maintenance_round_prices" />
        <button name="set_regular_prices" class="btn btn-info" value="true">Set Regular Prices</button>
        <button name="set_retail_prices" class="btn btn-info" value="true">Set Retail Prices</button>
        <button name="remove_retail_prices" class="btn btn-info" value="true">Remove Retail Prices</button>    
        <button name="set_markups" value="true" class="btn btn-info">Set Markups</button>
    </form>

    <h2>Vendors</h2>
    <button name="download_vendor_images" class="btn btn-info" type="button">Download Images</button>
</div>

<!--<h2>Page Cache</h2>
<form method="POST">
    <button name="clear_cache" value="true" class="btn btn-default">Clear Cached Product Pages</button>
</form>-->