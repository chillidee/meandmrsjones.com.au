<?php

class warehouse_products_class {

    public function warehouse_get_products_cats_by_vendor($vendor_id) {
        
        require_once 'warehouse_db_class.php';
        
        $db = new warehouse_database_class();
        $returnArray = array();
        
        if($vendor_id == '12430') 
        {
            $sql = "SELECT GROUP_CONCAT(id) AS Ids, `feed_id`, `Title`, MAX(`Short Description`) AS `Short Description`, GROUP_CONCAT(Size) AS Sizes, GROUP_CONCAT(Colour) AS Colours,
                    `Brand`, GROUP_CONCAT(`Wholesale Price`) AS `Wholesale Price`, `Category`, MAX(`Image`) AS `Image`, GROUP_CONCAT(`Barcode`), `AvailableStock`, `Weight`, `imported`, `new`
                    FROM `feed_products` 
                    WHERE `feed_id` = 12430 AND `imported` = 0 AND (LENGTH(Size) > 0 OR LENGTH(Colour) > 0)
                    GROUP BY `feed_id`, `Title`, `Brand`, `Category`, `AvailableStock`, `Weight`, `imported`, `new`";
        } 
        else 
        {
            $sql = "SELECT GROUP_CONCAT(id) AS Ids, `feed_id`, `Code`, `Title`, `Short Description`, `Long Description`, GROUP_CONCAT(Size) AS Sizes, GROUP_CONCAT(Colour) AS Colours,
                        `Brand`, `Wholesale Price`, `Category`, `Image`, `Barcode`, `AvailableStock`, `Weight`, `imported`, `new`
                    FROM `feed_products` 
                    WHERE `feed_id` = $vendor_id 
                    GROUP BY `feed_id`, `Code`, `Title`, `Short Description`, `Long Description`, `Brand`,
                            `Category`, `Image`, `Barcode`, `AvailableStock`, `Weight`, `imported`, `new`";
        }

        $products = $db->warehouse_db_do_query($sql);

        if ($products->num_rows > 0):
            while ($row = mysqli_fetch_assoc($products)):
                if (!in_array($row["Category"], $returnArray)):
                    array_push($returnArray, $row["Category"]);
                endif;
            endwhile;
            return $returnArray;
        else:
            return false;
        endif;
    }

    public function warehouse_get_products_by_vendor_and_cat($vendor_id, $cat_name) {

        require_once 'warehouse_db_class.php';

        $db = new warehouse_database_class();
        $returnArray = array();
        
        if($vendor_id == '12430') 
        {
            $sql = "SELECT GROUP_CONCAT(id) AS Ids, `feed_id`, `Title`, MAX(`Short Description`) AS `Short Description`, GROUP_CONCAT(Size) AS Sizes, GROUP_CONCAT(Colour) AS Colours,
                    `Brand`, GROUP_CONCAT(`Wholesale Price`) AS `Wholesale Price`, `Category`, MAX(`Image`) AS `Image`, GROUP_CONCAT(`Barcode`), `AvailableStock`, `Weight`, `imported`, `new`
                    FROM `feed_products` 
                    WHERE `feed_id` = 12430 AND `imported` = 0 AND (LENGTH(Size) > 0 OR LENGTH(Colour) > 0) AND `Category` LIKE '" . $cat_name . "'
                    GROUP BY `feed_id`, `Title`, `Brand`, `Category`, `AvailableStock`, `Weight`, `imported`, `new`";
        } 
        else 
        {
            $sql = "SELECT GROUP_CONCAT(id) AS Ids, `feed_id`, `Code`, `Title`, `Short Description`, `Long Description`, GROUP_CONCAT(Size) AS Sizes, GROUP_CONCAT(Colour) AS Colours,
                        `Brand`, `Wholesale Price`, `Category`, `Image`, `Barcode`, `AvailableStock`, `Weight`, `imported`, `new`
                    FROM `feed_products` 
                    WHERE `feed_id` = $vendor_id AND `imported` = 0 AND `Category` LIKE '" . $cat_name . "'
                    GROUP BY `feed_id`, `Code`, `Title`, `Short Description`, `Long Description`, `Brand`,
                            `Category`, `Image`, `Barcode`, `AvailableStock`, `Weight`, `imported`, `new`";
        }

        $products = $db->warehouse_db_do_query($sql);

        if ($products->num_rows > 0):
            while ($row = mysqli_fetch_assoc($products)):
                $image_link = $this->warehouse_get_image_name($row['Image']);
                if (file_exists(WAREHOUSE_TEMP_FOLDER . $vendor_id . '/' . $image_link)):
                    $row['Image'] = WAREHOUSE_TEMP_FOLDER_URL . $vendor_id . '/' . $image_link;
                else:
                    if (get_post_meta($vendor_id, '_feed_type', true) == 'ftp' && (strpos($row['Image'], 'http') == false || strpos($row['Image'], 'https') == false)):
                        $row['Image'] = 'ftp://' . get_post_meta($vendor_id, '_ftp_username', true) . ':' . get_post_meta($vendor_id, '_ftp_password', true) . '@' . get_post_meta($vendor_id, '_ftp_host', true) . $row['Image'];
                    endif;
                endif;
                array_push($returnArray, $row);
            endwhile;
            return $returnArray;
        else:
            return false;
        endif;
    }

    public function warehouse_get_new_products($vendor_id) {

        require_once 'warehouse_db_class.php';

        $db = new warehouse_database_class();
        $returnArray = array();

        if($vendor_id == '12430') 
        {
            $sql = "SELECT GROUP_CONCAT(id) AS Ids, `feed_id`, `Title`, MAX(`Short Description`) AS `Short Description`, GROUP_CONCAT(Size) AS Sizes, GROUP_CONCAT(Colour) AS Colours,
                    `Brand`, GROUP_CONCAT(`Wholesale Price`) AS `Wholesale Price`, `Category`, MAX(`Image`) AS `Image`, GROUP_CONCAT(`Barcode`), `AvailableStock`, `Weight`, `imported`, `new`
                    FROM `feed_products` 
                    WHERE `feed_id` = 12430 AND `imported` = 0 AND (LENGTH(Size) > 0 OR LENGTH(Colour) > 0) AND `new` LIKE 'Y'
                    GROUP BY `feed_id`, `Title`, `Brand`, `Category`, `AvailableStock`, `Weight`, `imported`, `new`";
        } 
        else 
        {
            $sql = "SELECT GROUP_CONCAT(id) AS Ids, `feed_id`, `Code`, `Title`, `Short Description`, `Long Description`, GROUP_CONCAT(Size) AS Sizes, GROUP_CONCAT(Colour) AS Colours,
                        `Brand`, `Wholesale Price`, `Category`, `Image`, `Barcode`, `AvailableStock`, `Weight`, `imported`, `new`
                    FROM `feed_products` 
                    WHERE `feed_id` = $vendor_id AND `imported` = 0 AND `new` LIKE 'Y'
                    GROUP BY `feed_id`, `Code`, `Title`, `Short Description`, `Long Description`, `Brand`,
                            `Category`, `Image`, `Barcode`, `AvailableStock`, `Weight`, `imported`, `new`";
        }

        $products = $db->warehouse_db_do_query($sql);

        if ($products->num_rows > 0):
            while ($row = mysqli_fetch_assoc($products)):
                $image_link = $this->warehouse_get_image_name($row['Image']);
                if (file_exists(WAREHOUSE_TEMP_FOLDER . $vendor_id . '/' . $image_link)):
                    $row['Image'] = WAREHOUSE_TEMP_FOLDER_URL . $vendor_id . '/' . $image_link;
                else:
                    //if (get_post_meta($vendor_id, '_feed_type', true) == 'ftp' && (strpos($row['Image'], 'http') == false || strpos($row['Image'], 'https') == false)):
                    // $row['Image'] = 'ftp://' . get_post_meta($vendor_id, '_ftp_username', true) . ':' . get_post_meta($vendor_id, '_ftp_password', true) . '@' . get_post_meta($vendor_id, '_ftp_host', true) . $row['Image'];
                    // endif;
                    $row['Image'] = false;
                endif;
                array_push($returnArray, $row);
            endwhile;
            return $returnArray;
        else:
            return false;
        endif;
    }

    private function warehouse_get_image_name($image_link) {
        $image_link = explode('/', $image_link);
        return $image_link[count($image_link) - 1];
    }

    public function warehouse_get_all_products($status = array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash')) {
        $args = array(
            'post_type' => array('product', 'product_variation'),
            'posts_per_page' => -1,
            "post_status" => $status,
            'fields' => 'ids',
        );
        $ids = new WP_Query($args);
        return $ids->posts;
    }

    public function warehouse_get_all_products_by_vendor($vendor_id) {

        require_once 'warehouse_db_class.php';

        $db = new warehouse_database_class();
        $returnArray = array();

        if($vendor_id == '12430') 
        {
            $sql = "SELECT GROUP_CONCAT(id) AS Ids, `feed_id`, `Title`, MAX(`Short Description`) AS `Short Description`, GROUP_CONCAT(Size) AS Sizes, GROUP_CONCAT(Colour) AS Colours,
                    `Brand`, GROUP_CONCAT(`Wholesale Price`) AS `Wholesale Price`, `Category`, MAX(`Image`) AS `Image`, GROUP_CONCAT(`Barcode`), `AvailableStock`, `Weight`, `imported`, `new`
                    FROM `feed_products` 
                    WHERE `feed_id` = 12430 AND `imported` = 0 AND (LENGTH(Size) > 0 OR LENGTH(Colour) > 0)
                    GROUP BY `feed_id`, `Title`, `Brand`, `Category`, `AvailableStock`, `Weight`, `imported`, `new`";
        } 
        else 
        {
            $sql = "SELECT GROUP_CONCAT(id) AS Ids, `feed_id`, `Code`, `Title`, `Short Description`, `Long Description`, GROUP_CONCAT(Size) AS Sizes, GROUP_CONCAT(Colour) AS Colours,
                        `Brand`, `Wholesale Price`, `Category`, `Image`, `Barcode`, `AvailableStock`, `Weight`, `imported`, `new`
                    FROM `feed_products` 
                    WHERE `feed_id` = $vendor_id AND `imported` = 0
                    GROUP BY `feed_id`, `Code`, `Title`, `Short Description`, `Long Description`, `Brand`,
                            `Category`, `Image`, `Barcode`, `AvailableStock`, `Weight`, `imported`, `new`";
        }

        $products = $db->warehouse_db_do_query($sql);

        if ($products->num_rows > 0):
            while ($row = mysqli_fetch_assoc($products)):
                $image_link = $this->warehouse_get_image_name($row['Image']);
                if (file_exists(WAREHOUSE_TEMP_FOLDER . $vendor_id . '/' . $image_link)):
                    $row['Image'] = WAREHOUSE_TEMP_FOLDER_URL . $vendor_id . '/' . $image_link;
                else:
//                    if (get_post_meta($vendor_id, '_feed_type', true) == 'ftp' && (strpos($row['Image'], 'http') == false || strpos($row['Image'], 'https') == false)):
//                        $row['Image'] = 'ftp://' . get_post_meta($vendor_id, '_ftp_username', true) . ':' . get_post_meta($vendor_id, '_ftp_password', true) . '@' . get_post_meta($vendor_id, '_ftp_host', true) . $row['Image'];
//                    endif;
                    $row['Image'] = false;
                endif;
                array_push($returnArray, $row);
            endwhile;
            return $returnArray;
        else:
            return false;
        endif;
    }

    public function warehouse_get_products_table($products) {
        if ($products):
            ?>
            <div class="text-center"><h3><?php echo sprintf('Listing %d product(s)', count($products)) ?></h3></div>
            <table id="warehouse-import-products-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>Image</td>
                        <td>Title</td>
                        <td>Short Description</td>
                        <td>Brand</td>                       
                        <td>Variations</td>
                        <td>Wholesale Price</td>                        
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($products as $product): ?>
                        <tr>
                            <td style="text-align: center;"><?php echo (isset($product['Image']) && !empty($product['Image'])) ? "<img height=100px src='" . $product['Image'] . "'/>" : '<i class="fa fa-cog fa-spin fa-3x fa-fw" data-productID="' . $product['id'] . '" aria-hidden="true"></i><span class="sr-only">Saving. Hang tight!</span>'; ?></td>
                            <td><p><?php echo $product["Title"] ?></p></td>
                            <td><p><?php echo $product["Short Description"] ?></p></td>                           

                            <?php                                            
                            $prices = explode(",", $product["Wholesale Price"]);
                            $sizes  = explode(",", $product["Sizes"]);
                            $colours  = explode(",", $product["Colours"]);

                            $price_variations = '<p><strong> </strong></p>';

                            if(count($sizes) > 1) {                                
                                $variations = '<p><strong>' . sprintf('There are %d variations:', count($sizes)) . '</strong></p>';
                            } else {                                
                                $variations = '<p><strong>There is only 1 variation:</strong></p>';
                            }

                            for ($i = 0; $i < count($prices); $i++) { 
                                $price_variations .= '<p>' . round($prices[$i], 2) . '</p>';
                            }

                            for ($i = 0; $i < count($sizes); $i++) {                                                                   
                                $variations .= '<p>' . $sizes[$i] . ' - ' .  $colours[$i] . '</p>';                                
                            }
                            ?>
                            
                            <td><p><?php echo $product["Brand"] ?></p></td>
                            <td><?php echo $variations ?></td>
                            <td><?php echo $price_variations ?></td>

                            <td>
                                <button type="button" class="btn btn-info" value="<?php echo $product['Ids'] ?>" name="edit-product">Edit &amp; Import</button>
                                <input type="hidden" name="sizes" value="<?php echo $product["Sizes"] ?>" />
                                <input type="hidden" name="colours" value="<?php echo $product["Colours"] ?>" />
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="col-lg-10 col-lg-offset-1 import-edit-product-container hidden"></div>
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.css"/>
            <script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.js"></script>
            <?php
        else:
            ?><h2>No Products to display</h2><?php
        endif;
    }

}
?>