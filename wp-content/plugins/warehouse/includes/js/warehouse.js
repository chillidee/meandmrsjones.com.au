jQuery(document).ready(function ($) {
    $('select[name=feed-type]').change(function () {
        $('.details-conatiner').each(function () {
            if (!$(this).hasClass('hidden')) {
                $(this).addClass('hidden');
            }
        });
        $('.' + $(this).val() + '-details').removeClass('hidden');
    });

    if ($('#warehouse-notes-table1').length > 0) {

        $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
            $.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
            warehouse_notes_clear_selected();
            warehouse_notes_button_handers();
            warehouse_notes_form_click();
        });

        $('table.table').DataTable({
            ajax: '/wp-content/plugins/warehouse/temp/notes.json'
            //ajax: $('#json_location').val()
        });

        $('#warehouse-notes-table2').DataTable().search('has gone out of stock and has been removed from site').draw();
        $('#warehouse-notes-table6').DataTable().search('has come back into stock and made live').draw();
        $('#warehouse-notes-table7').DataTable().search('be inported wholesale price is greater than the sale price').draw();
        $('#warehouse-notes-table3').DataTable().search('wholesale price has increased').draw();
        $('#warehouse-notes-table4').DataTable().search('wholesale price has decreased').draw();
        $('#warehouse-notes-table5').DataTable().search('too many results please check manualy').draw();

        $('.table').on('draw.dt', function () {
            warehouse_notes_clear_selected();
            warehouse_notes_button_handers();
            warehouse_notes_form_click(this);
            warehouse_count_table_rows(this);
        });

        $('.table').on('search.dt', function () {
            warehouse_notes_clear_selected();
            warehouse_notes_button_handers();
            warehouse_notes_form_click(this);
        });

        $('button[name=remove-table-notes]').click(function () {
            var btn = $(this);
            btn.attr('disabled', true);
            $('li[role="presentation"]').each(function () {
                var _tab = $(this);
                if (_tab.hasClass('active')) {
                    var tab_id = _tab.find('a').attr('href');
                    var table_id = $(tab_id).find('table').attr('id');
                    $('#' + table_id).DataTable().$('tr', {"filter": "applied"}).each(function () {
                        var row = $(this);
                        var note_id = row.find('input[name=note_id]').val();
                        warehouse_remove_note_from_database(note_id, function (response) {
                            if (response) {
                                $('.warehouse-notes-total').html(response);
                                warehouse_redraw_table(row);
                                row.remove();
                            }
                        });
                    });
                    warehouse_check_remove_button_status();
                    btn.attr('disabled', false);
                }
            });
        });

    }

    function warehouse_notes_button_handers() {
        $('.warehouse-delete-note').each(function () {
            var btn = $(this)
            if (btn.data('events') === undefined) {
                btn.click(function () {
                    btn.unbind('click');
                    btn.attr('disabled', true);
                    var parent = btn.parent().parent().parent();
                    warehouse_remove_note_from_database(parent.children('input[name=note_id]').val(), function (response) {
                        if (response) {
                            $('.warehouse-notes-total').html(response);
                            warehouse_redraw_table(parent.parent());
                            warehouse_check_remove_button_status();
                        }
                    })
                });
            }
        });
    }

    function warehouse_notes_clear_selected() {
        $('.table tr').each(function () {
            $(this).removeClass('selected');
        });
        warehouse_check_remove_button_status();
    }

    function warehouse_notes_form_click(table) {
        $(table).find('tbody tr').each(function () {
            var btn = $(this);
            if (btn.data('events') === undefined) {
                btn.click(function () {
                    if (btn.hasClass('selected')) {
                        btn.removeClass('selected')
                    } else {
                        btn.addClass('selected')
                    }
                    warehouse_check_remove_button_status();
                });
            }
        });
    }

    function warehouse_check_remove_button_status() {
        var number = 0;
        $('.table tr').each(function () {
            if ($(this).hasClass('selected')) {
                number++;
            }
        });
        var removeSelected = $('button[name=remove-selected-notes]');
        if (number > 0) {
            removeSelected.prop('disabled', false);
        } else {
            removeSelected.prop('disabled', true);
        }
        removeSelected.html('Remove Selected (' + number + ') Notes');
    }

    if ($('button[name=remove-selected-notes]').length > 0) {

        $('button[name=remove-selected-notes]').click(function () {
            $('.table tbody tr').each(function () {
                if ($(this).hasClass('selected')) {
                    var row = $(this);
                    warehouse_remove_note_from_database($(this).find($('input[name=note_id]')).val(), function (response) {
                        if (response) {
                            $('.warehouse-notes-total').html(response);
                            warehouse_check_remove_button_status();
                            warehouse_redraw_table(row);
                        }
                    })
                }
            });
        })

    }

    function warehouse_remove_note_from_database(note_id, callback) {
        var data = {
            action: 'warehouse_delete_note',
            note_id: note_id
        }
        $.post($('#ajax-url').val(), data, function (response) {
            callback(response);
        })
    }

    function warehouse_redraw_table(row) {
        $('table').each(function () {
            $(this).DataTable().row(row).remove().draw();
        })
    }

    function warehouse_count_table_rows(table) {
        var number_of_rows = $(table).DataTable().$('tr', {"filter": "applied"}).length;
        var parents = $(table).parentsUntil('.tab-content');
        parents = $(parents.get(parents.length - 1)).attr('id')
        if (parents) {
            $('a[role=tab]').each(function () {
                if ($(this).attr('href') === '#' + parents) {
                    $(this).find('.number_of_rows').html("(" + number_of_rows + ")")
                }
            })
        }
    }
}
);