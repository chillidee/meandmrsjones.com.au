jQuery(window).ready(function ($) {

    if ($('.gridNumbersOverlay').length > 0) {

        $('.close-grid-overlay').click(function () {
            hideGridOverlay(this);
        });

        $('.featured_products_grid').click(function (event) {

            var selectedGrids = [];

            if (!event.target.classList.contains('button')) {
                $('.gridNumber').each(function () {
                    if ($(this).html().length > 0) {
                        selectedGrids.push(parseInt($(this).html()));
                    }
                })
                $(this).find('button').each(function () {
                    if (selectedGrids.indexOf(parseInt($(this).val())) !== -1) {
                        if (!$(this).hasClass('grid-button-selected'))
                            $(this).addClass('grid-button-pselected');
                    } else {
                        $(this).removeClass('grid-button-pselected');
                    }
                })
                if (!event.target.classList.contains('fa')) {
                    $(this).find('.gridNumbersOverlay').removeClass('hidden');
                }
            } else {
                $(this).find('.gridNumbersOverlay').addClass('hidden');
            }
        });

        $('button[name=gridButtonNumber]').click(function () {
            var gridNumber = $(this).val();
            var productID = $(this).data('productnumber');
            $(this).parent().find('button').each(function () {
                if ($(this).hasClass('grid-button-selected') && $(this).val() !== gridNumber) {
                    $(this).removeClass('grid-button-selected');
                }
            });

            if ($(this).hasClass('grid-button-selected')) {
                $(this).removeClass('grid-button-selected');
                $('#' + productID).find('.gridNumber').html('');
            } else {
                $(this).addClass('grid-button-selected');
                var gridHolder = $('#' + productID).find('.gridNumber');
                $(gridHolder).html(gridNumber);
                $('.gridNumber').each(function () {
                    if (($(this).data('product_id') !== $(gridHolder).data('product_id')) && gridNumber === $(this).html()) {
                        $(this).html('')
                    }
                })
            }

            var selected = [];
            $('.gridNumber').each(function () {
                var grid = $(this);
                $('input[name=sortcut-button]').each(function () {
                    $(this).removeClass('grid-button-pselected')
                    if (parseInt($(this).val()) === parseInt($(grid).html())) {
                        selected.push($(this).val());
                    }
                })
            })
            $('input[name=sortcut-button]').each(function () {
                if (selected.indexOf($(this).val()) !== -1) {
                    $(this).addClass('grid-button-pselected')
                }
            });

            hideGridOverlay(this);
        });

        function hideGridOverlay(childElement) {
            $(childElement).parent().addClass('hidden');
        }

        $('input[name=sortcut-button]').click(function () {
            var shortcutValue = $(this).val();
            $('.gridNumber').each(function () {
                if (parseInt($(this).html()) === parseInt(shortcutValue)) {
                    var parent = $(this).parent();
                    $('.featured_products_grid_container_new').scrollTop($(parent).get(0).offsetTop)
                    $(parent).addClass('highlight_grid');
                    setTimeout(function () {
                        $(parent).removeClass('highlight_grid');
                    }, 3000);
                }
            })
        });

    }

});