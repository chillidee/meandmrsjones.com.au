<?php

class warehouse_maintenance {

    public function warehouse_create_customer_csv() {
        return $this->warehouse_create_csv();
    }

    private function warehouse_get_customer_data() {
        global $wpdb;
        $returnArray = array();
        $returnArray[0] = array('ID', 'Name', 'User Email');
        $query = "SELECT `ID`,`display_name`, `user_email` FROM wp_users t1, wp_usermeta t2 WHERE t1.ID = t2.user_id AND t2.meta_key LIKE 'subscribed' AND t2.meta_value LIKE 'true'";
        foreach ($wpdb->get_results($query, ARRAY_N) as $user):
            $returnArray[$user[0]] = array();
            $returnArray[$user[0]]['name'] = $user[1];
            $returnArray[$user[0]]['email'] = $user[2];
        endforeach;
        $query = "SELECT * FROM `email_subscribers`";
        foreach ($wpdb->get_results($query, ARRAY_N) as $user):
            if (!isset($returnArray[$user[1]])):
                $returnArray[$user[1]] = array();
                $returnArray[$user[1]]['name'] = '';
                $returnArray[$user[1]]['email'] = $user[2];
            endif;
        endforeach;
        return $returnArray;
    }

    private function warehouse_create_csv() {
        $result = false;
        $wp_upload_dir = wp_upload_dir();
        $destFolder = $wp_upload_dir['path'] . '/' . 'temp/';
        if (!file_exists($destFolder)) {
            mkdir($destFolder, 0775, true);
        }
        $destURL = $wp_upload_dir['url'] . '/' . 'temp/customer.csv';
        $filePath = $destFolder . 'customer.csv';
        $data = $this->warehouse_get_customer_data();
        if (isset($data) && !empty($data)):
            $file = fopen($filePath, "w");
            if ($file):
                foreach ($data as $index => $user):
                    if ($index != 0):
                        $user = array($index, str_replace('"', '', $user['name']), $user['email']);
                    endif;
                    $result = fputcsv($file, $user);
                endforeach;
            endif;
            fclose($file);
        endif;
        if ($result):
            $result = $destURL;
        endif;
        return $result;
    }

    public function warehouse_round_prices() {
        require_once 'warehouse_products_class.php';
        require_once 'warehouse_live_products_class.php';
        $productsClass = new warehouse_products_class();
        $liveProducts = new warehouse_live_products_class();
        $resultArray = array('changed' => 0, 'not_changed' => 0, 'string' => '');
        foreach ($productsClass->warehouse_get_all_products() as $id):
            $oldPrice = (float) get_post_meta((int) $id, '_price', TRUE);
            $newPrice = number_format(round($oldPrice), 2);
            if ($oldPrice == $newPrice):
                ++$resultArray['not_changed'];
            else:
                if (update_post_meta($id, '_price', $newPrice)):
                    ++$resultArray['changed'];
                    update_post_meta($id, '_regular_price', $newPrice);
                endif;
            endif;
            /*if ((int) $newPrice <= (int) get_post_meta($id, '_wholesale_price', true)):
                $liveProducts->warehouse_live_unpublish_product($id);
            endif;*/
            unset($id);
        endforeach;
        unset($productsClass);
        $resultArray['string'] .= ($resultArray['changed'] > 0) ? $resultArray['changed'] . " prices changed" : '';
        $resultArray['string'] .= ($resultArray['changed'] > 0 && $resultArray['not_changed'] > 0) ? ',' : '';
        $resultArray['string'] .= ($resultArray['not_changed'] > 0) ? " " . $resultArray['not_changed'] . " prices not changed" : '';
        echo $resultArray['string'];
    }

    public function warehouse_set_markups() {
        foreach ($this->get_all_products() as $id):
            $wholesale = get_post_meta($id, '_wholesale_price', true);
            $price = get_post_meta($id, '_price', true);
            update_post_meta($id, '_markup_precentage', number_format((($price - $wholesale) / $wholesale) * 100) + 0.99);
        endforeach;
    }

    public function warehouse_set_regular_prices() {
        $regular_price_markup = get_option('warehouse_feed_products_db_regular_price_markup') / 100;            
        foreach ($this->get_all_products() as $post_id):
            update_post_meta($post_id, '_regular_price', number_format((float) get_post_meta($post_id, '_wholesale_price', true) * $regular_price_markup) + 0.99);
            update_post_meta($post_id, '_price', number_format((float) get_post_meta($post_id, '_wholesale_price', true) * $regular_price_markup) + 0.99);
        endforeach;
    }

    public function warehouse_set_retail_prices() {
        $retail_price_markup = get_option('warehouse_feed_products_db_retail_price_markup') / 100;
        foreach ($this->get_all_products() as $post_id):
            update_post_meta($post_id, '_retail_price', number_format((float) get_post_meta($post_id, '_price', true) * $retail_price_markup) + 0.99);
        endforeach;
    }

    public function warehouse_remove_retail_prices() {
        foreach ($this->get_all_products() as $post_id):
            delete_post_meta($post_id, '_retail_price');
        endforeach;
    }

    private function get_all_products() {
        $query = new WP_Query(
                array(
            'post_status' => 'any',
            'post_type' => array('product', 'product_variation'),
            'posts_per_page' => -1,
            'fields' => 'ids'
                )
        );
        return $query->posts;
    }

    private function getAllProducts() {
        $args = array(
            'post_type' => 'product',
            'posts_per_page' => -1,
            "post_status" => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
            'fields' => 'ids'
        );

        $ids = new WP_Query($args);
        return $ids->posts;
    }

    public function getAllProductCodes() {
        $returnArray = array();
        foreach ($this->getAllProducts() as $post_id):
            array_push($returnArray, get_post_meta($post_id, '_code', true));
            unset($post_id);
        endforeach;
        $returnArray = array_unique($returnArray);
        return $returnArray;
    }

    private function searchForProductsWithSameCode($productCode) {
        $args = array(
            'post_type' => 'product',
            'posts_per_page' => -1,
            "post_status" => array('publish'),
            'fields' => 'ids',
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => '_code',
                    'value' => $productCode,
                    'compare' => '=',
                ),
                array(
                    'key' => 'vendor',
                    'value' => 'Wrapped Secrets',
                    'compare' => '=',
                ),
            ),
        );
        $ids = new WP_Query($args);
        return $ids->posts;
    }

    public function groupProducts() {
        foreach ($this->getAllProductCodes() as $code):
            $this->setParentChild($this->searchForProductsWithSameCode($code));
            unset($code);
        endforeach;
    }

    private function setParentChild($groupedProducts) {
        $parent = null;
        foreach ($groupedProducts as $product):
            if ($this->checkIfParent($groupedProducts)):
                $parent = $product;
                unset($product);
                break;
            endif;
        endforeach;
        if (!empty($parent)):
            $parent = $groupedProducts[0];
        endif;
        update_post_meta($parent, '_is_parent', true);
        delete_post_meta($parent, '_parent_id');
        update_post_meta($post_id, '_is_child', false);
        foreach ($groupedProducts as $product):
            update_post_meta($parent, '_parent_id', $parent);
            delete_post_meta($parent, '_is_parent');
            update_post_meta($post_id, '_is_child', false);
        endforeach;
    }

    private function checkIfParent($id) {
        $is_parent = get_post_meta($id, '_is_parent', true);
        return ($is_parent) ? true : false;
    }

}
