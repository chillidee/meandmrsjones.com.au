<?php
require_once 'vendors_classes.php';
$vendor_functions = new vendor_functions();
$current_vendors = $vendor_functions->vendor_get_vendors();
require_once 'warehouse_products_class.php';
$products = new warehouse_products_class();
?>
<div class="container-fluid">
    <h1>Import Products</h1>
    <div class="alert alert-warning alert-dismissible show" role="alert" style="margin-right: 20px">
        Before importing products, please make sure that the feed has been updated as it removes products that have been already imported.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <?php if (empty($_POST)): ?>
        <form method = "POST">
            <?php foreach ($current_vendors as $vendor):
                ?>
                <button value="<?php echo $vendor->ID ?>" class="btn btn-info" name="vendor_name"><?php echo $vendor->post_title; ?></button>
                <?php
            endforeach;
            ?>
        </form>
    <?php endif; ?>
    <?php if (isset($_POST['vendor_name'])): ?>
        <form method="POST">
            <button class="btn btn-success">Back To Vendors</button>
        </form>
        <?php
        $cats = $products->warehouse_get_products_cats_by_vendor((int) htmlentities($_POST['vendor_name']));
        if (get_post_meta((int) htmlentities($_POST['vendor_name']), '_feed_new_product', true)):
            ?>
            <div class="col-lg-2">
                <form method="post">
                    <input name="vendor_id" value="<?php echo $_POST['vendor_name'] ?>" type="hidden"/>
                    <button class="btn btn-info" name="new_products" value="true">New Products</button>
                </form>
            </div>
        <?php endif;
        ?>
        <div class="col-lg-2">
            <form method="post">
                <input name="vendor_id" value="<?php echo $_POST['vendor_name'] ?>" type="hidden"/>
                <button class="btn btn-info" name="all_products" value="true">All Products</button>
            </form>
        </div>
        <?php
        if ($cats):
            foreach ($cats as $cat):
                ?>
                <div class="col-lg-2">
                    <form method="post">
                        <input name="vendor_id" value="<?php echo $_POST['vendor_name'] ?>" type="hidden"/>
                        <button class="btn btn-info" name="product-cat" value="<?php echo $cat ?>"><?php echo $cat ?></button>
                    </form>
                </div>
                <?php
            endforeach;
        else:
            ?><h2>No Cats to display</h2><?php
        endif;
    endif;
    if (isset($_POST['product-cat'])):
        ?>
        <form method="post">
            <input name="vendor_name" value="<?php echo $_POST['vendor_id'] ?>" type="hidden"/>
            <button style="margin-bottom: 10px" class="btn btn-success">Back To Categories</button>
        </form>
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <div class="import-product-edit-box hidden">
            <div class="import-product-edit-box-wrapper">

            </div>
        </div>
        <?php
        $products->warehouse_get_products_table($products->warehouse_get_products_by_vendor_and_cat((int) htmlentities($_POST['vendor_id']), htmlentities($_POST['product-cat'])));
    endif;
    if (isset($_POST['new_products'])):
        ?>
        <form method="post">
            <input name="vendor_name" value="<?php echo $_POST['vendor_id'] ?>" type="hidden"/>
            <button style="margin-bottom: 10px;" class="btn btn-success">Back To Categories</button>
        </form>
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <div class="import-product-edit-box hidden">
            <div class="import-product-edit-box-wrapper">

            </div>
        </div>
        <?php
        $products->warehouse_get_products_table($products->warehouse_get_new_products((int) htmlentities($_POST['vendor_id'])));
    endif;
    if (isset($_POST['all_products'])):
        ?>
        <form method="post">
            <input name="vendor_name" value="<?php echo $_POST['vendor_id'] ?>" type="hidden"/>
            <button style="margin-bottom: 10px;" class="btn btn-success">Back To Categories</button>
        </form>
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <div class="import-product-edit-box hidden">
            <div class="import-product-edit-box-wrapper">

            </div>
        </div>
        <?php
        $products->warehouse_get_products_table($products->warehouse_get_all_products_by_vendor((int) htmlentities($_POST['vendor_id'])));
    endif;
    ?>
</div>