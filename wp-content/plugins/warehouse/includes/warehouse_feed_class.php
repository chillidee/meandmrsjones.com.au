<?php

class warehouse_feed_class {

    public function warehouse_test_feed($vendor_id) {
        return ($this->warehouse_get_product_data($vendor_id, get_post_meta($vendor_id, '_feed_type', true))) ? true : false;
    }

    public function warehouse_download_products($vendor_id) {
        $feed_type = get_post_meta($vendor_id, '_feed_type', true);
        switch (get_post_meta($vendor_id, '_file_type', true)):
            case 'xml':
                $result = $this->warehouse_save_data_into_db($vendor_id, $this->warehouse_parse_xml_data($this->warehouse_get_product_data($vendor_id, $feed_type)));
                break;
            case 'csv':
                $result = $this->warehouse_save_data_into_db($vendor_id, $this->warehouse_parse_csv_data($vendor_id, $this->warehouse_get_product_data($vendor_id, $feed_type)));
                break;
        endswitch;
        return ($result) ? $result : false;
    }

    private function warehouse_get_feed_connection_details($vendor_id, $feed_type) {
        $returnArray = array();
        switch ($feed_type):
            case 'ftp':
                $returnArray = array(
                    'host' => get_post_meta($vendor_id, '_ftp_host', true),
                    'username' => get_post_meta($vendor_id, '_ftp_username', true),
                    'password' => get_post_meta($vendor_id, '_ftp_password', true),
                    'port' => get_post_meta($vendor_id, '_ftp_port', true),
                    'path' => get_post_meta($vendor_id, '_ftp_path', true),
                    'filename' => get_post_meta($vendor_id, '_ftp_filename', true),
                );
                break;
            case 'live':
                $returnArray = array(
                    'url' => get_post_meta($vendor_id, '_live_url', true),
                    'post' => get_post_meta($vendor_id, '_live_pp', true),
                    'no_post' => get_post_meta($vendor_id, '_live_no_pp', true)
                );
                break;
            case 'file':
                $returnArray = array(
                    'filename' => get_post_meta($vendor_id, '_file_name', true)
                );
                break;
            default :
                $returnArray = false;
                break;
        endswitch;
        return $returnArray;
    }

    private function warehouse_get_product_data($vendor_id, $feed_type) {
        set_time_limit(0);
        ini_set('memory_limit', '2048M');
        $result = false;
        $connection_details = $this->warehouse_get_feed_connection_details($vendor_id, $feed_type);
        if ($connection_details):
            switch ($feed_type):
                case 'ftp':
                    $conn_id = ftp_connect(get_post_meta($vendor_id, '_ftp_host', true));
                    $result = ftp_login($conn_id, get_post_meta($vendor_id, '_ftp_username', true), get_post_meta($vendor_id, '_ftp_password', true));
                    $destFolder = WAREHOUSE_PLUGIN_PATH . 'feeds/';
                    if (!file_exists($destFolder)) {
                        mkdir($destFolder, 0775, true);
                    }
                    $local_file = $destFolder . get_post_meta($vendor_id, '_ftp_filename', true);
                    if (file_exists($local_file)) {
                        unlink($local_file);
                    }
                    $handle = fopen($local_file, 'w');
                    $server_file = get_post_meta($vendor_id, '_ftp_filename', true);
                    if ($result && $handle):
                        if (get_post_meta($vendor_id, '_ftp_path', true)):
                            $result = ftp_chdir($conn_id, get_post_meta($vendor_id, '_ftp_path', true));
                        endif;
                        $result = ftp_pasv($conn_id, true);
                        if ($result):
                            ob_start();
                            $result = ftp_get($conn_id, "php://output", $server_file, FTP_ASCII);
                            if ($result):
                                $result = fwrite($handle, ob_get_contents());
                            endif;
                            ob_end_clean();
                        endif;
                    else:
                        $result = false;
                    endif;
                    ftp_close($conn_id);
                    fclose($handle);
                    break;
                case 'live':
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $connection_details['url']);
                    if (isset($connection_details['post']) && !empty($connection_details['post'])):
                        curl_setopt($ch, CURLOPT_POST, $connection_details['no_post']);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $connection_details['post']);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    endif;
                    $result = curl_exec($ch);
                    curl_close($ch);
                    break;
                case 'file':

                    break;
            endswitch;
        endif;
        return $result;
    }

    private function warehouse_parse_xml_data($data) {
        if(isset($data) && !empty($data)){
            $destFolder = WAREHOUSE_PLUGIN_PATH . 'feeds/';
            $handle = fopen($destFolder . date('Y_m_d_h_i') . '.xml', "w");
            if ($handle) {
                fwrite($handle, $data);
                fclose($handle);
            }
        }
        $data = simplexml_load_string($data);        
        return ($data) ? $data : false;
    }

    private function warehouse_save_data_into_db($vendor_id, $data) {
        require_once 'warehouse_db_class.php';
        $resultArray = array();
        $product_db = new warehouse_database_class();
        $result = $product_db->warehouse_db_do_query("DELETE FROM `feed_products` WHERE `feed_products`.`feed_id` = " . $vendor_id);
        if ($result && (isset($data) && !empty($data))):
            foreach ($data as $index => $value):
                if (get_post_meta($vendor_id, '_file_type', true) == 'xml'):
                    $value = $this->warehouse_feed_get_product_values($vendor_id, (array) $value);
                endif;
                $value['sdesc'] = $this->warehouse_mysql_escape_mimic($value['sdesc']);
                $value['ldesc'] = $this->warehouse_mysql_escape_mimic($value['ldesc']);
                $value['colour'] = iconv('UTF-8', 'ISO-8859-1//IGNORE', $value['colour']);               

                $queryString = "INSERT INTO `feed_products` "
                        . "(`id`, `feed_id`, `Code`, `Title`, `Short Description`, `Long Description`, `Size`, `Colour`, `Brand`, `Wholesale Price`, `Category`, `Image`, `Barcode`, `AvailableStock`, `Weight`, `imported`,`new`) "
                        . "VALUES (NULL, '" . $vendor_id . "', '" . $value['code'] . "', '" . $value['title'] . "', '" . $value['sdesc'] . "', '" . $value['ldesc'] . "', '" . $value['size'] . "', '" . $value['colour'] . "', '"
                        . $value['brand'] . "', '" . $value['price'] . "', '" . $value['cat'] . "', '" . $value['image'] . "', '" . $value['barcode'] . "', '" . $value['qty'] . "', '0', 'no','" . $value['new'] . "')";
                ($product_db->warehouse_db_do_query($queryString)) ? $resultArray['success'] ++ : $resultArray['failure'] ++;
            endforeach;
        else:
            $resultArray['error'] = (!isset($data) || empty($data)) ? 'Error: No Data!' : 'Error deleting previous products';
        endif;
        return $resultArray;
    }

    private function warehouse_get_feed_meta($vendor_id) {
        $returnArray = array();
        $returnArray['code'] = get_post_meta($vendor_id, '_feed_meta_code', true);
        $returnArray['title'] = get_post_meta($vendor_id, '_feed_meta_title', true);
        $returnArray['sdesc'] = get_post_meta($vendor_id, '_feed_meta_sdesc', true);
        $returnArray['ldesc'] = get_post_meta($vendor_id, '_feed_meta_ldesc', true);
        $returnArray['size'] = get_post_meta($vendor_id, '_feed_meta_size', true);
        $returnArray['colour'] = get_post_meta($vendor_id, '_feed_meta_colour', true);
        $returnArray['brand'] = get_post_meta($vendor_id, '_feed_meta_brand', true);
        $returnArray['price'] = get_post_meta($vendor_id, '_feed_meta_price', true);
        $returnArray['cat'] = get_post_meta($vendor_id, '_feed_meta_cat', true);
        $returnArray['image'] = get_post_meta($vendor_id, '_feed_meta_image', true);
        $returnArray['barcode'] = get_post_meta($vendor_id, '_feed_meta_barcode', true);
        $returnArray['qty'] = get_post_meta($vendor_id, '_feed_meta_qty', true);
        $returnArray['new'] = get_post_meta($vendor_id, '_feed_new_product', true);
        return $returnArray;
    }

    private function warehouse_feed_get_product_values($vendor_id, $value) {
        $returnArray = array();
        $feed_meta = $this->warehouse_get_feed_meta($vendor_id);
        $returnArray['code'] = ((string) $value[$feed_meta['code']]) ? (string) $value[$feed_meta['code']] : '';
        $returnArray['title'] = ((string) $value[$feed_meta['title']]) ? (string) $value[$feed_meta['title']] : '';
        $returnArray['sdesc'] = ((string) $value[$feed_meta['sdesc']]) ? (string) $value[$feed_meta['sdesc']] : '';
        $returnArray['ldesc'] = ((string) $value[$feed_meta['ldesc']]) ? (string) $value[$feed_meta['ldesc']] : '';
        $returnArray['size'] = ((string) $value[$feed_meta['size']]) ? (string) $value[$feed_meta['size']] : '';
        $returnArray['colour'] = ((string) $value[$feed_meta['colour']]) ? (string) $value[$feed_meta['colour']] : '';
        $returnArray['brand'] = ((string) $value[$feed_meta['brand']]) ? (string) $value[$feed_meta['brand']] : '';
        $returnArray['price'] = ((float) $value[$feed_meta['price']]) ? (float) $value[$feed_meta['price']] : '';
        $returnArray['cat'] = ((string) $value[$feed_meta['cat']]) ? (string) $value[$feed_meta['cat']] : '';
        $returnArray['image'] = ((string) $value[$feed_meta['image']]) ? (string) $value[$feed_meta['image']] : '';
        $returnArray['barcode'] = ((string) $value[$feed_meta['barcode']]) ? (string) $value[$feed_meta['barcode']] : '';
        $returnArray['qty'] = ((string) $value[$feed_meta['qty']]) ? (string) $value[$feed_meta['qty']] : '';
        $returnArray['new'] = ((string) $value[$feed_meta['new']]) ? (string) $value[$feed_meta['new']] : 'N';
        return $returnArray;
    }

    private function warehouse_parse_csv_data($vendor_id, $data) {
        set_time_limit(0);
        ini_set('memory_limit', '2048M');
        $destFolder = WAREHOUSE_PLUGIN_PATH . 'feeds/';
        $returnArray = array();
        $handle = fopen($destFolder . get_post_meta($vendor_id, '_ftp_filename', true), "r");
        $delimter = get_post_meta($vendor_id, '_ftp_delimiter', true);
        $feed_meta = $this->warehouse_get_feed_meta($vendor_id);
        if ($handle):
            $first_line = true;
            while (($data = fgetcsv($handle, 0, $delimter)) !== FALSE):
                if ($first_line):
                    foreach ($feed_meta as $index => $value):
                        if (!empty($value)):
                            foreach ($data as $_index => $_value):
                                if ($value == $_value):
                                    $feed_meta[$index] = $_index;
                                endif;
                            endforeach;
                        endif;
                    endforeach;
                    $first_line = FALSE;
                else:
                    $tempArray = array();
                    $tempArray['code'] = ((string) $data[$feed_meta['code']]) ? (string) $data[$feed_meta['code']] : '';
                    $tempArray['title'] = ((string) $data[$feed_meta['title']]) ? (string) $data[$feed_meta['title']] : '';
                    $tempArray['sdesc'] = ((string) $data[$feed_meta['sdesc']]) ? (string) $data[$feed_meta['sdesc']] : '';
                    $tempArray['ldesc'] = ((string) $data[$feed_meta['ldesc']]) ? (string) $data[$feed_meta['ldesc']] : '';
                    $tempArray['size'] = ((string) $data[$feed_meta['size']]) ? (string) $data[$feed_meta['size']] : '';
                    $tempArray['colour'] = ((string) $data[$feed_meta['colour']]) ? (string) $data[$feed_meta['colour']] : '';
                    $tempArray['brand'] = ((string) $data[$feed_meta['brand']]) ? (string) $data[$feed_meta['brand']] : '';
                    $tempArray['price'] = ((string) $data[$feed_meta['price']]) ? (string) $data[$feed_meta['price']] : '';
                    $tempArray['cat'] = ((string) $data[$feed_meta['cat']]) ? (string) $data[$feed_meta['cat']] : '';
                    $tempArray['image'] = ((string) $data[$feed_meta['image']]) ? (string) $data[$feed_meta['image']] : '';
                    $tempArray['barcode'] = ((string) $data[$feed_meta['barcode']]) ? (string) $data[$feed_meta['barcode']] : '';
                    $tempArray['qty'] = ((string) $data[$feed_meta['qty']]) ? (string) $data[$feed_meta['qty']] : '';
                    $tempArray['new'] = ((string) $data[$feed_meta['new']]) ? (string) $data[$feed_meta['new']] : '';
                    array_push($returnArray, $tempArray);
                endif;
            endwhile;
        endif;
        return $returnArray;
    }

    private function warehouse_mysql_escape_mimic($inp) {
        if (is_array($inp)) {
            return array_map(__METHOD__, $inp);
        }

        if (!empty($inp) && is_string($inp)) {
            return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
        }

        return $inp;
    }

}
