<?php
if (isset($_POST['save_database'])):
    require_once 'warehouse_db_class.php';
    $products_db = new warehouse_database_class();
    $resultsArray = array();
    $resultsArray['db_host'] = update_option('warehouse_feed_products_db_host', $_POST['db_host'], 'no');
    $resultsArray['db_name'] = update_option('warehouse_feed_products_db_name', $_POST['db_name'], 'no');
    $resultsArray['db_username'] = update_option('warehouse_feed_products_db_usrname', $_POST['db_username'], 'no');
    $resultsArray['db_password'] = update_option('warehouse_feed_products_db_psw', $_POST['db_password'], 'no');    
    $resultsArray['db_notification'] = update_option('warehouse_feed_products_db_notification', $_POST['db_notification'], 'no');
    $resultsArray['db_regular_price_markup'] = update_option('warehouse_feed_products_db_regular_price_markup', $_POST['db_regular_price_markup'], 'no');
    $resultsArray['db_retail_price_markup'] = update_option('warehouse_feed_products_db_retail_price_markup', $_POST['db_retail_price_markup'], 'no');
    $resultsArray['db_setup'] = $products_db->warehouse_setup_feed_products_database();
    ?>
    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
        <p>Database host updated: <?php echo ($resultsArray['db_host']) ? 'yes' : 'no' ?></p>
        <p>Database name updated: <?php echo ($resultsArray['db_name']) ? 'yes' : 'no' ?></p>
        <p>Database username updated: <?php echo ($resultsArray['db_username']) ? 'yes' : 'no' ?></p>
        <p>Database password updated: <?php echo ($resultsArray['db_password']) ? 'yes' : 'no' ?></p>   
        <p>Database notification updated: <?php echo ($resultsArray['db_notification']) ? 'yes' : 'no' ?></p>      
        <p>Database regular price markup updated: <?php echo ($resultsArray['db_regular_price_markup']) ? 'yes' : 'no' ?></p>      
        <p>Database retail price markup updated: <?php echo ($resultsArray['db_retail_price_markup']) ? 'yes' : 'no' ?></p>      
        <p>Database table setup result: <?php echo (string) $resultsArray['db_setup'] ?></p>
    </div>
    <?php
endif;
?>
<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
    <h1>Warehouse Settings</h1>
    <div class="alert alert-warning alert-dismissible show" role="alert" style="margin-right: 20px">
        Please do not change these settings below, unless you would like to save your feeds in a different database or send notifications to a different email address.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <form method="POST">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <h3>Database Connection</h3>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <label for="db_host">Database Host</label>
            </div>
            <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                <input name="db_host" value="<?php echo get_option('warehouse_feed_products_db_host') ?>"/>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <label for="db_name">Database Name</label>
            </div>
            <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                <input name="db_name" value="<?php echo get_option('warehouse_feed_products_db_name') ?>"/>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <label for="db_username">Database Username</label>
            </div>
            <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                <input name="db_username" value="<?php echo get_option('warehouse_feed_products_db_usrname') ?>"/>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <label for="db_password">Database Password</label>
            </div>
            <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                <input name="db_password" value="<?php echo get_option('warehouse_feed_products_db_psw') ?>"/>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <label for="db_notification">Send Notifications To</label>
            </div>
            <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                <input name="db_notification" style="width:50%" value="<?php echo get_option('warehouse_feed_products_db_notification') ?>"/>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <h3>Global Prices</h3>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <label for="db_regular_price_markup">Regular Price Markup (%)</label>
            </div>
            <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                <input name="db_regular_price_markup" value="<?php echo get_option('warehouse_feed_products_db_regular_price_markup') ?>"/>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <label for="db_retail_price_markup">Retail Price Markup (%)</label>
            </div>
            <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                <input name="db_retail_price_markup" value="<?php echo get_option('warehouse_feed_products_db_retail_price_markup') ?>"/>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <button name="save_database" value="true" class="btn btn-success">Save</button>
            </div>
        </div>
    </form>
</div>