<?php

class warehouse_pages_featured_class {

    private $tableName = 'warehouse_featured_pages';

    public function __construct() {
        $this->prepareDataBase();
    }

    private function prepareDataBase() {
        global $wpdb;
        if (empty($wpdb->query("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '$this->tableName'"))):
            $query = "CREATE TABLE `" . DB_NAME . "`.`$this->tableName` ( "
                    . "`ID` INT NOT NULL AUTO_INCREMENT , "
                    . "`access_token` text NOT NULL , "
                    . "`access_token_expires` text NOT NULL , "
                    . "`refresh_token` text NOT NULL , "
                    . "`expires_in` text NOT NULL , "
                    . "`user` text NOT NULL , "
                    . "`username` text NOT NULL , "
                    . "`customer_id` text NOT NULL , "
                    . "`uri` text NOT NULL , "
                    . "`gstCodeUID` text NOT NULL , "
                    . "`accountsUid` text NOT NULL , "
                    . "`updated_at` timestamp NOT NULL , "
                    . "`created_at` timestamp NOT NULL , "
                    . "PRIMARY KEY (`ID`)) ENGINE = InnoDB;";
            $wpdb->query($query);
        endif;
    }

}
