<?php
add_action('admin_footer', 'warehouse_ajax_functions');
add_action('wp_ajax_warehouse_get_product_details', 'warehouse_get_product_details_callback');
add_action('wp_ajax_warehouse_get_product_simular_items', 'warehouse_get_product_simular_items_callback');
add_action('wp_ajax_warehouse_copy_parent_info', 'warehouse_copy_parent_info_callback');
add_action('wp_ajax_warehouse_download_image', 'warehouse_download_image_callback');
add_action('wp_ajax_warehouse_import_product', 'warehouse_import_product_callback');
add_action('wp_ajax_warehouse_get_cat_markup', 'warehouse_get_cat_markup_callback');
add_action('wp_ajax_warehouse_add_new_tag', 'warehouse_add_new_tag_callback');
add_action('wp_ajax_warehouse_delete_note', 'warehouse_delete_note_callback');
add_action('wp_ajax_warehouse_test_feed', 'warehouse_test_feed_callback');
add_action('wp_ajax_warehouse_round_prices', 'warehouse_round_prices_callback');
add_action('wp_ajax_warehouse_download_images', 'warehouse_download_images_callback');
add_action('wp_ajax_warehouse_download_missing_image', 'warehouse_download_missing_image_callback');
add_action('wp_ajax_warehouse_save_featured_array', 'warehouse_save_featured_array_callback');

function warehouse_ajax_functions() {
    ?>
    <script type="text/javascript" >
        jQuery(document).ready(function ($) {

            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
            if ($('#warehouse-import-products-table').length > 0) {
                var table = $('#warehouse-import-products-table').DataTable();
                downloadMissingImages();
                function downloadMissingImages() {
                    $('.fa-cog').each(function () {
                        var icon = $(this);
                        const product_id = icon.data('productid');
                        icon.data('productid', false)
                        if (product_id) {
                            $.post(ajaxurl, {action: 'warehouse_download_missing_image', product_id: product_id}, function (response) {
                                if (response) {
                                    icon.parent().append('<img height=100 src="' + response + '" />');
                                    icon.remove();
                                }
                            })
                        }
                    });
                }

                table.on('draw.dt', function () {
                    downloadMissingImages();
                });
            }


            if ($('button[name=download_vendor_images]').length > 0) {
                $('button[name=download_vendor_images]').click(function () {
                    $(this).attr('disabled', true);
                    $.post(ajaxurl, {action: 'warehouse_download_images'}, function (response) {
                        console.log(response);
                    });
                });
            }

            if ($('button[name=edit-product]').length > 0) {
                editProductClickHandler();
                function editProductClickHandler() {
                    $('button[name=edit-product]').each(function () {
                        if ($(this).data('events') === undefined) {
                            $(this).click(function (event) {
                                //var scrollPosition = $('body').scrollTop();
                                //$('.import-product-edit-box-wrapper').html('');
                                //$('body').addClass('model-open'); 
                                $('body').css('cursor', 'progress');  
                                $('button[name=edit-product]').attr("disabled", true);
                                var data = {
                                    action: 'warehouse_get_product_details',
                                    product_id: event.target.value,
                                    sizes: $(this).next('input[name=sizes]').val(),
                                    colours: $(this).next('input[name=sizes]').next('input[name=colours]').val()
                                }                                
                                $.post(ajaxurl, data, function (response) { 

                                    $('body').css('cursor', 'default');
                                    $('button[name=edit-product]').attr("disabled", false);                              

                                    if (response) {

                                        //DEBUG LINE
                                        //alert(response);                              
                                        window.location.href = response.replace("edit0", "edit").replace("&amp;", "&");;

                                        /*$('.import-product-edit-box-wrapper').html(response).promise().done(function () {

                                            $('.import-product-edit-box').scrollTop(0);
                                            if ($('input[name=vendor]').val() === '') {
                                                alert('Vendor not set');
                                            }

                                            var media_uploader = null;
                                            if ($('.default-image').html() !== undefined && $('.default-image').html().length > 0) {
                                                var data = {
                                                    action: 'warehouse_download_image',
                                                    vendor_id: $('input[name=vendor_id]').val(),
                                                    link: $('.default-image').html()
                                                }
                                                $.post(ajaxurl, data, function (response) {
                                                    console.log(response);
                                                    if (response) {
                                                        $('.image-container-wrapper').append('<div class="col-lg-2 image-container"><input type="hidden" name="attachment_id" value="no-id" /><img src="' + response + '"/><div class="col-lg-12 image-button-container"><button type="button" value="" name="main-image" class="col-lg-6 btn btn-default">Main image</button><button type="button" value="" name="gallery-image" class="col-lg-6 btn btn-default">Gallery image</button></div></div>');
                                                        warehouse_add_image_click_handlers();
                                                    } else {
                                                        alert("Can't download image from feed");
                                                    }
                                                });
                                            }

                                            $('.import-product-edit-box').removeClass('hidden');                         
                                                           
                                            tinymce.init({
                                                selector: 'textarea',
                                                height: 300
                                            });
                                            $('button[name=cancel]').click(function () {
                                                $('.import-product-edit-box-wrapper').html('');
                                                $('.import-product-edit-box').addClass('hidden');
                                                $('body').removeClass('model-open');
                                                $('body').scrollTop(scrollPosition);
                                            });
                                            $('input[name=category]').click(function () {
                                                if (parseInt($('select[name=set-price-by]').val()) === 0 && $(this).attr('checked') === 'checked') {
                                                    $.post(ajaxurl, {action: 'warehouse_get_cat_markup', cats: $(this).val()}, function (response) {
                                                        $('select[name=markup-by-cat]').append(response);
                                                    });
                                                }
                                            });
                                            $('button[name=new-tag-show]').click(function () {
                                                $('.new-tag-container').removeClass('hidden');
                                            });
                                            $('button[name=add-new-tag]').click(function () {
                                                if ($('input[name=new-tag-name]').val().length > 0) {
                                                    $.post(ajaxurl, {action: 'warehouse_add_new_tag', name: $('input[name=new-tag-name]').val()}, function (response) {
                                                        response = JSON.parse(response);
                                                        if (response.term_id) {
                                                            $('.tag-container').append('<div class="checkbox col-lg-2"><input type="checkbox" checked name="tag" value="' + response.term_id + '"/><label for="' + response.term_id + '">' + $('input[name=new-tag-name]').val() + '</label></div>');
                                                            $('input[name=new-tag-name]').val('');
                                                        } else {
                                                            alert("Tag already exists");
                                                        }
                                                    });
                                                } else {
                                                    alert('Please enter a tag name');
                                                }
                                            });
                                            $('select[name=set-price-by]').change(function () {
                                                $('.manual-markup').addClass('hidden');
                                                $('.category-markup-container').html('');
                                                $('.category-markup-container').addClass('hidden');
                                                if (parseInt($(this).val()) === 1) {
                                                    $('.manual-markup').removeClass('hidden');
                                                }
                                                if (parseInt($(this).val()) === 0) {

                                                    var cats = warehouse_get_selected_cat_array();
                                                    if (cats) {
                                                        $('.category-markup-container').html('<select class="form-control" name="markup-by-cat"><option value="-1">Select a markup</option></select>');
                                                        $.post(ajaxurl, {action: 'warehouse_get_cat_markup', cats: cats}, function (response) {

                                                            $('select[name=markup-by-cat]').append(response).promise().done(function () {

                                                                $('.category-markup-container').removeClass('hidden');
                                                                $('select[name=markup-by-cat]').change(function () {
                                                                    var value = parseInt($(this).val());
                                                                    if (value !== -1) {
                                                                        var markup = parseFloat(value);
                                                                        var wholesale = parseFloat($('input[name=w_price]').val());
                                                                        var price = parseFloat(((wholesale / 100) * markup) + wholesale).toFixed(2);
                                                                        $('input[name=price]').val(price);
                                                                    }
                                                                });
                                                            });
                                                        })
                                                    } else {
                                                        alert('Please select a category first');
                                                        $('select[name=set-price-by]').val(2);
                                                    }
                                                }

                                            });
                                            $('input[name=m_markup]').on('input', function () {
                                                var markup = parseFloat($(this).val());
                                                var wholesale = parseFloat($('input[name=w_price]').val());
                                                var price = parseFloat(((wholesale / 100) * markup) + wholesale).toFixed(2);
                                                $('input[name=price]').val(price);
                                            })

                                            $('button[name=image-uploader]').click(function () {
                                                open_media_uploader_image(function (result) {
                                                    if (result) {
                                                        $('.image-container-wrapper').append('<div class="col-lg-2 image-container"><input type="hidden" name="attachment_id" value="' + result.id + '" /><img src="' + result.url + '"/><div class="col-lg-12 image-button-container"><button type="button" value="' + result.id + '" name="main-image" class="col-lg-6 btn btn-default">Main image</button><button type="button" value="' + result.id + '" name="gallery-image" class="col-lg-6 btn btn-default">Gallery image</button></div></div>');
                                                        warehouse_add_image_click_handlers();
                                                    }
                                                });
                                            });
                                            $('input[name=price]').on('input', function () {
                                                $('select[name=set-price-by]').val(2);
                                                $('select[name=set-price-by]').trigger('change');
                                            });
                                            $('button[name=import]').click(function () {
                                                var button = $(this);
                                                var cancel = $(this).next();
                                                var price = parseFloat($('input[name=price]').val()).toFixed(2), wholesale = parseFloat($('input[name=w_price]').val()).toFixed(2), priceValidation = parseInt(price) > parseInt(wholesale), mainImageValidation = false, catValidation = false, cats = [], mainImage = false;
                                                $('button[name=main-image]').each(function () {
                                                    if ($(this).hasClass('main-image-selected')) {
                                                        mainImageValidation = true;
                                                        mainImage = ($(this).parent().parent().children('input[name=attachment_id]').val() !== 'no-id') ? $(this).parent().parent().children('input[name=attachment_id]').val() : $(this).parent().parent().children('img').prop('src');
                                                    }
                                                });
                                                $('input[name=category]').each(function () {
                                                    if ($(this).attr('checked') === 'checked') {
                                                        catValidation = true;
                                                        cats.push($(this).val());
                                                    }
                                                });
                                                if (priceValidation && mainImageValidation && catValidation) {
                                                    button.prop('disabled', true);
                                                    cancel.prop('disabled', true);
                                                    if ($('input[name=child]').attr('checked') === 'checked') {
                                                        var parent_id = false;
                                                        $('.sub_parent-container').each(function () {
                                                            if ($(this).hasClass('admin-parent-selected')) {
                                                                parent_id = $(this).children('input[name=_product_parent]').val();
                                                            }
                                                        });
                                                    }
                                                    var galleryImages = [];
                                                    $('button[name=gallery-image]').each(function () {
                                                        if ($(this).hasClass('gallery-image-selected')) {
                                                            galleryImages.push(($(this).parent().parent().children('input[name=attachment_id]').val() !== 'no-id') ? $(this).parent().parent().children('input[name=attachment_id]').val() : $(this).parent().parent().children('img').prop('src'));
                                                        }
                                                    });
                                                    var tags = [];
                                                    $('input[name=tag]').each(function () {
                                                        if ($(this).attr('checked') === 'checked') {
                                                            tags.push($(this).next().html());
                                                        }
                                                    });
                                                    tags = tags.join(',');
                                                    var data = {
                                                        action: 'warehouse_import_product',
                                                        title: $('input[name=title]').val(),
                                                        seo_title: $('input[name=seo-title]').val(),
                                                        s_desc: tinyMCE.get('s_desc_' + $('input[name=product_id_edit_box]').val()).getContent(),
                                                        seo_desc: $('input[name=seo-desc]').val(),
                                                        l_desc: tinyMCE.get('l_desc_' + $('input[name=product_id_edit_box]').val()).getContent(),
                                                        size: $('input[name=size]').val(),
                                                        colour: $('input[name=colour]').val(),
                                                        brand: $('input[name=brand]').val(),
                                                        code: $('input[name=code]').val(),
                                                        video: $('input[name=video]').val(),
                                                        barcode: $('input[name=barcode]').val(),
                                                        related_cat: $('select[name=realated-cat]').val(),
                                                        price_by: $('select[name=set-price-by]').val(),
                                                        w_price: wholesale,
                                                        price: price,
                                                        cats: cats.join(','),
                                                        mainImage: mainImage,
                                                        vendor: $('input[name=vendor]').val(),
                                                        m_markup: $('input[name=m_markup]').val(),
                                                        tags: tags,
                                                        parent_id: parent_id,
                                                        db_id: $('input[name=product_id_edit_box]').val(),
                                                        import_related_products: ($('input[name=import-related-products]').attr('checked') === 'checked') ? 1 : 0
                                                    }
                                                    if (parent_id) {
                                                        data.parent_id = parent_id;
                                                    }
                                                    if (galleryImages.length > 0) {
                                                        data.galleryImages = galleryImages.join(',');
                                                    }
                                                    $.post(ajaxurl, data, function (response) {
                                                        if (response) {
                                                            response = Object.values(JSON.parse(response));
                                                            $('button[name=cancel]').trigger('click');
                                                            table.rows().every(function () {
                                                                var data = this.data();
                                                                if (data !== undefined) {
                                                                    var db_id = $(data[data.length - 1]).val();
                                                                    if ($.inArray(db_id, response) !== -1) {
                                                                        this.remove().draw();
                                                                    }
                                                                }
                                                            });
                                                        } else {
                                                            button.prop('disabled', false);
                                                            cancel.prop('disabled', false);
                                                            alert('Problem importing product');
                                                        }
                                                    });
                                                } else {
                                                    if (!priceValidation) {
                                                        alert('Price is lower or same as wholesale price');
                                                    }
                                                    if (!mainImageValidation) {
                                                        alert('Main image not selected');
                                                    }
                                                    if (!catValidation) {
                                                        alert('Category not selected');
                                                    }
                                                }
                                            });
                                            $('input[name=child]').click(function () {

                                                if ($(this).attr('checked') === 'checked') {

                                                    var data = {
                                                        action: 'warehouse_get_product_simular_items',
                                                        brand: $('input[name=brand]').val(),
                                                        code: $('input[name=code]').val()
                                                    }

                                                    $.post(ajaxurl, data, function (_response) {
                                                        if (_response) {

                                                            $('.parent-container').html(_response).promise().done(function () {

                                                                $('.parent-container').removeClass('hidden');
                                                                $('.sub_parent-container').click(function (event) {
                                                                    if (event.target.classList.contains('copy_parent_info')) {
                                                                        var data = {
                                                                            action: 'warehouse_copy_parent_info',
                                                                            id: $(this).children('.parent-input').val()
                                                                        }
                                                                        $.post(ajaxurl, data, function (__response) {
                                                                            var parentObject = JSON.parse(__response);
                                                                            (parentObject.title) ? $('input[name=title]').val(parentObject.title) : '';
                                                                            (parentObject.s_desc) ? tinyMCE.get('s_desc').setContent(parentObject.s_desc) : '';
                                                                            (parentObject.l_desc) ? tinyMCE.get('l_desc').setContent(parentObject.l_desc) : '';
                                                                            (parentObject.price) ? parseFloat($('input[name=price]').val(parentObject.price)).toFixed(2) : '';
                                                                            (parentObject.related_cats) ? $('select[name=realated-cat]').val(parentObject.related_cats) : '';
                                                                            (parentObject.video) ? $('select[name=video]').val(parentObject.video) : '';
                                                                            (parentObject.select_price_save) ? $('select[name=set-price-by]').val(parentObject.select_price_save) : '';
                                                                            (parentObject.seo_title) ? $('input[name=seo-title]').val(parentObject.seo_title) : '';
                                                                            (parentObject.seo_desc) ? $('input[name=seo_desc]').val(parentObject.seo_desc) : '';
                                                                            for (var key in parentObject.image) {
                                                                                (parentObject.image[key]) ? $('.image-container-wrapper').append('<div class="col-lg-2 image-container"><input type="hidden" name="attachment_id" value="' + key + '" /><img src="' + parentObject.image[key] + '"/><div class="col-lg-12 image-button-container"><button type="button" value="" name="main-image" class="col-lg-6 btn btn-default ">Main image</button><button type="button" value="" name="gallery-image" class="col-lg-6 btn btn-default">Gallery image</button></div></div>') : '';
                                                                            }
                                                                            for (var _image in parentObject.gallery) {
                                                                                if (_image === 'no-id') {
                                                                                    for (var __image in parentObject.gallery[_image]) {
                                                                                        $('.image-container-wrapper').append('<div class="col-lg-2 image-container"><input type="hidden" name="attachment_id" value="' + _image + '" /><img src="' + parentObject.gallery[_image][__image] + '"/><div class="col-lg-12 image-button-container"><button type="button" value="" name="main-image" class="col-lg-6 btn btn-default ">Main image</button><button type="button" value="" name="gallery-image" class="col-lg-6 btn btn-default">Gallery image</button></div></div>');
                                                                                    }
                                                                                } else {
                                                                                    $('.image-container-wrapper').append('<div class="col-lg-2 image-container"><input type="hidden" name="attachment_id" value="' + _image + '" /><img src="' + parentObject.gallery[_image] + '"/><div class="col-lg-12 image-button-container"><button type="button" value="" name="main-image" class="col-lg-6 btn btn-default ">Main image</button><button type="button" value="" name="gallery-image" class="col-lg-6 btn btn-default">Gallery image</button></div></div>');
                                                                                }
                                                                            }

                                                                            $('input[name=category]').each(function () {
                                                                                if ($.inArray(parseInt($(this).val()), parentObject.cats) !== -1) {
                                                                                    $(this).attr('checked', true)
                                                                                }
                                                                            })

                                                                            $('input[name=tag]').each(function () {
                                                                                if ($.inArray(parseInt($(this).val()), parentObject.tags) !== -1) {
                                                                                    $(this).attr('checked', true)
                                                                                }
                                                                            })

                                                                            $('select[name=set-price-by]').trigger('change');
                                                                            warehouse_add_image_click_handlers();
                                                                        });
                                                                    } else {
                                                                        var selecting = true;
                                                                        if ($(this).hasClass('admin-parent-selected')) {
                                                                            selecting = false;
                                                                        }
                                                                        $('.sub_parent-container').each(function () {
                                                                            $(this).removeClass('admin-parent-selected');
                                                                            $(this).children('.parent-input').removeAttr('id');
                                                                            $(this).children('.parent-input').removeAttr('name');
                                                                            $(this).children('.copy_parent_info').addClass('hidden');
                                                                        });
                                                                        if (selecting) {
                                                                            $(this).addClass('admin-parent-selected');
                                                                            $(this).children('.parent-input').attr('id', '_product_parent');
                                                                            $(this).children('.parent-input').attr('name', '_product_parent');
                                                                            $(this).children('.copy_parent_info').removeClass('hidden');
                                                                        }
                                                                    }
                                                                });
                                                            });
                                                        }
                                                    })
                                                } else {
                                                    $('.parent-container').addClass('hidden');
                                                    $('.parent-container').html('');
                                                }
                                            });
                                        });*/
                                    } else {
                                        alert('Error getting products details');
                                    }
                                });                                
                            });
                        }
                    });
                }
                $('#warehouse-import-products-table').on('search.dt', function () {
                    editProductClickHandler();
                });
                $('#warehouse-import-products-table').on('draw.dt', function () {
                    editProductClickHandler();
                });
            }

            if ($('input[name=vendor_test]').length > 0) {
                $('input[name=vendor_test]').click(function () {
                    var message_container = $('.ajax_result_message'), button = $(this);
                    if (message_container.hasClass('successfull_message') || message_container.hasClass('failure_message')) {
                        message_container.removeClass('successfull_message');
                        message_container.removeClass('failure_message');
                        message_container.html('');
                    }
                    button.attr('disabled', true);
                    var data = {
                        vendor_id: button.prev().prev().val(),
                        action: 'warehouse_test_feed'
                    }
                    $.post(ajaxurl, data, function (result) {
                        if (result) {
                            message_container.addClass('successfull_message');
                            message_container.html('<p>Connection result successfull</p>');
                        } else {
                            message_container.addClass('failure_message');
                            message_container.html('<p>Connection result failure</p>');
                        }
                        button.attr('disabled', false);
                    });
                });
            }

            if ($('input[name=warehouse_maintenance_round_prices]').length > 0) {
                var round_prices_button = $('input[name=warehouse_maintenance_round_prices]');
                var ajaxMessage = $('.ajax_result_message');
                round_prices_button.click(function () {
                    round_prices_button.attr('disabled', true);
                    $.post(ajaxurl, {action: 'warehouse_round_prices'}, function (response) {
                        ajaxMessage.html(response);
                        round_prices_button.attr('disabled', false);
                    })
                });
            }

            function warehouse_get_selected_cat_array() {
                var cats = [];
                $('input[name=category]').each(function () {
                    if ($(this).attr('checked') === 'checked') {
                        cats.push($(this).val());
                    }
                });
                return (cats.length > 0) ? cats.join(',') : false;
            }

            function warehouse_add_image_click_handlers() {
                $('button[name="main-image"]').unbind();
                $('button[name="gallery-image"]').unbind();
                $('button[name="main-image"]').click(function () {
                    var clicked = $(this);
                    if (clicked.hasClass('main-image-selected')) {
                        clicked.removeClass('main-image-selected');
                        clicked.removeClass('btn-success');
                    } else {
                        $('button[name="main-image"]').each(function () {
                            $(this).removeClass('main-image-selected');
                            $(this).removeClass('btn-success');
                        });
                        clicked.addClass('btn-success');
                        clicked.addClass('main-image-selected');
                        if (clicked.next().hasClass('gallery-image-selected')) {
                            clicked.next().removeClass('gallery-image-selected');
                            clicked.next().removeClass('btn-success');
                        }
                    }
                });
                $('button[name="gallery-image"]').click(function () {
                    var clicked = $(this);
                    if (clicked.hasClass('gallery-image-selected')) {
                        clicked.removeClass('gallery-image-selected');
                        clicked.removeClass('btn-success');
                    } else {
                        clicked.addClass('btn-success');
                        clicked.addClass('gallery-image-selected');
                        if (clicked.prev().hasClass('main-image-selected')) {
                            clicked.prev().removeClass('main-image-selected');
                            clicked.prev().removeClass('btn-success');
                        }
                    }
                });
            }

            if ($('button[name=add_featured_product]').length > 0) {

            }

            function open_media_uploader_image(callback)
            {
                media_uploader = wp.media({
                    frame: "post",
                    state: "insert",
                    multiple: true
                });
                media_uploader.on("insert", function () {
                    callback(media_uploader.state().get("selection").first().toJSON());
                });
                media_uploader.open();
            }

            $('button[name=save_featured_products]').click(function () {
                $('.loading-overlay').removeClass('hidden');
                var gridNumbers = {};
                for (var i = 1; i < 16; i++) {
                    gridNumbers[i] = '';
                }
                $('.gridNumber').each(function () {
                    if ($(this).html().length > 0) {
                        gridNumbers[$(this).html()] = $(this).next().val();
                    }
                })
                var data = {
                    action: 'warehouse_save_featured_array',
                    post_id: $('#post_id').val(),
                    gridArray: JSON.stringify(gridNumbers)
                }
                $.post(ajaxurl, data, function (response) {
                    $('.loading-overlay').addClass('hidden');
                })

            });
        });
        //}
        //);
    </script> <?php
}

//DEVELOPMENT
function create_product() {

    try {

        require_once 'warehouse_live_products_class.php';
        $live_products = new warehouse_live_products_class();

        $product_ids  = explode(",", $_POST['product_id']);
        $sizes  = explode(",", $_POST['sizes']);
        $colours  = explode(",", $_POST['colours']);

        $product_id = $product_ids[0];   

        $product = $live_products->warehouse_get_product_by_id((int) $product_id);

        if (isset($product) && !empty($product)) { 

            $feed_id = $product["feed_id"];
            $category = $product["Category"];
            $image = $product["Image"];
            
            // Get equivalence for categories and wc attributes        
            require_once 'warehouse_db_class.php';

            $db = new warehouse_database_class();
            $post_category = array();
            $pa_attributes = array();
                        
            $sql = "SELECT category_id_woocommerce, pa_attributes
                    FROM `feed_categories` 
                    WHERE `feed_id` = $feed_id AND `category_description_vendor` = '" . $category . "'";

            $categories = $db->warehouse_db_do_query($sql);

            if ($categories->num_rows > 0){
                while ($row = mysqli_fetch_assoc($categories)){                
                    array_push($post_category, $row['category_id_woocommerce']);
                    $pa_attributes = array_merge($pa_attributes, explode(",", $row['pa_attributes']));                    
                }
            }

            // Create parent product
            $post = array(  'post_status' => "draft",
                            'post_title' => $product['Title'],
                            'post_type' => "product",                        
                            'post_content' => htmlspecialchars($product['Long Description']),
                            'comment_status' => 'closed',
                            'post_excerpt' => htmlspecialchars($product['Short Description'])
                        );

            $post_id = wp_insert_post($post);

            /*if($post_id == 0) { // If it fails it will try to insert product without special characters

                $post = array(  'post_status' => "draft",
                                'post_title' => $product['Title'],
                                'post_type' => "product",                        
                                'post_content' => htmlspecialchars($product['Long Description']),
                                'comment_status' => 'closed',
                                'post_excerpt' => htmlspecialchars($product['Short Description'])
                            );

                $post_id = wp_insert_post($post);
            }*/

            // Retrieve wc product and set category
            $wc_product = wc_get_product($post_id);
            $wc_product->set_category_ids($post_category);
            $wc_product->save();

            // Make product type variable
            wp_set_object_terms ($post_id, 'variable', 'product_type');

            // Set vendor for product
            update_post_meta($post_id, 'vendor', get_the_title( $feed_id ));           
            
            // Download image and set it
            $image = $live_products->warehouse_download_feed_image($feed_id, $image);

            // magic sideload image returns an HTML image, not an ID
            $media = media_sideload_image($image, $post_id);

            // therefore we must find it so we can set it as featured ID
            if(!empty($media) && !is_wp_error($media)){
                $args = array(
                    'post_type' => 'attachment',
                    'posts_per_page' => -1,
                    'post_status' => 'any',
                    'post_parent' => $post_id
                );

                // reference new image to set as featured
                $attachments = get_posts($args);

                if(isset($attachments) && is_array($attachments)){
                    foreach($attachments as $attachment){
                        // grab source of full size images (so no 300x150 nonsense in path)
                        $image = wp_get_attachment_image_src($attachment->ID, 'full');
                        // determine if in the $media image we created, the string of the URL exists
                        if(strpos($media, $image[0]) !== false){
                            // if so, we found our image. set it as thumbnail
                            set_post_thumbnail($post_id, $attachment->ID);
                            // only want one image
                            break;
                        }
                    }
                }
            }

            // Yoast SEO
            update_post_meta($post_id, '_yoast_wpseo_title', $product['Title'] . ' | ' . get_bloginfo('name'));
            update_post_meta($post_id, '_yoast_wpseo_metadesc', $product['Short Description']);  

            $product_attributes = array();        

            // Set WooCommerce attributes
            for ($i=0; $i<count($pa_attributes); $i++) {           

                if(strpos($pa_attributes[$i], 'size')) 
                {
                    wp_set_object_terms($post_id, $sizes, $pa_attributes[$i]);
                } 
                else if (strpos($pa_attributes[$i], 'colour')) 
                {
                    wp_set_object_terms($post_id, $colours, $pa_attributes[$i]);
                }

                $product_attributes[$pa_attributes[$i]] = Array('name'=>$pa_attributes[$i], 
                                                                'value'=> '',
                                                                'is_visible' => '1',
                                                                'is_variation' => '1',
                                                                'is_taxonomy' => '1'
                                                            );
            }

            update_post_meta($post_id,'_product_attributes', $product_attributes);

            // Load all product data 
            $products = array();
            
            $sql = "SELECT *
                    FROM `feed_products` 
                    WHERE `feed_id` = " . $feed_id . " AND `id` IN (". $_POST['product_id'] .")";

            $products_found = $db->warehouse_db_do_query($sql);

            if ($products_found->num_rows > 0){
                while ($row = mysqli_fetch_assoc($products_found)){                
                    $products[$row["id"]] = $row;
                }
            }

            $size = '';
            $colour = '';

            $regular_price_markup = get_option('warehouse_feed_products_db_regular_price_markup') / 100;
            $retail_price_markup = get_option('warehouse_feed_products_db_retail_price_markup') / 100;

            $regular_price = 0;
            $retail_price = 0;

            // Set variations
            for ($i=0; $i<count($product_ids); $i++) {

                $attributes = array();

                for ($n=0; $n<count($pa_attributes); $n++) { 
                    if(strpos($pa_attributes[$n], 'size')) 
                    {
                        $attributes[$pa_attributes[$n]] = $sizes[$i];
                        $size = $sizes[$i];
                    } 
                    else if (strpos($pa_attributes[$n], 'colour')) 
                    {
                        $attributes[$pa_attributes[$n]] = $colours[$i];
                        $colour = $colours[$i];
                    }
                }

                $regular_price = round($products[$product_ids[$i]]["Wholesale Price"] * $regular_price_markup) + 0.99;
                $retail_price = round($regular_price * $retail_price_markup) + 0.99;

                // Set data for variation               
                $variation_data = array (   
                    'attributes'        => $attributes,
                    'sku'               => $products[$product_ids[$i]]["Barcode"],
                    'regular_price'     => $regular_price,
                    '_code'             => $products[$product_ids[$i]]["Code"],
                    '_brand'            => $products[$product_ids[$i]]["Brand"],
                    '_wholesale_price'  => $products[$product_ids[$i]]["Wholesale Price"],
                    '_retail_price'     => $retail_price,
                    'sale_price'        => '',
                    'st_size'           => $size,                                                                       // compatibility with outdated routines
                    'st_colour'         => $colour,                                                                     // compatibility with outdated routines
                    '_barcode'          => $products[$product_ids[$i]]["Barcode"],                                      // compatibility with outdated routines
                    'description'       => htmlentities($products[$product_ids[$i]]["Long Description"]),
                    'image'             => $products[$product_ids[$i]]["Image"],
                );

                // DEBUG LINE
                //echo var_dump($variation_data);
                //return;
                
                // ****************
            
                create_product_variation( $feed_id, $post_id, $variation_data );
            }

            // Redirect to product page
            echo get_edit_post_link($post_id);
        }
    } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }
}

function create_product_variation( $feed_id, $product_id, $variation_data ){

    require_once 'warehouse_live_products_class.php';
    $live_products = new warehouse_live_products_class();

    // Get the Variable product object (parent)
    $product = wc_get_product($product_id);
    
    $variation_post = array(
        'post_title'    => $product->get_title(),
        'post_name'     => 'product-'.$product_id.'-variation',
        'post_status'   => 'publish',
        'post_parent'   => $product_id,
        'post_type'     => 'product_variation',        
        'guid'          => $product->get_permalink()        
    );
    
    // Creating the product variation
    $variation_id = wp_insert_post( $variation_post );

    // Get an instance of the WC_Product_Variation object
    $variation = new WC_Product_Variation( $variation_id );

    if ($feed_id == 12430) { // Unfortunatelly, only Windsor Wholeseller has different images for each product

        // Download image and set it
        $image = $live_products->warehouse_download_feed_image($feed_id, $variation_data['image']);

        // magic sideload image returns an HTML image, not an ID
        $media = media_sideload_image($image, $variation_id);

        // Get image ID
        $attachments = get_posts(array(
            'post_type' => 'attachment',
            'post_status' => null,
            'post_parent' => $variation_id,
            'orderby' => 'post_date',
            'order' => 'DESC'
        ));

         // Set image ID
        $variation->set_image_id($attachments[0]->ID);
    }
       
    // Iterating through the variations attributes
    foreach ($variation_data['attributes'] as $attribute => $term_name )
    {
        $taxonomy = $attribute; // The attribute taxonomy
    
        // If taxonomy doesn't exists we create it
        if( ! taxonomy_exists( $taxonomy ) ){
            register_taxonomy(
                $taxonomy,
                'product_variation',
                array(
                    'hierarchical' => false,
                    'label' => ucfirst( $taxonomy ),
                    'query_var' => true,
                    'rewrite' => array( 'slug' => '$taxonomy') // The base slug
                )
            );
        }
    
        // Check if the Term name exist and if not we create it.
        if( ! term_exists( $term_name, $taxonomy ) )
            wp_insert_term( $term_name, $taxonomy ); // Create the term
    
        $term_slug = get_term_by('name', $term_name, $taxonomy )->slug; // Get the term slug
    
        // Get the post Terms names from the parent variable product.
        $post_term_names =  wp_get_post_terms( $product_id, $taxonomy, array('fields' => 'names') );
    
        // Check if the post term exist and if not we set it in the parent variable product.
        if( ! in_array( $term_name, $post_term_names ) )
            wp_set_post_terms( $product_id, $term_name, $taxonomy, true );
    
        // Set/save the attribute data in the product variation
        update_post_meta( $variation_id, 'attribute_'.$taxonomy, $term_slug );
    }
    
    ## Set/save all other data

    // Custom fields    
    update_post_meta( $variation_id, '_code', $variation_data['_code'] );
    update_post_meta( $variation_id, '_brand', $variation_data['_brand'] );
    update_post_meta( $variation_id, '_wholesale_price', $variation_data['_wholesale_price'] );
    update_post_meta( $variation_id, '_retail_price', $variation_data['_retail_price'] );

    // Not shown on front-end, but kept on back-end for compatibility sake
    update_post_meta( $variation_id, 'st_size', $variation_data['st_size'] );
    update_post_meta( $variation_id, 'st_colour', $variation_data['st_colour'] );
    update_post_meta( $variation_id, '_barcode', $variation_data['_barcode'] );

    //$variation->set_description($variation_data['description']);
    
    // SKU
    if( ! empty( $variation_data['sku'] ) )
        $variation->set_sku( $variation_data['sku'] );
    
    // Prices
    if( empty( $variation_data['sale_price'] ) ){
        $variation->set_price( $variation_data['regular_price'] );
    } else {
        $variation->set_price( $variation_data['sale_price'] );
        $variation->set_sale_price( $variation_data['sale_price'] );
    }
    $variation->set_regular_price( $variation_data['regular_price'] );
    
    // Stock
    if( ! empty($variation_data['stock_qty']) ){
        $variation->set_stock_quantity( $variation_data['stock_qty'] );
        $variation->set_manage_stock(true);
        $variation->set_stock_status('');
    } else {
        $variation->set_manage_stock(false);
    }
    
    $variation->set_weight(''); // weight (reseting)
    
    $variation->save(); // Save the data
}

function warehouse_get_product_details_callback() {

    create_product();

    /*require_once 'warehouse_live_products_class.php';

    $live_products = new warehouse_live_products_class();
    
    $product_ids  = explode(",", $_POST['product_id']);
    $product_id = $product_ids[0];   

    $product = $live_products->warehouse_get_product_by_id((int) $product_id);    

    if (isset($product) && !empty($product)):        
        require_once 'vendors_classes.php';
        $vendors = new vendor_functions();        
        $cats = $live_products->warehouse_get_cats();        
        $container = '<div class="col-lg-12"><h1>Import Product</h1>';
        $container .= '<input type="hidden" name="product_id_edit_box" value="' . $product_id . '"/>';
        $container .= '<input type="hidden" name="vendor" value="' . $vendors->vendor_get_vendor_name(htmlentities($_POST['vendor_id'])) . '"/>';
        $container .= '<input type="hidden" name="vendor_id" value="' . $_POST['vendor_id'] . '"/>';
        //$container .= '<h2>Child Product</h2>';
        //$container .= '<div class="checkbox col-lg-12"><input type="checkbox" name="child"/><label for="child">Add as Child Product</label></div>';
        $container .= '<div class="col-lg-12 hidden parent-container"></div>';

        $container .= '<h2>Details</h2>';

        $container .= '<div class="row">';

        $container .= '<div class="form-group col-md-6">
                            <label for="title">Title</label><input class="form-control" name="title" value="' . htmlentities($product["Brand"]) . ' ' . htmlentities($product['Title']) . ' ' . htmlentities($product["Colour"]) . ' ' . htmlentities($product["Size"]) . '"/>
                            <label for="seo-title">SEO Title</label><input class="form-control" name="seo-title" value="' . htmlentities($product["Brand"]) . ' ' . htmlentities($product['Title']) . ' ' . htmlentities($product["Colour"]) . ' ' . htmlentities($product["Size"]) . '"/>
                            <label for="seo-desc">SEO Description</label><input class="form-control" name="seo-desc" value="' . htmlentities($product['Short Description']) . '"/>
                            <label for="size">Size</label><input class="form-control" name="size" value="' . htmlentities($product["Size"]) . '"/>
                            <label for="colour">Colour</label><input class="form-control" name="colour" value="' . htmlentities($product["Colour"]) . '"/>
                            <label for="brand">Brand</label><input class="form-control" name="brand" value="' . htmlentities($product["Brand"]) . '"/>
                            <label for="code">Code</label><input class="form-control" name="code" value="' . htmlentities($product["Code"]) . '"/>
                            <label for="code">Video Link</label><input class="form-control" name="video" value=""/>
                            <label for="barcode">Barcode</label><input class="form-control" name="barcode" value="' . htmlentities($product["Barcode"]) . '"/>
                            <h2>Images</h2>
                            <p class="hidden default-image">' . $product["Image"] . '</p>
                            <button class="btn btn-default" name="image-uploader" type="button">Upload Images</button>
                            <div class="col-lg-12 image-container-wrapper"></div>
                       </div>';
        
        $container .= '<div class="form-group col-md-6">
                            <label for="s_desc_' . $product_id . '">Short Description</label><textarea class="form-control" name="s_desc_' . htmlentities($_POST['product_id']) . '">' . htmlentities($product["Short Description"]) . '</textarea>
                            <label for="l_desc_' . $product_id . '">Long Description</label><textarea class="form-control" name="l_desc_' . htmlentities($_POST['product_id']) . '">' . htmlentities($product["Long Description"]) . '</textarea>
                       </div>';

        $container .= '</div>';              
    
        $container .= '<h2>Categories</h2>';

        $container .= '<div class="col-lg-12">';
        foreach ($live_products->warehouse_get_cat_array() as $index => $value):
            $container .= ($value["name"]) ? "<div class='col-lg-4'><h2>" . $value['name'] . "</h2>" : "";
            foreach ($value as $id => $children):
                if ($id != 'name'):
                    $container .= '<div class="checkbox col-lg-12"><input type="checkbox" name="category" value="' . $id . '"/><label for="' . $id . '">' . $children['name'] . '</label>';
                    foreach ($children as $_id => $child):
                        if (is_int($_id)):
                            $container .= '<div class="checkbox col-lg-12"><input type="checkbox" name="category" value="' . $_id . '"/><label for="' . $_id . '">' . $child['name'] . '</label></div>';
                        endif;
                    endforeach;
                    $container .= '</div>';
                endif;
            endforeach;
            $container .= "</div>";
        endforeach;
        $container .= '</div>';
        $container .= '<label for="realated-cat">Show related products from category:</label><select name="realated-cat" class="form-control">';
        foreach ($cats as $index => $value):
            $container .= '<option value="' . $index . '">' . $value . '</option>';
        endforeach;
        $container .= '</select>';
        $container .= '<h2>Tags</h2>';
        $container .= '<div class="col-lg-12 tag-container">';
        foreach (get_terms(array('taxonomy' => 'product_tag', 'hide_empty' => false)) as $tag):
            $container .= '<div class="checkbox col-lg-2"><input type="checkbox" name="tag" value="' . $tag->term_id . '"/><label for="' . $tag->term_id . '">' . $tag->name . '</label></div>';
        endforeach;
        $container .= '</div>';
        $container .= '<div class="col-lg-12"><button type="button" name="new-tag-show" class="btn btn-default">Add New Tag</button>';
        $container .= '<div class="col-lg-12 new-tag-container hidden"><input class="form-control" name="new-tag-name" placeholder="Tag Name" /><button type="button" class="btn btn-success" name="add-new-tag">Add</button>';
        $container .= '</div></div>';
        $container .= '<h2>Prices</h2>';
        $container .= '<label for="set-price-by">Set Price By:</label>';
        $container .= '<select name="set-price-by" class="form-control"><option value="0">Category markup</option><option value="1">Manual markup</option><option selected value="2">Manual Price</option></select>';
        $container .= '<div class="form-group hidden manual-markup"><label for="m_markup">Manual Markup Percentage</label><input class="form-control" name="m_markup" value=""/></div>';
        $container .= '<div class="form-group hidden category-markup-container"></div>';
        $container .= '<div class="form-group"><label for="w_price">Wholesale Price</label><input readonly class="form-control" name="w_price" value="' . round($product["Wholesale Price"], 2) . '"/></div>';
        $container .= '<div class="form-group"><label for="price">Price</label><input class="form-control" name="price" value=""/></div>';
        /*if ($live_products->warehouse_find_similar_products($product['Code'])):
            $container .= '<div class="col-lg-12 tag-container">';
            $container .= '<div class="checkbox col-lg-12"><input type="checkbox" name="import-related-products" value="true"/><label for="import-related-products">Import Related Products</label></div>';
            $container .= '</div>';
        endif;*/
        /*$container .= '<button name="import" class="btn btn-success" type="button">Import</button><button name="cancel" style="margin-left:20px" class="btn btn-danger" type="button">Cancel</button></div>';
        echo $container;
    else:
        echo false;
    endif;
    wp_die();*/
}

function warehouse_get_product_simular_items_callback() {
    foreach (st_get_similar_products_array(htmlentities($_POST['code']), $_POST['brand']) as $_post):
        $_post_id = $_post->ID;
        if ($_post_id != $post_id && !get_post_meta($_post_id, '_is_child', true)):
            $returnString .= '<div class="col-lg-3 sub_parent-container" style="border:3px solid #ccc;padding:10px;">';
            $returnString .= '<input type="hidden" class="parent-input" value="' . $_post_id . '"/>';
            $returnString .= '<p style="text-align: center">' . $_post->post_title . '</p>';
            $returnString .= '<p style="text-align: center">Code: ' . get_post_meta($_post_id, "_code", true) . '</p>';
            $returnString .= '<p style="text-align: center">Size: ' . get_post_meta($_post_id, "st_size", true) . '</p>';
            $returnString .= '<p style="text-align: center">Colour: ' . get_post_meta($_post_id, "st_colour", true) . '</p>';
            $image = st_get_the_post_full_image($_post_id);
            if (strpos($image, '<img') !== false):
                $returnString .= $image;
            else:
                $returnString .= '<img width="150" class="center-block" src=' . st_get_the_post_full_image($_post_id) . ' />';
            endif;
            $returnString .= '<a style="text-align: center" target="_blank" href="' . $_post->guid . '">View</a>';
            $returnString .= '<button type="button" class="btn btn-success hidden copy_parent_info" value="' . $_post_id . '" name="copy_parent_info" >Copy Parent Infomation</button>';
            $returnString .= '</div>';
        endif;
    endforeach;
    echo $returnString;
    wp_die();
}

function warehouse_copy_parent_info_callback() {
    $id = (int) htmlentities($_POST['id']);
    $post = get_post($id);
    $returnArray = array();
    $returnArray['title'] = $post->post_title;
    $returnArray['s_desc'] = $post->post_excerpt;
    $returnArray['l_desc'] = $post->post_content;
    $returnArray['price'] = (float) get_post_meta($id, '_price', true);
    $returnArray['select_price_save'] = get_post_meta($id, '_select_price_save', true);
    $returnArray['related_cats'] = get_post_meta($id, '_select_show_related_products_from_cat', true);
    $returnArray['video'] = get_post_meta($id, 'youtube_link', true);
    $returnArray['seo_title'] = get_post_meta($id, '_yoast_wpseo_title', true);
    $returnArray['seo_desc'] = get_post_meta($id, '_yoast_wpseo_metadesc', true);
    if (get_post_meta($id, '_thumbnail_id', true)):
        $post = get_post(get_post_meta($id, '_thumbnail_id', true));
        $returnArray['image'][$post->ID] = $post->guid;
    else:
        $returnArray['image']['no-id'] = get_post_meta($id, 'main_image_link', true);
    endif;
    $returnArray['gallery'] = array();
    if (get_post_meta($id, '_product_image_gallery', true)):
        foreach (explode(',', get_post_meta($id, '_product_image_gallery', true)) as $_id):
            $post = get_post($_id);
            array_push($returnArray['gallery'][$post->ID], $post->guid);
        endforeach;
    else:
        $returnArray['gallery']['no-id'] = array();
        foreach (explode(' ', get_post_meta($id, 'gallery_image_link', true)) as $link):
            if (isset($link) && !empty($link)):
                array_push($returnArray['gallery']['no-id'], $link);
            endif;
        endforeach;
    endif;
    $returnArray['cats'] = array();
    foreach (get_the_terms($id, 'product_cat') as $cat):
        array_push($returnArray['cats'], $cat->term_id);
    endforeach;
    $returnArray['tags'] = array();
    foreach (get_the_terms($id, 'product_tag') as $tag):
        array_push($returnArray['tags'], $tag->term_id);
    endforeach;
    echo json_encode($returnArray);
    wp_die();
}

function warehouse_download_image_callback() {
    require_once 'warehouse_live_products_class.php';
    $live_products = new warehouse_live_products_class();
    if (!file_exists(WAREHOUSE_TEMP_FOLDER . basename(htmlentities($_POST['link'])))):
        echo $live_products->warehouse_download_feed_image((int) htmlentities($_POST['vendor_id']), htmlentities($_POST['link']));
    else:
        echo WAREHOUSE_TEMP_FOLDER_URL . basename(htmlentities($_POST['link']));
    endif;
    wp_die();
}

function warehouse_enqueue_media_uploader() {
    wp_enqueue_media();
}

add_action("admin_enqueue_scripts", "warehouse_enqueue_media_uploader");

function warehouse_download_images_callback() {
    require_once 'warehouse_download_images.php';
    new warehouse_download_images();
}

function warehouse_import_product_callback() {
    $returnArray = array();
    require_once 'warehouse_live_products_class.php';
    $live_products = new warehouse_live_products_class();
    $post = array('post_status' => "publish", 'post_title' => $_POST['title'], 'post_type' => "product", 'post_content' => $_POST['l_desc'], 'comment_status' => 'closed', 'post_excerpt' => $_POST['s_desc']);
    $post_id = wp_insert_post($post);
    $parent_id = (isset($_POST['parent_id']) && !empty($_POST['parent_id'])) ? (int) $_POST['parent_id'] : false;
    if ($post_id):
        if ($parent_id):
            update_post_meta($parent_id, '_is_parent', true);
            update_post_meta($post_id, '_is_child', true);
            update_post_meta($post_id, '_parent_id', $parent_id);
        endif;
        if (isset($_POST['barcode'])):
            update_post_meta($post_id, '_sku', $_POST['barcode']);
            update_post_meta($post_id, '_barcode', $_POST['barcode']);
        endif;
        if (isset($_POST['brand'])):
            update_post_meta($post_id, '_brand', $_POST['brand']);
        endif;
        if (isset($_POST['w_price'])):
            update_post_meta($post_id, '_wholesale_price', $_POST['w_price']);
        endif;
        if (isset($_POST['code'])):
            update_post_meta($post_id, '_code', $_POST['code']);
        endif;
        if (isset($_POST['price'])):
            update_post_meta($post_id, '_regular_price', number_format((float) $_POST['price']));
            update_post_meta($post_id, '_price', number_format((float) $_POST['price']));
        endif;
        if (isset($_POST['related_cat'])):
            update_post_meta($post_id, '_select_show_related_products_from_cat', $_POST['related_cat']);
        endif;
        if (isset($_POST['video'])):
            update_post_meta($post_id, 'youtube_link', $_POST['video']);
        endif;
        if (isset($_POST['vendor'])):
            update_post_meta($post_id, 'vendor', $_POST['vendor']);
        endif;
        if (!empty($_POST['colour'])):
            update_post_meta($post_id, 'st_colour', $_POST['colour']);
        endif;
        if (!empty($_POST['size'])):
            update_post_meta($post_id, 'st_size', $_POST['size']);
        endif;
        if ($_POST['price_by'] == '1'):
            update_post_meta($post_id, '_price_markup', $_POST['m_markup']);
        endif;
        if (isset($_POST['price_by'])):
            update_post_meta($post_id, '_select_price_save', $_POST['price_by']);
        endif;
        if (isset($_POST['seo_desc'])):
            update_post_meta($post_id, '_yoast_wpseo_metadesc', $_POST['seo_desc']);
        endif;
        if (isset($_POST['seo_title'])):
            update_post_meta($post_id, '_yoast_wpseo_title', $_POST['seo_title']);
        endif;
        $cats = explode(',', $_POST[cats]);
        $catsArray = array();
        if (isset($cats)):            
            foreach ($cats as $cat):
                array_push($catsArray, (int) $cat);
            endforeach;
            wp_set_object_terms($post_id, $catsArray, 'product_cat');
        endif;
        if ($_POST['mainImage']):
            if (strpos($_POST['mainImage'], 'http') !== false || strpos($_POST['mainImage'], 'https') !== false):
                $attchment_id = warehouse_add_image_attchment($_POST['mainImage'], $_POST['vendor']);
                if ($attchment_id):
                    set_post_thumbnail($post_id, $attchment_id);
                endif;
            else:
                set_post_thumbnail($post_id, (int) $_POST['mainImage']);
            endif;
        endif;
        if ($_POST['galleryImages']):
            $galleryArray = array();
            foreach (explode(',', $_POST['galleryImages']) as $image):
                if (strpos($image, 'http') !== false || strpos($image, 'https') !== false):
                    $image = warehouse_add_image_attchment($image);
                endif;
                if ($image):
                    array_push($galleryArray, $image);
                endif;
            endforeach;
            update_post_meta($post_id, '_product_image_gallery', implode(',', $galleryArray));
        endif;
        if (isset($_POST['tags']) && !empty($_POST['tags'])):
            wp_set_post_terms($post_id, $_POST['tags'], 'product_tag');
        endif;

        $live_products->warehouse_set_live_product_import($_POST['db_id']);
        if (isset($_POST['import_related_products']) && !empty($_POST['import_related_products'])):
            $returnArray = $live_products->warehouse_import_related_products(array(
                'attachment_id' => (!empty($attchment_id)) ? $attchment_id : (int) $_POST['mainImage'],
                'code' => (isset($_POST['code']) && !empty($_POST['code'])) ? $_POST['code'] : false,
                'title' => (isset($_POST['title']) && !empty($_POST['title'])) ? $_POST['title'] : false,
                'sdescription' => (isset($_POST['s_desc']) && !empty($_POST['s_desc'])) ? $_POST['s_desc'] : false,
                'ldescription' => (isset($_POST['l_desc']) && !empty($_POST['l_desc'])) ? $_POST['l_desc'] : false,
                'parent_id' => ($parent_id) ? $parent_id : $post_id,
                'brand' => (isset($_POST['brand']) && !empty($_POST['brand'])) ? $_POST['brand'] : false,
                'price' => (isset($_POST['price']) && !empty($_POST['price'])) ? $_POST['price'] : false,
                'related_cat' => (isset($_POST['related_cat']) && !empty($_POST['related_cat'])) ? $_POST['related_cat'] : false,
                'video' => (isset($_POST['video']) && !empty($_POST['video'])) ? $_POST['video'] : false,
                'vendor' => (isset($_POST['vendor']) && !empty($_POST['vendor'])) ? $_POST['vendor'] : false,
                'price_markup' => (isset($_POST['m_markup']) && !empty($_POST['m_markup'])) ? $_POST['m_markup'] : false,
                'select_price_save' => (isset($_POST['price_by']) && !empty($_POST['price_by'])) ? $_POST['price_by'] : false,
                '_yoast_wpseo_metadesc' => (isset($_POST['seo_desc']) && !empty($_POST['seo_desc'])) ? $_POST['seo_desc'] : false,
                '_yoast_wpseo_title' => (isset($_POST['seo_title']) && !empty($_POST['seo_title'])) ? $_POST['seo_title'] : false,
                'cats_array' => (isset($catsArray) && !empty($catsArray)) ? $catsArray : false,
                'gallery_images' => (isset($galleryArray) && !empty($galleryArray)) ? implode(',', $galleryArray) : false,
                'tags' => (isset($_POST['tags']) && !empty($_POST['tags'])) ? $_POST['tags'] : false,
                'db_id' => (isset($_POST['db_id']) && !empty($_POST['db_id'])) ? $_POST['db_id'] : false
            ));
        endif;

        // Used for WooCommerce and Filtering        
        updateWooCommerceAttributes($post_id, $catsArray, $_POST['colour'], $_POST['size']);
        //-----------------------------------

        $returnArray[$_POST['db_id']] = $_POST['db_id'];
        echo json_encode($returnArray);
    else:
        echo false;
    endif;
    wp_die();
}


function updateWooCommerceAttributes($post_id, $cats, $colour, $size) {

    $toy_size = false;
    $toys_colour = false;
    $shoe_size = false;
    $shoe_heel_height = false;
    $bra_size = false;
    $lingerie_colour = false;
    $apparel_size = false;
    $accessories_colour = false;
    $fetish_colour = false;

    foreach ($cats as $cat):       

        if(in_array($cat, array(19, 38, 34, 36, 25, 100, 105, 106, 108, 1078, 324, 121, 109, 107, 230))) {
            $toy_size = true;
            $toys_colour = true;
        }    
        if(in_array($cat, array(22, 271, 1021, 1030, 1052))) {  	
            $shoe_size = true;
            $shoe_heel_height = true;      
        }    
        if(in_array( $cat, array(22, 63, 1018, 172, 1015))) {
            $bra_size = true;
        }    
        if(in_array( $cat, array(22, 66, 119, 1044, 63, 172, 1015, 1055, 1027, 1043))) {
            $lingerie_colour = true;
        }    
        if(in_array( $cat, array(22, 66, 119, 118, 63, 1015, 1032, 1027, 1043, 172, 1055))) {
            $apparel_size = true;
        }    
        if(in_array( $cat, array(22, 1044, 120))) {
            $accessories_colour = true;
        }    
        if(in_array( $cat, array(20, 39, 321, 41, 40, 1058, 1061))) {
            $fetish_colour = true;
        }

    endforeach;

    global $wpdb;

    $sql = 'INSERT INTO `wp_postmeta`(`post_id`, `meta_key`, `meta_value`) VALUES (
            ' . $post_id . ', "_product_attributes", \'a:9:{s:11:"pa_toy-size";a:6:{s:4:"name";s:11:"pa_toy-size";s:5:"value";s:0:"";s:8:"position";s:1:"0";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}s:14:"pa_toys-colour";a:6:{s:4:"name";s:14:"pa_toys-colour";s:5:"value";s:0:"";s:8:"position";s:1:"1";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}s:12:"pa_shoe-size";a:6:{s:4:"name";s:12:"pa_shoe-size";s:5:"value";s:0:"";s:8:"position";s:1:"2";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}s:19:"pa_shoe-heel-height";a:6:{s:4:"name";s:19:"pa_shoe-heel-height";s:5:"value";s:0:"";s:8:"position";s:1:"3";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}s:11:"pa_bra-size";a:6:{s:4:"name";s:11:"pa_bra-size";s:5:"value";s:0:"";s:8:"position";s:1:"4";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}s:18:"pa_lingerie-colour";a:6:{s:4:"name";s:18:"pa_lingerie-colour";s:5:"value";s:0:"";s:8:"position";s:1:"5";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}s:15:"pa_apparel-size";a:6:{s:4:"name";s:15:"pa_apparel-size";s:5:"value";s:0:"";s:8:"position";s:1:"6";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}s:21:"pa_accessories-colour";a:6:{s:4:"name";s:21:"pa_accessories-colour";s:5:"value";s:0:"";s:8:"position";s:1:"7";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}s:16:"pa_fetish-colour";a:6:{s:4:"name";s:16:"pa_fetish-colour";s:5:"value";s:0:"";s:8:"position";s:1:"8";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}}\'
            )';

    $wpdb->query($sql);            

    if($toy_size == true) {
    
        $sql = 'SELECT
                    wp_posts.id AS object_id,
                    wp_terms1.term_id AS term_taxonomy_id,
                    0 AS term_order
                FROM wp_posts
                INNER JOIN wp_postmeta wp_postmeta1 -- LEFT JOIN wp_postmeta wp_postmeta1
                    ON wp_postmeta1.post_id = wp_posts.ID
                    AND wp_postmeta1.meta_key = "st_size"	
                INNER JOIN wp_terms wp_terms1
                    ON REPLACE(wp_postmeta1.meta_value, "\'\'", "\"") = wp_terms1.name	
                    AND wp_terms1.term_id IN (SELECT term_id FROM `wp_term_taxonomy` WHERE `taxonomy` = "pa_toy-size")
                LEFT JOIN wp_postmeta wp_postmeta2
                    ON wp_postmeta2.post_id = wp_posts.ID
                    AND wp_postmeta2.meta_key = "st_colour"
                INNER JOIN wp_term_relationships
                    ON wp_term_relationships.object_id = wp_posts.ID AND wp_term_relationships.term_taxonomy_id IN (19, 38, 34, 36, 25, 100, 105, 106, 108, 1078, 324, 121, 109, 107, 230) -- Category
                LEFT JOIN wp_term_taxonomy
                    ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                    AND wp_term_taxonomy.taxonomy = "product_cat"
                LEFT JOIN wp_terms wp_terms2
                    ON wp_term_taxonomy.term_id = wp_terms2.term_id
                WHERE wp_posts.post_type = "product"
                AND wp_posts.post_status = "publish"
                AND wp_posts.id = ' . $post_id . '
                GROUP BY wp_posts.id';

        $filters = $wpdb->get_results($sql);
                    
        foreach ( $filters as $filter ) {	
            $sql = 'INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order)
                    VALUES (' . $filter->object_id . ', ' . $filter->term_taxonomy_id . ', ' . $filter->term_order . ' )';
            $wpdb->query($sql);
        }
    }
    if($toys_colour == true) {     
            
        $sql = 'SELECT
                    wp_posts.id AS object_id,
                    wp_terms1.term_id AS term_taxonomy_id,
                    0 AS term_order
                FROM wp_posts
                LEFT JOIN wp_postmeta wp_postmeta1
                    ON wp_postmeta1.post_id = wp_posts.ID
                    AND wp_postmeta1.meta_key = "st_size"
                INNER JOIN wp_postmeta wp_postmeta2 -- LEFT JOIN wp_postmeta wp_postmeta2
                    ON wp_postmeta2.post_id = wp_posts.ID
                    AND wp_postmeta2.meta_key = "st_colour"
                INNER JOIN wp_terms wp_terms1
                    ON wp_postmeta2.meta_value = wp_terms1.name		
                    AND wp_terms1.term_id IN (SELECT term_id FROM `wp_term_taxonomy` WHERE `taxonomy` = "pa_toys-colour")
                INNER JOIN wp_term_relationships
                    ON wp_term_relationships.object_id = wp_posts.ID AND wp_term_relationships.term_taxonomy_id IN (19, 38, 34, 36, 25, 100, 105, 106, 108, 1078, 324, 121, 109, 107, 230) -- Category
                LEFT JOIN wp_term_taxonomy
                    ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                    AND wp_term_taxonomy.taxonomy = "product_cat"
                LEFT JOIN wp_terms wp_terms2
                    ON wp_term_taxonomy.term_id = wp_terms2.term_id
                WHERE wp_posts.post_type = "product"
                AND wp_posts.post_status = "publish"
                AND wp_posts.id = ' . $post_id . '
                GROUP BY wp_posts.id';

        $filters = $wpdb->get_results($sql);
                    
        foreach ( $filters as $filter ) {	
            $sql = 'INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order)
                    VALUES (' . $filter->object_id . ', ' . $filter->term_taxonomy_id . ', ' . $filter->term_order . ' )';
            $wpdb->query($sql);
        }
    }

    if($shoe_size == true) { 
        
        $sql = 'SELECT
                    wp_posts.id AS object_id,
                    wp_terms1.term_id AS term_taxonomy_id,
                    0 AS term_order
                FROM wp_posts
                INNER JOIN wp_terms wp_terms1
                    ON wp_terms1.name = "' .  '"
                    AND wp_terms1.term_id IN (SELECT term_id FROM `wp_term_taxonomy` WHERE `taxonomy` = "pa_shoe-size")
                LEFT JOIN wp_postmeta wp_postmeta2
                    ON wp_postmeta2.post_id = wp_posts.ID
                    AND wp_postmeta2.meta_key = "st_colour"
                INNER JOIN wp_term_relationships
                    ON wp_term_relationships.object_id = wp_posts.ID AND wp_term_relationships.term_taxonomy_id IN (22, 271, 1021, 1030, 1052) -- Category
                LEFT JOIN wp_term_taxonomy
                    ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                    AND wp_term_taxonomy.taxonomy = "product_cat"
                LEFT JOIN wp_terms wp_terms2
                    ON wp_term_taxonomy.term_id = wp_terms2.term_id
                WHERE wp_posts.post_type = "product"
                AND wp_posts.post_status = "publish"
                AND wp_posts.id = ' . $post_id . '
                GROUP BY wp_posts.id';

        $filters = $wpdb->get_results($sql);
                    
        foreach ( $filters as $filter ) {	
            $sql = 'INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order)
                    VALUES (' . $filter->object_id . ', ' . $filter->term_taxonomy_id . ', ' . $filter->term_order . ' )';
            $wpdb->query($sql);
        }
    }
    if($shoe_heel_height == true) {                   
        //$sql = '';        
        //$results = $wpdb->get_results($sql);
    }
    if($bra_size == true) { 
    
        $sql = 'SELECT
                    wp_posts.id AS object_id,
                    wp_terms1.term_id AS term_taxonomy_id,
                    0 AS term_order
                FROM wp_posts
                INNER JOIN wp_postmeta wp_postmeta1 -- LEFT JOIN wp_postmeta wp_postmeta1
                    ON wp_postmeta1.post_id = wp_posts.ID
                    AND wp_postmeta1.meta_key = "st_size"	
                INNER JOIN wp_terms wp_terms1
                    ON wp_postmeta1.meta_value = wp_terms1.name	
                    AND wp_terms1.term_id IN (SELECT term_id FROM `wp_term_taxonomy` WHERE `taxonomy` = "pa_bra-size")
                LEFT JOIN wp_postmeta wp_postmeta2
                    ON wp_postmeta2.post_id = wp_posts.ID
                    AND wp_postmeta2.meta_key = "st_colour"
                INNER JOIN wp_term_relationships
                    ON wp_term_relationships.object_id = wp_posts.ID AND wp_term_relationships.term_taxonomy_id IN (22, 63, 1018, 172, 1015) -- Category
                LEFT JOIN wp_term_taxonomy
                    ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                    AND wp_term_taxonomy.taxonomy = "product_cat"
                LEFT JOIN wp_terms wp_terms2
                    ON wp_term_taxonomy.term_id = wp_terms2.term_id
                WHERE wp_posts.post_type = "product"
                AND wp_posts.post_status = "publish"
                AND wp_posts.id = ' . $post_id . '
                GROUP BY wp_posts.id';

        $filters = $wpdb->get_results($sql);
                    
        foreach ( $filters as $filter ) {	
            $sql = 'INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order)
                    VALUES (' . $filter->object_id . ', ' . $filter->term_taxonomy_id . ', ' . $filter->term_order . ' )';
            $wpdb->query($sql);
        }
    }
    if($lingerie_colour == true) { 
    
        $sql = 'SELECT
                    wp_posts.id AS object_id,
                    wp_terms1.term_id AS term_taxonomy_id,
                    0 AS term_order
                FROM wp_posts
                LEFT JOIN wp_postmeta wp_postmeta1
                    ON wp_postmeta1.post_id = wp_posts.ID
                    AND wp_postmeta1.meta_key = "st_size"
                INNER JOIN wp_postmeta wp_postmeta2 -- LEFT JOIN wp_postmeta wp_postmeta2
                    ON wp_postmeta2.post_id = wp_posts.ID
                    AND wp_postmeta2.meta_key = "st_colour"
                INNER JOIN wp_terms wp_terms1
                    ON wp_postmeta2.meta_value = wp_terms1.name		
                    AND wp_terms1.term_id IN (SELECT term_id FROM `wp_term_taxonomy` WHERE `taxonomy` = "pa_lingerie-colour")
                INNER JOIN wp_term_relationships
                    ON wp_term_relationships.object_id = wp_posts.ID AND wp_term_relationships.term_taxonomy_id IN (22, 66, 119, 1044, 63, 172, 1015, 1055, 1027, 1043) -- Category
                LEFT JOIN wp_term_taxonomy
                    ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                    AND wp_term_taxonomy.taxonomy = "product_cat"
                LEFT JOIN wp_terms wp_terms2
                    ON wp_term_taxonomy.term_id = wp_terms2.term_id
                WHERE wp_posts.post_type = "product"
                AND wp_posts.post_status = "publish"
                AND wp_posts.id = ' . $post_id . '
                GROUP BY wp_posts.id';

        $filters = $wpdb->get_results($sql);
                    
        foreach ( $filters as $filter ) {	
            $sql = 'INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order)
                    VALUES (' . $filter->object_id . ', ' . $filter->term_taxonomy_id . ', ' . $filter->term_order . ' )';
            $wpdb->query($sql);
        }
    }
    if($apparel_size == true) { 

        $sql = 'SELECT
                    wp_posts.id AS object_id,
                    wp_terms1.term_id AS term_taxonomy_id,
                    0 AS term_order
                FROM wp_posts
                INNER JOIN wp_postmeta wp_postmeta1 -- LEFT JOIN wp_postmeta wp_postmeta1
                    ON wp_postmeta1.post_id = wp_posts.ID
                    AND wp_postmeta1.meta_key = "st_size"	
                INNER JOIN wp_terms wp_terms1
                    ON wp_postmeta1.meta_value = wp_terms1.name	
                    AND wp_terms1.term_id IN (SELECT term_id FROM `wp_term_taxonomy` WHERE `taxonomy` = "pa_apparel-size")
                LEFT JOIN wp_postmeta wp_postmeta2
                    ON wp_postmeta2.post_id = wp_posts.ID
                    AND wp_postmeta2.meta_key = "st_colour"
                INNER JOIN wp_term_relationships
                    ON wp_term_relationships.object_id = wp_posts.ID AND wp_term_relationships.term_taxonomy_id IN (22, 66, 119, 118, 63, 1015, 1032, 1027, 1043, 172, 1055) -- Category
                LEFT JOIN wp_term_taxonomy
                    ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                    AND wp_term_taxonomy.taxonomy = "product_cat"
                LEFT JOIN wp_terms wp_terms2
                    ON wp_term_taxonomy.term_id = wp_terms2.term_id
                WHERE wp_posts.post_type = "product"
                AND wp_posts.post_status = "publish"
                AND wp_posts.id = ' . $post_id . '
                GROUP BY wp_posts.id';

        $filters = $wpdb->get_results($sql);
                    
        foreach ( $filters as $filter ) {	
            $sql = 'INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order)
                    VALUES (' . $filter->object_id . ', ' . $filter->term_taxonomy_id . ', ' . $filter->term_order . ' )';
            $wpdb->query($sql);
        }
    }
    if($accessories_colour == true) { 
            
        $sql = 'SELECT
                    wp_posts.id AS object_id,
                    wp_terms1.term_id AS term_taxonomy_id,
                    0 AS term_order
                FROM wp_posts
                LEFT JOIN wp_postmeta wp_postmeta1
                    ON wp_postmeta1.post_id = wp_posts.ID
                    AND wp_postmeta1.meta_key = "st_size"
                INNER JOIN wp_postmeta wp_postmeta2 -- LEFT JOIN wp_postmeta wp_postmeta2
                    ON wp_postmeta2.post_id = wp_posts.ID
                    AND wp_postmeta2.meta_key = "st_colour"
                INNER JOIN wp_terms wp_terms1
                    ON wp_postmeta2.meta_value = wp_terms1.name		
                    AND wp_terms1.term_id IN (SELECT term_id FROM `wp_term_taxonomy` WHERE `taxonomy` = "pa_accessories-colour")
                INNER JOIN wp_term_relationships
                    ON wp_term_relationships.object_id = wp_posts.ID AND wp_term_relationships.term_taxonomy_id IN (22, 1044, 120) -- Category
                LEFT JOIN wp_term_taxonomy
                    ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                    AND wp_term_taxonomy.taxonomy = "product_cat"
                LEFT JOIN wp_terms wp_terms2
                    ON wp_term_taxonomy.term_id = wp_terms2.term_id
                WHERE wp_posts.post_type = "product"
                AND wp_posts.post_status = "publish"
                AND wp_posts.id = ' . $post_id . '
                GROUP BY wp_posts.id';

        $filters = $wpdb->get_results($sql);
                    
        foreach ( $filters as $filter ) {	
            $sql = 'INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order)
                    VALUES (' . $filter->object_id . ', ' . $filter->term_taxonomy_id . ', ' . $filter->term_order . ' )';
            $wpdb->query($sql);
        }
        
    }
    if($fetish_colour == true) { 
        
        $sql = 'SELECT
                    wp_posts.id AS object_id,
                    wp_terms1.term_id AS term_taxonomy_id,
                    0 AS term_order
                FROM wp_posts
                LEFT JOIN wp_postmeta wp_postmeta1
                    ON wp_postmeta1.post_id = wp_posts.ID
                    AND wp_postmeta1.meta_key = "st_size"
                INNER JOIN wp_postmeta wp_postmeta2 -- LEFT JOIN wp_postmeta wp_postmeta2
                    ON wp_postmeta2.post_id = wp_posts.ID
                    AND wp_postmeta2.meta_key = "st_colour"
                INNER JOIN wp_terms wp_terms1
                    ON wp_postmeta2.meta_value = wp_terms1.name		
                    AND wp_terms1.term_id IN (SELECT term_id FROM `wp_term_taxonomy` WHERE `taxonomy` = "pa_fetish-colour")
                INNER JOIN wp_term_relationships
                    ON wp_term_relationships.object_id = wp_posts.ID AND wp_term_relationships.term_taxonomy_id IN (20, 39, 321, 41, 40, 1058, 1061) -- Category
                LEFT JOIN wp_term_taxonomy
                    ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                    AND wp_term_taxonomy.taxonomy = "product_cat"
                LEFT JOIN wp_terms wp_terms2
                    ON wp_term_taxonomy.term_id = wp_terms2.term_id
                WHERE wp_posts.post_type = "product"
                AND wp_posts.post_status = "publish"
                AND wp_posts.id = ' . $post_id . '
                GROUP BY wp_posts.id';

        $filters = $wpdb->get_results($sql);
                    
        foreach ( $filters as $filter ) {	
            $sql = 'INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order)
                    VALUES (' . $filter->object_id . ', ' . $filter->term_taxonomy_id . ', ' . $filter->term_order . ' )';
            $wpdb->query($sql);
        }
    }
}

function warehouse_add_image_attchment($link, $vendor_name) {
    ini_set('memory_limit', '512M');
    $wp_upload_dir = wp_upload_dir();
    $tempPath = $wp_upload_dir['path'] . '/temp/' . basename($link);

    if (!file_exists($tempPath)):
        require_once 'vendors_classes.php';
        $tempPath = WAREHOUSE_TEMP_FOLDER . vendor_functions::getVendorIdFromName($vendor_name) . '/' . basename($link);
    endif;

    if (!file_exists($tempPath)):
        require_once 'warehouse_live_products_class.php';
        $live_products = new warehouse_live_products_class();
        $tempPath = $live_products->warehouse_get_image_from_link($link, WAREHOUSE_TEMP_FOLDER . vendor_functions::getVendorIdFromName($vendor_name) . '/' . basename($link));
    endif;

    $filename = $wp_upload_dir['path'] . '/' . basename($link);
    warehouse_move_and_resize_image($tempPath, $filename);

    $filetype = wp_check_filetype(basename($filename), null);

    $attachment = array(
        'guid' => $wp_upload_dir['url'] . '/' . basename($filename),
        'post_mime_type' => $filetype['type'],
        'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
        'post_content' => '',
        'post_status' => 'inherit',
        'comment_status' => 'closed'
    );

    $attach_id = wp_insert_attachment($attachment, $filename);

    require_once( ABSPATH . 'wp-admin/includes/image.php' );

    $attach_data = wp_generate_attachment_metadata($attach_id, $filename);

    wp_update_attachment_metadata($attach_id, $attach_data);

    return $attach_id;
}

function warehouse_move_and_resize_image($originalFile, $saveto) {
    ini_set('memory_limit', '512M');
    require_once 'plugins/php-image-resize/lib/ImageResize.php';
    $newWidth = 700;
    $newHeight = 700;
    $image = new \Eventviva\ImageResize($originalFile);

    $image->resizeToBestFit($newHeight, $newWidth, true);

    $image->save($saveto);
}

function warehouse_get_cat_markup_callback() {
    $returnString = '';
    $cats = explode(',', $_POST['cats']);
    foreach ($cats as $cat):
        $termName = get_term($cat);
        $termName = $termName->name;
        $markup = (int) st_get_category_markup($cat);
        $returnString .= '<option value="' . $markup . '">' . $termName . ' - ' . $markup . '%</option>';
    endforeach;
    echo $returnString;
    wp_die();
}

function warehouse_add_new_tag_callback() {
    echo json_encode(wp_insert_term(htmlentities($_POST['name']), 'product_tag'));
    wp_die();
}

function warehouse_delete_note_callback() {
    require_once 'warehouse_db_class.php';
    $feed_db = new warehouse_database_class();
    $query = "DELETE FROM `notifications` WHERE `notifications`.`id` = " . $_POST['note_id'];
    $result = $feed_db->warehouse_db_do_query($query);
    if ($result):
        $warnings = $feed_db->warehouse_db_do_query("SELECT * FROM `notifications`");
        echo $warnings->num_rows;
    else:
        echo $result;
    endif;
    wp_die();
}

function warehouse_test_feed_callback() {
    require_once 'warehouse_feed_class.php';
    $feed_class = new warehouse_feed_class();
    echo $feed_class->warehouse_test_feed($_POST['vendor_id']);
    wp_die();
}

function warehouse_round_prices_callback() {
    require_once 'warehouse_maintenance_class.php';
    $maintenanceClass = new warehouse_maintenance();
    echo $maintenanceClass->warehouse_round_prices();
    wp_die();
}

function warehouse_save_featured_array_callback() {
    require_once 'warehouse_product_edit_extra_functions.php';
    echo warehouse_product_edit_extra_functions::save_featured_products($_POST['post_id'], $_POST['gridArray']);
    wp_die();
}

function warehouse_download_missing_image_callback() {
    require_once 'warehouse_db_class.php';
    $feed_db_class = new warehouse_database_class();
    require_once 'warehouse_live_products_class.php';
    $live_products = new warehouse_live_products_class();
    $result = $feed_db_class->warehouse_db_do_query("SELECT * FROM feed_products WHERE id = " . $_POST['product_id']);
    $result = mysqli_fetch_assoc($result);
    $result = $live_products->warehouse_download_feed_image($result['feed_id'], $result['Image']);
    if (isset($result) && !empty($result)):
        $result = explode('/', $result);
        $result = '/wp-content/plugins/warehouse/temp/' . $result[count($result) - 2] . '/' . $result[count($result) - 1];
    endif;
    echo $result;
    wp_die();
}
