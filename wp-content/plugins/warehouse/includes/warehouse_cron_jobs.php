<?php

class warehouse_cron_jobs {

    protected static $_instance = null;

	public static function instance()
	{
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self;
		}

		return self::$_instance;
	}

    public function __construct() {
        $this->warehouse_update_feed_products();
        $this->warehouse_crons_download_images();
        //$this->warehouse_set_woocommerce_product_filters();
    }

    public function warehouse_update_feed_products() {

        set_time_limit(0);

        require_once 'vendors_classes.php';
        $vendor_functions = new vendor_functions();
        require_once 'warehouse_feed_class.php';
        $feed_functions = new warehouse_feed_class();
        require_once 'warehouse_live_products_class.php';

        $live_products = new warehouse_live_products_class();
        $current_vendors = $vendor_functions->vendor_get_vendors();
        
        $html = '<style>h2, h3, p {font-family: sans-serif;}</style>';        
        $html .= '<p>MMJ website has just UPDATED automatically some product FEEDS.</p><hr>';

        foreach ($current_vendors as $vendor):

            $html .= '<div><h2><strong>Vendor: ' . $vendor_functions->vendor_get_vendor_name($vendor->ID) . '</strong></h2>';

            if ((boolean) get_post_meta($vendor->ID, '_update_auto', true)):

                $result = $feed_functions->warehouse_download_products($vendor->ID);

                if ($result):                  
                    if (isset($result['success'])):
                        $html .= '<p>Products found in Feeds: ' . $result['success'] . '</p>';
                        if (isset($result['failure'])):
                            $html .= '<p>Products not found in Feeds: ' . $result['failure'] . '</p>';
                        endif;
                    endif;
                    if (isset($result['error'])):
                        $html .= '<p>Error: ' . $result['error'] . '</p>';                    
                    endif;
                    $live_products->warehouse_live_get_all_products($vendor->ID);
                endif;

            endif;

            $html .= '</div><br>';     

        endforeach;       

        require_once 'warehouse_db_class.php';
        $warehouse_db = new warehouse_database_class();
        $notes = $warehouse_db->warehouse_db_do_query("SELECT * FROM `notifications`");
        $notes_count = $notes->num_rows;

        if($notes_count) {
            $html .= '<hr><div>'; 
            $html .= '<h2><strong>Update Summary:</strong></h2>';

            $notes = $warehouse_db->warehouse_db_do_query("SELECT COUNT(id) AS total FROM `notifications` WHERE note like '%into stock%'");

            if ($notes->num_rows == 1){
                $notes = mysqli_fetch_assoc($notes);                
                $html .= '<p>Products moved to IN STOCK: ' . $notes['total'] . '</p>';
            }

            $notes = $warehouse_db->warehouse_db_do_query("SELECT COUNT(id) AS total FROM `notifications` WHERE note like '%out of stock%'");

            if ($notes->num_rows == 1){
                $notes = mysqli_fetch_assoc($notes);                
                $html .= '<p>Products moved to OUT OF STOCK: ' . $notes['total'] . '</p>';
            }

            $notes = $warehouse_db->warehouse_db_do_query("SELECT COUNT(id) AS total FROM `notifications` WHERE note like '%price has increased%'");

            if ($notes->num_rows == 1){
                $notes = mysqli_fetch_assoc($notes);                
                $html .= '<p>Products that wholesale PRICE has INCREASED: ' . $notes['total'] . '</p>';
            }

            $notes = $warehouse_db->warehouse_db_do_query("SELECT COUNT(id) AS total FROM `notifications` WHERE note like '%price has decreased%'");

            if ($notes->num_rows == 1){
                $notes = mysqli_fetch_assoc($notes);                
                $html .= '<p>Products that wholesale PRICE has DECREASED: ' . $notes['total'] . '</p>';
            }

            $html .= '<br><h3><strong>Total of notes: ' . $notes_count . '</strong></h3>';  
            $html .= "<hr><p>Please review notes in MMJ's Warehouse area at your earliest convenience.</p>";          
            $html .= '</div>'; 
        }
        
        $email_address = $warehouse_db->get_notification_email_list();
        if($email_address) {
            wp_mail($email_address, 'MMJ - Daily Feed Update', $html, 'From: MMJ <info@meandmrsjones.com.au>' . "\r\n");
        }
    }

    private function warehouse_crons_download_images() {
        require_once 'includes/warehouse_download_images.php';
        new warehouse_download_images();
    }

    public function warehouse_set_woocommerce_product_filters() {        

        try {
            global $wpdb;

            $html = '<style>h2, h3, p {font-family: sans-serif;}</style>';        
            $html .= '<p>MMJ website has just SET automatically some woocommerce filters for the following products:</p><hr>';
            
            $products = $wpdb->get_results( "SELECT p.ID, p.post_title, post_date, GROUP_CONCAT(t.term_id) AS categories,
                                            (SELECT pm1.meta_value FROM wp_postmeta AS pm1 WHERE pm1.post_id = p.ID AND meta_key = 'st_size') AS size,
                                            (SELECT pm2.meta_value FROM wp_postmeta AS pm2 WHERE pm2.post_id = p.ID AND meta_key = 'st_colour') AS colour
                                            FROM wp_posts AS p
                                            LEFT JOIN wp_term_relationships rel ON rel.object_id = p.ID
                                            LEFT JOIN wp_term_taxonomy tax ON tax.term_taxonomy_id = rel.term_taxonomy_id AND tax.taxonomy = 'product_cat'
                                            LEFT JOIN wp_terms t ON t.term_id = tax.term_id
                                            WHERE p.post_type = 'product' AND p.post_status = 'publish' AND  post_date > DATE_SUB(CURDATE(), INTERVAL 1 DAY)");       
            
            $total = $wpdb->num_rows;

            foreach ( $products as $product ) {		    

                $this->update_woocommerce_product_filters($product->ID, explode (",", $product->categories), $product->colour, $product->size); 

                $html .= '<h3>Product: ' . $product->post_title . '</h3>';
                $html .= '<p>Size: ' . $product->size . '</p>';
                $html .= '<p>Colour: ' . $product->colour . '</p>';
                $html .= '<br>';
            }	  

            if($total > 0) {
                $html .= '<br><h3><strong>Total of products: ' . $total . '</strong></h3>';  
                wp_mail('boni@chillidee.com.au', 'MMJ - Woocommerce Filters', $html, 'From: MMJ <info@meandmrsjones.com.au>' . "\r\n");
            }

        } catch (Exception $e) {
            wp_mail('boni@chillidee.com.au', 'MMJ - ERROR Woocommerce Filters',
                     $e->getMessage() . ' - ' . $e->getTraceAsString(), 'From: MMJ <info@meandmrsjones.com.au>' . "\r\n");
        }
    }

    public function update_woocommerce_product_filters($post_id, $cats, $colour, $size) {

        $toy_size = false;
        $toys_colour = false;
        $shoe_size = false;
        $shoe_heel_height = false;
        $bra_size = false;
        $lingerie_colour = false;
        $apparel_size = false;
        $accessories_colour = false;
        $fetish_colour = false;
    
        foreach ($cats as $cat):       
    
            if(in_array($cat, array(19, 38, 34, 36, 25, 100, 105, 106, 108, 1078, 324, 121, 109, 107, 230))) {
                $toy_size = true;
                $toys_colour = true;
            }    
            if(in_array($cat, array(22, 271, 1021, 1030, 1052))) {  	
                $shoe_size = true;
                $shoe_heel_height = true;      
            }    
            if(in_array( $cat, array(22, 63, 1018, 172, 1015))) {
                $bra_size = true;
            }    
            if(in_array( $cat, array(22, 66, 119, 1044, 63, 172, 1015, 1055, 1027, 1043))) {
                $lingerie_colour = true;
            }    
            if(in_array( $cat, array(22, 66, 119, 118, 63, 1015, 1032, 1027, 1043, 172, 1055))) {
                $apparel_size = true;
            }    
            if(in_array( $cat, array(22, 1044, 120))) {
                $accessories_colour = true;
            }    
            if(in_array( $cat, array(20, 39, 321, 41, 40, 1058, 1061))) {
                $fetish_colour = true;
            }
    
        endforeach;
  
        global $wpdb;
    
        $sql = 'INSERT INTO `wp_postmeta`(`post_id`, `meta_key`, `meta_value`) VALUES (
               ' . $post_id . ', "_product_attributes", \'a:9:{s:11:"pa_toy-size";a:6:{s:4:"name";s:11:"pa_toy-size";s:5:"value";s:0:"";s:8:"position";s:1:"0";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}s:14:"pa_toys-colour";a:6:{s:4:"name";s:14:"pa_toys-colour";s:5:"value";s:0:"";s:8:"position";s:1:"1";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}s:12:"pa_shoe-size";a:6:{s:4:"name";s:12:"pa_shoe-size";s:5:"value";s:0:"";s:8:"position";s:1:"2";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}s:19:"pa_shoe-heel-height";a:6:{s:4:"name";s:19:"pa_shoe-heel-height";s:5:"value";s:0:"";s:8:"position";s:1:"3";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}s:11:"pa_bra-size";a:6:{s:4:"name";s:11:"pa_bra-size";s:5:"value";s:0:"";s:8:"position";s:1:"4";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}s:18:"pa_lingerie-colour";a:6:{s:4:"name";s:18:"pa_lingerie-colour";s:5:"value";s:0:"";s:8:"position";s:1:"5";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}s:15:"pa_apparel-size";a:6:{s:4:"name";s:15:"pa_apparel-size";s:5:"value";s:0:"";s:8:"position";s:1:"6";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}s:21:"pa_accessories-colour";a:6:{s:4:"name";s:21:"pa_accessories-colour";s:5:"value";s:0:"";s:8:"position";s:1:"7";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}s:16:"pa_fetish-colour";a:6:{s:4:"name";s:16:"pa_fetish-colour";s:5:"value";s:0:"";s:8:"position";s:1:"8";s:10:"is_visible";s:1:"1";s:12:"is_variation";s:1:"0";s:11:"is_taxonomy";s:1:"1";}}\'
               )';
    
        $wpdb->query($sql);            
    
        if($toy_size == true) {
        
            $sql = 'SELECT
                        wp_posts.id AS object_id,
                        wp_terms1.term_id AS term_taxonomy_id,
                        0 AS term_order
                    FROM wp_posts
                    INNER JOIN wp_postmeta wp_postmeta1 -- LEFT JOIN wp_postmeta wp_postmeta1
                        ON wp_postmeta1.post_id = wp_posts.ID
                        AND wp_postmeta1.meta_key = "st_size"	
                    INNER JOIN wp_terms wp_terms1
                        ON REPLACE(wp_postmeta1.meta_value, "\'\'", "\"") = wp_terms1.name	
                        AND wp_terms1.term_id IN (SELECT term_id FROM `wp_term_taxonomy` WHERE `taxonomy` = "pa_toy-size")
                    LEFT JOIN wp_postmeta wp_postmeta2
                        ON wp_postmeta2.post_id = wp_posts.ID
                        AND wp_postmeta2.meta_key = "st_colour"
                    INNER JOIN wp_term_relationships
                        ON wp_term_relationships.object_id = wp_posts.ID AND wp_term_relationships.term_taxonomy_id IN (19, 38, 34, 36, 25, 100, 105, 106, 108, 1078, 324, 121, 109, 107, 230) -- Category
                    LEFT JOIN wp_term_taxonomy
                        ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                        AND wp_term_taxonomy.taxonomy = "product_cat"
                    LEFT JOIN wp_terms wp_terms2
                        ON wp_term_taxonomy.term_id = wp_terms2.term_id
                    WHERE wp_posts.post_type = "product"
                    AND wp_posts.post_status = "publish"
                    AND wp_posts.id = ' . $post_id . '
                    GROUP BY wp_posts.id';

            $filters = $wpdb->get_results($sql);
                        
            foreach ( $filters as $filter ) {	
                $sql = 'INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order)
                        VALUES (' . $filter->object_id . ', ' . $filter->term_taxonomy_id . ', ' . $filter->term_order . ' )';
                $wpdb->query($sql);
            }
        }
        if($toys_colour == true) {     
               
            $sql = 'SELECT
                        wp_posts.id AS object_id,
                        wp_terms1.term_id AS term_taxonomy_id,
                        0 AS term_order
                    FROM wp_posts
                    LEFT JOIN wp_postmeta wp_postmeta1
                        ON wp_postmeta1.post_id = wp_posts.ID
                        AND wp_postmeta1.meta_key = "st_size"
                    INNER JOIN wp_postmeta wp_postmeta2 -- LEFT JOIN wp_postmeta wp_postmeta2
                        ON wp_postmeta2.post_id = wp_posts.ID
                        AND wp_postmeta2.meta_key = "st_colour"
                    INNER JOIN wp_terms wp_terms1
                        ON wp_postmeta2.meta_value = wp_terms1.name		
                        AND wp_terms1.term_id IN (SELECT term_id FROM `wp_term_taxonomy` WHERE `taxonomy` = "pa_toys-colour")
                    INNER JOIN wp_term_relationships
                        ON wp_term_relationships.object_id = wp_posts.ID AND wp_term_relationships.term_taxonomy_id IN (19, 38, 34, 36, 25, 100, 105, 106, 108, 1078, 324, 121, 109, 107, 230) -- Category
                    LEFT JOIN wp_term_taxonomy
                        ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                        AND wp_term_taxonomy.taxonomy = "product_cat"
                    LEFT JOIN wp_terms wp_terms2
                        ON wp_term_taxonomy.term_id = wp_terms2.term_id
                    WHERE wp_posts.post_type = "product"
                    AND wp_posts.post_status = "publish"
                    AND wp_posts.id = ' . $post_id . '
                    GROUP BY wp_posts.id';

            $filters = $wpdb->get_results($sql);
                        
            foreach ( $filters as $filter ) {	
                $sql = 'INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order)
                        VALUES (' . $filter->object_id . ', ' . $filter->term_taxonomy_id . ', ' . $filter->term_order . ' )';
                $wpdb->query($sql);
            }
        }
    
        if($shoe_size == true) { 
           
            $sql = 'SELECT
                        wp_posts.id AS object_id,
                        wp_terms1.term_id AS term_taxonomy_id,
                        0 AS term_order
                    FROM wp_posts
                    INNER JOIN wp_terms wp_terms1
                        ON wp_terms1.name = "' .  '"
                        AND wp_terms1.term_id IN (SELECT term_id FROM `wp_term_taxonomy` WHERE `taxonomy` = "pa_shoe-size")
                    LEFT JOIN wp_postmeta wp_postmeta2
                        ON wp_postmeta2.post_id = wp_posts.ID
                        AND wp_postmeta2.meta_key = "st_colour"
                    INNER JOIN wp_term_relationships
                        ON wp_term_relationships.object_id = wp_posts.ID AND wp_term_relationships.term_taxonomy_id IN (22, 271, 1021, 1030, 1052) -- Category
                    LEFT JOIN wp_term_taxonomy
                        ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                        AND wp_term_taxonomy.taxonomy = "product_cat"
                    LEFT JOIN wp_terms wp_terms2
                        ON wp_term_taxonomy.term_id = wp_terms2.term_id
                    WHERE wp_posts.post_type = "product"
                    AND wp_posts.post_status = "publish"
                    AND wp_posts.id = ' . $post_id . '
                    GROUP BY wp_posts.id';

            $filters = $wpdb->get_results($sql);
                        
            foreach ( $filters as $filter ) {	
                $sql = 'INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order)
                        VALUES (' . $filter->object_id . ', ' . $filter->term_taxonomy_id . ', ' . $filter->term_order . ' )';
                $wpdb->query($sql);
            }
        }
        if($shoe_heel_height == true) {                   
            //$sql = '';        
            //$results = $wpdb->get_results($sql);
        }
        if($bra_size == true) { 
        
            $sql = 'SELECT
                        wp_posts.id AS object_id,
                        wp_terms1.term_id AS term_taxonomy_id,
                        0 AS term_order
                    FROM wp_posts
                    INNER JOIN wp_postmeta wp_postmeta1 -- LEFT JOIN wp_postmeta wp_postmeta1
                        ON wp_postmeta1.post_id = wp_posts.ID
                        AND wp_postmeta1.meta_key = "st_size"	
                    INNER JOIN wp_terms wp_terms1
                        ON wp_postmeta1.meta_value = wp_terms1.name	
                        AND wp_terms1.term_id IN (SELECT term_id FROM `wp_term_taxonomy` WHERE `taxonomy` = "pa_bra-size")
                    LEFT JOIN wp_postmeta wp_postmeta2
                        ON wp_postmeta2.post_id = wp_posts.ID
                        AND wp_postmeta2.meta_key = "st_colour"
                    INNER JOIN wp_term_relationships
                        ON wp_term_relationships.object_id = wp_posts.ID AND wp_term_relationships.term_taxonomy_id IN (22, 63, 1018, 172, 1015) -- Category
                    LEFT JOIN wp_term_taxonomy
                        ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                        AND wp_term_taxonomy.taxonomy = "product_cat"
                    LEFT JOIN wp_terms wp_terms2
                        ON wp_term_taxonomy.term_id = wp_terms2.term_id
                    WHERE wp_posts.post_type = "product"
                    AND wp_posts.post_status = "publish"
                    AND wp_posts.id = ' . $post_id . '
                    GROUP BY wp_posts.id';

            $filters = $wpdb->get_results($sql);
                        
            foreach ( $filters as $filter ) {	
                $sql = 'INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order)
                        VALUES (' . $filter->object_id . ', ' . $filter->term_taxonomy_id . ', ' . $filter->term_order . ' )';
                $wpdb->query($sql);
            }
        }
        if($lingerie_colour == true) { 
      
            $sql = 'SELECT
                        wp_posts.id AS object_id,
                        wp_terms1.term_id AS term_taxonomy_id,
                        0 AS term_order
                    FROM wp_posts
                    LEFT JOIN wp_postmeta wp_postmeta1
                        ON wp_postmeta1.post_id = wp_posts.ID
                        AND wp_postmeta1.meta_key = "st_size"
                    INNER JOIN wp_postmeta wp_postmeta2 -- LEFT JOIN wp_postmeta wp_postmeta2
                        ON wp_postmeta2.post_id = wp_posts.ID
                        AND wp_postmeta2.meta_key = "st_colour"
                    INNER JOIN wp_terms wp_terms1
                        ON wp_postmeta2.meta_value = wp_terms1.name		
                        AND wp_terms1.term_id IN (SELECT term_id FROM `wp_term_taxonomy` WHERE `taxonomy` = "pa_lingerie-colour")
                    INNER JOIN wp_term_relationships
                        ON wp_term_relationships.object_id = wp_posts.ID AND wp_term_relationships.term_taxonomy_id IN (22, 66, 119, 1044, 63, 172, 1015, 1055, 1027, 1043) -- Category
                    LEFT JOIN wp_term_taxonomy
                        ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                        AND wp_term_taxonomy.taxonomy = "product_cat"
                    LEFT JOIN wp_terms wp_terms2
                        ON wp_term_taxonomy.term_id = wp_terms2.term_id
                    WHERE wp_posts.post_type = "product"
                    AND wp_posts.post_status = "publish"
                    AND wp_posts.id = ' . $post_id . '
                    GROUP BY wp_posts.id';

            $filters = $wpdb->get_results($sql);
                        
            foreach ( $filters as $filter ) {	
                $sql = 'INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order)
                        VALUES (' . $filter->object_id . ', ' . $filter->term_taxonomy_id . ', ' . $filter->term_order . ' )';
                $wpdb->query($sql);
            }
        }
        if($apparel_size == true) { 

            $sql = 'SELECT
                        wp_posts.id AS object_id,
                        wp_terms1.term_id AS term_taxonomy_id,
                        0 AS term_order
                    FROM wp_posts
                    INNER JOIN wp_postmeta wp_postmeta1 -- LEFT JOIN wp_postmeta wp_postmeta1
                        ON wp_postmeta1.post_id = wp_posts.ID
                        AND wp_postmeta1.meta_key = "st_size"	
                    INNER JOIN wp_terms wp_terms1
                        ON wp_postmeta1.meta_value = wp_terms1.name	
                        AND wp_terms1.term_id IN (SELECT term_id FROM `wp_term_taxonomy` WHERE `taxonomy` = "pa_apparel-size")
                    LEFT JOIN wp_postmeta wp_postmeta2
                        ON wp_postmeta2.post_id = wp_posts.ID
                        AND wp_postmeta2.meta_key = "st_colour"
                    INNER JOIN wp_term_relationships
                        ON wp_term_relationships.object_id = wp_posts.ID AND wp_term_relationships.term_taxonomy_id IN (22, 66, 119, 118, 63, 1015, 1032, 1027, 1043, 172, 1055) -- Category
                    LEFT JOIN wp_term_taxonomy
                        ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                        AND wp_term_taxonomy.taxonomy = "product_cat"
                    LEFT JOIN wp_terms wp_terms2
                        ON wp_term_taxonomy.term_id = wp_terms2.term_id
                    WHERE wp_posts.post_type = "product"
                    AND wp_posts.post_status = "publish"
                    AND wp_posts.id = ' . $post_id . '
                    GROUP BY wp_posts.id';

            $filters = $wpdb->get_results($sql);
                        
            foreach ( $filters as $filter ) {	
                $sql = 'INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order)
                        VALUES (' . $filter->object_id . ', ' . $filter->term_taxonomy_id . ', ' . $filter->term_order . ' )';
                $wpdb->query($sql);
            }
        }
        if($accessories_colour == true) { 
               
            $sql = 'SELECT
                        wp_posts.id AS object_id,
                        wp_terms1.term_id AS term_taxonomy_id,
                        0 AS term_order
                    FROM wp_posts
                    LEFT JOIN wp_postmeta wp_postmeta1
                        ON wp_postmeta1.post_id = wp_posts.ID
                        AND wp_postmeta1.meta_key = "st_size"
                    INNER JOIN wp_postmeta wp_postmeta2 -- LEFT JOIN wp_postmeta wp_postmeta2
                        ON wp_postmeta2.post_id = wp_posts.ID
                        AND wp_postmeta2.meta_key = "st_colour"
                    INNER JOIN wp_terms wp_terms1
                        ON wp_postmeta2.meta_value = wp_terms1.name		
                        AND wp_terms1.term_id IN (SELECT term_id FROM `wp_term_taxonomy` WHERE `taxonomy` = "pa_accessories-colour")
                    INNER JOIN wp_term_relationships
                        ON wp_term_relationships.object_id = wp_posts.ID AND wp_term_relationships.term_taxonomy_id IN (22, 1044, 120) -- Category
                    LEFT JOIN wp_term_taxonomy
                        ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                        AND wp_term_taxonomy.taxonomy = "product_cat"
                    LEFT JOIN wp_terms wp_terms2
                        ON wp_term_taxonomy.term_id = wp_terms2.term_id
                    WHERE wp_posts.post_type = "product"
                    AND wp_posts.post_status = "publish"
                    AND wp_posts.id = ' . $post_id . '
                    GROUP BY wp_posts.id';

            $filters = $wpdb->get_results($sql);
                        
            foreach ( $filters as $filter ) {	
                $sql = 'INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order)
                        VALUES (' . $filter->object_id . ', ' . $filter->term_taxonomy_id . ', ' . $filter->term_order . ' )';
                $wpdb->query($sql);
            }
            
        }
        if($fetish_colour == true) { 
           
            $sql = 'SELECT
                        wp_posts.id AS object_id,
                        wp_terms1.term_id AS term_taxonomy_id,
                        0 AS term_order
                    FROM wp_posts
                    LEFT JOIN wp_postmeta wp_postmeta1
                        ON wp_postmeta1.post_id = wp_posts.ID
                        AND wp_postmeta1.meta_key = "st_size"
                    INNER JOIN wp_postmeta wp_postmeta2 -- LEFT JOIN wp_postmeta wp_postmeta2
                        ON wp_postmeta2.post_id = wp_posts.ID
                        AND wp_postmeta2.meta_key = "st_colour"
                    INNER JOIN wp_terms wp_terms1
                        ON wp_postmeta2.meta_value = wp_terms1.name		
                        AND wp_terms1.term_id IN (SELECT term_id FROM `wp_term_taxonomy` WHERE `taxonomy` = "pa_fetish-colour")
                    INNER JOIN wp_term_relationships
                        ON wp_term_relationships.object_id = wp_posts.ID AND wp_term_relationships.term_taxonomy_id IN (20, 39, 321, 41, 40, 1058, 1061) -- Category
                    LEFT JOIN wp_term_taxonomy
                        ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                        AND wp_term_taxonomy.taxonomy = "product_cat"
                    LEFT JOIN wp_terms wp_terms2
                        ON wp_term_taxonomy.term_id = wp_terms2.term_id
                    WHERE wp_posts.post_type = "product"
                    AND wp_posts.post_status = "publish"
                    AND wp_posts.id = ' . $post_id . '
                    GROUP BY wp_posts.id';

            $filters = $wpdb->get_results($sql);
                        
            foreach ( $filters as $filter ) {	
                $sql = 'INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order)
                        VALUES (' . $filter->object_id . ', ' . $filter->term_taxonomy_id . ', ' . $filter->term_order . ' )';
                $wpdb->query($sql);
            }
        }
    }
}