<?php

class warehouse_download_images {

    private $warehouse_db_class;
    private $warehouse_products_class;
    private $warehouse_vendors_class;

    public function __construct() {
        require_once 'warehouse_db_class.php';
        require_once 'warehouse_live_products_class.php';
        require_once 'vendors_classes.php';
        $this->warehouse_db_class = new warehouse_database_class();
        $this->warehouse_products_class = new warehouse_live_products_class();
        $this->warehouse_vendors_class = new vendor_functions();
        if (!file_exists(WAREHOUSE_TEMP_FOLDER)) {
            mkdir(WAREHOUSE_TEMP_FOLDER, 0775, true);
        }
        if (!file_exists(WAREHOUSE_LOG_FOLDER)) {
            mkdir(WAREHOUSE_LOG_FOLDER, 0775, true);
        }
        $this->warehouse_images_download_images($this->warehouse_images_get_image_download_array());
    }

    private function warehouse_images_get_image_download_array() {
        $returnArray = array();
        $results = $this->warehouse_db_class->warehouse_db_do_query("SELECT feed_id,image FROM `feed_products` WHERE `imported` = 0");
        while ($row = mysqli_fetch_assoc($results)):
            if ((isset($row['image']) && !empty($row['image'])) && (isset($row['feed_id']) && !empty($row['feed_id']))):
                if (!$this->warehouse_check_image_exists($row['image'], $row['feed_id'])):
                    $row['feed_type'] = $this->warehouse_vendors_class->warehouse_vendors_get_download_type($row['feed_id']);
                    array_push($returnArray, (object) $row);
                endif;
            endif;
        endwhile;
        echo count($returnArray);
        return $returnArray;
    }

    private function warehouse_check_image_exists($image_link, $feed_id) {
        $image_link = explode('/', $image_link);
        $image_link = $image_link[count($image_link) - 1];
        return file_exists(WAREHOUSE_TEMP_FOLDER . $feed_id . '/' . $image_link);
    }

    private function warehouse_images_download_images($imagesArray) {
        if (isset($imagesArray) && !empty($imagesArray)):
            foreach ($imagesArray as $image):
                switch ($image->feed_type):
                    case 'ftp':
                        $result = $this->warehouse_products_class->warehouse_download_image_ftp($image->feed_id, $image->image, WAREHOUSE_TEMP_FOLDER . $image->feed_id . '/');
                        break;
                    case 'live':
                        $result = $this->warehouse_products_class->warehouse_get_image_from_link($image->image, WAREHOUSE_TEMP_FOLDER . $image->feed_id . '/');
                        break;
                endswitch;
                $this->warehouse_add_note($image->image . ':' . $result);
            endforeach;
        endif;
    }

    private function warehouse_add_note($info = false) {
        if ($info):
            $tz = 'Australia/Sydney';
            $timestamp = time();
            $dt = new DateTime("now", new DateTimeZone($tz));
            $dt->setTimestamp($timestamp);
            $date = $dt->format('d-m-Y H:i:s');
            $note = "[$date] - $info\r\n";
            file_put_contents(WAREHOUSE_LOG_FOLDER . $dt->format('d-m-Y') . '.txt', $note, FILE_APPEND | LOCK_EX);
        endif;
    }

}
