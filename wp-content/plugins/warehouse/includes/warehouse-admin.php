<?php
require_once 'warehouse_notes_class.php';
$notes_class = new warehouse_notes_class();
?>
<input value="<?php echo $notes_class->warehouse_get_notes_json_file() ?>" type="hidden" id="json_location"/>
<?php
if (isset($_POST['notes-clear-all'])):
    $notes_class->warehouse_delete_all_notes();
endif;
?>
<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
    <h1 style="margin: 20px auto">Warehouse Inventory</h1>
    <input type="hidden" id="ajax-url" value="<?php echo admin_url('admin-ajax.php'); ?>"/>
    <div class="alert alert-warning alert-dismissible show" role="alert" style="margin-right: 20px">
    Please review any products that are either in stock or out of stock as well as the ones which price has increased or decreased. Then, remove their notes as you go.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
    <form method="post" style="display: inline;">
        <button name="notes-clear-all" class="btn btn-info">Clear All Notes</button>
    </form>
    <button disabled name="remove-selected-notes" class="btn btn-info">Remove Selected Notes</button>
    <button name="remove-table-notes" class="btn btn-info">Clear Notes Below</button>
</div>
<div class="container-fluid" style="margin: 20px auto">
    <ul class="nav nav-tabs" role="tablist" style="margin: 20px auto">
        <li role="presentation" class="active"><a href="#allNotes" aria-controls="allNotes" role="tab" data-toggle="tab">All Notes <span class="number_of_rows"></span></a></li>
        <li role="presentation"><a href="#ofs" aria-controls="ofs" role="tab" data-toggle="tab">Out Of Stock <span class="number_of_rows"></span></a></li>
        <li role="presentation"><a href="#is" aria-controls="is" role="tab" data-toggle="tab">In Stock <span class="number_of_rows"></span></a></li>
        <li role="presentation"><a href="#isnl" aria-controls="isnl" role="tab" data-toggle="tab">In Stock (Not Live) <span class="number_of_rows"></span></a></li>
        <li role="presentation"><a href="#pi" aria-controls="pi" role="tab" data-toggle="tab">Price Increased <span class="number_of_rows"></span></a></li>
        <li role="presentation"><a href="#pd" aria-controls="pd" role="tab" data-toggle="tab">Price Decreased <span class="number_of_rows"></span></a></li>
        <li role="presentation"><a href="#tmr" aria-controls="tmr" role="tab" data-toggle="tab">Too Many Results <span class="number_of_rows"></span></a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="allNotes">
            <table id="warehouse-notes-table1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>Vendor</td>
                        <td>Title</td>
                        <td>Code</td>
                        <td>Barcode</td>
                        <td>Note</td>
                        <td>Actions</td>
                    </tr>
                </thead>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="ofs">
            <table id="warehouse-notes-table2" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>Vendor</td>
                        <td>Title</td>
                        <td>Code</td>
                        <td>Barcode</td>
                        <td>Note</td>
                        <td>Actions</td>
                    </tr>
                </thead>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="is">
            <table id="warehouse-notes-table6" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>Vendor</td>
                        <td>Title</td>
                        <td>Code</td>
                        <td>Barcode</td>
                        <td>Note</td>
                        <td>Actions</td>
                    </tr>
                </thead>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="isnl">
            <table id="warehouse-notes-table7" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>Vendor</td>
                        <td>Title</td>
                        <td>Code</td>
                        <td>Barcode</td>
                        <td>Note</td>
                        <td>Actions</td>
                    </tr>
                </thead>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="pi">
            <table id="warehouse-notes-table3" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>Vendor</td>
                        <td>Title</td>
                        <td>Code</td>
                        <td>Barcode</td>
                        <td>Note</td>
                        <td>Actions</td>
                    </tr>
                </thead>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="pd">
            <table id="warehouse-notes-table4" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>Vendor</td>
                        <td>Title</td>
                        <td>Code</td>
                        <td>Barcode</td>
                        <td>Note</td>
                        <td>Actions</td>
                    </tr>
                </thead>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="tmr">
            <table id="warehouse-notes-table5" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>Vendor</td>
                        <td>Title</td>
                        <td>Code</td>
                        <td>Barcode</td>
                        <td>Note</td>
                        <td>Actions</td>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.js"></script>
<?php
admin_scripts();
?>