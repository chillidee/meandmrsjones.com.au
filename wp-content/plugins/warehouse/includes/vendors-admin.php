<?php
require_once 'vendors_classes.php';
$vendor_functions = new vendor_functions();
if (isset($_POST['save_vendor'])):
    if (!$_POST['save_vendor']):
        $post_id = wp_insert_post(array(
            'post_title' => htmlentities($_POST['vendor_name']),
            'post_type' => 'vendors',
            'post_content' => htmlentities($_POST['vendor_name']),
            'post_status' => 'publish'));
        update_post_meta($post_id, '_downloaded_products', false);
    else:
        $edit_vendor = array(
            'ID' => (int) $_POST['save_vendor'],
            'post_title' => $_POST['vendor_name'],
            'post_excerpt' => $_POST['vendor_name'],
        );
        wp_update_post($edit_vendor);
        $post_id = (int) $_POST['save_vendor'];
    endif;
    update_post_meta($post_id, '_feed_type', $_POST["feed-type"]);
    update_post_meta($post_id, '_ftp_host', $_POST["feed-ftp-host"]);
    update_post_meta($post_id, '_ftp_username', $_POST["feed-ftp-username"]);
    update_post_meta($post_id, '_ftp_password', $_POST["feed-ftp-password"]);
    update_post_meta($post_id, '_ftp_port', $_POST["feed-ftp-port"]);
    update_post_meta($post_id, '_ftp_path', $_POST["feed-ftp-path"]);
    update_post_meta($post_id, '_ftp_filename', $_POST["feed-ftp-filename"]);
    update_post_meta($post_id, '_ftp_delimiter', $_POST["feed-ftp-delimiter"]);
    update_post_meta($post_id, '_ftp_image_path', $_POST["feed-ftp-image-path"]);
    update_post_meta($post_id, '_live_url', $_POST["feed-live-url"]);
    update_post_meta($post_id, '_live_pp', $_POST["feed-live-pp"]);
    update_post_meta($post_id, '_live_no_pp', $_POST["feed-live-no-pp"]);
    update_post_meta($post_id, '_file_name', $_POST["feed-file-name"]);
    update_post_meta($post_id, '_file_type', $_POST["feed-data-type"]);
    update_post_meta($post_id, '_update_auto', $_POST["feed_update_auto"]);
endif;
if (isset($_POST['vendor_delete'])):
    $vendor_id = (int) $_POST['vendor_id'];
    wp_delete_post($vendor_id, true);
endif;
if (isset($_POST['vendor_edit']) || $_POST['add_new_vendor']):
    if ($_POST['vendor_edit']):
        $vendor_id = (int) $_POST['vendor_id'];
        vendor_edit_vendor($vendor_id);
    else:
        vendor_edit_vendor();
    endif;
else:
    $current_vendors = $vendor_functions->vendor_get_vendors();
    ?>    
    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
        <h1>Warehouse Vendors</h1>
        <div class="alert alert-warning alert-dismissible show" role="alert" style="margin-right: 20px">
            Please do not change these settings below, unless you would like to manage vendors by adding new ones, updating or deleting them. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="col-lg-6 col-md-8 col-sm-10 col-xs-12">
            <table>
                <thead>
                    <tr>
                        <td><strong>Vendor Name</strong></td>
                        <td><strong>Actions</strong></td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($current_vendors as $vendor): ?>
                        <tr>
                            <td>
                                <?php echo $vendor->post_title ?>
                            </td>
                            <td>
                                <form method="POST">
                                    <input name="vendor_id" value="<?php echo $vendor->ID ?>" type="hidden"/>
                                    <input type="submit" name="vendor_edit" value="Edit" class="btn-info btn"/>
                                    <input type="submit" name="vendor_delete" value="Delete" class="btn-danger btn"/>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach;
                    ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            <form method="POST">
                <button class="btn btn-success" name="add_new_vendor" value="true">Add New</button>
            </form>
        </div>
    </div>
<?php
endif;

function vendor_edit_vendor($post_id = null) {
    if ($post_id):
        $post = get_post($post_id);
    endif;
    ?>
    <div class="col-lg-12">
        <h1>Vendor Details</h1>
    </div>
    <form method="post">
        <div class="col-lg-12">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <label for="vendor_name">Name</label>
            </div>
            <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                <input name="vendor_name" value="<?php echo ($post->post_title) ? $post->post_title : '' ?>"/>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <label for="feed-data-type">Feed Data Type</label>
            </div>
            <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                <select name="feed-data-type">
                    <option <?php echo (get_post_meta($post_id, '_file_type', true) == 'csv') ? 'selected' : '' ?> value="csv">CSV</option>
                    <option <?php echo (get_post_meta($post_id, '_file_type', true) == 'xml') ? 'selected' : '' ?> value="xml">XML</option>
                </select>
            </div>            
        </div>
        <div class="col-lg-12">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <label for="feed-ftp-delimiter">CSV Delimiter</label>
            </div>
            <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                <input name="feed-ftp-delimiter" value="<?php echo (get_post_meta($post_id, '_ftp_delimiter', true)) ? get_post_meta($post_id, '_ftp_delimiter', true) : '' ?>"/>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <label for="feed-type">Feed Type</label>
            </div>
            <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                <select name="feed-type">
                    <option <?php echo (get_post_meta($post_id, '_feed_type', true) == 'ftp') ? 'selected' : '' ?> value="ftp">FTP</option>
                    <option <?php echo (get_post_meta($post_id, '_feed_type', true) == 'live') ? 'selected' : '' ?> value="live">Live</option>
                    <option <?php echo (get_post_meta($post_id, '_feed_type', true) == 'file') ? 'selected' : '' ?> value="file">File</option>
                </select>
            </div>
        </div>
        <div class="<?php echo (get_post_meta($post_id, '_feed_type', true) == 'ftp') ? '' : 'hidden' ?> ftp-details details-conatiner">
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="feed-ftp-host">Feed Host</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="feed-ftp-host" value="<?php echo (get_post_meta($post_id, '_ftp_host', true)) ? get_post_meta($post_id, '_ftp_host', true) : '' ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="feed-ftp-username">Feed Username</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="feed-ftp-username" value="<?php echo (get_post_meta($post_id, '_ftp_username', true)) ? get_post_meta($post_id, '_ftp_username', true) : '' ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="feed-ftp-username">Feed Password</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="feed-ftp-password" value="<?php echo (get_post_meta($post_id, '_ftp_password', true)) ? get_post_meta($post_id, '_ftp_password', true) : '' ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="feed-ftp-username">Feed Port</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="feed-ftp-port" value="<?php echo (get_post_meta($post_id, '_ftp_port', true)) ? get_post_meta($post_id, '_ftp_port', true) : '' ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="feed-ftp-path">Feed Path</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="feed-ftp-path" value="<?php echo (get_post_meta($post_id, '_ftp_path', true)) ? get_post_meta($post_id, '_ftp_path', true) : '' ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="feed-ftp-filename">Feed Filename</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="feed-ftp-filename" value="<?php echo (get_post_meta($post_id, '_ftp_filename', true)) ? get_post_meta($post_id, '_ftp_filename', true) : '' ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="feed-ftp-image-path">Image Path</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="feed-ftp-image-path" value="<?php echo (get_post_meta($post_id, '_ftp_image_path', true)) ? get_post_meta($post_id, '_ftp_image_path', true) : '' ?>"/>
                </div>
            </div>
        </div>
        <div class="<?php echo (get_post_meta($post_id, '_feed_type', true) == 'live') ? '' : 'hidden' ?> live-details details-conatiner">
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="feed-live-url">Feed URL</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="feed-live-url" value="<?php echo (get_post_meta($post_id, '_live_url', true)) ? get_post_meta($post_id, '_live_url', true) : '' ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="feed-live-no-pp">Number of POST Parms</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="feed-live-no-pp" value="<?php echo (get_post_meta($post_id, '_live_no_pp', true)) ? get_post_meta($post_id, '_live_no_pp', true) : '' ?>"/>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="feed-live-pp">Feed POST Parms (seperate with &)</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="feed-live-pp" value="<?php echo (get_post_meta($post_id, '_live_pp', true)) ? get_post_meta($post_id, '_live_pp', true) : '' ?>"/>
                </div>
            </div>
        </div>
        <div class="col-lg-12 <?php echo (get_post_meta($post_id, '_feed_type', true) == 'file') ? '' : 'hidden' ?> file-details details-conatiner">
            <div class="col-lg-12">
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                    <label for="feed-file-name">File Name</label>
                </div>
                <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                    <input name="feed-file-name" value="<?php echo (get_post_meta($post_id, '_file_name', true)) ? get_post_meta($post_id, '_file_name', true) : '' ?>"/>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <label for="feed_update_auto">Update Automatically</label>
            </div>
            <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12">
                <select name="feed_update_auto">
                    <option value="true" <?php echo (get_post_meta($post_id, '_update_auto', true) == 'true') ? 'selected' : '' ?>>Yes</option>
                    <option value="false" <?php echo (get_post_meta($post_id, '_update_auto', true) == 'false') ? 'selected' : '' ?>>No</option>
                </select>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <button name="save_vendor" value="<?php echo $post_id ?>" class="btn btn-success">Save</button>
                <button name="cancel" class="btn btn-danger">Cancel</button>
            </div>
        </div>
    </form>
    <?php
}
