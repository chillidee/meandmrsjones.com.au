<?php

class warehouse_live_products_class {

    private $db;

    public function __construct() {
        require_once 'warehouse_db_class.php';
        $this->db = new warehouse_database_class();
    }

    public function warehouse_live_get_all_products($vendor_id) {
        set_time_limit(0);
        $returnArray = array();
        $this->warehouse_live_compare_live_feed_products($vendor_id);
        return $returnArray;
    }

    private function warehouse_live_get_vendor_name($vendor_id) {
        $wholeseller_name = get_post($vendor_id);
        return $wholeseller_name->post_title;
    }

    private function get_vendor_name($vendor_id) {        
        return get_the_title( $vendor_id );
    }
    
    private function warehouse_live_get_products_by_vendor($vendor_id) {
        $args = array(
            'post_type' => 'product',
            'posts_per_page' => -1,
            'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
            //'post_status' => array('publish'), // It is set to change only products that are LIVE
            'fields' => 'ids',
            'meta_query' => array(
                array(
                    'key' => 'vendor',
                    'value' => $this->warehouse_live_get_vendor_name($vendor_id),
                    'compare' => '=',
                )
            )
        );
        $ids = new WP_Query($args);
        $ids = $ids->posts;        
        return $ids;
    }

    private function warehouse_live_compare_live_feed_simple_product($vendor_id, $id) {

        require_once 'warehouse_db_class.php';
        $feed_db_class = new warehouse_database_class();
        $unplubished = false;

        $post = get_post($id);
        $queryString = "SELECT * FROM `feed_products` WHERE `Code` LIKE '" . get_post_meta($id, '_code', TRUE) . "' AND `feed_id` LIKE '$vendor_id'";
        $result = $feed_db_class->warehouse_db_do_query($queryString);
        if ($result->num_rows > 1):
            $queryString = "SELECT * FROM `feed_products` WHERE `Code` LIKE '" . get_post_meta($id, '_code', TRUE) . "' AND `Size` LIKE '" . get_post_meta($id, 'st_size', TRUE) . "' AND `Colour` LIKE '" . get_post_meta($id, 'st_colour', TRUE) . "' AND `feed_id` LIKE '$vendor_id'";
            $result = $feed_db_class->warehouse_db_do_query($queryString);
        else:
            if (($result->num_rows == 0 && get_post_meta($id, '_barcode', TRUE)) || ($result->num_rows > 1 && get_post_meta($id, '_barcode', TRUE))):
                $queryString = "SELECT * FROM `feed_products` WHERE `Barcode` LIKE '" . get_post_meta($id, '_barcode', TRUE) . "' AND `feed_id` LIKE '$vendor_id'";
                $result = $feed_db_class->warehouse_db_do_query($queryString);
            endif;
        endif;
        if ($result->num_rows == 1):
            $result = mysqli_fetch_assoc($result);
            $this->warehouse_set_live_product_import($result["id"]);

            //--------------------------------------------------------
            // Quick Fix to update product long & short descriptions - it only updates the ones in stock (come in feeds)            
            /*$product_post = array(
                'ID'                =>      $id,
                'post_content'      =>      $result['Long Description'],
                'post_excerpt'      =>      $result['Short Description']
            );
        
            // Update the post into the database
            wp_update_post( $product_post );*/
            //--------------------------------------------------------

            if (number_format((float) $result["Wholesale Price"], 2) > number_format((float) get_post_meta($id, '_wholesale_price', TRUE), 2)):
                $queryString = "INSERT INTO `notifications` (`id`, `feed_id`, `note`, `product_code`, `product_barcode`, `product_id`, `product_title`) VALUES (NULL, '" . $vendor_id . "', '" . $post->post_title . " wholesale price has increased from $" . get_post_meta($id, '_wholesale_price', TRUE) . " to $" . number_format((float) $result["Wholesale Price"], 2) . "', '" . get_post_meta($id, '_code', TRUE) . "', '" . get_post_meta($id, '_barcode', TRUE) . "', '" . $id . "', '" . $post->post_title . "')";
                $feed_db_class->warehouse_db_do_query($queryString);
                $priceDifference = (float) number_format((float) $result["Wholesale Price"], 2) - (float) number_format((float) get_post_meta($id, '_wholesale_price', TRUE), 2);
                $priceDifference = number_format((float) get_post_meta($id, '_price', TRUE) + $priceDifference, 2);
                $priceDifference = round($priceDifference);
                update_post_meta($id, '_price', $priceDifference);
                update_post_meta($id, '_regular_price', $priceDifference);
                update_post_meta($id, '_wholesale_price', number_format((float) $result["Wholesale Price"], 2));
                if (number_format((float) get_post_meta($id, '_wholesale_price', TRUE), 2) > number_format((float) get_post_meta($id, '_price', TRUE), 2)):
                    $this->warehouse_live_unpublish_product($id);
                    $queryString = "INSERT INTO `notifications` (`id`, `feed_id`, `note`, `product_code`, `product_barcode`, `product_id`, `product_title`) VALUES (NULL, '" . $vendor_id . "', '" . $post->post_title . " has been removed as the wholesale price is higher than the sale price', '" . get_post_meta($id, '_code', TRUE) . "', '" . get_post_meta($id, '_barcode', TRUE) . "', '" . $id . "', '" . $post->post_title . "')";
                    $feed_db_class->warehouse_db_do_query($queryString);
                    $unplubished = true;
                endif;
            endif;
            if (number_format((float) $result["Wholesale Price"], 2) < number_format((float) get_post_meta($id, '_wholesale_price', TRUE), 2)):
                $queryString = "INSERT INTO `notifications` (`id`, `feed_id`, `note`, `product_code`, `product_barcode`, `product_id`, `product_title`) VALUES (NULL, '" . $vendor_id . "', '" . $post->post_title . " wholesale price has decreased from $" . get_post_meta($id, '_wholesale_price', TRUE) . " to $" . number_format((float) $result["Wholesale Price"], 2) . "', '" . get_post_meta($id, '_code', TRUE) . "', '" . get_post_meta($id, '_barcode', TRUE) . "', '" . $id . "', '" . $post->post_title . "')";
                $feed_db_class->warehouse_db_do_query($queryString);
                $differance = round((float) get_post_meta($id, '_wholesale_price', TRUE) - (float) $result["Wholesale Price"]);
                $new_price = number_format((float) get_post_meta($id, '_price', true) - (float) $differance, 2);
                update_post_meta($id, '_price', $new_price);
                update_post_meta($id, '_regular_price', $new_price);
                update_post_meta($id, '_wholesale_price', number_format((float) $result["Wholesale Price"], 2));
                if ((int) get_post_meta($id, '_wholesale_price', TRUE) >= (int) get_post_meta($id, '_price', true)):
                    $this->warehouse_live_unpublish_product($id);
                endif;
            endif;
            if ($post->post_status == 'draft' && !$unplubished):
                if (number_format((float) get_post_meta($id, '_wholesale_price', TRUE), 2) > number_format((float) get_post_meta($id, '_price', TRUE), 2)):
                    $queryString = "INSERT INTO `notifications` (`id`, `feed_id`, `note`, `product_code`, `product_barcode`, `product_id`, `product_title`) VALUES (NULL, '" . $vendor_id . "', '" . $post->post_title . " can\'t be inported wholesale price is greater than the sale price', '" . get_post_meta($id, '_code', TRUE) . "', '" . get_post_meta($id, '_barcode', TRUE) . "', '" . $id . "', '" . $post->post_title . "')";
                    $feed_db_class->warehouse_db_do_query($queryString);
                else:
                    $result = $this->warehouse_live_pubish_product($id);
                    if ($result):
                        $queryString = "INSERT INTO `notifications` (`id`, `feed_id`, `note`, `product_code`, `product_barcode`, `product_id`, `product_title`) VALUES (NULL, '" . $vendor_id . "', '" . $post->post_title . " has come back into stock and made live', '" . get_post_meta($id, '_code', TRUE) . "', '" . get_post_meta($id, '_barcode', TRUE) . "', '" . $id . "', '" . $post->post_title . "')";
                        $feed_db_class->warehouse_db_do_query($queryString);
                    endif;
                endif;
            endif;
        else:
            if ($result->num_rows == 0 && $post->post_status != 'draft'):
                $queryString = "INSERT INTO `notifications` (`id`, `feed_id`, `note`, `product_code`, `product_barcode`, `product_id`, `product_title`) VALUES (NULL, '" . $vendor_id . "', '" . $post->post_title . " has gone out of stock and has been removed from site', '" . get_post_meta($id, '_code', TRUE) . "', '" . get_post_meta($id, '_barcode', TRUE) . "', '" . $id . "', '" . $post->post_title . "')";
                $feed_db_class->warehouse_db_do_query($queryString);
                $this->warehouse_live_unpublish_product($id);
            else:
                if ($result->num_rows > 1):
                    $queryString = "INSERT INTO `notifications` (`id`, `feed_id`, `note`, `product_code`, `product_barcode`, `product_id`, `product_title`) VALUES (NULL, '" . $vendor_id . "', ' Can\'t compare " . $post->post_title . " too many results please check manualy', '" . get_post_meta($id, '_code', TRUE) . "', '" . get_post_meta($id, '_barcode', TRUE) . "', '" . $id . "', '" . $post->post_title . "')";
                    $feed_db_class->warehouse_db_do_query($queryString);
                endif;
            endif;
        endif;
    }

    private function warehouse_live_compare_live_feed_variation_product($vendor_id, $id, $product) {
        foreach ( $product->get_children() as $child_id ) 
        { 
            $this->warehouse_live_compare_live_feed_simple_product($vendor_id, $child_id);
        }
    }

    private function warehouse_live_compare_live_feed_products($vendor_id) {
        
        $num_variation_products = 0;
        $num_simple_products = 0;

        foreach ($this->warehouse_live_get_products_by_vendor($vendor_id) as $id):
            
            $product = wc_get_product($id);            

            if($product->has_child()) {
                $num_variation_products += 1;
                $this->warehouse_live_compare_live_feed_variation_product($vendor_id, $id, $product);                 
            } else {
                $num_simple_products += 1;
                $this->warehouse_live_compare_live_feed_simple_product($vendor_id, $id);              
            }
            unset($id);
        endforeach;

        // DEBUG
        //echo '\n Variations: ' . $num_variation_products;
        //echo '\n Simple: ' . $num_simple_products;
        //die();
    }

    public function warehouse_live_unpublish_product($post_id) {
        if (get_post_meta($post_id, '_is_parent', true)):
            update_post_meta($post_id, '_is_parent', false);
            update_post_meta($post_id, '_is_child', true);
            $changed_parent = false;
            $new_parent_id = 0;
            foreach ($this->warehouse_live_get_published_product_ids() as $id):
                if (get_post_meta($id, '_parent_id', true) === $post_id):
                    if (!$changed_parent):
                        update_post_meta($id, '_is_child', false);
                        update_post_meta($id, '_is_parent', true);
                        delete_post_meta($id, '_parent_id');
                        $changed_parent = true;
                        $new_parent_id = $id;
                    else:
                        update_post_meta($id, '_parent_id', $new_parent_id);
                    endif;
                endif;
                unset($id);
            endforeach;
            update_post_meta($post_id, '_parent_id', $new_parent_id);
        endif;
        $result = wp_update_post(array('ID' => (int) $post_id, 'post_status' => 'Draft'));
        update_post_meta($post_id, '_stock_status', 'outofstock');
        return ($result) ? true : false;
    }

    private function warehouse_live_get_published_product_ids() {
        $ids = new WP_Query(array(
            'post_type' => 'product',
            'posts_per_page' => -1,
            "post_status" => "publish",
            'fields' => 'ids',
        ));
        return $ids->posts;
    }

    private function warehouse_live_pubish_product($post_id) {
        update_post_meta($post_id, '_stock_status', 'instock');
        $date = date("Y-m-d H:i:s");
        $result = wp_update_post(array('ID' => $post_id, 'post_status' => 'publish', 'post_date' => $date, 'post_date_gmt' => $date));
        return ($result) ? true : false;
    }

    public function warehouse_get_product_by_id($id) {
        require_once 'warehouse_db_class.php';
        $feed_db_class = new warehouse_database_class();
        $queryString = "SELECT * FROM `feed_products` WHERE `id` = " . $id;
        $result = $feed_db_class->warehouse_db_do_query($queryString);
        return ($result->num_rows == 1) ? mysqli_fetch_assoc($result) : false;
    }

    public function warehouse_get_cats() {
        $cleanarray = array();
        $parent_array = $this->warehouse_get_top_level_cat_names();
        foreach ($this->warehouse_get_all_cats() as $cat):
            $cleanarray[$cat->term_id] = $parent_array[mmj_cat_top_level_parent($cat->term_id)] . $this->warehouse_get_parent_name($cat->parent) . ' - ' . $cat->name;
        endforeach;
        return $cleanarray;
    }

    public function warehouse_get_cat_array() {
        $returnArray = array();
        foreach ($this->warehouse_get_top_level_cats() as $cat):
            ($cat->name != 'Search') ? $returnArray[$cat->term_id] = array('name' => $cat->name) : '';
        endforeach;
        foreach ($returnArray as $id => $value):
            foreach ($this->warehouse_get_children_cats($id) as $child):
                $returnArray[$id][$child->term_id] = array('name' => $child->name);
            endforeach;
        endforeach;
        foreach ($returnArray as $index => $value):
            foreach ($value as $id => $children):
                if ($id != 'name'):
                    foreach ($this->warehouse_get_children_cats($id) as $child):
                        $returnArray[$index][$id][$child->term_id] = array('name' => $child->name);
                    endforeach;
                endif;
            endforeach;
        endforeach;
        return $returnArray;
    }

    private function warehouse_get_top_level_cat_names() {
        $cats = $this->warehouse_get_all_cats();
        foreach ($cats as $cat):
            if (!$cat->parent):
                $parent_array[$cat->term_id] = $cat->name;
            endif;
        endforeach;
        return $parent_array;
    }

    private function warehouse_get_children_cats($id) {
        return get_categories(array('show_option_none' => '',
            'hide_empty' => 0,
            'parent' => $id,
            'taxonomy' => 'product_cat'));
    }

    private function warehouse_get_top_level_cats() {
        return get_categories(array('show_option_none' => '',
            'hide_empty' => 0,
            'parent' => 0,
            'taxonomy' => 'product_cat'));
    }

    private function warehouse_get_all_cats() {
        return get_categories(array('show_option_none' => '',
            'hide_empty' => 0,
            'taxonomy' => 'product_cat'));
    }

    private function warehouse_get_parent_name($child) {
        $cats = $this->warehouse_get_all_cats();
        foreach ($cats as $cat):
            if ($cat->term_id === $child):
                return ' - ' . $cat->name;
            endif;
        endforeach;
        return '';
    }

    public function warehouse_download_feed_image($vendor_id, $link) {
        $result = $this->warehouse_live_check_if_local_image($vendor_id, $link);
        if (!$result):
            switch (get_post_meta($vendor_id, '_feed_type', true)):
                case 'ftp':
                    $result = $this->warehouse_download_image_ftp($vendor_id, $link, WAREHOUSE_TEMP_FOLDER . $vendor_id . '/');
                    break;
                case 'live':
                    $result = $this->warehouse_get_image_from_link($link, WAREHOUSE_TEMP_FOLDER . $vendor_id . '/');
                    break;
            endswitch;
        endif;
        return $result;
    }

    private function warehouse_live_check_if_local_image($vendor_id, $link) {
        $link = explode('/', $link);
        $link = $link[count($link) - 1];
        return (file_exists(WAREHOUSE_TEMP_FOLDER . $vendor_id . '/' . $link)) ? WAREHOUSE_TEMP_FOLDER_URL . $vendor_id . '/' . $link : false;
    }

    public function warehouse_download_image_ftp($vendor_id, $link, $destFolder) {

        require_once 'warehouse_feed_class.php';
        $feed_class = new warehouse_feed_class();

        $result = false;

        if (get_post_meta($vendor_id, '_ftp_image_path', true)):
            $link = explode(get_post_meta($vendor_id, '_ftp_image_path', true), $link);
            $link = $link[count($link) - 1];
        endif;

        $wp_upload_dir = wp_upload_dir();

        if (!isset($destFolder) || empty($destFolder)):

            $destFolder = $wp_upload_dir['path'] . '/' . 'temp';

        endif;

        if (!file_exists($destFolder)) {
            mkdir($destFolder, 0775, true);
        }

        $dest = $destFolder . $link;
        $conn_id = ftp_connect(get_post_meta($vendor_id, '_ftp_host', true));
        $_result = ftp_login($conn_id, get_post_meta($vendor_id, '_ftp_username', true), get_post_meta($vendor_id, '_ftp_password', true));
        $_result = ftp_pasv($conn_id, true);
        if ($_result):
            if (get_post_meta($vendor_id, '_ftp_image_path', true)):
                ftp_chdir($conn_id, get_post_meta($vendor_id, '_ftp_image_path', true));
            endif;
            $result = ftp_get($conn_id, $dest, $link, FTP_BINARY);
            if ($result):
                $result = $destFolder . $link;
            endif;
        endif;
        ftp_close($conn_id);
        return $result;
    }

    public function warehouse_get_image_from_link($url, $destFolder) {

        ini_set('allow_url_fopen', 1);

        $saveto = explode('/', $url);

        $imageName = $saveto[count($saveto) - 1];

        $returnPath = WAREHOUSE_TEMP_FOLDER_URL . $saveto[count($saveto) - 2] . '/' . $imageName;

        if (!isset($destFolder) || empty($destFolder)):

            $wp_upload_dir = wp_upload_dir();
            $destFolder = $wp_upload_dir['path'] . '/' . 'temp/';
            $returnPath = $wp_upload_dir['url'] . '/' . 'temp/' . $imageName;

        endif;

        $saveto = $destFolder . $imageName;

        if (!file_exists($destFolder)) {
            mkdir($destFolder, 0775, true);
        }

        if (!file_exists($saveto)):

            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
            $raw = curl_exec($ch);
            curl_close($ch);
            $fp = fopen($saveto, 'x');
            fwrite($fp, $raw);
            fclose($fp);

        endif;

        return $returnPath;
    }

    public function warehouse_set_live_product_import($db_id) {
        $this->db->warehouse_db_do_query("UPDATE `feed_products` SET `imported` = '1' WHERE `feed_products`.`id` = " . $db_id);
    }

    public function warehouse_find_similar_products($code) {
        return ($this->db->warehouse_db_do_query("SELECT * FROM `feed_products` WHERE `Code` LIKE '$code'")->num_rows > 1) ? true : false;
    }

    public function warehouse_import_related_products($productData) {
        $returnArray = array();
        $products = $this->db->warehouse_db_do_query("SELECT * FROM `feed_products` WHERE `feed_id` NOT LIKE '" . $productData['db_id'] . "' AND `Code` LIKE '" . $productData['code'] . "' AND `imported` = 0");
        while ($product = mysqli_fetch_assoc($products)):
            $post_id = wp_insert_post(array('post_status' => "publish",
                'post_title' => $productData['title'],
                'post_type' => "product",
                'post_content' => $productData['ldescription'],
                'comment_status' => 'closed',
                'post_excerpt' => $productData['sdescription']));
            if ($post_id):
                update_post_meta($productData['parent_id'], '_is_parent', true);
                update_post_meta($post_id, '_is_child', true);
                update_post_meta($post_id, '_parent_id', $productData['parent_id']);
                if (isset($product['Barcode']) && !empty(isset($product['Barcode']))):
                    update_post_meta($post_id, '_sku', $product['Barcode']);
                    update_post_meta($post_id, '_barcode', $product['Barcode']);
                endif;
                if ($productData['brand']):
                    update_post_meta($post_id, '_brand', $productData['brand']);
                endif;
                if (isset($product["Wholesale Price"]) && !empty($product["Wholesale Price"])):
                    update_post_meta($post_id, '_wholesale_price', $product["Wholesale Price"]);
                endif;
                if ($productData['code']):
                    update_post_meta($post_id, '_code', $productData['code']);
                endif;
                if ($productData['price']):
                    update_post_meta($post_id, '_regular_price', $productData['price']);
                    update_post_meta($post_id, '_price', $productData['price']);
                endif;
                if ($productData['related_cat']):
                    update_post_meta($post_id, '_select_show_related_products_from_cat', $productData['related_cat']);
                endif;
                if ($productData['video']):
                    update_post_meta($post_id, 'youtube_link', $productData['video']);
                endif;
                if ($productData['vendor']):
                    update_post_meta($post_id, 'vendor', $productData['vendor']);
                endif;
                if (isset($product['Colour']) && !empty($product['Colour'])):
                    update_post_meta($post_id, 'st_colour', $product['Colour']);
                endif;
                if (isset($product['Size']) && !empty($product['Size'])):
                    update_post_meta($post_id, 'st_size', $product['Size']);
                endif;
                if ($productData['select_price_save'] == '1'):
                    update_post_meta($post_id, '_price_markup', $productData['price_markup']);
                endif;
                if ($productData['select_price_save']):
                    update_post_meta($post_id, '_select_price_save', $productData['select_price_save']);
                endif;
                if ($productData['_yoast_wpseo_metadesc']):
                    update_post_meta($post_id, '_yoast_wpseo_metadesc', $productData['_yoast_wpseo_metadesc']);
                endif;
                if ($productData['_yoast_wpseo_title']):
                    update_post_meta($post_id, '_yoast_wpseo_title', $productData['_yoast_wpseo_title']);
                endif;
                if ($productData['cats_array']):
                    wp_set_object_terms($post_id, $productData['cats_array'], 'product_cat');
                endif;
                set_post_thumbnail($post_id, $productData['attachment_id']);
                if ($productData['gallery_images']):
                    update_post_meta($post_id, '_product_image_gallery', $productData['gallery_images']);
                endif;
                if ($productData['tags']):
                    wp_set_post_terms($post_id, $productData['tags'], 'product_tag');
                endif;
                $this->warehouse_set_live_product_import($product['id']);
                $returnArray[$product['id']] = $product['id'];
            endif;
        endwhile;
        return $returnArray;
    }

}
