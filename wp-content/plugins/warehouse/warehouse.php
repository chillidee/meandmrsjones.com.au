<?php

/**
 * Plugin Name: Warehouse
 * Description: Warehouse plugin for Me & Mrs Jones Website.
 * Version: 1.0
 * Author: Chillidee - Matthew Dunn
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

function init() {
    define('WAREHOUSE_PLUGIN_PATH', plugin_dir_path(__FILE__));
    define('WAREHOUSE_PLUGIN_URL', plugin_dir_url(__FILE__));
    define('WAREHOUSE_TEMP_FOLDER', plugin_dir_path(__FILE__) . 'temp/');
    define('WAREHOUSE_TEMP_FOLDER_URL', plugin_dir_url(__FILE__) . 'temp/');
    define('WAREHOUSE_LOG_FOLDER', plugin_dir_path(__FILE__) . 'logs/');
    add_action('admin_menu', 'wpdocs_register_my_custom_menu_page');
    //add_action('add_meta_boxes', 'warehouse_register_meta_boxes');
    add_action('save_post', 'warehouse_add_page_save_gallery_images', 10, 1);
    //register_activation_hook(__FILE__, 'warehouse_cron_activation');
    //warehouse_cron_activation();
    require_once 'includes/ajax_functions.php';
}

function wpdocs_register_my_custom_menu_page() {
    require_once 'includes/warehouse_db_class.php';
    $warehouse_db = new warehouse_database_class();
    $warnings = $warehouse_db->warehouse_db_do_query("SELECT * FROM `notifications`");
    $warning_count = $warnings->num_rows;
    $warning_title = esc_attr(sprintf('%d new notifications', $warning_count));

    $menu_label = sprintf(__('Warehouse %s'), "<span class='update-plugins count-$warning_count' title='$warning_title'><span class='update-count warehouse-notes-total'>" . $warning_count . "</span></span>");
    $menu = add_menu_page(__('Warehouse', 'textdomain'), $menu_label, 'manage_options', 'warehouse/includes/warehouse-admin.php', '', '', 6);
    add_action("admin_print_styles-" . $menu, 'admin_scripts');

    $menu = add_submenu_page('warehouse/includes/warehouse-admin.php', 'Feeds', 'Feeds', 'manage_options', 'warehouse-feeds', 'warehouse_feeds_admin_page');
    add_action("admin_print_styles-" . $menu, 'admin_scripts');

    $menu = add_submenu_page('warehouse/includes/warehouse-admin.php', 'Vendors', 'Vendors', 'manage_options', 'warehouse-vendors', 'warehouse_vendors_admin_page');
    add_action("admin_print_styles-" . $menu, 'admin_scripts');    

    $menu = add_submenu_page('warehouse/includes/warehouse-admin.php', 'Import Products', 'Import Products', 'manage_options', 'warehouse-products', 'warehouse_products_admin_page');
    add_action("admin_print_styles-" . $menu, 'admin_scripts');
    
    //$menu = add_submenu_page('warehouse/includes/warehouse-admin.php', 'Featured Products', 'Featured Products', 'manage_options', 'featured-products', 'warehouse_featured_products_admin_page');
    //add_action("admin_print_styles-" . $menu, 'admin_scripts');

    $menu = add_submenu_page('warehouse/includes/warehouse-admin.php', 'Maintenance', 'Maintenance', 'manage_options', 'warehouse-maintenance', 'warehouse_maintenance_admin');
    add_action("admin_print_styles-" . $menu, 'admin_scripts');

    $menu = add_submenu_page('warehouse/includes/warehouse-admin.php', 'Settings', 'Settings', 'manage_options', 'settings', 'warehouse_settings_admin_page');
    add_action("admin_print_styles-" . $menu, 'admin_scripts');
}

function warehouse_settings_admin_page() {
    require_once 'includes/warehouse_settings.php';
}

function warehouse_featured_products_admin_page() {
    require_once 'includes/warehouse_pages_featured_admin.php';
}

function warehouse_vendors_admin_page() {
    require_once 'includes/vendors-admin.php';
}

function warehouse_products_admin_page() {
    require_once 'includes/warehouse_products_admin_page.php';
}

function warehouse_feeds_admin_page() {
    require_once 'includes/warehouse_feeds_admin_page.php';
}

function warehouse_maintenance_admin() {
    require_once 'includes/warehouse_maintenance_admin.php';
}

function admin_scripts() {
    wp_enqueue_style('warehouse', plugins_url() . '/warehouse/includes/css/warehouse.css', array(), '1.1');
    wp_enqueue_script('warehouse', plugin_dir_url(__FILE__) . '/includes/js/warehouse.js', array(), '1.1');
    wp_enqueue_style('bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css',array());    
}

function warehouse_cron_activation() {
    if (!wp_next_scheduled('warehouse_daily_cron_action')) {
        wp_schedule_event(time(), 'daily', 'warehouse_daily_cron_action');
    }
}

add_action('warehouse_daily_cron', 'warehouse_daily_cron_action');

register_deactivation_hook(__FILE__, 'warehouse_deactivation');

function warehouse_deactivation() {
    wp_clear_scheduled_hook('warehouse_daily_cron');
}

function warehouse_daily_cron_action() {
    require_once 'includes/warehouse_cron_jobs.php';
    new warehouse_cron_jobs();
}

function warehouse_register_meta_boxes() {
    $screen = get_current_screen();
    if ($screen->id == 'page'):
        add_meta_box('page_gallery_meta_box', __('Gallery', 'woocommerce'), 'WC_Meta_Box_Product_Images::output', 'page', 'side', 'low');
        wp_register_script('wc-admin-product-meta-boxes', WC()->plugin_url() . '/assets/js/admin/meta-boxes-product.min.js', array('wc-admin-meta-boxes', 'media-models'), WC_VERSION);
        wp_enqueue_script('wc-admin-product-meta-boxes');
    endif;
}

function warehouse_add_page_save_gallery_images($post_id) {
    $attachment_ids = isset($_POST['product_image_gallery']) ? array_filter(explode(',', wc_clean($_POST['product_image_gallery']))) : array();
    update_post_meta($post_id, '_product_image_gallery', implode(',', $attachment_ids));
}

function my_login_logo() {
    ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/header-logo.png);
            padding-bottom: 30px;
        }
    </style>
    <?php

}

add_action('login_enqueue_scripts', 'my_login_logo');

function my_login_stylesheet() {
    wp_enqueue_style('custom-login', WAREHOUSE_PLUGIN_URL . 'includes/css/login_style.css');
}

add_action('login_enqueue_scripts', 'my_login_stylesheet');

add_action('init', 'init');
