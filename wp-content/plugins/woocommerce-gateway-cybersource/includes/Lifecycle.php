<?php
/**
 * WooCommerce CyberSource
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce CyberSource to newer
 * versions in the future. If you wish to customize WooCommerce CyberSource for your
 * needs please refer to http://docs.woocommerce.com/document/cybersource-payment-gateway/
 *
 * @author      SkyVerge
 * @copyright   Copyright (c) 2012-2019, SkyVerge, Inc.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

namespace SkyVerge\WooCommerce\Cybersource;

defined( 'ABSPATH' ) or exit;

use SkyVerge\WooCommerce\PluginFramework\v5_4_0 as Framework;

/**
 * Plugin lifecycle handler.
 *
 * @since 1.9.0
 *
 * @method \WC_Cybersource get_plugin()
 */
class Lifecycle extends Framework\Plugin\Lifecycle {


	/**
	 * Lifecycle constructor.
	 *
	 * @since \WC_Cybersource 1.9.2
	 *
	 * @param $plugin
	 */
	public function __construct( $plugin ) {

		parent::__construct( $plugin );

		$this->upgrade_versions = [
			'1.1.1',
			'1.1.2',
			'1.2',
		];
	}


	/**
	 * Updates to version 1.1.1
	 *
	 * Standardizes debug_mode setting.
	 *
	 * @since 1.9.2
	 */
	protected function upgrade_to_1_1_1() {

		$settings = get_option( 'woocommerce_' . \WC_CyberSource::PLUGIN_ID . '_settings', [] );

		if ( $settings ) {

			// previous settings
			$log_enabled   = isset( $settings['log'] )   && 'yes' === $settings['log'];
			$debug_enabled = isset( $settings['debug'] ) && 'yes' === $settings['debug'];

			// logger -> debug_mode
			if ( $log_enabled && $debug_enabled ) {
				$settings['debug_mode'] = 'both';
			} elseif ( ! $log_enabled && ! $debug_enabled ) {
				$settings['debug_mode'] = 'off';
			} elseif ( $log_enabled ) {
				$settings['debug_mode'] = 'log';
			} else {
				$settings['debug_mode'] = 'checkout';
			}

			unset( $settings['log'], $settings['debug'] );

			// set the updated options array
			update_option( 'woocommerce_' . \WC_CyberSource::PLUGIN_ID . '_settings', $settings );
		}
	}


	/**
	 * Updates to version 1.1.2
	 *
	 * Standardizes enable_csc setting.
	 *
	 * @since 1.9.2
	 */
	protected function upgrade_to_1_1_2() {

		$settings = get_option( 'woocommerce_' . \WC_CyberSource::PLUGIN_ID . '_settings', [] );

		if ( $settings ) {

			$settings['enable_csc'] = ! isset( $settings['cvv'] ) || 'yes' === $settings['cvv'] ? 'yes' : 'no';

			unset( $settings['cvv'] );

			// set the updated options array
			update_option( 'woocommerce_' . \WC_CyberSource::PLUGIN_ID . '_settings', $settings );
		}
	}


	/**
	 * Updates to version 1.2
	 *
	 * Standardizes order meta names and values (with the exception of the cc expiration, which just isn't worth the effort at the moment).
	 *
	 * @since 1.9.2
	 */
	protected function upgrade_to_1_2() {
		global $wpdb;

		$settings = get_option( 'woocommerce_' . \WC_CyberSource::PLUGIN_ID . '_settings', [] );

		if ( $settings ) {

			// testmode -> environment
			if ( isset( $settings['testmode'] ) && 'yes' === $settings['testmode'] ) {
				$settings['environment'] = 'test';
			} else {
				$settings['environment'] = 'production';
			}
			unset( $settings['testmode'] );

			// cardtypes -> card_types
			if ( isset( $settings['cardtypes'] ) ) {
				$settings['card_types'] = $settings['cardtypes'];
			}
			unset( $settings['cardtypes'] );

			// salemethod -> transaction_type, 'AUTH_ONLY' -> 'authorization', 'AUTH_CAPTURE' -> 'charge'
			if ( isset( $settings['salemethod'] ) ) {
				$settings['transaction_type'] = 'AUTH_ONLY' === $settings['salemethod'] ? 'authorization' : 'charge';
			}
			unset( $settings['salemethod'] );

			// set the updated options array
			update_option( 'woocommerce_' . \WC_CyberSource::PLUGIN_ID . '_settings', $settings );
		}

		$wpdb->query( "UPDATE {$wpdb->postmeta} SET meta_key='_wc_cybersource_trans_id'         WHERE meta_key='_cybersource_request_id'" );

		$wpdb->query( "UPDATE {$wpdb->postmeta} SET meta_value='test'                           WHERE meta_key='_cybersource_orderpage_environment' AND meta_value='TEST'" );
		$wpdb->query( "UPDATE {$wpdb->postmeta} SET meta_value='production'                     WHERE meta_key='_cybersource_orderpage_environment' AND meta_value='PRODUCTION'" );
		$wpdb->query( "UPDATE {$wpdb->postmeta} SET meta_key='_wc_cybersource_environment'      WHERE meta_key='_cybersource_orderpage_environment'" );

		$wpdb->query( "UPDATE {$wpdb->postmeta} SET meta_value='visa'                           WHERE meta_key='_cybersource_card_type' AND meta_value='Visa'" );
		$wpdb->query( "UPDATE {$wpdb->postmeta} SET meta_value='mc'                             WHERE meta_key='_cybersource_card_type' AND meta_value='MasterCard'" );
		$wpdb->query( "UPDATE {$wpdb->postmeta} SET meta_value='amex'                           WHERE meta_key='_cybersource_card_type' AND meta_value='American Express'" );
		$wpdb->query( "UPDATE {$wpdb->postmeta} SET meta_value='disc'                           WHERE meta_key='_cybersource_card_type' AND meta_value='Discover'" );
		$wpdb->query( "UPDATE {$wpdb->postmeta} SET meta_key='_wc_cybersource_card_type'        WHERE meta_key='_cybersource_card_type'" );

		$wpdb->query( "UPDATE {$wpdb->postmeta} SET meta_key='_wc_cybersource_account_four'     WHERE meta_key='_cybersource_card_last4'" );

		// older entries will be in the form MM/YYYY
		$wpdb->query( "UPDATE {$wpdb->postmeta} SET meta_key='_wc_cybersource_card_expiry_date' WHERE meta_key='_cybersource_card_expiration'" );
	}


}
