<?php
/**
 * WooCommerce CyberSource
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce CyberSource to newer
 * versions in the future. If you wish to customize WooCommerce CyberSource for your
 * needs please refer to http://docs.woocommerce.com/document/cybersource-payment-gateway/
 *
 * @author      SkyVerge
 * @copyright   Copyright (c) 2012-2019, SkyVerge, Inc.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

defined( 'ABSPATH' ) or exit;

use SkyVerge\WooCommerce\PluginFramework\v5_4_0 as Framework;

/**
 * WooCommerce CyberSource Gateway main class.
 *
 * @since 1.0
 */
class WC_CyberSource extends Framework\SV_WC_Plugin {


	/** version number */
	const VERSION = '1.9.2';

	/** @var WC_CyberSource single instance of this plugin */
	protected static $instance;

	/** gateway id */
	const PLUGIN_ID = 'cybersource';

	/** class name to load as gateway, can be base or subscriptions class */
	const GATEWAY_CLASS_NAME = 'WC_Gateway_CyberSource';


	/**
	 * Initializes the main plugin class.
	 *
	 * @since 1.0
	 */
	public function __construct() {

		parent::__construct(
			self::PLUGIN_ID,
			self::VERSION,
			array(
				'text_domain'        => 'woocommerce-gateway-cybersource',
				'dependencies' => array(
					'php_extensions' => array(
						'soap',
						'dom',
					),
				),
			)
		);

		$this->load_classes();
	}


	/**
	 * Loads the gateway classes.
	 *
	 * @internal
	 *
	 * @since 1.0
	 */
	public function load_classes() {

		// CyberSource gateway
		require_once( $this->get_plugin_path() . '/includes/class-wc-gateway-cybersource.php' );

		// Add class to WC Payment Methods
		add_filter( 'woocommerce_payment_gateways', array( $this, 'load_gateway' ) );
	}


	/**
	 * Adds the gateway to the list of available payment gateways.
	 *
	 * @since 1.0
	 *
	 * @param string[]|array $gateways array of gateway names or objects
	 * @return string[]|array
	 */
	public function load_gateway( $gateways ) {

		$gateways[] = self::GATEWAY_CLASS_NAME;

		return $gateways;
	}


	/**
	 * Builds the lifecycle handler instance.
	 *
	 * @since 1.9.0
	 */
	protected function init_lifecycle_handler() {

		require_once( $this->get_plugin_path() . '/includes/Lifecycle.php' );

		$this->lifecycle_handler = new SkyVerge\WooCommerce\Cybersource\Lifecycle( $this );
	}


	/**
	 * Gets the plugin documentation url, which for CyberSource is non-standard.
	 *
	 * @since 1.2
	 *
	 * @return string documentation URL
	 */
	public function get_documentation_url() {

		return 'http://docs.woocommerce.com/document/cybersource-payment-gateway/';
	}


	/**
	 * Gets the plugin sales page URL.
	 *
	 * @since 1.9.0
	 *
	 * @return string
	 */
	public function get_sales_page_url() {

		return 'https://woocommerce.com/products/cybersource-payment-gateway/';
	}


	/**
	 * Gets the plugin support URL.
	 *
	 * @since 1.4.0
	 *
	 * @return string
	 */
	public function get_support_url() {

		return 'https://woocommerce.com/my-account/marketplace-ticket-form/';
	}


	/**
	 * Returns the admin configuration url for the gateway.
	 *
	 * @since 2.2.0-1
	 *
	 * @return string admin configuration url for the gateway
	 */
	public function get_payment_gateway_configuration_url() {

		return admin_url( 'admin.php?page=wc-settings&tab=checkout&section=' . self::PLUGIN_ID );
	}


	/**
	 * Determines if the current page is the gateway configuration page.
	 *
	 * @since 2.2.0
	 *
	 * @return bool
	 */
	public function is_payment_gateway_configuration_page() {

		return isset( $_GET['page'], $_GET['tab'], $_GET['section'] )
			   && 'wc-settings'   === $_GET['page']
			   && 'checkout'      === $_GET['tab']
			   && self::PLUGIN_ID === $_GET['section'];
	}


	/**
	 * Gets the gateway settings screen section ID.
	 *
	 * @since 1.6.0
	 * @deprecated 1.8.0
	 *
	 * TODO remove this method by version 1.10.0 or by February 2019, whichever comes first {FN 2019-01-04}
	 *
	 * @return string
	 */
	public function get_payment_gateway_configuration_section() {

		Framework\SV_WC_Plugin_Compatibility::wc_doing_it_wrong( 'WC_CyberSource::get_payment_gateway_configuration_section()', 'Deprecated! Use the plain gateway ID instead.', '1.8.0' );

		return strtolower( self::PLUGIN_ID );
	}


	/**
	 * Gets the gateway configuration URL.
	 *
	 * @since 1.2
	 *
	 * @param string|null $_ unused
	 * @return string plugin settings URL
	 */
	public function get_settings_url( $_ = null ) {

		return $this->get_payment_gateway_configuration_url();
	}


	/**
	 * Returns true if on the gateway settings page.
	 *
	 * @since 1.2
	 *
	 * @return bool
	 */
	public function is_plugin_settings() {

		return $this->is_payment_gateway_configuration_page();
	}


	/**
	 * Checks if required PHP extensions are loaded and SSL is enabled.
	 *
	 * Adds an admin notice if either check fails.
	 * Also gateway settings are checked as well.
	 *
	 * @internal
	 *
	 * @since 1.2.2
	 */
	public function add_delayed_admin_notices() {

		parent::add_delayed_admin_notices();

		// show a notice for any settings/configuration issues
		$this->add_ssl_required_admin_notice();
	}


	/**
	 * Shows admin notices.
	 *
	 * @internal
	 *
	 * @since 1.9.0
	 */
	public function add_admin_notices() {

		parent::add_admin_notices();

		if ( 'yes' === get_option( 'woocommerce_cybersource_show_timeout_notice', 'no' ) ) {

			$message = sprintf(
			/* translators: Placeholders: %1$s - <strong> tag, %2$s - </strong> tag, %3$s - <code> tag, %4$s - </code> tag */
				__( '%1$sCyberSource Gateway%2$s: A request to CyberSource timed out. Try increasing the value of %3$sdefault_socket_timeout%4$s in your PHP configuration to prevent future timeouts.', 'woocommerce-gateway-cybersource' ),
				'<strong>', '</strong>',
				'<code>', '</code>'
			);

			$this->get_admin_notice_handler()->add_admin_notice( $message, 'increase-socket-timeout', array(
				'notice_class' => 'notice-warning',
			) );
		}
	}


	/**
	 * Renders the SSL Required notice, as needed.
	 *
	 * @since 1.2.2
	 */
	private function add_ssl_required_admin_notice() {

		// check settings:  gateway active and SSl enabled
		$settings = get_option( 'woocommerce_cybersource_settings' );

		if (    isset( $settings['enabled'], $settings['environment'] )
			 && 'yes'        === $settings['enabled']
			 && 'production' === $settings['environment'] ) {

			// SSL check if gateway enabled/production mode
			if ( ! wc_checkout_is_https() ) {

				$message = sprintf(
					/* translators: Placeholders: %1$s - <strong> tag, %2$s - </strong> tag */
					__( '%1$sCyberSource Error%2$s: Your store is not using HTTPS; your customer\'s credit card data is at risk.', 'woocommerce-gateway-cybersource' ),
					'<strong>', '</strong>'
				);
				$this->get_admin_notice_handler()->add_admin_notice( $message, 'ssl-required', array(
					'notice_class' => 'error',
				) );
			}
		}
	}


	/**
	 * Returns the main CyberSource Instance.
	 *
	 * Ensures only one instance is/can be loaded.
	 *
	 * @since 1.3.0
	 *
	 * @return \WC_CyberSource
	 */
	public static function instance() {

		if ( null === self::$instance ) {

			self::$instance = new self();
		}

		return self::$instance;
	}


	/**
	 * Returns the plugin name, localized.
	 *
	 * @since 1.2
	 *
	 * @return string the plugin name
	 */
	public function get_plugin_name() {

		return __( 'WooCommerce CyberSource', 'woocommerce-gateway-cybersource' );
	}


	/**
	 * Returns __FILE__.
	 *
	 * @since 1.2
	 *
	 * @return string the full path and filename of the plugin file
	 */
	protected function get_file() {

		return __FILE__;
	}


}


/**
 * Returns the One True Instance of CyberSource.
 *
 * @since 1.3.0
 *
 * @return \WC_CyberSource
 */
function wc_cybersource() {

	return \WC_CyberSource::instance();
}
