=== WooCommerce CyberSource Gateway ===
Author: skyverge
Tags: woocommerce
Requires PHP: 5.4
Requires at least: 4.4
Tested up to: 5.1.1

Accept credit cards in WooCommerce with the CyberSource (SOAP) payment gateway

See https://docs.woocommerce.com/document/cybersource-payment-gateway/ for full documentation.

== Installation ==

1. Upload the entire 'woocommerce-gateway-cybersource' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
