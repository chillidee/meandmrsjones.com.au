<?php
add_action('wp_footer', 'my_action_javascript');
add_action('wp_ajax_change_subscribe_status', 'st_change_subscribe_status_callback');
add_action('wp_ajax_nopriv_change_subscribe_status', 'st_change_subscribe_status_callback');
add_action('wp_ajax_my_action', 'my_action_callback');
add_action('wp_ajax_nopriv_my_action', 'my_action_callback');
add_action('wp_ajax_sort_items', 'sort_items_callback');
add_action('wp_ajax_nopriv_sort_items', 'sort_items_callback');
add_action('wp_ajax_custom_login', 'custom_login_callback');
add_action('wp_ajax_nopriv_custom_login', 'custom_login_callback');
add_action('wp_ajax_check_user', 'check_user_name_callback');
add_action('wp_ajax_nopriv_check_user', 'check_user_name_callback');
add_action('wp_ajax_create_user', 'st_create_user_callback');
add_action('wp_ajax_nopriv_create_user', 'st_create_user_callback');
add_action('wp_ajax_st_authenticate_user', 'st_authenticate_user_callback');
add_action('wp_ajax_nopriv_st_authenticate_user', 'st_authenticate_user_callback');
add_action('wp_ajax_st_remove_item', 'st_remove_item_callback');
add_action('wp_ajax_nopriv_st_remove_item', 'st_remove_item_callback');
add_action('wp_ajax_st_add_item_to_cookie', 'st_add_item_to_cookie_wishlist_callback');
add_action('wp_ajax_nopriv_st_add_item_to_cookie', 'st_add_item_to_cookie_wishlist_callback');
add_action('wp_ajax_st_add_wishlist_to_cart', 'st_add_wishlist_to_cart_callback');
add_action('wp_ajax_nopriv_st_add_wishlist_to_cart', 'st_add_wishlist_to_cart_callback');
add_action('wp_ajax_st_get_cart_contents', 'st_get_cart_contents_callback');
add_action('wp_ajax_nopriv_st_get_cart_contents', 'st_get_cart_contents_callback');
add_action('wp_ajax_st_get_number_of_items_on_wishlist', 'st_get_number_of_items_on_wishlist_callback');
add_action('wp_ajax_nopriv_st_get_number_of_items_on_wishlist', 'st_get_number_of_items_on_wishlist_callback');
add_action('wp_ajax_st_get_full_archive_product', 'st_get_full_archive_product_callback');
add_action('wp_ajax_nopriv_st_get_full_archive_product', 'st_get_full_archive_product_callback');
add_action('wp_ajax_st_add_to_cart', 'st_add_to_cart_callback');
add_action('wp_ajax_nopriv_st_add_to_cart', 'st_add_to_cart_callback');
add_action('wp_ajax_st_populate_cart_overlay', 'st_populate_cart_overlay_callback');
add_action('wp_ajax_nopriv_st_populate_cart_overlay', 'st_populate_cart_overlay_callback');
add_action('wp_ajax_st_sort_products_new', 'st_sort_products_new_callback');
add_action('wp_ajax_nopriv_st_sort_products_new', 'st_sort_products_new_callback');
add_action('wp_ajax_st_get_more_products', 'st_get_more_products_callback');
add_action('wp_ajax_nopriv_st_get_more_products', 'st_get_more_products_callback');
add_action('wp_ajax_st_import_product', 'st_import_product_callback');
add_action('wp_ajax_nopriv_st_import_product', 'st_import_product_callback');
add_action('wp_ajax_st_put_excluded_product', 'st_put_excluded_product_callback');
add_action('wp_ajax_nopriv_st_put_excluded_product', 'st_put_excluded_product_callback');
add_action('wp_ajax_st_get_searched_products', 'st_get_searched_products_callback');
add_action('wp_ajax_nopriv_st_get_searched_products', 'st_get_searched_products_callback');
add_action('wp_ajax_st_get_cart_total', 'st_get_cart_total_callback');
add_action('wp_ajax_nopriv_st_get_cart_total', 'st_get_cart_total_callback');
add_action('wp_ajax_st_get_cart_total_price', 'st_get_cart_total_price_callback');
add_action('wp_ajax_nopriv_st_get_cart_total_price', 'st_get_cart_total_price_callback');
add_action('wp_ajax_send_contact_email', 'st_send_contact_email_callback');
add_action('wp_ajax_nopriv_send_contact_email', 'st_send_contact_email_callback');
add_action('wp_ajax_st_new_sort_products', 'st_get_more_products_callback');
add_action('wp_ajax_nopriv_st_new_sort_products', 'st_get_more_products_callback');
add_action('wp_ajax_st_get_address', 'st_get_address_callback');
add_action('wp_ajax_nopriv_st_st_get_address', 'st_get_address_callback');
add_action('wp_ajax_st_create_pdf', 'st_email_pdf_callback');
add_action('wp_ajax_nopriv_st_create_pdf', 'st_email_pdf_callback');
add_action('wp_ajax_st_get_postcodes', 'st_get_postcodes_callback');
add_action('wp_ajax_nopriv_st_get_postcodes', 'st_get_postcodes_callback');
add_action('wp_ajax_st_is_valid_order_number', 'st_is_valid_order_number_callback');
add_action('wp_ajax_nopriv_st_is_valid_order_number', 'st_is_valid_order_number_callback');
add_action('wp_ajax_st_get_order_items', 'st_get_order_items_callback');
add_action('wp_ajax_nopriv_st_get_order_items', 'st_get_order_items_callback');
add_action('wp_ajax_st_home_subscribe', 'st_home_subscribe_callback');
add_action('wp_ajax_nopriv_st_home_subscribe', 'st_home_subscribe_callback');
add_action('wp_ajax_st_image_download', 'st_image_download_callback');
add_action('wp_ajax_nopriv_st_image_download', 'st_image_download_callback');
add_action('wp_ajax_st_set_account_phone', 'st_set_account_phone_callback');
add_action('wp_ajax_nopriv_st_set_account_phone', 'st_set_account_phone_callback');
add_action('wp_ajax_st_get_product_reviews', 'st_get_product_reviews_callback');
add_action('wp_ajax_nopriv_st_get_product_reviews', 'st_get_product_reviews_callback');

function my_action_javascript() {
    ?>
    <script src="<?php echo str_replace('http:', 'https:', get_template_directory_uri()) ?>/js/ajax-functions.js" rel="nofollow" ></script> 
    <?php
}