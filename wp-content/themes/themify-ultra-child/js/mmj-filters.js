jQuery(function($){

  $(document).ready(function() {
      //if (window.location.href.indexOf("search-products") > -1) {        
        $('.wpf_form_advanced_product_filter .wpf_item_wpf_cat input[type=checkbox]').change(function() {
            toogleAdvancedFilters();
        });
        toogleAdvancedFilters();
      //}
      setPopupFilter();
  });

  function toogleAdvancedFilters() {

    toySize = false;
    toysColour = false;
    shoeSize = false;
    shoeHeelHeight = false;
    braSize = false;
    lingerieColour = false;
    apparelSize = false;
    accessoriesColour = false;
    fetishColour = false;


    $('.wpf_form_advanced_product_filter .wpf_item_wpf_cat').find('input[type=checkbox]').each(function( index ) {

      if($(this).is(':checked')){  

        termId = parseInt($( this ).attr('id').replace('wpf_advanced_product_filter_', ''));   

        if($.inArray( termId, [ 19, 38, 34, 36, 25, 100, 105, 106, 108, 1078, 324, 121, 109, 107, 230 ] ) == 1) {
          toySize = true;
          toysColour = true;
        }    
        if($.inArray( termId, [ 22, 271, 1021, 1030, 1052 ] ) == 1) {    	
          shoeSize = true;
          shoeHeelHeight = true;      
        }    
        if($.inArray( termId, [ 22, 63, 1018, 172, 1015 ] ) == 1) {
          braSize = true;
        }    
        if($.inArray( termId, [ 22, 66, 119, 1044, 63, 172, 1015, 1055, 1027, 1043 ] ) == 1) {
          lingerieColour = true;
        }    
        if($.inArray( termId, [ 22, 66, 119, 118, 63, 1015, 1032, 1027, 1043, 172, 1055 ] ) == 1) {
          apparelSize = true;
        }    
        if($.inArray( termId, [ 22, 1044, 120 ] ) == 1) {
          accessoriesColour = true;
        }    
        if($.inArray( termId, [ 20, 39, 321, 41, 40, 1058, 1061 ] ) == 1) {
          fetishColour = true;
        }
      } 
    });

    toySize ? $('.wpf_item_pa_toy-size').show() : $('.wpf_item_pa_toy-size').hide(), $('.wpf_item_pa_toy-size input[type=checkbox]').prop( "checked", false );
    toysColour ? $('.wpf_item_pa_toys-colour').show() : $('.wpf_item_pa_toys-colour').hide(), $('.wpf_item_pa_toys-colour input[type=checkbox]').prop( "checked", false );
    shoeSize ? $('.wpf_item_pa_shoe-size').show() : $('.wpf_item_pa_shoe-size').hide(), $('.wpf_item_pa_shoe-size input[type=checkbox]').prop( "checked", false );
    shoeHeelHeight ? $('.wpf_item_pa_shoe-heel-height').show() : $('.wpf_item_pa_shoe-heel-height').hide(), $('.wpf_item_pa_shoe-heel-height input[type=checkbox]').prop( "checked", false );
    braSize ? $('.wpf_item_pa_bra-size').show() : $('.wpf_item_pa_bra-size').hide(), $('.wpf_item_pa_bra-size input[type=checkbox]').prop( "checked", false );
    lingerieColour ? $('.wpf_item_pa_lingerie-colour').show() : $('.wpf_item_pa_lingerie-colour').hide(), $('.wpf_item_pa_lingerie-colour input[type=checkbox]').prop( "checked", false );
    apparelSize ? $('.wpf_item_pa_apparel-size').show() : $('.wpf_item_pa_apparel-size').hide(), $('.wpf_item_pa_apparel-size input[type=checkbox]').prop( "checked", false );
    accessoriesColour ? $('.wpf_item_pa_accessories-colour').show() : $('.wpf_item_pa_accessories-colour').hide(), $('.wpf_item_pa_accessories-colour input[type=checkbox]').prop( "checked", false );
    fetishColour ? $('.wpf_item_pa_fetish-colour').show() : $('.wpf_item_pa_fetish-colour').hide(), $('.wpf_item_pa_fetish-colour input[type=checkbox]').prop( "checked", false );

  }

  function setPopupFilter(){    
    setTimeout(function(){ 
      $('.popup-wrapper .wpf_item_name').slice(2).click(); 
      toogleAdvancedFilters();
    }, 3000);    
  }

  $(".top-search-icon" ).click(function() {
    $(".wpf_form_default_product_filter").submit();
  });

  $(document).bind('DOMSubtreeModified', function () {
    if($('.page-id-15616').length) {
      $('.wpf-pagination a').click(function() {
        window.scrollTo({ top: 0, behavior: 'smooth' });
      });
    }
  });

});