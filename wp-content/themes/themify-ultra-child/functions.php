<?php


/*
 * Add search bar and discreet packaging to the menu
 */
function theme_header_end() {
    echo do_shortcode('[themify_layout_part slug="menu-search-and-discreet-packaging"]');
};
add_action('themify_header_end', 'theme_header_end');

/*
 * Set product images from AWS into WordPress
 */
require_once 'ajax_functions.php';

function mmj_change_admin_featured_product_image( $content, $post_id ) {	
	$post = get_post($post_id); 	
	if ( 'product' == get_post_type($post) ) {     
        $content = str_replace( 'TB_iframe=1"', 'TB_iframe=1&amp;width=85%&amp;height=90%"', $content);
		$content = str_replace( 'Set product image', '<img width="266" height="266" src="'.mmj_get_product_image($post_id).'" class="attachment-266x266 size-266x266" alt="">Only available on Amazon S3', $content );		
	}
	return $content;
}
add_filter( 'admin_post_thumbnail_html', 'mmj_change_admin_featured_product_image', 10, 2 );

function mmj_change_admin_product_image_list( $column, $postid ) {
    if ( $column == 'thumb' ) {   
           $elem_id = uniqid(); 
		   if (!has_post_thumbnail($postid)){				
                echo '<img width="150" height="150" src="'.mmj_get_product_image($postid).'" class="attachment-thumbnail size-thumbnail wp-post-image '.$elem_id.'" alt="">';                		
                echo '<script>
                        jQuery(".'.$elem_id.'").siblings().first().html("");
                        jQuery(".'.$elem_id.'").appendTo(jQuery(".'.$elem_id.'").siblings().first());
                    </script>';
		   }
    }
}

add_action( 'manage_product_posts_custom_column', 'mmj_change_admin_product_image_list', 99, 2 );

function mmj_get_product_image($post_id) {
   
    if(!isset($image) || trim($image) === ''){ // Featured Image         
        $attachment = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'full' );
        if (is_array( $attachment ) || !($attachment )) {
            $image = $attachment[0];
        }
    }
    if(!isset($image) || trim($image) === ''){ 
        $image = get_post_meta($post_id, 'main_image_link', true); // AWS
    }
    if(!isset($image) || trim($image) === ''){ 
        $image = get_post_meta($post_id, 'post_image', true); // Leftover
    }
    if(!isset($image) || trim($image) === ''){  // Placeholder
        $image = 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/product-image-placeholder.jpg';
    }
    return $image;
}

function mmj_change_product_image_link( $html, $thumbnail_id) {
  $post = get_post();
  $product = wc_get_product( $post->ID );
  if ( is_product() && !$product->is_type('variable')){
	return '<img style="display: block;" class="attachment-full size-full wp-post-image main_image '.$post->ID.'" src="'.mmj_get_product_image($post->ID).'" alt="'.$product->get_title().'">';
  } else {
    return $html;
  }
}

add_filter('woocommerce_single_product_image_thumbnail_html', 'mmj_change_product_image_link', 10, 2);

/*
 * Get a list of all categories
 */

function mmj_get_all_categories() {
    $taxonomy = 'product_cat';
    $orderby = 'name';
    $args = array(
        'taxonomy' => $taxonomy,
        'orderby' => $orderby);
    $all_categories = get_categories($args);
    return $all_categories;
}

/*
 * Get categories top level parent |
 */

function mmj_cat_top_level_parent($cat_id) {
    $cat_obj = get_term($cat_id, 'product_cat');
    $parent_cat = '';
    if ($cat_obj->parent):
        while ($cat_obj->parent):
            if ($cat_obj->parent):
                $parent_cat = $cat_obj->parent;
            endif;
            $cat_obj = get_term($cat_obj->parent, 'product_cat');
        endwhile;
    else:
        $parent_cat = $cat_id;
    endif;
    return $parent_cat;
}

/*
 * Get the percentage markup from the category page
 */

function mmj_get_category_markup($catId) {
    if ($catId):
        $optionsArray = get_option("taxonomy_$catId");
        return $optionsArray["category_percentage_markup"];
    endif;
}

function mmj_get_category_meta($catId, $meta) {
    if ($catId):
        $optionsArray = get_option("taxonomy_$catId");
        return $optionsArray[$meta];
    endif;
}

/*
 * Add product custom meta to product admin
 */
function mmj_add_custom_product_general_fields() {

    // Not allowed for variations
    $post = get_post();
    $product = wc_get_product( $post->ID );

	if ( $product->is_type( 'variable' ) ) {
        return;
    }
    // ---------------------------------------

    global $woocommerce, $post;
    $cats_array = mmj_get_all_categories();
    $cats_options = array();
    foreach ($cats_array as $cat):
        $cats_options[$cat->term_id] = $cat->parent . ' - ' . $cat->name;
    endforeach;
    unset($cats_array);
    // Hide some WooCommerce unused fields
    echo '<style>#general_product_data .pricing, ._tax_status_field, ._tax_class_field, ._price_markup_field, ._cat_markup_field,
                 ._select_price_save_field, ._update_automaticaly_field, ._youtube_video_field, ._select_show_related_products_from_cat_field {display:none!important}</style>';
    echo '<div class="options_group">';
    woocommerce_wp_text_input(
            array(
                'id' => '_price',
                'label' => __('Price ($)', 'woocommerce'),
                'type' => 'float'
            )
    );
    woocommerce_wp_text_input(
            array(
                'id' => '_wholesale_price',
                'label' => __('Wholesale Price ($)', 'woocommerce'),
                'type' => 'float'
            )
    );
    woocommerce_wp_text_input(
            array(
                'id' => '_retail_price',
                'label' => __('Retail Price ($)', 'woocommerce'),
                'type' => 'float'
            )
    );
    woocommerce_wp_text_input(
            array(
                'id' => '_brand',
                'label' => __('Brand', 'woocommerce'),
            )
    );
    woocommerce_wp_text_input(
            array(
                'id' => '_code',
                'label' => __('Code', 'woocommerce'),
            )
    );
    $post_ID = $post->ID;
    woocommerce_wp_text_input(
            array(
                'id' => '_price_markup',
                'label' => __('Markup on Wholesale Price (%)', 'woocommerce'),
                'type' => 'number',
            )
    );
    $terms = get_the_terms($post_ID, 'product_cat');
    $cat_id = $terms[0]->term_id;
    woocommerce_wp_text_input(
            array(
                'id' => '_cat_markup',
                'label' => __('Category Markup (%)', 'woocommerce'),
                'value' => mmj_get_category_markup($cat_id)
            )
    );
    woocommerce_wp_text_input(
            array(
                'id' => '_st_size',
                'label' => __('Product Size', 'woocommerce'),
                'value' => get_post_meta($post_ID, 'st_size', true)
            )
    );
    woocommerce_wp_text_input(
            array(
                'id' => '_st_colour',
                'label' => __('Product Colour', 'woocommerce'),
                'value' => get_post_meta($post_ID, 'st_colour', true)
            )
    );
    woocommerce_wp_text_input(
            array(
                'id' => '_youtube_video',
                'label' => __('YouTube Video Link', 'woocommerce')
            )
    );
    woocommerce_wp_select(
            array(
                'id' => '_select_price_save',
                'label' => __('Set Price By:', 'woocommerce'),
                'type' => 'Dropdown Select',
                'options' => array(
                    0 => __('Category markup', 'woocommerce'),
                    1 => __('Manual markup', 'woocommerce'),
                    2 => __('Manual Price', 'woocommerce'),
                )
            )
    );
    woocommerce_wp_checkbox(array(
        'id' => '_update_automaticaly',
        'label' => __('Update Price If Category Markup Changed', 'woocommerce'),
            )
    );
    woocommerce_wp_select(
            array(
                'id' => '_select_show_related_products_from_cat',
                'label' => __('Show Related Categories From:', 'woocommerce'),
                'type' => 'Dropdown Select',
                'options' => $cats_options
            )
    );
    echo '</div>';
}

add_action('woocommerce_product_options_general_product_data', 'mmj_add_custom_product_general_fields', 10, 2);

/**
 * Save custom product fields
 */
function mmj_save_custom_product_general_fields($post_ID) {

    // Not allowed for variations       
    $product = wc_get_product( $post_ID );

    if ( $product->is_type( 'variable' ) ) {
        return;
    }
    // ---------------------------------------


    $saveType = (int) $_POST['_select_price_save'];
    $wholePrice = ($_POST['_wholesale_price']) ? (float) $_POST['_wholesale_price'] : (float) get_post_meta($post_ID, '_wholesale_price', true);
    if ($saveType == 2):
        update_post_meta($post_ID, '_select_price_save', 2);
        $newPrice = (float) $_POST['_price'];
        mmj_update_product_price($post_ID, $newPrice);
    endif;
    if ($saveType == 1):
        update_post_meta($post_ID, '_select_price_save', 1);
        $price_markup = (int) $_POST['_price_markup'];
        update_post_meta($post_ID, '_price_markup', $price_markup);
        mmj_update_product_price($post_ID, mmj_round_product_price($wholePrice, $price_markup));
    endif;
    if ($saveType == 0):
        $catMarkup = (int) $_POST['_cat_markup'];
        mmj_update_product_price($post_ID, mmj_round_product_price($wholePrice, $catMarkup));
        update_post_meta($post_ID, '_select_price_save', 0);
    endif;
    update_post_meta($post_ID, '_update_automaticaly', $_POST['_update_automaticaly']);
    update_post_meta($post_ID, '_select_show_related_products_from_cat', $_POST['_select_show_related_products_from_cat']);
    update_post_meta($post_ID, '_youtube_video', $_POST['_youtube_video']);
    update_post_meta($post_ID, 'st_size', $_POST['_st_size']);
    update_post_meta($post_ID, 'st_colour', $_POST['_st_colour']);
    update_post_meta($post_ID, '_retail_price', $_POST['_retail_price']);
    update_post_meta($post_ID, '_brand', $_POST['_brand']);
    update_post_meta($post_ID, '_price_markup', $_POST['_price_markup']);
    update_post_meta($post_ID, '_code', $_POST['_code']);
    update_post_meta($post_ID, '_wholesale_price', $wholePrice);
}

add_action('woocommerce_process_product_meta', 'mmj_save_custom_product_general_fields', 5);
add_action('publish_post', 'mmj_save_custom_product_general_fields', 5);


/*
 * Add product variations custom meta to product admin
 */
function variation_settings_fields_before_pricing( $loop, $variation_data, $variation ) {	
    
    // Text Field
	woocommerce_wp_text_input( 
		array( 
			'id'          => '_code[' . $variation->ID . ']', 
			'label'       => __( 'Code', 'woocommerce' ), 
			'placeholder' => '',		
            'value'       => get_post_meta( $variation->ID, '_code', true ),
            'wrapper_class' => 'form-row form-row-first'
		)
    );
    // Text Field
	woocommerce_wp_text_input( 
		array( 
			'id'          => '_brand[' . $variation->ID . ']', 
			'label'       => __( 'Brand', 'woocommerce' ), 
			'placeholder' => '',		
            'value'       => get_post_meta( $variation->ID, '_brand', true ),
            'wrapper_class' => 'form-row form-row-last'
		)
    );
}

function variation_settings_fields_after_pricing( $loop, $variation_data, $variation ) {	
    // Text Field
	woocommerce_wp_text_input( 
		array( 
			'id'          => '_wholesale_price[' . $variation->ID . ']', 
			'label'       => __( 'Wholesale Price', 'woocommerce' ),
			'placeholder' => '00.00',
			'desc_tip'    => 'true',
			'description' => __( 'This is the price of the product according to its vendor.', 'woocommerce' ),
            'value'       => get_post_meta( $variation->ID, '_wholesale_price', true ),
            'wrapper_class' => 'form-row form-row-first'
		)
    );
    // Text Field
	woocommerce_wp_text_input( 
		array( 
			'id'          => '_retail_price[' . $variation->ID . ']', 
			'label'       => __( 'Retail Price', 'woocommerce' ), 
			'placeholder' => '00.00',
			'desc_tip'    => 'true',
			'description' => __( 'This is the before price, which is showed with that line through.', 'woocommerce' ),
            'value'       => get_post_meta( $variation->ID, '_retail_price', true ),
            'wrapper_class' => 'form-row form-row-last'
		)
    );
}

function mmj_add_custom_javascript() {

    // Not allowed for non variations
    $post = get_post();
    $product = wc_get_product( $post->ID );

    if ( !$product->is_type( 'variable' ) ) {
        return;
    }
    
    // Get children product variation IDs in an array
    $children_ids = $product->get_children();

    if(count($children_ids) > 0) {

        // Get the first ID value
        $children_id = reset($children_ids); 
        // or 
        $children_id = $children_ids[0];

        // ---------------------------------------
        
        // Select variations tab by default
        echo '<script>$(document).ready(function(){0<$(".variations_options a").length&&$(".variations_options a").click()});</script>';
        
        // It selects and scroll to variation in order to facilitate editing       
        echo '<script>
                $(document).ready(function(){
                    setTimeout(function(){
                        var_header = $(".woocommerce_variation strong").filter(function(){return $(this).text()=="#'. $children_id .' "});
                        var_header.click();
                        $("html, body").stop().animate({ scrollTop: var_header.offset().top }, 500);                       
                    },1500)
                });
            </script>'; 
    }
}

add_action('woocommerce_product_options_general_product_data', 'mmj_add_custom_javascript', 10, 2);

// Add Variation Settings
add_action( 'woocommerce_variation_options', 'variation_settings_fields_before_pricing', 10, 3 );
add_action( 'woocommerce_variation_options_pricing', 'variation_settings_fields_after_pricing', 10, 3 );

/**
 * Add custom fields for variations
 *
*/
function load_variation_settings_fields( $variations ) {	
	// duplicate the line for each field
    $variations['code'] = get_post_meta( $variations[ 'variation_id' ], '_code', true );	
    $variations['brand'] = get_post_meta( $variations[ 'variation_id' ], '_brand', true );	
    $variations['wholesale_price'] = wc_price(get_post_meta( $variations[ 'variation_id' ], '_wholesale_price', true ));	
    $variations['retail_price'] = wc_price(get_post_meta( $variations[ 'variation_id' ], '_retail_price', true ));	
    $variations['regular_price'] = wc_price(get_post_meta( $variations[ 'variation_id' ], '_regular_price', true ));
	return $variations;
}

// Add New Variation Settings
add_filter( 'woocommerce_available_variation', 'load_variation_settings_fields' );

 /**
 * Save custom product variations fields
 */
function save_variation_settings_fields( $post_id ) {	
    // Text Field
	$wholesale_price = $_POST['_wholesale_price'][ $post_id ];
	if( ! empty( $wholesale_price ) ) {
		update_post_meta( $post_id, '_wholesale_price', esc_attr( $wholesale_price ) );
    }
    // Text Field
	$retail_price = $_POST['_retail_price'][ $post_id ];
	if( ! empty( $retail_price ) ) {
		update_post_meta( $post_id, '_retail_price', esc_attr( $retail_price ) );
    }
    // Text Field
	$brand_field = $_POST['_brand'][ $post_id ];
	if( ! empty( $brand_field ) ) {
		update_post_meta( $post_id, '_brand', esc_attr( $brand_field ) );
    }
    // Text Field
	$code = $_POST['_code'][ $post_id ];
	if( ! empty( $code ) ) {
		update_post_meta( $post_id, '_code', esc_attr( $code ) );
    }
}

// Save Variation Settings
add_action( 'woocommerce_save_product_variation', 'save_variation_settings_fields', 10, 2 );

/**
 * Update product price fields
 */
function mmj_update_product_price($post_id, $new_price) {
    global $wpdb;
    $wpdb->query("UPDATE `wp_postmeta` SET `meta_value` = " . $new_price . " WHERE `post_id` = " . $post_id . " AND meta_key = '_regular_price'");
    $wpdb->flush();
    $wpdb->query("UPDATE `wp_postmeta` SET `meta_value` = " . $new_price . " WHERE `post_id` = " . $post_id . " AND meta_key = '_price'");
    $wpdb->flush();
}

/**
 * Calculate product price rounded
 */
function mmj_round_product_price($wholesale_price, $percentage) {
    return number_format(round($wholesale_price + (($wholesale_price / 100) * $percentage), 0), 2);
}

/**
 * Change number of products per row to 4 or 3
 */
if (!function_exists('mmj_loop_columns')) {
	function mmj_loop_columns() {
        if(get_the_ID() == 15616) { //search-products
            return 3;    
        } else {
            return 4;        
        }
	}
}

add_filter('loop_shop_columns', 'mmj_loop_columns', 999);
add_filter('storefront_loop_columns', 'mmj_loop_columns', 999);


/**
 * Split category and products per row
 */
function mmj_split_product_categories( $args = array() ) {
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

    if($paged == 1) {
        $parentid = get_queried_object_id();         
        $args = array(
            'parent' => $parentid
        );     
        $terms = get_terms( 'product_cat', $args );     
        if ( $terms ) {  
            
            if(is_front_page() || is_home()) {
                echo '<ul class="product-cats products loops-wrapper grid3 masonry-done">';                  
            } else {
                echo '<ul class="product-cats products loops-wrapper grid4 masonry-done">';
            }
                foreach ( $terms as $term ) {                             
                    echo '<li class="category product-category product">';                
                        echo '<a href="' .  esc_url( get_term_link( $term ) ) . '" class="' . $term->slug . '">';
                            woocommerce_subcategory_thumbnail( $term );                     
                            echo '<h2>';                        
                                echo $term->name;                        
                            echo '</h2>';        
                        echo '</a>';                                                                 
                    echo '</li>';                                                                        
                }         
            echo '</ul>';     
        }
    }
}

add_action( 'woocommerce_before_shop_loop', 'mmj_split_product_categories', 0 );


/**
 * Don't show categories in the same row with products 
 */
remove_filter( 'woocommerce_product_loop_start', 'woocommerce_maybe_show_product_subcategories' );


/**
 * This snippet removes the action that inserts thumbnails to products in teh loop
 * and re-adds the function customized with our wrapper in it.
 * It applies to all archives with products.
 */
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);

/**
 * WooCommerce Loop Product Thumbs
 **/
 if ( ! function_exists( 'woocommerce_template_loop_product_thumbnail' ) ) {
	function woocommerce_template_loop_product_thumbnail() {
		echo woocommerce_get_product_thumbnail();
	} 
 }

/**
 * WooCommerce Product Thumbnail
 **/
 if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {
	
	function woocommerce_get_product_thumbnail( $size = 'full', $placeholder_width = 0, $placeholder_height = 0  ) {	
            global $post;		
			$output = '<div class="imagewrapper">';		
			$output .= '<img src="'.mmj_get_product_image($post->ID).'" width="300" height="300"/>';		
			$output .= '</div>';			
			return $output;
	}
 }
 
/**
 * Send order to vendors
 **/
add_action('woocommerce_payment_complete_order_status', 'mmj_send_vendor_orders', 10, 2);

function mmj_send_vendor_orders($order_status, $order_id) {  
    $order_forder = get_template_directory() . '-child/orders/';
    if (!file_exists( $order_forder )) {
        mkdir( $order_forder , 0775, true);
    }    
    $order = wc_get_order($order_id);    
    mmj_order_created($order);
    return $order_status;
}

function mmj_order_created($order) {
    $customer = array();
    $vendors = array();
    $billingAddress = array(
        'first_name' => $order->billing_first_name,
        'last_name' => $order->billing_last_name,
        'address_1' => $order->billing_address_1,
        'address_2' => $order->billing_address_2,
        'city' => $order->billing_city,
        'state' => $order->billing_state,
        'postcode' => $order->billing_postcode,
        'country' => $order->billing_country,
        'phone' => $order->billing_phone,
        'email' => $order->billing_email
    );
    $customer['billingAddress'] = $billingAddress;
    $billingAddress = array(
        'first_name' => $order->shipping_first_name,
        'last_name' => $order->shipping_last_name,
        'address_1' => $order->shipping_address_1,
        'address_2' => $order->shipping_address_2,
        'city' => $order->shipping_city,
        'state' => $order->shipping_state,
        'postcode' => $order->shipping_postcode,
        'country' => $order->shipping_country,
        'phone' => $order->shipping_phone
    );
    $customer['shippingAddress'] = $billingAddress;
    $customer['details'] = array(
        'order_number' => $order->id
    );

    $items = $order->get_items();   

    foreach ($items as $index => $item):

        $vendor = get_post_meta($item['product_id'], 'vendor', true);

        if(isset($item['variation_id']) && $item['variation_id'] > 0) {
            $customer['vendors'][$vendor][$item['variation_id']] = $item;            
        } else {	
            $customer['vendors'][$vendor][$item['product_id']] = $item;            
        }
    endforeach;    

    mmj_sendVendorEmails($customer);
}

function mmj_createOrderTextFile($vendor, $data) {
    $file = get_template_directory() . '-child/orders/2417.' . $data['details']['order_number'] . '.txt';    
    $content = "001\t2417\t" . $data['shippingAddress']['first_name'] . " " . $data['shippingAddress']['last_name'] . "\tMe & Mrs Jones\tinfo@meandmrsjones.com.au\t1300 780 182\t" . $data['shippingAddress']['address_1'] . "," . $data['shippingAddress']['address_2'] . "\t" . $data['shippingAddress']['city'] . "\t" . $data['shippingAddress']['state'] . "\t" . $data['shippingAddress']['postcode'] . "\tDrop Ship\t" . $data['details']['order_number'] . "\t2417\r\n";
      
    foreach ($data['vendors'] as $_vendor => $item):
        if ($_vendor == $vendor):
            foreach ($item as $_item):
                if(isset($_item['variation_id']) && $_item['variation_id'] > 0) {
                    $content .= "002\t" . get_post_meta($_item['variation_id'], '_code', true) . "\t" . $_item['name'] . "\t\t\t" . $_item['quantity'] . "\t\r\n";
                } else {	
                    //$content .= "002\t" . get_post_meta($_item['product_id'], '_code', true) . "\t" . $_item['name'] . "\t\t\t" . $_item['item_meta']['_qty'][0] . "\t\r\n";
                    $content .= "002\t" . get_post_meta($_item['product_id'], '_code', true) . "\t" . $_item['name'] . "\t\t\t" . $_item['quantity'] . "\t\r\n";
                }
            endforeach;
        endif;
    endforeach;
    $content .= "003\tAcct\t\t\t\t\t\t\t\r\n";
    $content .= "999\r";
    file_put_contents($file, $content);
    return $file;
}

function mmj_createSimpleOrderTextFile($vendor, $data) {
    $vendorTrim = strtolower(str_replace(' ', '', $vendor));
    $file = get_template_directory() . '-child/orders/' . $vendorTrim . "-" . $data['details']['order_number'] . '.txt';    
    $content = "New Order From MMJ\r\n\r\n";
    $content .= "Order number " . $data['details']['order_number'] . "\r\n\r\n";
    $content .= "Customer Details\r\n\r\n";
    $content .= "Billing Address\r\n\r\n";
    $content .= "{$data['billingAddress']['first_name']} {$data['billingAddress']['last_name']}\r\n{$data['billingAddress']['email']}\r\n{$data['billingAddress']['address_1']}\r\n {$data['billingAddress']['address_2']}\r\n {$data['billingAddress']['city']}\r\n {$data['billingAddress']['state']}\r\n {$data['billingAddress']['postcode']}\r\n\r\n";
    $content .= "Shipping Address\r\n\r\n";
    $content .= "{$data['shippingAddress']['first_name']} {$data['shippingAddress']['last_name']}\r\n {$data['shippingAddress']['address_1']}\r\n {$data['shippingAddress']['address_2']}\r\n {$data['shippingAddress']['city']}\r\n {$data['shippingAddress']['state']}\r\n {$data['shippingAddress']['postcode']}\r\n\r\n";
    $content .= "Item Details\r\n\r\n";
    foreach ($data['vendors'] as $_vendor => $item):
        if ($_vendor == $vendor):
            foreach ($item as $_item):
                if(isset($_item['variation_id']) && $_item['variation_id'] > 0) {
                    $content .= "Code: " . get_post_meta($_item['variation_id'], '_code', true) . ", Name: " . $_item['name'] . ", Size: " . get_post_meta($_item['variation_id'], 'st_size', true) . ", Colour: " . get_post_meta($_item['variation_id'], 'st_colour', true) . ", Quantity: " . $_item['quantity'] . ", Barcode: " . get_post_meta($_item['variation_id'], '_barcode', true) . "\r\n";
                } else {
                    $content .= "Code: " . get_post_meta($_item['product_id'], '_code', true) . ", Name: " . $_item['name'] . ", Size: " . get_post_meta($_item['product_id'], 'st_size', true) . ", Colour: " . get_post_meta($_item['product_id'], 'st_colour', true) . ", Quantity: " . $_item['item_meta']['_qty'][0] . ", Barcode: " . get_post_meta($_item['product_id'], '_barcode', true) . "\r\n";
                }                
            endforeach;
        endif;
    endforeach;
    file_put_contents($file, $content);
    return $file;
}

function mmj_sendVendorEmails($data) {
    $email_address = 'boni@chillidee.com.au';
    $subject = 'MMJ Order Placed';
    $headers = 'From: MMJ <info@meandmrsjones.com.au>' . "\r\n";
    $file = '';
    foreach ($data['vendors'] as $vendor => $items):
        switch ($vendor):
            case 'Windsor Wholeseller':
                // ----------
                $email_address = array('weborders@windsorwholesale.com.au', 'info@meandmrsjones.com.au', 'boni@chillidee.com.au');
                //$email_address = array('boni@chillidee.com.au');
                // ----------
                $file = mmj_createOrderTextFile($vendor, $data);
                if (!mmj_ftp_windsor_order($file)):
                    wp_mail($email_address, $subject, 'Please view attached order', $headers, $file);
                endif;
                break;
            case 'Wrapped Secrets':
                // ----------            
                $email_address = array('admin@wrappedsecrets.com.au', 'des@wrappedsecrets.com.au', 'wstracking@meandmrsjones.com.au', 'info@meandmrsjones.com.au', 'boni@chillidee.com.au');
                //$email_address = array('boni@chillidee.com.au');
                // ----------
                $file = mmj_createSimpleOrderTextFile($vendor, $data);
                wp_mail($email_address, $subject, 'Please view attached order', $headers, $file);
                break;
            default:
                $file = mmj_createSimpleOrderTextFile($vendor, $data);
                // ----------
                $email_address = array('info@meandmrsjones.com.au', 'boni@chillidee.com.au');
                //$email_address = array('boni@chillidee.com.au');
                // ----------                
                wp_mail($email_address, 'ALERT! Order not sent to vendor', 'ALERT! Order not sent to vendor', $headers, $file);                
                break;
        endswitch;
    endforeach;
}

function mmj_ftp_windsor_order($orderFile) {
    $result = false;
    $ftp_server = "windsorftp.dyndns.info";
    $ftp_user_name = "2417";
    $ftp_user_pass = "AP_2303";
    $conn_id = ftp_connect($ftp_server);
    $result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
    if ($result):
        $result = ftp_chdir($conn_id, '/webninja/');
        if ($result):
            $result = ftp_put($conn_id, basename($orderFile), $orderFile, FTP_ASCII);
        endif;
    endif;
    return $result;
}

//add_action('admin_enqueue_scripts', 'mmj_selectively_enqueue_admin_script');

apply_filters('wp_mail_from', 'info@meandmrsjones.com.au');
apply_filters('wp_mail_from_name', 'Me & Mrs Jones');

add_theme_support( 'woocommerce' );

add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );

/**
 * Set category filter when users are in category pages or copy filters from default filter to advanced filters
 **/
function set_category_filter() {   
    
    global $post;

    if (is_product_category()) {
        
        $terms = get_the_terms( $post->ID, 'product_cat' );  
                
        if($terms) {
            echo '<script>';
            echo "\r\n";
            echo 'jQuery(function($){';
            echo "\r\n";
            echo '      $(document).ready(function() {';
                echo "\r\n";
            foreach ( $terms as $term ) {
                echo '          $(":checkbox[value='.$term->slug.']").prop("checked","true");';
                echo "\r\n";
            }
            echo '      });';
            echo "\r\n";
            echo '  });';
            echo "\r\n";                
            echo '</script>';
            echo "\r\n";
        }
    }

    if($post->post_name == 'search-products') {

        if (isset($_REQUEST['id'])) {

            $id = $_REQUEST['id'];
        
            if($id == 'default_product_filter') {
        
                if (isset($_REQUEST['wpf_title'])) {
        
                    $wpf_title = $_REQUEST['wpf_title'];
        
                    echo '<script>';
                    echo "\r\n";
                    echo 'jQuery(function($){';
                    echo "\r\n";
                    echo '      $(document).ready(function() {';
                    echo '          $("input[name=wpf_name]").val("'. $wpf_title .'");';
                    echo "\r\n";

                    if (isset($_REQUEST['wpf_cat'])) {

                        $wpf_cat = $_REQUEST['wpf_cat'];
                        $terms = explode(',', $wpf_cat);

                        foreach ( $terms as $term ) {
                            echo '          $(":checkbox[value='.$term.']").prop("checked","true");';
                            echo "\r\n";
                        }                        
                    }

                    echo '      });';
                    echo "\r\n";
                    echo '  });';
                    echo "\r\n";                
                    echo '</script>';
                    echo "\r\n";                    
                }        
            }
        }     
    }

    echo '<script type="text/javascript" src="' . get_stylesheet_directory_uri() . '/js/mmj-filters.js' . '"></script>';   
 }

add_action( 'wp_footer', 'set_category_filter');

/**
 * Set category filter when users are in category pages or copy filters from default filter to advanced filters
 **/
add_filter( 'woocommerce_product_add_to_cart_text', function( $text ) {
	global $product;
	if ( $product->is_type( 'variable' ) ) {
		$text = $product->is_purchasable() ? __( 'ADD TO CART', 'woocommerce' ) : __( 'Read more', 'woocommerce' );
	}
	return $text;
}, 10 );



function pre_filter_products( $query ) {

    global $post;    
    
    session_start();  
    
    if( $post->post_name == 'search-products' && (isset($_REQUEST['brand']) || $_SESSION['filter_brand'])){ //$post->post_name == 'search-products'

        $brand = '';

        if(isset($_REQUEST['brand'])) {
            $brand =  $_REQUEST['brand'];            
        } else {
            $brand = $_SESSION['filter_brand'];
        }

        $_SESSION['filter_brand'] = $brand;

        $query->query_vars['meta_key'] = '_brand';
        $query->query_vars['meta_value'] = sanitize_text_field($brand);
        $query->query_vars['meta_compare'] = 'LIKE';
        
    }
    
}
  
add_action( 'pre_get_posts' , 'pre_filter_products' );

function clear_filter_session() {
    if(!is_page('search-products')) {
        session_start();
        $_SESSION['filter_brand'] = null;
    }
}

add_action( 'wp_head', 'clear_filter_session');
  