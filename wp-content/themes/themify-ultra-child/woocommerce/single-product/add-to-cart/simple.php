<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

echo wc_get_stock_html( $product ); // WPCS: XSS ok.

if ( $product->is_in_stock() ) : ?>	

	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>

		<!-- Quantity buttons section -->
		<span class="qty-label">QTY:</span>
		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
		
		<?php
		do_action( 'woocommerce_before_add_to_cart_quantity' );

		woocommerce_quantity_input( array(
			'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
			'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
			'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
		) );

		do_action( 'woocommerce_after_add_to_cart_quantity' );
		?>

		<?php /* echo do_shortcode('[themify_layout_part slug=single-product-accordions]'); */ ?>
		
		<!-- Delivery section -->
        <div class="clear">
			<ul class="ui module-accordion plus-icon-button transparent tf-init-accordion delivery-section">
				<li aria-expanded="false" class="">
					<div class="accordion-title">
						<a href="#">
							<div class="delivery-section-icon pull-left">
								<img src="https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/wp-content/uploads/20190520183901/discreet-packaging-icon-24x24px.png" alt="discreet-delivery" height="24" width="24"/>							
							</div>
							<span>DISCREET DELIVERY AUSTRALIA WIDE</span>                    
							<i class="accordion-icon fa fa-arrow-down"></i>
							<i class="accordion-active-icon fa fa-arrow-up"></i>
						</a>
					</div>
					<div class="accordion-content clearfix  default-closed" aria-expanded="false" style="display: none;">
						<p>All products are discreetly packaged with no direct mention of the nature of the products inside. We deliver within approximately 3-10 business days. We ship only within Australia.</p>
					</div>
				</li>
				<li aria-expanded="false" class="">
					<div class="accordion-title">
						<a href="#">	
							<div class="delivery-section-icon pull-left">						
								<img src="https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/wp-content/uploads/20190520183905/secure-payment-con-24x24px.png" alt="secure-payment" height="24" width="24"/>
							</div>	
							<span>100% SECURE PAYMENT</span>
							<i class="accordion-icon fa fa-arrow-down"></i>
							<i class="accordion-active-icon fa fa-arrow-up"></i>
						</a>
					</div>
					<div class="accordion-content clearfix  default-closed" aria-expanded="false" style="display: none;">
						<div>
							<p>The Me &amp; Mrs. Jones website uses 128-bit encryption to protect all personal information you may submit online. To safeguard your security, Me &amp; Mrs. Jones does not store any credit card details. Me &amp; Mrs. Jones will never disclose, sell or transfer any personal information provided by our clients to a third party, except as might be required by law.</p>
						</div>
					</div>
				</li>
			</ul>			
		</div>

		<!-- Add to cart button section -->
		<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
		
		<!-- Hint Hint section -->
		<div class="clear">
			<p class="hint-hint">Hint Hint</p>
			<div class="pull-left">	
				<?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
			</div>
			<div class="share-product pull-left">				
				<ul class="ui module-accordion">
					<li aria-expanded="false">
						<div class="accordion-title">
							<a href="#" class="share-product-link">						
								<button value="Share" class="share-product-button"><i class="fa fa-envelope"></i>SHARE</button>
							</a>
						</div>
						<div class="accordion-content clearfix  default-closed">
							<?php echo do_shortcode('[ssba-buttons]'); ?>
						</div>
					</li>
				</ul>
			</div>					
		</div>
		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>

<div class="clear product-information">
	<!-- Product description section -->
	<h4>PRODUCT INFORMATION</h4>

	<?php if ( $heading ) : ?>
	<h2><?php echo $heading; ?></h2>
	<?php endif; ?>

	<?php the_content(); ?>

	<!-- View guide line section -->
	<div class="want-explore-image pull-left">
		<img src="https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/wp-content/uploads/20190520183903/guides-icon-24x24px.png" alt="want-explore" height="24" width="24"/>
	</div>
	<div class="want-explore-text pull-left">
		<p>WANT TO EXPLORE MORE?<a href="<?php echo get_site_url() . '/sex-toy-guides/' ?>" target="_blank" class="want-explore-link"> View our guides here...</a></p>
	</div>
</div>