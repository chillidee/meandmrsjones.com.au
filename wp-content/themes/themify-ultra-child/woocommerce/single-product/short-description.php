<?php
/**
 * Single product short description
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/short-description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $post;

$brand_meta = get_post_meta($post->ID, '_brand', true);
$colour_meta = get_post_meta($post->ID, 'st_colour', true);
$size_meta = get_post_meta($post->ID, 'st_size', true);

if ($brand_meta): ?>
	<p><?php echo ($brand_meta) ? '<span class="brand-label">BRAND:</span> ' . $brand_meta : '' ?></p>	
<?php endif; ?>

<?php if ($colour_meta): ?>
	<p><?php echo ($colour_meta) ? '<span class="colour-label">COLOUR:</span> ' . $colour_meta : '' ?></p>	
<?php endif; ?>

<?php if ($size_meta): ?>
	<p><?php echo ($size_meta) ? '<span class="size-label">SIZE:</span> ' . $size_meta : '' ?></p>	
<?php endif; ?>

<?php
/*

$short_description = apply_filters( 'woocommerce_short_description', $post->post_excerpt );

if ( ! $short_description ) {
	return;
}

?>
<div class="woocommerce-product-details__short-description">
	<?php echo $short_description; // WPCS: XSS ok. ?>
</div>

*/ ?>