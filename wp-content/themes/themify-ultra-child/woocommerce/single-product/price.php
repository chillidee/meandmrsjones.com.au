<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>
<?php

$retail_price = null;

if ( $product->is_type( 'variable' ) ) {

	$children_ids = $product->get_children();

    if(count($children_ids) > 0) {
        // Get the first ID value
        $children_id = reset($children_ids); 
        // or 
		$children_id = $children_ids[0];

		$retail_price = get_post_meta($children_id, '_retail_price', true);
	}	
} else {
	$retail_price = get_post_meta($product->get_id(), '_retail_price', true);
}

if ($retail_price):
	?>
	<span class="retail_price_full">WAS $<?php echo $retail_price ?></span>
<?php endif; ?>
<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) );?>">NOW <?php echo $product->get_price_html(); ?></p>
