<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 */
get_header(); ?>
     <div class ='container'>

			<?php while ( have_posts() ) : the_post(); ?>
				<h1><?php the_title(); ?></h1>
			        <p><?php the_content(); ?></p>
			<?php endwhile; // end of the loop. ?>

      </div><!-- .container -->
<?php/* get_sidebar(); */?>
<?php get_footer(); ?>