<?php
/**
 * Empty cart page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-empty.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.0.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
?>
<div class="row">
    <div class="sub-cat-background-image sub-cat-background-image-gray">
    </div>
    <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 content-404-wrapper archive-products sub-cat-background-image-gray">
        <div class="col-lg-12 sub-cat-background-image-gray">
            <h1 class="heading-404"><?php _e('Your cart is currently empty.', 'woocommerce') ?></h1>
            <h2 class="col-lg-12">Here are some items we think you'll love.</h2>
        </div>
        <div class="col-lg-12 archive-product-wrapper">
            <h3>Most Popular</h3>
            <?php
            $posts = get_most_popular_products(5);
            if ($posts->have_posts()):
                while ($posts->have_posts()): $posts->the_post();
                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 archive-product-container">
                        <div class="archive-product-container-inner">
                            <a href="<?php the_guid() ?>">
                                <div class="col-lg-12 col-md-12 col-sm-12 archive-product-image">
                                    <?php
                                    $product = new WC_Product(get_the_ID());
                                    if (!get_post_meta(get_the_ID(), 'main_image_link', true)):
                                        the_post_thumbnail();
                                    else:
                                        ?>
                                        <img class="attachment-thumb size-thumb wp-post-image" src="<?php echo get_post_meta(get_the_ID(), 'main_image_link', true) ?>"alt="<?php the_title() ?>"/>
                                    <?php endif;
                                    ?>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 archive-product-details">
                                    <div class="col-lg-12 col-sm-12 archive-product-title">
                                        <h4><?php the_title(); ?></h4>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding product-price-button-container">
                                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 archive-product-price">
                                        <?php $retail_price = get_post_meta(get_the_ID(), '_retail_price', true); ?>
                                        <p class="retail_price"><?php echo ($retail_price) ? 'Don\'t pay $' . number_format((float) $retail_price, 2) : '' ?></p>
                                        <p class="price">Now $<?php echo number_format((float) get_post_meta(get_the_ID(), '_price', true), 2); ?></p>
                                    </div>                    
                                </div>                                
                            </a>
                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 no-padding">
                                <div class="col-sm-12 no-padding">
                                    <button id="<?php echo get_the_ID() ?>" class="btn archive-product-container-button add-to-cart-ajax" type="button" onclick="addToCartAjax(this);">Add to Cart</button>                            
                                </div>
                                <div class="col-sm-12 no-padding">    
                                    <button id="<?php echo get_the_ID() ?>" type="button" class="btn archive-product-container-button buy-now" onclick="addToCartAjax(this,'<?php echo WC_Cart::get_checkout_url(); ?>')">Buy Now</button>
                                </div>
                            </div> 
                        </div>
                    </div>                    
                    <?php
                endwhile;
            endif;
            ?>
            <h3>Newest</h3>
            <?php
            $posts = get_newest_products(5);
            if ($posts->have_posts()):
                while ($posts->have_posts()): $posts->the_post();
                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 archive-product-container">
                        <div class="archive-product-container-inner">
                            <a href="<?php the_guid() ?>">
                                <div class="col-lg-12 col-md-12 col-sm-12 archive-product-image">
                                    <?php
                                    $product = new WC_Product(get_the_ID());
                                    if (!get_post_meta(get_the_ID(), 'main_image_link', true)):
                                        the_post_thumbnail();
                                    else:
                                        ?>
                                        <img class="attachment-thumb size-thumb wp-post-image" src="<?php echo get_post_meta(get_the_ID(), 'main_image_link', true) ?>"alt="<?php the_title() ?>"/>
                                    <?php endif;
                                    ?>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 archive-product-details">
                                    <div class="col-lg-12 col-sm-12 archive-product-title">
                                        <h4><?php the_title(); ?></h4>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding product-price-button-container">
                                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 archive-product-price">
                                        <?php $retail_price = get_post_meta(get_the_ID(), '_retail_price', true); ?>
                                        <p class="retail_price"><?php echo ($retail_price) ? 'Don\'t pay $' . number_format((float) $retail_price, 2) : '' ?></p>
                                        <p class="price">Now $<?php echo number_format((float) get_post_meta(get_the_ID(), '_price', true), 2); ?></p>
                                    </div>                    
                                </div>                                
                            </a>
                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 no-padding">
                                <div class="col-sm-12 no-padding">
                                    <button id="<?php echo get_the_ID() ?>" class="btn archive-product-container-button add-to-cart-ajax" type="button" onclick="addToCartAjax(this);">Add to Cart</button>                            
                                </div>
                                <div class="col-sm-12 no-padding">    
                                    <button id="<?php echo get_the_ID() ?>" type="button" class="btn archive-product-container-button buy-now" onclick="addToCartAjax(this,'<?php echo WC_Cart::get_checkout_url(); ?>')">Buy Now</button>
                                </div>
                            </div> 
                        </div>
                    </div>                    
                    <?php
                endwhile;
            endif;
            ?>
        </div>
        <?php /*
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
            <h3>Featured</h3>
            <?php
            $posts = st_get_taged_products(4, 'featured');
            if ($posts->have_posts()):
                while ($posts->have_posts()): $posts->the_post();
                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <a href="<?php the_guid() ?>">
                            <?php
                            $terms = wp_get_post_terms(get_the_ID(), 'product_cat');
                            $cat_guid = st_get_category_guid($terms[0]->term_id);
                            $product = new WC_Product(get_the_ID());
                            if (!get_post_meta(get_the_ID(), 'main_image_link', true)):
                                the_post_thumbnail();
                            else:
                                ?>
                                <img class="attachment-thumb size-thumb wp-post-image" src="<?php echo get_post_meta(get_the_ID(), 'main_image_link', true) ?>"alt="<?php the_title() ?>"/>
                            <?php endif;
                            ?>
                            <h4><?php the_title(); ?></h4>
                            <h3 class="price related-price"><?php echo $product->get_price_html(); ?></h3>
                        </a>
                    </div>
                    <?php
                endwhile;
            endif;
            unset($posts);
            ?>
        </div>
        */ ?>        
    </div>
</div>
<?php
get_footer();
?>