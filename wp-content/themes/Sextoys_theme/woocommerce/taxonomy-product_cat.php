<?php
get_header();
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
$slug = explode('/', $_SERVER["REQUEST_URI"]);
$slug = $slug[count($slug) - 2];
if (isset($_GET["header-search"])):
    $search_term = strip_tags($_GET["header-search"]);
    $search_query = st_product_search($search_term);
    ?>
    <script>
        if (typeof ga !== 'undefined') {
            ga('send', 'event', 'Search', 'Search', 'User Search', "<?php echo $search_term ?>");
        }
        if (typeof fbq !== 'undefined') {
            fbq('track', 'Search');
        }
    </script>
    <?php
    unset($search_term);
endif;
global $post;
$cat_object = get_queried_object();
$apparelCatIdArrays = st_get_apparel_cats();
if (isset($cat_object)):
    $image_array = st_get_product_background_image($cat_object->term_id);
    $children = st_cat_get_children($cat_object->term_id);
    $parent_cat_object = st_get_cat_object($cat_object->parent);
    $cat_id = $cat_object->term_id;
endif;
if (isset($cat_id)):
    $parent_cat = st_get_cat_object(st_cat_top_level_parent($cat_id));
endif;
if (isset($parent_cat)):
    $parent_children = st_cat_get_children($parent_cat->term_id);
endif;
$active_path = $_SERVER["REQUEST_SCHEME"] . '://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
$catSlug = $cat_object->name;
?>
<input id="archive_category_number" type="hidden" value="<?php echo $cat_object->term_id ?>"/>
<div class="row">
    <div class="col-sm-12 col-xs-12 col-md-12 mob-only">
        <button class="archive-mobile-back-button archive-full-mobile-back-button" type="button"><i class="fa fa-chevron-left" aria-hidden="true"></i>Back</button>
    </div>
    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class = "col-lg-12 prodct-cat-background-image" style = "background-image: url(<?php echo $image_array[0] ?>)"></div>
        <?php
        unset($image_array);
        $prevoius_page = $_SERVER['HTTP_REFERER'];
        ?>
        <div id="archive-sidebar" class="col-lg-2 archive-sidebar for-her-sidebar title-sidebar">
            <?php
            if (!isset($search_query)):
                include get_template_directory() . '/partials/all_products_sidebar.php';
            else:
                include get_template_directory() . '/partials/all_products_sidebar.php';
            endif;
            ?>
        </div>
        <?php
        if ($children && !isset($_POST["cat-number"]) && !st_is_third_tier_cat($cat_object)):
            include_once 'partials/archive-child-titles.php';
        else:
            if (!isset($search_query)):
                include_once 'partials/archive-products-list.php';
            else:
                include_once 'partials/archive-products-search.php';
            endif;
            unset($search_query);
        endif;
        unset($cat_object);
        unset($children);
        unset($parent_cat_object);
        ?>
    </div>
</div>
</div>
<?php
$cat_object = get_queried_object();
if (get_field('column_1_title', $cat_object)):
    ?>
    <div class="col-lg-10 col-lg-offset-2 col-md-offset-1 col-md-10 col-sm-12 col-xs-12 below-fold-content" title="About Us">
        <div class="home-middle-logo-container">
            <img src="https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/header-logo.png" alt="Me &amp; Mrs Jones"/>
            <h2 class="home-page-slogan">Adult Boutique</h2>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 below-fold-content-text-wrapper">
            <h1><?php the_field('column_1_title', $cat_object) ?></h1>
            <p><?php the_field('column_1_text', $cat_object); ?></p>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 below-fold-content-text-wrapper">
            <h2><?php the_field('column_2_title', $cat_object) ?></h2>
            <p><?php the_field('column_2_text', $cat_object); ?></p>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 below-fold-content-text-wrapper">
            <h3><?php the_field('column_3_title', $cat_object) ?></h3>
            <p><?php the_field('column_3_text', $cat_object); ?></p>
        </div>
    </div>
    <?php
endif;
wp_reset_postdata();
get_footer();
?>