<?php
$url = explode('/', $_SERVER["REQUEST_URI"]);
?>
<div id="archive-sidebar" class="col-lg-2 archive-sidebar <?php
echo ($parent_cat->slug) ? $parent_cat->slug . '-sidebar' : '';
echo ($children && !st_is_third_tier_cat($cat_object) && !isset($_POST["cat-number"])) ? ' title-sidebar' : '';
?> product-sidebar">
     <?php if (isset($parent_cat->name)): ?>
        <h2><?php echo $parent_cat->name ?></h2>
        <?php
    endif;
    if ($parent_children):
        ?>
        <ul class="parent-sidebar-sub-menu" id="sidebar-menu-wrapper">
            <?php
            foreach ($parent_children as $child):
                $child_children = st_cat_get_children($child->term_id);
                ?>
                <li class="<?php
                echo ($child_children) ? 'parent-sidebar-sub-menu-has-child' : 'parent-sidebar-sub-menu-no-child';
                echo ($cat_object->parent == $child->term_id || $parent_cat_object->parent == $child->term_id || strtolower($url[count($url) - 2]) == strtolower($child->slug)) ? ' sub-menu-open' : '';
                ?> parent-sidebar"><a class="archive-cat-title" href="<?php echo st_get_category_guid($child->term_id); ?>">
                    <?php echo $child->name; ?>
                    <?php if ($child_children):
                        ?></a>
                        <ul class="sidebar-sub-menu">
                            <?php
                            foreach ($child_children as $child_sub):
                                ?>
                                <li>
                                    <a <?php echo ($child_sub->name == $catSlug || $child_sub->term_id == $cat_object) ? 'class="active-category"' : ''; ?> href="<?php echo st_get_category_guid($child_sub->term_id) ?>"><?php echo $child_sub->name; ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif;
                    ?>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>