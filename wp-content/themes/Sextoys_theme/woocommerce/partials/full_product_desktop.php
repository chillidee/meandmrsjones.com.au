<?php
$brand_meta = get_post_meta($_product_id, '_brand', true);
if (st_is_size_chart_for_brand($brand_meta)):
    ?>
    <div class="archive-size-guide-overlay footer-overlay">
        <?php st_get_size_chart_for_brand($brand_meta) ?>
    </div>
<?php endif;
?>
<div class="col-lg-8 col-md-6 col-sm-12 col-xs-12 archive-right-col-product-text archive-full-product-bottom-text no-padding">
    <div class="col-lg-12 col-sm-12 col-xs-12"><h1><?php echo $post->post_title ?></h1></div>      
    <div class="col-lg-6 col-sm-12 col-xs-12 price"><h4>
            <?php
            $retail_price = get_post_meta($_product_id, '_retail_price', true);
            if ($retail_price):
                ?>
                <span class="retail_price_full">Retail Price $<?php echo $retail_price ?> AUD</span>
            <?php endif; ?>
            <span class="amount">Our Price <?php echo strip_tags($_product->get_price_html() . ' AUD'); ?>
            </span></h4></div>
    <div class="col-lg-12 size-guide-link-container"><?php if (st_is_size_chart_for_brand($brand_meta)): ?> <p class="size-guide-link">What size am i?</p> <?php endif; ?></div>
    <?php if ($brand_meta): ?>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <div class="col-lg-8 col-md-12 col-xs-12 col-sm-12 product-size-container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-md-offset-0 col-sm-offset-4 col-xs-offset-2 product-persifics-label no-padding"><p><?php echo ($brand_meta) ? 'Brand' : '' ?></p></div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-8 col-md-offset-0 col-sm-offset-4 col-xs-offset-2 product-persifics-dropdown no-padding">
                    <input class="single-attribute" type="text" disabled readonly="true" value="<?php echo $brand_meta ?>" />
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php
    if ($_product_colour != ''):
        $colour_array = array();
        $colour_array[$_product_id] = $_product_colour;
        ?>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">

            <div class="col-lg-8 col-md-12 col-xs-12 col-sm-12 product-size-container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-md-offset-0 col-sm-offset-4 col-xs-offset-2 product-persifics-label no-padding"><p><?php echo ($colour_array[$_product_id] != '') ? 'Colour' : '' ?></p></div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-8 col-md-offset-0 col-sm-offset-4 col-xs-offset-2 product-persifics-dropdown no-padding">
                    <?php
                    if (count($associated_products) > 0):
                        foreach ($associated_products as $product):
                            $associated_product_colour = get_post_meta($product, 'st_colour', true);
                            if (!in_array($associated_product_colour, $colour_array, true) && $_product_colour && get_post_status($product) == 'publish'):
                                $colour_array[$product] = $associated_product_colour;
                            endif;
                        endforeach;
                    endif;
                    if (count($colour_array) > 1):
                        ?>
                        <select class="product-option-dropdown product-option-dropdown-st_colour" id="product-option-dropdown-st_colour">
                            <?php foreach ($colour_array as $index => $product): ?>
                                <option <?php echo ($product == $_product_colour) ? 'selected' : '' ?> value="<?php echo $index ?>"><?php echo $product ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php
                    else:
                        if ($colour_array[$_product_id] != ''):
                            ?>
                            <input class="single-attribute" type="text" disabled readonly="true" value="<?php echo $colour_array[$_product_id] ?>" />
                            <?php
                        endif;
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <?php
    endif;
    if ($_product_size != ''):
        $size_array = array();
        $size_array[$_product_id] = $_product_size;
        ?>
        <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="col-lg-8 col-md-12 col-xs-12 col-sm-12 product-size-container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-md-offset-0 col-sm-offset-4 col-xs-offset-2 product-persifics-label no-padding"><p><?php echo ($size_array[$_product_id] != '') ? 'Size' : '' ?></p></div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-8 col-md-offset-0 col-sm-offset-4 col-xs-offset-2 product-persifics-dropdown no-padding">
                    <?php
                    if (count($associated_products) > 0):
                        foreach ($associated_products as $product):
                            $associated_product_colour = get_post_meta($product, 'st_colour', true);
                            if (strtolower($associated_product_colour) == strtolower($_product_colour)):
                                $associated_product_size = get_post_meta($product, 'st_size', true);
                                if ($associated_product_size != $_product_size && get_post_status($product) === 'publish'):
                                    $size_array[$product] = $associated_product_size;
                                endif;
                            endif;
                        endforeach;
                    endif;
                    if (count($size_array) > 1):
                        uasort($size_array, 'st_sort_size_array_comparison');
                        ?>
                        <select class="product-option-dropdown product-option-dropdown-st_size" id="product-option-dropdown-st_size">
                            <?php foreach ($size_array as $index => $product): ?>
                                <option <?php echo ($product == $_product_size) ? 'selected' : '' ?> value="<?php echo $index ?>"><?php echo $product ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php
                    else:
                        if ($size_array[$_product_id] != ''):
                            ?>
                            <input class="single-attribute" disabled type="text" readonly="true" value="<?php echo $size_array[$_product_id] ?>" />
                            <?php
                        endif;
                    endif;
                    ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <div class="col-lg-8 col-md-12 col-xs-12 col-sm-12 col-md-offset-0 col-sm-offset-4 col-xs-offset-2 archive-add-to-cart-container">
            <div class="qty-fields"><p>QTY</p></div>
            <div class="qty-fields"><i id="quantity-decrease" class="fa fa-minus quantity-decrease"></i></div>
            <div class="qty-fields"><input id="quantity-total" class="quantity-total" value="1"></div>
            <div class="qty-fields"><i id="quantity-increase" class="fa fa-plus quantity-increase"></i></div>
        </div>
    </div>
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <div class="col-lg-8 col-sm-8 col-xs-8 col-md-8 col-md-offset-0 col-sm-offset-4 col-xs-offset-2 cart no-padding">
            <button id="<?php echo $post->ID ?>-ajax-to-cart" class="add-to-cart-ajax" type="button">Add to Cart</button>
        </div>
        <div class="col-lg-8 col-sm-8 col-xs-8 col-md-8 col-md-offset-0 col-sm-offset-4 col-xs-offset-2 cart no-padding">
            <button class="buy-now-product" type="button" onclick="window.location.href='<?php echo WC_Cart::get_checkout_url(); ?>'">Buy Now</button>            
        </div>        
    </div>
    <?php st_delivery_information(); ?>
    <div class="col-lg-12 col-md-12 col-xs-8 col-sm-8 col-md-offset-0 col-sm-offset-4 col-xs-offset-2"><h5>Product Information</h5></div>
    <!-- <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12"><h4>Product Description</h4></div> -->
    <div class="col-lg-12 col-md-12 col-xs-8 col-sm-8 col-md-offset-0 col-sm-offset-4 col-xs-offset-2"><p class="product-desciption-text"><?php echo $post->post_content ?></p></div>

    <?php
    if (reviews_class::hasProductReviews($_product_id)):
        wp_enqueue_style('stars-products');
        ?>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12"><h5>Reviews</h5></div>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 full-product-reviews-container">
            <i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>
            <span class="sr-only">Loading...</span>
        </div>
    <?php endif;
    ?>

</div>