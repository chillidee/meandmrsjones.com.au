<?php
$offset = 48;
$products = array();
if (!$post_cat_id_array):
    $post_cat_id_array = st_get_product_category($search_query);
endif;
$products[1] = $post_cat_id_array;
wp_enqueue_style('stars-products');
?>
<!--<div id="archive-display-product" class="archive-display-product col-xs-12 col-sm-12 col-md-12 col-lg-7 col-lg-offset-2"></div>-->
<input name="offset_number_orginial" type="hidden" value="<?php echo $offset ?>"/>
<input name="offset_number" type="hidden" value="<?php echo $offset ?>"/>
<input name="refined" type="hidden" value=""/>
<input name="refined_gender" type="hidden" value=""/>
<input id="archive-cat-parent" type="hidden" value=""/>
<input name="is_search" type="hidden" value="true"/>
<input id="archive-cat-id" type="hidden" value="<?php echo (is_array($post_cat_id_array)) ? implode(',', $post_cat_id_array) : $post_cat_id_array ?>"/>
<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 no-padding-mobile-450 product-sort-bar-container">
    <div class="col-lg-12 product-sort-bar">
        <div class="col-lg-12 col-lg-offset-0 col-xs-12 archive-sort-bar-wrapper">                        
            <div class="col-lg-12 col-xs-3 product-sort-by-button-wrapper">
                <p id="product-sort-by-button"></p>
            </div>
            <div id="archive-sort-overlay" class="archive-sort-overlay col-lg-5 col-lg-offset-12 no-padding">
                <?php include get_template_directory() . '/partials/all_products_sort_overlay.php' ?>
            </div>
        </div>
    </div>
    <div class="archive-product-open-backup"><button type="button">Back</div>
</div>
<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
    <p class="product_overlay_close product_overlay_back_text">Back</p>
</div>
<div id="archive-products" class="col-lg-10 col-xs-12 archive-products"><?php ?>
    <input id="archive_loaded_products" type="hidden" value="<?php echo implode(',', $search_query) ?>"/>
    <div class="archive-product-wrapper">
        <?php if (count($search_query) > 0):
            ?>
            <div class="archive-loading-more-prodcuts-info">Loading more products...</div>
            <?php
            $product_number = 0;
            foreach ($search_query as $id):
                $product_number = st_archive_product_container($id, $product_number);
            endforeach;
        else:
            ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-products">
                <h3>No products found, please refine your search</h3>
            </div>
            <div class="empty-cart-container empty-search-containter col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3>Most Popular</h3>
                    <?php
                    $posts = get_most_popular_products(3);
                    foreach ($posts->posts as $post):
                        st_archive_product_container($post->ID);
                    endforeach;
                    ?>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3>Newest</h3>
                    <?php
                    $posts = get_newest_products(3);
                    foreach ($posts->posts as $post):
                        st_archive_product_container($post->ID);
                    endforeach;
                    ?>
                </div>
                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                    <h3>Featured</h3>
                    <?php
                    $posts = st_get_taged_products(3, 'featured');
                    foreach ($posts->posts as $post):
                        st_archive_product_container($post->ID);
                    endforeach;
                    unset($posts);
                    ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
</div>