<div class="col-lg-7 col-xs-12 archive-right-column child-titles <?php echo ($children && !st_is_third_tier_cat($cat_object) && !isset($_POST["cat-number"])) ? 'archive-title-holder' : '' ?>">
    <?php if (isset($cat_object)): ?>
        <div class="breadcrumb-cats-top-level">
            <span><a href="<?php echo st_get_category_guid($cat_object->term_id) ?>"><?php echo $cat_object->name ?></a></span>
        </div>
    <?php endif; ?>
    <div class="col-lg-12">
        <div class="archive-view-all-products col-lg-12">
            <form method="post">
                <input type="hidden" name="cat-number" value="<?php echo $cat_object->term_id ?>"/>
                <button type="submit">View all <?php echo ($cat_object->name) ? $cat_object->name : '' ?></button>
            </form>
        </div>
    </div>
    <div class="col-lg-12 col-sm-7 col-md-7 col-xs-12">
        <?php
        $in = 99;
        $children_sorted = array();
        foreach ($children as $child):
            $order = (int) st_get_category_meta($child->term_id, 'cat_order');
            if (!$order):
                $order = $in;
                $in--;
            endif;
            $children_sorted[$order] = $child;
        endforeach;
        ksort($children_sorted);
        foreach ($children_sorted as $index => $element):
            //          st_var_dump($index);
//            st_var_dump((int) st_get_category_meta($element->term_id, 'cat_order'));
        endforeach;
        foreach ($children_sorted as $child):
            $child_image_array = st_get_category_image($child->term_id, 'full');
            if ($child_image_array):
                ?>
                <div class="col-lg-4 archive-child-image-container">
                    <div class="archive-child-wrapper">
                        <a href="<?php echo st_get_category_guid($child->term_id); ?>">
                            <img src="<?php echo $child_image_array[0] ?>" alt="<?php echo $child->name ?>"/>
                            <p class="subcat-page-text"><?php echo $child->name ?></p>
                        </a>
                    </div>
                </div>
                <?php
            endif;
        endforeach;
        unset($children_sorted);
        unset($in);
        ?>
    </div>
</div>
<div class="cat-page-title desktop-only">
    <?php if (isset($parent_cat_object->name) && isset($parent_cat->name)): ?>
        <p><?php echo ($parent_cat_object->name == $parent_cat->name) ? $cat_object->name : $parent_cat_object->name ?></p>
    <?php endif; ?>
    <?php if (isset($parent_cat->name)): ?>
        <p><?php echo $parent_cat->name ?></p>
    <?php endif; ?>
</div>