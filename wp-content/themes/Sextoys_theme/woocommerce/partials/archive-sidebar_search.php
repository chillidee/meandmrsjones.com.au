<?php
$cats = get_all_categories();
$parent_cats = array();
foreach ($cats as $cat):
    if ($cat->parent == 0 && $cat->slug != 'search'):
        $parent_cats[$cat->term_id] = $cat->name;
    endif;
endforeach;
unset($cats);
?>
<div id="archive-sidebar" class="col-lg-2 archive-sidebar search-sidebar">
    <h2>Shop</h2>
    <div class="archive-sidebar-mobile-close"><img src="https://s3.amazonaws.com/adult-toys/images/site_images/close_toffee.png" alt="Close"/></div>
    <ul class="parent-sidebar-sub-menu" id="sidebar-menu-wrapper">
        <?php
        foreach ($parent_cats as $index => $value):
            $side_bar_children = st_cat_get_children($index);
            ?>
            <li class="<?php echo ($side_bar_children) ? 'parent-sidebar-sub-menu-has-child' : 'parent-sidebar-sub-menu-no-child'; ?> parent-sidebar sub-menu-open"><?php echo $value ?>
                <a class="archive-view-all" href="<?php echo st_get_category_guid($index); ?>">View All</a>
                <?php if ($side_bar_children): ?>
                    <ul class="sidebar-sub-menu sidebar-menu-open">
                        <?php
                        foreach ($side_bar_children as $side_bar_childrens):
                            ?>
                            <li>
                                <a href="<?php echo st_get_category_guid($side_bar_childrens->term_id) ?>"><?php echo $side_bar_childrens->name; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </li>
            <?php
        endforeach;
        unset($parent_cats);
        ?>
    </ul>
</div>