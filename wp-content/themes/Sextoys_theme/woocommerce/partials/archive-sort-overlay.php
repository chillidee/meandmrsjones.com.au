<div class="overlay-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
    <i class="archive-sort-overlay-close fa fa-times" aria-hidden="true"></i>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <p class="title">Sort &#47; Filter</p>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 archive-sort-overlay-filters-wrapper">
        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 no-padding archive-sort-overlay-sort-wrapper">
            <p class="archive-sort-overlay-sort-by">Sort By</p>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 archieve-sort-by-select-wrapper no-padding">
            <select id="archieve-sort-by-select">
                <option value="0">Price: High to Low</option>
                <option value="1">Price: Low to High</option>
                <option value="2">Title: Z to A</option>
                <option value="3">Title: A to Z</option>
            </select>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 archive-sort-overlay-sort-wrapper">
            <p class="archive-sort-overlay-sort-default">Clear</p>
        </div>
    </div>
    <?php if (get_term_meta($post_cat_id_array, 'tags', true) != 'no'): ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 archive-sort-overlay-filters-wrapper">                
                <?php
                getProductsTags($post_cat_id_array);
                ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (get_term_meta($post_cat_id_array, 'brand', true) != 'no'): ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding archive-sort-overlay-filter-wrapper">
                <p>Brand<i class="fa fa-plus archive-overlay-expland" aria-hidden="true"></i></p>
                <ul>
                    <?php
                    st_sort_overlay_meta_options($post_cat_id_array, '_brand', 'brand');
                    ?>
                </ul>
            </div>
        </div>
        <?php
        if ($post_cat_id_array === 25):
            $sizes = array('2-6 inches', '6-10 inches', '10-14 inches', 'over 14 inches');
        else:
            if (in_array($post_cat_id_array, st_get_apparel_cats())):
                $sizes = array('One Size', 'Small', 'Medium', 'Large', 'X Large', 'Plus');
            endif;
        endif;
    endif;
    if (get_term_meta($post_cat_id_array, 'size', true) != 'no'):
        ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding archive-sort-overlay-filter-wrapper">
                <p>Size<i class="fa fa-plus archive-overlay-expland" aria-hidden="true"></i></p>
                <ul>
                    <li class="squaredFour margin-5">
                        <p>All</p>
                        <input name="size" id="size-all-sort-label" type="checkbox" checked value="all"/>
                        <label for="size-all-sort-label"></label>
                    </li>
                    <?php
                    if ($sizes):
                        foreach ($sizes as $size):
                            ?>
                            <li class="squaredFour margin-5">
                                <p><?php echo $size; ?></p>
                                <input name="size" type="checkbox" id="<?php echo trim(strtolower($size)); ?>-sort-label" value="<?php echo $size; ?>"/>
                                <label for="<?php echo trim(strtolower($size)); ?>-sort-label"></label>
                            </li>
                            <?php
                        endforeach;
                    else:
                        st_sort_overlay_meta_options($post_cat_id_array, 'st_size', 'size');
                    endif;
                    unset($sizes);
                    ?>
                </ul>
            </div>
        </div>
        <?php
    endif;
    if (get_term_meta($post_cat_id_array, 'colour', true) != 'no'):
        ?>
        <div <?php echo ($cat_object->term_id === 25) ? ' style="display:none" ' : '' ?> class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding archive-sort-overlay-filter-wrapper">
                <p>Colour<i class="fa fa-plus archive-overlay-expland" aria-hidden="true"></i></p>
                <ul>
                    <li class="squaredFour margin-5">
                        <p>All</p>
                        <input name="colour" id="colour-all" checked type="checkbox" value="all"/>
                        <label for="colour-all"></label>
                    </li>
                    <?php
                    st_sort_overlay_meta_options($post_cat_id_array, 'st_colour', 'colour');
                    ?>
                </ul>
            </div>
        </div>
        <?php
        unset($colours);
    endif;
    if (get_term_meta($post_cat_id_array, 'price', true) != 'no'):
        ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding archive-sort-overlay-filter-wrapper">
                <p>Price Range<i class="fa fa-plus archive-overlay-expland" aria-hidden="true"></i></p>
                <ul id="archive-overlay-price-inputs">
                    <li class="squaredFour margin-5">
                        <p>Below $20</p>
                        <input type="checkbox" id="below_20_price" name="price" value="19.99"/>
                        <label for="below_20_price"></label>
                    </li>
                    <li class="squaredFour margin-5">
                        <p>Below $40</p>
                        <input type="checkbox" id="below_40_price" name="price" value="39.99"/>
                        <label for="below_40_price"></label>
                    </li>
                    <li class="squaredFour margin-5">
                        <p>Below $60</p>
                        <input type="checkbox" id="below_60_price" name="price" value="59.99"/>
                        <label for="below_60_price"></label>
                    </li>
                    <li class="squaredFour margin-5">
                        <p>Over $60</p>
                        <input type="checkbox" id="over_60_price" name="price" value="60.00"/>
                        <label for="over_60_price"></label>
                    </li>
                </ul>
            </div>
        </div>
    <?php endif; ?>
</div>