<?php
if (isset($_POST["cat-number"])):
    $post_cat_id_array = st_get_all_subcats_ids($_POST["cat-number"]);
else:
    $post_cat_id_array = $cat_object->term_id;
endif;
//$price_range_array = st_get_price_range_from_cat_array($post_cat_id_array);
?>
<!--<div id="archive-display-product" class="archive-display-product col-lg-7 col-lg-offset-2 col-md-12 col-xs-12 col-sm-12 no-padding-mobile-450"></div>-->
<input type="hidden" id="archive-offset-number" value="50">
<input type="hidden" id="archive-list-close-position" value="0">
<input id="archive-cat-id" type="hidden" value="<?php echo (is_array($post_cat_id_array)) ? implode(',', $post_cat_id_array) : $post_cat_id_array ?>"/>
<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 no-padding-mobile-450 product-sort-bar-container">
    <?php if (isset($cat_object->name)): ?>
        <div class="col-lg-12 product-sort-bar">
            <div class="col-lg-12 col-lg-offset-0 col-xs-12 archive-sort-bar-wrapper">
                <div class="col-xs-3 mob-only archive-back-button-wrapper">
                    <a class='mobile-previous-page archive-mobile-back-button' onclick=" window.history.go(-1)" style='display:block;'><i class='fa fa-chevron-left' aria-hidden='true'></i>Back</a>
                </div>
                <div class="col-lg-12 col-xs-6 archive-cat-title-wrapper">
                    <p class="archive-cat-title"><?php echo $cat_object->name ?></p>
                </div>
                <div class="col-lg-12 col-xs-3 product-sort-by-button-wrapper">
                    <p id="product-sort-by-button"><span>Sort &#47; </span>Filter</p>
                    <div id="archive-sort-overlay" class="archive-sort-overlay col-lg-5 col-lg-offset-12 no-padding">
                        <?php
                        include_once 'archive-sort-overlay.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="archive-product-open-backup"><button type="button">Back</div>
        <?php
    endif;
    ?>
</div>
<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
    <p class="product_overlay_close product_overlay_back_text">Back</p>
</div>
<div id="archive-products" class="col-lg-10 col-xs-12 archive-products"><?php
    $products = st_sort_product_ids($post_cat_id_array);
    unset($post_cat_id_array);
    ?>
    <div class="thinking hidden"><img class="center-block" src="https://s3.amazonaws.com/adult-toys/images/site_images/main-header-logo.png"/></div>
    <input id="archive_loaded_products" type="hidden" value="<?php //echo implode(',', $products[1])        ?>"/>
    <div class="archive-product-wrapper">
        <?php if (count($products[0]) > 0):
            ?>
            <div class="archive-loading-more-prodcuts-info">Loading more products...</div>
            <?php
            $product_number = 0;
            foreach ($products[0] as $index => $id):
                $product_number = st_archive_product_container($id, $product_number);
            endforeach;
            unset($products);
        else:
            ?>
            <div class="col-lg-12 no-products">
                <h3>No products found, please refine your search</h3>
            </div>
            <div class="empty-cart-container empty-search-containter col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3>Most Popular</h3>
                    <?php
                    $posts = get_most_popular_products(3);
                    foreach ($posts->posts as $post):
                        st_archive_product_container($post->ID);
                    endforeach;
                    ?>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3>Newest</h3>
                    <?php
                    $posts = get_newest_products(3);
                    foreach ($posts->posts as $post):
                        st_archive_product_container($post->ID);
                    endforeach;
                    ?>
                </div>
                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                    <h3>Featured</h3>
                    <?php
                    $posts = st_get_taged_products(3, 'featured');
                    foreach ($posts->posts as $post):
                        st_archive_product_container($post->ID);
                    endforeach;
                    unset($posts);
                    ?>
                </div>
            </div>
        <?php
        endif;
        unset($products);
        ?>
    </div>
</div>
</div>
</div>
<?php
if (!st_is_third_tier_cat($cat_object)):
    ?>
    <div class="cat-page-title desktop-only hidden">
        <?php if (isset($parent_cat_object->name) && isset($parent_cat->name)): ?>
            <p><?php echo ($parent_cat_object->name == $parent_cat->name) ? $cat_object->name : $parent_cat_object->name ?></p>
        <?php endif; ?>
        <?php if (isset($parent_cat->name)): ?>
            <p><?php echo $parent_cat->name ?></p>
        <?php endif; ?>
        <?php
    endif;
    ?>
</div>