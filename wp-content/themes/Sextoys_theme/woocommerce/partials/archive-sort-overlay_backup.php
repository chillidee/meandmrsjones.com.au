<i class="archive-sort-overlay-close fa fa-times" aria-hidden="true"></i>
<div class="col-lg-12">
    <p class="title">Sort &#47; Filter</p>
</div>
<div class="col-lg-12 no-padding">
    <div class="col-lg-3">
        <p class="archive-sort-overlay-sort-by">Sort By</p>
    </div>
    <div class="col-lg-6">
        <select id="archieve-sort-by-select">
            <option value="0">Price: Descending</option>
            <option value="1">Price: Ascending</option>
            <option value="2">Title: Descending</option>
            <option value="3">Title: Ascending</option>
        </select>
    </div>
    <div class="col-lg-3">
        <p class="archive-sort-overlay-sort-default">Default</p>
    </div>
</div>
<div class="col-lg-12">
    <div class="col-lg-12 no-padding archive-sort-overlay-filters-wrapper">
        <p>Filters: <span><?php echo $cat_object->name ?></span></p>
        <p class="archive-sort-overlay-sort-clear">Clear All</p>
    </div>
</div>
<div class="col-lg-12">
    <div class="col-lg-12 no-padding archive-sort-overlay-filter-wrapper">
        <p>Brand<i class="fa fa-plus archive-overlay-expland" aria-hidden="true"></i></p>
        <ul>
            <?php foreach (st_get_brands_from_cat_array($post_cat_id_array) as $brand): ?>
                <li><label for="<?php echo trim($brand); ?>-label"><?php echo $brand; ?></label>
                    <input name="brand" type="checkbox" checked="true" value="<?php echo $brand; ?>"/>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<div class="col-lg-12">
    <div class="col-lg-12 no-padding archive-sort-overlay-filter-wrapper">
        <p>Size<i class="fa fa-plus archive-overlay-expland" aria-hidden="true"></i></p>
        <ul>
            <?php foreach (st_get_product_meta_from_cat_array($post_cat_id_array, 'st_size') as $size): ?>
                <li>
                    <label for="<?php echo trim($size); ?>-label"><?php echo $size; ?></label>
                    <input name="size" type="checkbox" checked="true" value="<?php echo $size; ?>"/>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<div class="col-lg-12">
    <div class="col-lg-12 no-padding archive-sort-overlay-filter-wrapper">
        <p>Colour<i class="fa fa-plus archive-overlay-expland" aria-hidden="true"></i></p>
        <ul>
            <?php foreach (st_get_product_meta_from_cat_array($post_cat_id_array, 'st_colour') as $size): ?>
                <li>
                    <label for="<?php echo trim($size); ?>-label"><?php echo $size; ?></label>
                    <input name="colour" type="checkbox" checked="true" value="<?php echo $size; ?>"/>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<div class="col-lg-12">
    <div class="col-lg-12 no-padding archive-sort-overlay-filter-wrapper">
        <p>Price Range<i class="fa fa-plus archive-overlay-expland" aria-hidden="true"></i></p>
        <ul id="archive-overlay-price-inputs">
            <li>
                <label for="below_20_price">Below $20</label>
                <input type="checkbox" name="price" value="19.99" checked="true"/>
            </li>
            <li>
                <label for="below_20_price">Below $40</label>
                <input type="checkbox" name="price" value="39.99" checked="true"/>
            </li>
            <li>
                <label for="below_20_price">Below $60</label>
                <input type="checkbox" name="price" value="59.99" checked="true"/>
            </li>
            <li>
                <label for="below_20_price">Over $60</label>
                <input type="checkbox" name="price" value="60.00" checked="true"/>
            </li>
        </ul>
    </div>
</div>
<div class="col-lg-12">
    <button id="archive-sort-overlay-apply-button">Apply</button>
</div>