<div class="overlay-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
    <i class="archive-sort-overlay-close fa fa-times" aria-hidden="true"></i>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <p class="title">Sort &#47; Filter</p>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 archive-sort-overlay-filters-wrapper">
        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 no-padding archive-sort-overlay-sort-wrapper">
            <p class="archive-sort-overlay-sort-by">Sort By</p>
        </div>
        <div class="col-lg-6 archieve-sort-by-select-wrapper no-padding">
            <select id="archieve-sort-by-select">
                <option value="0">Price: High to Low</option>
                <option value="1">Price: Low to High</option>
                <option value="2">Title: Z to A</option>
                <option value="3">Title: A to Z</option>
            </select>
        </div>
        <div class="col-lg-3 archive-sort-overlay-sort-wrapper">
            <p class="archive-sort-overlay-sort-default">Default</p>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 archive-sort-overlay-filters-wrapper">            
            <?php foreach (getProductsTags($search_query, 'ids') as $tag): ?>
                <div class="col-lg-12 no-padding squaredFour no-margin margin-5">
                    <p><?php echo $tag ?></p>
                    <input type="checkbox" id="<?php echo $tag ?>" value="<?php echo $tag ?>" name="tag"/><label for="<?php echo $tag ?>"></label>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding archive-sort-overlay-filter-wrapper">
            <p>Brand<i class="fa fa-plus archive-overlay-expland" aria-hidden="true"></i></p>
            <ul>
                <?php foreach (st_get_products_metas($search_query, '_brand') as $brand): ?>
                    <li class="squaredFour margin-5">
                        <p><?php echo $brand; ?></p>
                        <input name="brand" type="checkbox" id="<?php echo trim(strtolower($brand)); ?>" value="<?php echo $brand; ?>"/>
                        <label for="<?php echo trim(strtolower($brand)); ?>"></label>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding archive-sort-overlay-filter-wrapper">
            <p>Size<i class="fa fa-plus archive-overlay-expland" aria-hidden="true"></i></p>
            <ul>
                <li class="squaredFour margin-5">
                    <p>All</p>
                    <input name="size" id="size-all-sort-label" type="checkbox" checked value="all"/>
                    <label for="size-all-sort-label"></label>
                </li>
                <?php foreach (st_get_products_metas($search_query, 'st_size') as $size): ?>
                    <li class="squaredFour margin-5">
                        <p><?php echo $size; ?></p>
                        <input name="size" type="checkbox" id="<?php echo trim(strtolower($size)); ?>-sort-label" value="<?php echo $size; ?>"/>
                        <label for="<?php echo trim(strtolower($size)); ?>-sort-label"></label>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding archive-sort-overlay-filter-wrapper">
            <p>Colour<i class="fa fa-plus archive-overlay-expland" aria-hidden="true"></i></p>
            <ul>
                <li class="squaredFour margin-5">
                    <p>All</p>
                    <input name="colour" id="colour-all" checked type="checkbox" value="all"/>
                    <label for="colour-all"></label>
                </li>
                <?php foreach (st_get_products_metas($search_query, 'st_colour') as $size): ?>
                    <li class="squaredFour margin-5">
                        <p><?php echo $size; ?></p>
                        <input name="colour" type="checkbox" id="<?php echo trim($size); ?>" value="<?php echo $size; ?>"/>
                        <label for="<?php echo trim($size); ?>"></label>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding archive-sort-overlay-filter-wrapper">
            <p>Price Range<i class="fa fa-plus archive-overlay-expland" aria-hidden="true"></i></p>
            <ul id="archive-overlay-price-inputs">
                <li class="squaredFour margin-5">
                    <p>Below $20</p>
                    <input type="checkbox" id="below_20_price" name="price" value="19.99"/>
                    <label for="below_20_price"></label>
                </li>
                <li class="squaredFour margin-5">
                    <p>Below $40</p>
                    <input type="checkbox" id="below_40_price" name="price" value="39.99"/>
                    <label for="below_40_price"></label>
                </li>
                <li class="squaredFour margin-5">
                    <p>Below $60</p>
                    <input type="checkbox" id="below_60_price" name="price" value="59.99"/>
                    <label for="below_60_price"></label>
                </li>
                <li class="squaredFour margin-5">
                    <p>Over $60</p>
                    <input type="checkbox" id="over_60_price" name="price" value="60.00"/>
                    <label for="over_60_price"></label>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <button id="archive-sort-overlay-apply-button">Apply</button>
    </div>
</div>