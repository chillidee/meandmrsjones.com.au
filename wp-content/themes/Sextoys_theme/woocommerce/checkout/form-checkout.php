<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */
if (!defined('ABSPATH')) {
    exit;
}

wc_print_notices();

//do_action('woocommerce_before_checkout_form', $checkout);
// If checkout registration is disabled and not logged in, the user cannot checkout
if (!$checkout->enable_signup && !$checkout->enable_guest_checkout && !is_user_logged_in()) {
    echo apply_filters('woocommerce_checkout_must_be_logged_in_message', __('You must be logged in to checkout.', 'woocommerce'));
    return;
}
?>
<div class="row">
    <div class="sub-cat-background-image">
    </div>    
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-7 col-lg-offset-1 col-sm-12 col-md-12 main-content">
            <div class="cart-checkout-header">
                <p>My Checkout</p>
            </div>
            <div class="col-lg-12 checkout_form_wrapper col-md-12 col-sm-12 col-xs-12">
                <?php if (!is_user_logged_in()): ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2>Already have an account?</h2>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <button class="login-button" type="button">Login to my account</button>
                        <a href="">Forgotten Password?</a>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2 class="guess-checkout-heading">Guest checkout &#47; Create Account</h2>
                    </div>
                <?php else: ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Welcome back, <?php echo st_get_user_display_name() ?></h3>
                    </div>
                    <div class="col-lg-12">
                        <p>To help save you time and get you back to the bedroom quicker, we've filled out this section for you using your last order details. Please take a moment to check your details and update them if necessary</p>
                    </div>
                <?php endif; ?>
                <div class="coupon-container ocl-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php do_action('woocommerce_before_checkout_form');
                    ?>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3 class="checkout-billing-details">Billing Details</h3>
                </div>

                <form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">

                    <?php if (sizeof($checkout->checkout_fields) > 0) : ?>

                        <?php do_action('woocommerce_checkout_before_customer_details'); ?>

                        <div class="col-lg-12 no-padding" id="customer_details">
                            <div class="col-lg-12 no-padding">
                                <?php do_action('woocommerce_checkout_billing'); ?>
                            </div>

                            <div class="col-lg-12">
                                <?php
                                do_action('woocommerce_checkout_shipping');
                                ?>
                            </div>
                        </div>

                        <?php do_action('woocommerce_checkout_after_customer_details'); ?>

                        <?php
                    endif;
                    ?>
                    <div class="col-lg-12">
                        <h3 id="order_review_heading"><?php _e('Your order', 'woocommerce'); ?></h3>

                        <?php do_action('woocommerce_checkout_before_order_review'); ?>

                        <div id="order_review" class="woocommerce-checkout-review-order">
                            <?php do_action('woocommerce_checkout_order_review'); ?>
                        </div>

                        <?php do_action('woocommerce_checkout_after_order_review'); ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php do_action('woocommerce_after_checkout_form', $checkout); ?>
