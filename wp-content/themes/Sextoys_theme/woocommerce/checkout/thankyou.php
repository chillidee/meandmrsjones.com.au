<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */
if (!defined('ABSPATH')) {
    exit;
}
?>
<script>
    if (typeof fbq !== 'undefined') {
        fbq('track', 'Purchase');
    }
</script>
<div class="row thank-you">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 receipt-page prodct-cat-background-image"></div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-7 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 my-dashboard-main-title-container">
                <h1 class="my-account-title my-dashboard-main-title">Your receipt</h1>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>
            <div class="col-lg-2 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 my-account-sidebar" id="my-account-sidebar">
                <?php if ($order) : ?>

                    <?php if ($order->has_status('failed')) : ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 thankyou-payment-unsuccess-heading">
                            <h2>Payment Unsuccessful <i class="fa fa-times" aria-hidden="true"></i></i></h2>
                        </div>
                        <?php
                    else:
                        if (isset($_POST["create-account"])):
                            ?>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 thankyou-payment-success-heading">
                                <h2 class="thank-you-create-password-title">Create a password</h2>
                            </div>
                            <form method="POST" action="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>" ng-app id="thankYouCreateAccountForm" name="thankYouCreateAccountForm" novalidate>
                                <?php echo(isset($_POST["create-account"]) ? "<input id='thankyouOrderId' type='hidden' value='{$_POST["create-account"]}'/>" : "") ?>
                                <input type="hidden" id="requestURL" value="/my-account/"/>
                                <input id="thankyouBillingAddress1" value="<?php echo $order->billing_address_1 ?>" type="hidden"/>
                                <input id="thankyouBillingAddress2" value="<?php echo $order->billing_address_2 ?>" type="hidden"/>
                                <input id="thankyouBillingAddressCity" value="<?php echo $order->billing_city ?>" type="hidden"/>
                                <input id="thankyouBillingAddressCountry" value="<?php echo $order->billing_country ?>" type="hidden"/>
                                <input id="thankyouBillingAddressPostcode" value="<?php echo $order->billing_postcode ?>" type="hidden"/>
                                <input id="thankyouBillingAddressState" value="<?php echo $order->billing_state ?>" type="hidden"/>
                                <label for="titleSelectTY">Title</label>
                                <select class="titleSelectTY" name="titleSelectTY">
                                    <option value="Miss">Miss</option>
                                    <option value="Ms">Ms</option>
                                    <option value="Mr">Mr</option>
                                    <option value="Sir">Sir</option>
                                    <option value="Mrs">Mrs</option>
                                    <option value="Dr">Dr</option>
                                </select>
                                <label for="thankYouCreateFirstName" class="required">First name</label>
                                <input required ng-model="thankYouCreateFirstName" id="thankYouCreateFirstName" name="thankYouCreateFirstName" ng-init="thankYouCreateFirstName = <?php echo(isset($_POST["create-account-fn"]) ? "'{$_POST["create-account-fn"]}'" : "") ?>" type="text"/>
                                <p class="user_name_error" ng-show="thankYouCreateAccountForm.thankYouCreateFirstName.$touched && thankYouCreateAccountForm.thankYouCreateFirstName.$invalid">Please enter an valid first name</p>
                                <label for="thankYouCreateLastName" class="required">Surname</label>
                                <input required ng-model="thankYouCreateLastName" id="thankYouCreateLastName" name="thankYouCreateLastName" ng-init="thankYouCreateLastName = <?php echo(isset($_POST["create-account-ln"]) ? "'{$_POST["create-account-ln"]}'" : "") ?>" type="text"/>
                                <p class="user_name_error" ng-show="thankYouCreateAccountForm.thankYouCreateLastName.$touched && thankYouCreateAccountForm.thankYouCreateLastName.$invalid">Please enter an valid last name</p>
                                <label for="thankYouCreateEmailAddress" class="required">Email Address</label>
                                <input required ng-model="thankYouCreateEmailAddress" id="thankYouCreateEmailAddress" name="thankYouCreateEmailAddress" ng-init="thankYouCreateEmailAddress = <?php echo(isset($_POST["create-account-email"]) ? "'{$_POST["create-account-email"]}'" : "") ?>" type="email"/>
                                <p class="user_name_error" ng-show="thankYouCreateAccountForm.thankYouCreateEmailAddress.$touched && thankYouCreateAccountForm.thankYouCreateEmailAddress.$invalid">Please enter an valid email address</p>
                                <p class="email-in-use user_name_error">Email address already in use</p>
                                <label for="thankYouCreatePhone" class="required">Mobile phone number</label>
                                <input required ng-model="thankYouCreatePhone" id="thankYouCreatePhone" name="thankYouCreatePhone" ng-init="thankYouCreatePhone =<?php echo(isset($_POST["create-account-phone"]) ? "'{$_POST["create-account-phone"]}'" : "") ?>" type="tel"/>
                                <p class="user_name_error" ng-show="thankYouCreateAccountForm.thankYouCreatePhone.$touched && thankYouCreateAccountForm.thankYouCreatePhone.$invalid">Please enter an valid phone number</p>
                                <label for="thankYouCreatePassword" class="required">Create Password</label>
                                <input required ng-model="thankYouCreatePassword" id="thankYouCreatePassword" name="thankYouCreatePassword" type="password"/>
                                <p class="user_name_error" ng-show="thankYouCreateAccountForm.thankYouCreatePassword.$touched && thankYouCreateAccountForm.thankYouCreatePassword.$invalid">Please enter an valid password</p>
                                <label for="thankYouCreatePasswordConfirm" class="required">Verify Password</label>
                                <p class="user_name_error thank-you-password-error"></p>
                                <input required ng-model="thankYouCreatePasswordConfirm" id="thankYouCreatePasswordConfirm" name="thankYouCreatePasswordConfirm" type="password"/>
                                <p class="user_name_error" ng-show="thankYouCreateAccountForm.thankYouCreatePasswordConfirm.$touched && thankYouCreateAccountForm.thankYouCreatePasswordConfirm.$invalid">Please enter an valid password</p>
                                <label for="signUpOverlayDOB">Date of birth</label>
                                <?php
                                $monthArray = array('January', 'Febuary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
                                echo '<select tabindex="3" class="signUpOverlayDOBDay" name="day">';
                                echo '<option>Day</option>';
                                for ($i = 1; $i <= 31; $i++) {
                                    $dayIndex = $i;
                                    $i = str_pad($i, 2, 0, STR_PAD_LEFT);
                                    echo "<option value='$dayIndex'>$i</option>";
                                }
                                echo '</select>';
                                echo '<select tabindex="4" class="signUpOverlayDOBMonth" name="month">';
                                echo '<option>Month</option>';
                                for ($i = 1; $i <= 12; $i++) {
                                    $monthIndex = $i;
                                    $monthArrayIndex = $i - 1;
                                    $i = str_pad($i, 2, 0, STR_PAD_LEFT);
                                    echo "<option value='$monthIndex'>$monthArray[$monthArrayIndex]</option>";
                                }
                                echo '</select>';
                                echo '<select tabindex="5" class="signUpOverlayDOBYear" name="year">';
                                echo '<option>Year</option>';
                                for ($i = date('Y'); $i >= date('Y', strtotime('-100 years')); $i--) {
                                    echo "<option value='$i'>$i</option>";
                                }
                                echo '</select>';
                                ?>
                                <p class="signup-form-dateofbirth-error user_name_error">Please enter your date of birth</p>
                                <div class="col-lg-12 squaredFour">
                                    <input id="thankYouCreateSubscribe" name="thankYouCreateSubscribe" type="checkbox" checked/>
                                    <label class="text-label" for="thankYouCreateSubscribe"></label>
                                    <p>I would like to receive news, special offers &AMP; discounts from Me &AMP; Mrs. Jones</p>
                                </div>
                                <button ng-disabled="!thankYouCreateAccountForm.$valid" id="thankYouCreateButton" type="button">Create Account</button>
                            </form>
                        <?php else: ?>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 thankyou-payment-success-heading">
                                <h2>Payment Successful <i class="fa fa-check" aria-hidden="true"></i></h2>
                            </div>
                            <div class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12">
                                <p class="thank-you-sidebar-ordernuber">
                                    Your order <span>#<?php echo $order->get_order_number(); ?></span> is on its way!
                                </p>
                                <p>
                                    We've emailed you a copy of your order and you will be notified when it has been dispatched from our warehouse with a tracking number for your piece of mind.
                                </p>
                                <?php if (!is_user_logged_in()): ?>
                                    <p class="thank-you-sidebar-p-title">
                                        Want to be able to check the status of your order anytime?
                                    </p>
                                    <p>
                                        We can save all your details and create an account for you! You can also reorder, manage preferences and stalk your order right to your front door!
                                    </p>
                                    <p class="thank-you-sidebar-p-title">
                                        All you have to do is create a password!
                                    </p>
                                    <form method="POST">
                                        <input type="hidden" name="create-account" value="<?php echo $order->id ?>"/>
                                        <input type="hidden" name="create-account-email" value="<?php echo $order->billing_email ?>"/>
                                        <input type="hidden" name="create-account-phone" value="<?php echo $order->billing_phone ?>"/>
                                        <input type="hidden" name="create-account-fn" value="<?php echo $order->billing_first_name ?>"/>
                                        <input type="hidden" name="create-account-ln" value="<?php echo $order->billing_last_name ?>"/>
                                        <button id="thank-you-create-account-button" type="submit">Yes, create a password</button>
                                    </form>
                                </div>
                                <?php
                            else:
                                ?>
                                <p>To view your account details <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>">here</a></p>
                            </div>
                        <?php
                        endif;
                    endif;
                    ?>
                <?php
                endif;
            endif;
            ?>
        </div>
        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 my-account-content-wrapper" id="my-account-content-wrapper">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php wc_get_template('myaccount/my-order-new-detailed.php', array('order' => $order)); ?>
            </div>
        </div>
    </div>
</div>
</div>