<?php
/**
 * Checkout terms and conditions checkbox
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.0
 */
if (!defined('ABSPATH')) {
    exit;
}
?>
<p class="form-row marketing wc-terms-and-conditions">
<div class="squaredFour">
    <input type="checkbox" class="input-checkbox" name="marketing" checked id="marketing" />
    <label for="marketing" class="checkbox"></label>
    <p>Send me exclusive deals, discount vouchers & more!</p>
    <input type="hidden" name="marketing-field" value="1" />
</div>
</p>
<?php
if (wc_get_page_id('terms') > 0 && apply_filters('woocommerce_checkout_show_terms', true)) :
    ?>
    <p class="form-row terms wc-terms-and-conditions">
    <div class="squaredFour">
        <input type="checkbox" class="input-checkbox" name="terms" <?php checked(apply_filters('woocommerce_terms_is_checked_default', isset($_POST['terms'])), true); ?> id="terms" />
        <label for="terms" class="checkbox"></label>
        <p><?php printf(__('I&rsquo;ve read and accept the <a href="%s" target="_blank">terms &amp; conditions</a>', 'woocommerce'), esc_url(wc_get_page_permalink('terms'))); ?> <span class="required">*</span></p>
        <input type="hidden" name="terms-field" value="1" />
    </div>
    </p>
<?php endif; ?>
