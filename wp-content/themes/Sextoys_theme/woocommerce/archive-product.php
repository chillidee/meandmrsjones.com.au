<?php
//
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
if (isset($_GET["header-search"])):
    $search_term = strip_tags($_GET["header-search"]);
    $search_query = st_product_search($search_term);
    ?>
    <script>
        if (typeof ga !== 'undefined') {
            ga('send', 'event', 'Search', 'Search', 'User Search', "<?php echo $search_term ?>");
        }
        if (typeof fbq !== 'undefined') {
            fbq('track', 'Search');
        }
    </script>
    <?php
endif;
global $post;
$cat_object = get_queried_object();
$offset = 0;
$apparelCatIdArrays = st_get_apparel_cats();
if (isset($cat_object)):
    $image_array = st_get_product_background_image($cat_object->term_id);
    $children = st_cat_get_children($cat_object->term_id);
    $parent_cat_object = st_get_cat_object($cat_object->parent);
    $cat_id = $cat_object->term_id;
endif;
if (isset($cat_id)):
    $parent_cat = st_get_cat_object(st_cat_top_level_parent($cat_id));
endif;
if (isset($parent_cat)):
    $parent_children = st_cat_get_children($parent_cat->term_id);
endif;
$active_path = $_SERVER["REQUEST_SCHEME"] . '://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
$catSlug = $cat_object->name;
?>
<input id="archive_category_number" type="hidden" value="<?php echo $cat_object->term_id ?>"/>
<div class="row">
    <div class="col-sm-12 col-xs-12 col-md-12 mob-only">
        <button class="archive-mobile-back-button archive-full-mobile-back-button" type="button"><i class="fa fa-chevron-left" aria-hidden="true"></i>Back</button>
    </div>
    ?>
    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class = "col-lg-12 prodct-cat-background-image"></div>
        <?php
        unset($image_array);
        $prevoius_page = $_SERVER['HTTP_REFERER'];
        if (!isset($search_query)):
            include get_template_directory() . '/partials/all_products_sidebar.php';
        else:
            include get_template_directory() . '/partials/all_products_sidebar.php';
        endif;
        if ($children && !isset($_POST["cat-number"]) && !st_is_third_tier_cat($cat_object)):
            include 'partials/archive-child-titles.php';
        else:
            if (!isset($search_query)):
                include get_template_directory() . '/partials/all_products_sidebar.php';
            else:
                include get_template_directory() . '/partials/all_products_sidebar.php';
            endif;
            unset($search_query);
        endif;
        unset($children);
        unset($parent_cat_object);
        ?>
    </div>
</div>
</div>
<?php
wp_reset_postdata();
?>