<?php
/**
 * Lost password confirmation text.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/lost-password-confirmation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */
if (!defined('ABSPATH')) {
    exit;
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-12 prodct-cat-background-image" style="background: url(https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/Backgroun-MMJ-Guide.jpg) no-repeat bottom right !important;background-size: cover !important"></div>
        <div class="col-lg-7 col-lg-offset-1 lost_password-conatiner">
            <div class="col-lg-12">
                <h1 class="my-account-title">My Account</h1>
            </div>
            <div class="col-lg-12">
                <?php
                wc_print_notices();
                wc_print_notice(__('Password reset email has been sent.', 'woocommerce'));
                ?>

                <p><?php echo apply_filters('woocommerce_lost_password_message', __('A password reset email has been sent to the email address on file for your account, but may take several minutes to show up in your inbox. Please wait at least 10 minutes before attempting another reset.', 'woocommerce')); ?></p>

            </div>
        </div>
    </div>