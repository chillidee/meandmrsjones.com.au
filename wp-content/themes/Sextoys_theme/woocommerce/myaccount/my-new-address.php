<?php
$customer_id = get_current_user_id();
if (isset($_POST["my-account-edit-address"])):
    $address_name = $_POST["my-account-delete-address-name"];
    $address_first_name = get_user_meta($customer_id, "{$address_name}_first_name", true);
    $address_first_last_name = get_user_meta($customer_id, "{$address_name}_last_name", true);
    $address_first_company = get_user_meta($customer_id, "{$address_name}_company", true);
    $address_first_address_1 = get_user_meta($customer_id, "{$address_name}_address_1", true);
    $address_first_address_2 = get_user_meta($customer_id, "{$address_name}_address_2", true);
    $address_first_city = get_user_meta($customer_id, "{$address_name}_city", true);
    $address_first_postcode = get_user_meta($customer_id, "{$address_name}_postcode", true);
    $address_first_state = get_user_meta($customer_id, "{$address_name}_state", true);
    $address_first_phone = get_user_meta($customer_id, "{$address_name}_phone", true);

endif;
?>
<div class="my-account-content">
    <h2 class="my-account-title">Address Book</h2>
    <h3 class="add-address-subtitle">Add / edit address</h3>
    <form action="" method="post" id="newAddressFrom" name="newAddressFrom" ng-app novalidate>
        <input type="hidden" name="address_book" value="address_book"/>
        <input type="hidden" name="address_book_save_address" value="address_book_save_address"/>
        <?php if (isset($_POST["my-account-edit-address"])): ?>
            <input type="hidden" name="address_book_edited_address" value="address_book_edited_address"/>
            <input type="hidden" name="address_book_edited_orignalName" value="<?php echo $address_name ?>"/>
        <?php endif; ?>
        <input type="hidden" name="address_book_save_address" value="address_book_save_address"/>
        <div class="col-lg-12">
            <div class="col-lg-6">
                <div class="col-lg-12">
                    <label class="required" for="newAddressName">Address Name</label>
                    <input <?php echo ($address_name == 'billing' || $address_name == 'shipping') ? 'disabled' : '' ?> ng-model="newAddressName" name="newAddressName" ng-init="newAddressName = '<?php echo ($address_name) ? $address_name : '' ?>'" required/>
                    <p class="user_name_error" ng-show="newAddressFrom.newAddressName.$touched && newAddressFrom.newAddressName.$invalid">Please enter an valid address name</p>
                </div>
                <div class="col-lg-6">
                    <label class="required" for="newAddressFirstName">First Name</label>
                    <input ng-model="newAddressFirstName" name="newAddressFirstName" ng-init="newAddressFirstName = '<?php echo ($address_first_name) ? $address_first_name : '' ?>'" required/>
                    <p class="user_name_error" ng-show="newAddressFrom.newAddressFirstName.$touched && newAddressFrom.newAddressFirstName.$invalid">Please enter an valid first name</p>
                </div>
                <div class="col-lg-6">
                    <label class="required" for="newAddressLastName">Surname</label>
                    <input ng-model="newAddressLastName" name="newAddressLastName" ng-init="newAddressLastName = '<?php echo ($address_first_last_name) ? $address_first_last_name : '' ?>'" required/>
                    <p class="user_name_error" ng-show="newAddressFrom.newAddressLastName.$touched && newAddressFrom.newAddressLastName.$invalid">Please enter an valid surname</p>
                </div>
                <div class="col-lg-12">
                    <label class="required" for="newAddressContactNumber">Contact number at address</label>
                    <input ng-model="newAddressContactNumber" name="newAddressContactNumber" ng-init="newAddressContactNumber = '<?php echo ($address_first_phone) ? $address_first_phone : '' ?>'" required/>
                    <p class="user_name_error" ng-show="newAddressFrom.newAddressContactNumber.$touched && newAddressFrom.newAddressContactNumber.$invalid">Please enter an valid number</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="col-lg-12">
                    <label class="required" for="newAddressAddressLine1">Address</label>
                    <input ng-model="newAddressAddressLine1" name="newAddressAddressLine1"  ng-init="newAddressAddressLine1 = '<?php echo ($address_first_address_1) ? $address_first_address_1 : '' ?>'" required/>
                    <p class="user_name_error" ng-show="newAddressFrom.newAddressAddressLine1.$touched && newAddressFrom.newAddressAddressLine1.$invalid">Please enter an valid address</p>
                    <input name="new-address-address-line2" <?php echo ($address_first_address_2) ? "value='{$address_first_address_2}'" : '' ?>/>
                </div>
                <div class="col-lg-12 newAddressAddressSuburb">
                    <label class="required" for="newAddressAddressSuburb">City / suburb</label>
                    <input ng-model="newAddressAddressSuburb" name="newAddressAddressSuburb" ng-init="newAddressAddressSuburb = '<?php echo ($address_first_city) ? $address_first_city : '' ?>'" required/>
                    <p class="user_name_error" ng-show="newAddressFrom.newAddressAddressSuburb.$touched && newAddressFrom.newAddressAddressSuburb.$invalid">Please enter an valid city/suburb name</p>
                </div>
                <div class="col-lg-6">
                    <label for="new-address-address-state">State</label>
                    <select name="new-address-address-state">
                        <option value="ACT" <?php echo ($address_first_state == 'ACT') ? "selected" : '' ?>>ACT</option>
                        <option value="NSW" <?php echo ($address_first_state == 'NSW') ? "selected" : '' ?>>NSW</option>
                        <option value="NT" <?php echo ($address_first_state == 'NT') ? "selected" : '' ?>>NT</option>
                        <option value="QLD" <?php echo ($address_first_state == 'QLD') ? "selected" : '' ?>>QLD</option>
                        <option value="SA" <?php echo ($address_first_state == 'SA') ? "selected" : '' ?>>SA</option>
                        <option value="TAS" <?php echo ($address_first_state == 'TAS') ? "selected" : '' ?>>TAS</option>
                        <option value="VIC" <?php echo ($address_first_state == 'VIC') ? "selected" : '' ?>>VIC</option>
                        <option value="WA" <?php echo ($address_first_state == 'WA') ? "selected" : '' ?>>WA</option>
                    </select>
                </div>
                <div class="col-lg-6">
                    <label class="required" for="newAddressAddressPostcode">Postcode</label>
                    <input ng-model="newAddressAddressPostcode" name="newAddressAddressPostcode" ng-init="newAddressAddressPostcode = '<?php echo ($address_first_postcode) ? $address_first_postcode : '' ?>'" required/>
                    <p class="user_name_error" ng-show="newAddressFrom.newAddressAddressPostcode.$touched && newAddressFrom.newAddressAddressPostcode.$invalid">Please enter an valid postcode</p>
                </div>
            </div>
            <div class="col-lg-6 col-lg-offset-3 myaccount-set-address-as-default-billing-wrapper squaredFour">
                <input type="checkbox" name="myaccount-set-address-as-default-billing" id="myaccount-set-address-as-default-billing"/>
                <label for="myaccount-set-address-as-default-billing"></label>
                <p>Use as my default billing address</p>
            </div>
            <div class="col-lg-6 col-lg-offset-3 myaccount-set-address-as-default-shipping-wrapper squaredFour">
                <input type="checkbox" name="myaccount-set-address-as-default-shipping" id="myaccount-set-address-as-default-shipping"/>
                <label for="myaccount-set-address-as-default-shipping"></label>
                <p>Use as my default shipping address</p>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-3">
                </div>
                <div class="col-lg-6 new-address-save-button-container">
                    <p>Please fill in required fields</p>
                    <button class="new-address-save-button" type="submit" ng-disabled="!newAddressFrom.$valid">Save</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        angular.bootstrap($('#newAddressFrom'));
    });
</script>