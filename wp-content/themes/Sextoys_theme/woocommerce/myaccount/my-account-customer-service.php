<div class="my-account-content">
    <h2 class="my-account-title">Customer Service</h2>
    <div class="col-lg-12">
        <p>
            If you have an enquiry regarding an order, a product or general feedback, please choose one of the below options.
        </p>
    </div>
    <form class="customer-service-form" action="">
        <div class="col-lg-12">
            <input type="radio" name="group" value="order"/>
            <label>An order I have</label><br>
            <input type="radio" name="group" value="return"/>
            <label>I want to return an item</label><br>
            <input type="radio" name="group" value="question"/>
            <label>I have a question about a product</label><br>
            <input type="radio" name="group" value="else"/>
            <label>Something else</label><br>
        </div>
        <div class="col-lg-12 continue-to-customer-service-button-wrapper">
            <button type="button" id="continue-to-customer-service-button">Next <i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
        </div>
    </form>
</div>