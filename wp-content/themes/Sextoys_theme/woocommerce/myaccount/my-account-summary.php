<?php
$customer_orders = get_posts(apply_filters('woocommerce_my_account_my_orders_query', array(
    'numberposts' => $order_count,
    'meta_key' => '_customer_user',
    'meta_value' => get_current_user_id(),
    'post_type' => wc_get_order_types('view-orders'),
    'post_status' => array_keys(wc_get_order_statuses())
        )));
$customer_id = get_current_user_id();
if (!wc_ship_to_billing_address_only() && get_option('woocommerce_calc_shipping') !== 'no') {
    $page_title = apply_filters('woocommerce_my_account_my_address_title', __('My Addresses', 'woocommerce'));
    $get_addresses = apply_filters('woocommerce_my_account_get_addresses', array(
        'billing' => __('Billing Address', 'woocommerce'),
        'shipping' => __('Shipping Address', 'woocommerce')
            ), $customer_id);
} else {
    $page_title = apply_filters('woocommerce_my_account_my_address_title', __('My Address', 'woocommerce'));
    $get_addresses = apply_filters('woocommerce_my_account_get_addresses', array(
        'billing' => __('Billing Address', 'woocommerce')
            ), $customer_id);
}
$address = array();
foreach ($get_addresses as $name => $title) :
    $address[$name] = array(
        'first_name' => get_user_meta($customer_id, $name . '_first_name', true),
        'last_name' => get_user_meta($customer_id, $name . '_last_name', true),
        'company' => get_user_meta($customer_id, $name . '_company', true),
        'address_1' => get_user_meta($customer_id, $name . '_address_1', true),
        'address_2' => get_user_meta($customer_id, $name . '_address_2', true),
        'city' => get_user_meta($customer_id, $name . '_city', true),
        'state' => get_user_meta($customer_id, $name . '_state', true),
        'postcode' => get_user_meta($customer_id, $name . '_postcode', true),
        'country' => get_user_meta($customer_id, $name . '_country', true)
    );
endforeach;
?>
<div class="my-account-content">
    <h2 class="my-account-title">My Dashboard</h2>
    <p>Hi <?php echo st_get_user_display_name() ?></p>
    <p>
        This is your Me & Mrs Jones dashboard. Keep your details up to date to make checking out a breeze and save you time, so you can get back to pleasure as quickly as possible. Head over to &OpenCurlyDoubleQuote; My Orders &CloseCurlyDoubleQuote; to check out where your order is or get in touch with our customer service team should you have any questions.
    </p>
    <div class="row">
        <div class="col-lg-6 my-account-summary-text-container">
            <h3>Contact Information</h3><form method="post"><input type="hidden" value="personal_details" name="personal_details"/><button type="submit">edit</button></form>
            <?php
            $user = wp_get_current_user();
            $user = $user->data;
            ?>
            <?php
            echo ($current_user->first_name) ? "<p class='first-p'>" . esc_attr($current_user->first_name . " " . $current_user->last_name) . "</p>" : "";
            echo ($user->user_email) ? "<p>" . esc_attr($user->user_email) . "</p>" : "";
            ?>
            <form method = "post"><input type = "hidden" value = "personal_details" name = "personal_details"/><button class="button-no-padding" type = "submit">Change Password</button></form>
        </div>
        <div class="col-lg-6">
            <h3>Manage Subscriptions</h3>
            <div class="onoffswitch" id="my-account-subscribe-toggle">
                <input id="my-account-customer-id" type="hidden" value="<?php echo $customer_id ?>"/>
                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" <?php echo (st_is_subscribed_user($customer_id) == 'true') ? 'checked' : '' ?>>
                <label class="onoffswitch-label" for="myonoffswitch">
                    <span class="onoffswitch-inner"></span>
                    <span class="onoffswitch-switch"></span>
                </label>
            </div>

            <p class="my-account-subscribe-status">You are currently <?php echo (st_is_subscribed_user($customer_id) == 'true') ? '' : ' not ' ?> subscribed to Me & Mrs Jones Emails. -<?php echo (st_is_subscribed_user($customer_id) == 'true') ? 'We hope you enjoy all the ' : 'Subcribe now for ' ?>offers from us and our affiliates!</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <h3>Default Billing Address</h3><form method="post"><input type="hidden" value="address_book" name="address_book"/><button type="submit">edit</button></form>
            <p>
                <?php
                $formatted_address = WC()->countries->get_formatted_address($address['billing']);

                if (!$formatted_address)
                    _e('You have not set up this type of address yet.', 'woocommerce');
                else
                    echo $formatted_address;
                ?>
            </p>
        </div>
        <div class="col-lg-6">
            <h3>Default Shipping Address</h3><form method="post"><input type="hidden" value="address_book" name="address_book"/><button type="submit">edit</button></form>
            <p>
                <?php
                $formatted_address = WC()->countries->get_formatted_address($address['shipping']);

                if (!$formatted_address)
                    _e('You have not set up this type of address yet.', 'woocommerce');
                else
                    echo $formatted_address;
                ?>
            </p>
        </div>
    </div>
    <h3 class="my-account-summary-last-order">Your last order</h3><form method="post"><input type="hidden" value="my_orders" name="my_orders"/><button type="submit">View All</button></form>
    <div class="col-lg-12 my-account-summary-order-wrapper">
        <?php
        if (count($customer_orders) > 0):
            $order = wc_get_order($customer_orders[0]);
            $item_count = $order->get_item_count();
            ?>
            <table>
                <thead>
                    <tr>
                        <td>Order</td>
                        <td>Date</td>
                        <td>Status</td>
                        <td>Total</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <form method="post">
                                <input type="hidden" name="my-order-detailed" value="my-order-detailed"/>
                                <input type="hidden" name="order_numnber" value="<?php echo $order->get_order_number(); ?>"/>
                                <button type="submit"><?php echo _x('#', 'hash before order number', 'woocommerce') . $order->get_order_number(); ?></button>
                            </form>
                        </td>
                        <td><time datetime="<?php echo date('Y-m-d', strtotime($order->order_date)); ?>" title="<?php echo esc_attr(strtotime($order->order_date)); ?>"><?php echo date_i18n(get_option('date_format'), strtotime($order->order_date)); ?></time></td>
                        <td><?php echo wc_get_order_status_name($order->get_status()); ?></td>
                        <td><?php echo sprintf(_n('%s for %s item', '%s for %s items', $item_count, 'woocommerce'), $order->get_formatted_order_total(), $item_count); ?></td>
                        <td>
                            <form method="post">
                                <input type="hidden" name="my-order-detailed" value="my-order-detailed"/>
                                <input type="hidden" name="order_numnber" value="<?php echo $order->get_order_number(); ?>"/>
                                <button type="submit">View</button>
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        <?php else: ?>
            <div class="col-lg-6">
                <p class="my-account-summary-order-text">
                    Looks like you haven't bought anything yet.<br>
                    What are you waiting for??
                </p>
            </div>
            <div class="col-lg-6">
                <a href="/">Continue Shopping<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
            </div>
        <?php endif; ?>
    </div>
</div>