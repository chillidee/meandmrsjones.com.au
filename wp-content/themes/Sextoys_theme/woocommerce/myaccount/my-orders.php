<?php
/**
 * My Orders
 *
 * Shows recent orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-orders.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */
if (!defined('ABSPATH')) {
    exit;
}
$my_orders_columns = apply_filters('woocommerce_my_account_my_orders_columns', array(
    'order-number' => __('Order', 'woocommerce'),
    'order-date' => __('Date', 'woocommerce'),
    'order-status' => __('Status', 'woocommerce'),
    'order-total' => __('Total', 'woocommerce'),
    'order-actions' => '&nbsp;',
        ));

$customer_orders = get_posts(apply_filters('woocommerce_my_account_my_orders_query', array(
    'numberposts' => $order_count,
    'meta_key' => '_customer_user',
    'meta_value' => get_current_user_id(),
    'post_type' => wc_get_order_types('view-orders'),
    'post_status' => array_keys(wc_get_order_statuses())
        )));
?>


<div class="my-account-content">
    <h2 class="my-account-title">My Orders</h2>

    <?php if ($customer_orders) : ?>
        <table class="order-history-table">
            <thead>
                <tr>
                    <td>Order</td>
                    <td>Date</td>
                    <td>Status</td>
                    <td>Total</td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($customer_orders as $customer_order) :
                    $order = wc_get_order($customer_order);
                    $item_count = $order->get_item_count();
                    ?>
                    <tr>
                        <td>
                            <form method="post">
                                <input type="hidden" name="my-order-detailed" value="my-order-detailed"/>
                                <input type="hidden" name="order_numnber" value="<?php echo $order->get_order_number(); ?>"/>
                                <button type="submit"><?php echo _x('#', 'hash before order number', 'woocommerce') . $order->get_order_number(); ?></button>
                            </form>
                        </td>
                        <td><time datetime="<?php echo date('Y-m-d', strtotime($order->order_date)); ?>" title="<?php echo esc_attr(strtotime($order->order_date)); ?>"><?php echo date_i18n(get_option('date_format'), strtotime($order->order_date)); ?></time></td>
                        <td><?php echo wc_get_order_status_name($order->get_status()); ?></td>
                        <td><?php echo sprintf(_n('%s for %s item', '%s for %s items', $item_count, 'woocommerce'), $order->get_formatted_order_total(), $item_count); ?></td>
                        <td>
                            <form method="post">
                                <input type="hidden" name="my-order-detailed" value="my-order-detailed"/>
                                <input type="hidden" name="order_numnber" value="<?php echo $order->get_order_number(); ?>"/>
                                <button type="submit">View</button>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>
</div>