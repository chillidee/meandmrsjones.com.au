<?php
/**
 * My Addresses
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
$customer_id = get_current_user_id();
if (isset($_POST['address_book_save_address'])):
    $cleanAddressBook = array();
    $address_book = get_user_meta($customer_id, 'address_book', true);
    foreach ($address_book as $address):
        if (isset($_POST['address_book_edited_address'])):
            if ($_POST["address_book_edited_orignalName"] != $address):
                array_push($cleanAddressBook, $address);
            endif;
        else:
            array_push($cleanAddressBook, $address);
        endif;
    endforeach;
    array_push($cleanAddressBook, $_POST['newAddressName']);
    $address_name = strip_tags($_POST['newAddressName']);
    $setBillingAddress = strip_tags($_POST["myaccount-set-address-as-default-billing"]);
    if ($setBillingAddress === 'on'):
        update_user_meta($customer_id, "billing_first_name", strip_tags($_POST["newAddressFirstName"]));
        update_user_meta($customer_id, "billing_last_name", strip_tags($_POST["newAddressLastName"]));
        update_user_meta($customer_id, "billing_company", strip_tags($_POST["newAddressCompanyName"]));
        update_user_meta($customer_id, "billing_address_1", strip_tags($_POST["newAddressAddressLine1"]));
        update_user_meta($customer_id, "billing_address_2", strip_tags($_POST["new-address-address-line2"]));
        update_user_meta($customer_id, "billing_city", strip_tags($_POST["newAddressAddressSuburb"]));
        update_user_meta($customer_id, "billing_postcode", strip_tags($_POST["newAddressAddressPostcode"]));
        update_user_meta($customer_id, "billing_state", strip_tags($_POST["new-address-address-state"]));
        update_user_meta($customer_id, "billing_phone", strip_tags($_POST["newAddressContactNumber"]));
    endif;
    $setShippingAddress = strip_tags($_POST["myaccount-set-address-as-default-shipping"]);
    if ($setShippingAddress === 'on'):
        update_user_meta($customer_id, "shipping_first_name", strip_tags($_POST["newAddressFirstName"]));
        update_user_meta($customer_id, "shipping_last_name", strip_tags($_POST["newAddressLastName"]));
        update_user_meta($customer_id, "shipping_company", strip_tags($_POST["newAddressCompanyName"]));
        update_user_meta($customer_id, "shipping_address_1", strip_tags($_POST["newAddressAddressLine1"]));
        update_user_meta($customer_id, "shipping_address_2", strip_tags($_POST["new-address-address-line2"]));
        update_user_meta($customer_id, "shipping_city", strip_tags($_POST["newAddressAddressSuburb"]));
        update_user_meta($customer_id, "shipping_postcode", strip_tags($_POST["newAddressAddressPostcode"]));
        update_user_meta($customer_id, "shipping_state", strip_tags($_POST["new-address-address-state"]));
        update_user_meta($customer_id, "shipping_phone", strip_tags($_POST["newAddressContactNumber"]));
    endif;
    update_user_meta($customer_id, "{$address_name}_first_name", strip_tags($_POST["newAddressFirstName"]));
    update_user_meta($customer_id, "{$address_name}_last_name", strip_tags($_POST["newAddressLastName"]));
    update_user_meta($customer_id, "{$address_name}_company", strip_tags($_POST["newAddressCompanyName"]));
    update_user_meta($customer_id, "{$address_name}_address_1", strip_tags($_POST["newAddressAddressLine1"]));
    update_user_meta($customer_id, "{$address_name}_address_2", strip_tags($_POST["new-address-address-line2"]));
    update_user_meta($customer_id, "{$address_name}_city", strip_tags($_POST["newAddressAddressSuburb"]));
    update_user_meta($customer_id, "{$address_name}_postcode", strip_tags($_POST["newAddressAddressPostcode"]));
    update_user_meta($customer_id, "{$address_name}_state", strip_tags($_POST["new-address-address-state"]));
    update_user_meta($customer_id, "{$address_name}_phone", strip_tags($_POST["newAddressContactNumber"]));
    update_user_meta($customer_id, 'address_book', $cleanAddressBook);
endif;
if (isset($_POST["my-account-delete-address"])):
    $address_name = $_POST["my-account-delete-address-name"];
    $cleanAddressBook = array();
    $address_book = get_user_meta($customer_id, 'address_book', true);
    foreach ($address_book as $address):
        if ($address_name != $address):
            array_push($cleanAddressBook, $address);
        endif;
    endforeach;
    update_user_meta($customer_id, 'address_book', $cleanAddressBook);
    delete_user_meta($customer_id, "{$address_name}_first_name");
    delete_user_meta($customer_id, "{$address_name}_last_name");
    delete_user_meta($customer_id, "{$address_name}_company");
    delete_user_meta($customer_id, "{$address_name}_address_1");
    delete_user_meta($customer_id, "{$address_name}_address_2");
    delete_user_meta($customer_id, "{$address_name}_city");
    delete_user_meta($customer_id, "{$address_name}_postcode");
    delete_user_meta($customer_id, "{$address_name}_state");
    delete_user_meta($customer_id, "{$address_name}_phone");
endif;
$addresses = get_user_meta($customer_id, 'address_book', true);
$cleanAddresses = array();
$cleanAddresses["billing"] = __('Billing Address', 'woocommerce');
$cleanAddresses["shipping"] = __('Shipping Address', 'woocommerce');
foreach ($addresses as $address):
    $addressuc = ucfirst($address);
    $cleanAddresses["{$address}"] = __("{$addressuc} Address", "woocommerce");
endforeach;

if (!wc_ship_to_billing_address_only() && get_option('woocommerce_calc_shipping') !== 'no') {
    $page_title = apply_filters('woocommerce_my_account_my_address_title', __('My Addresses', 'woocommerce'));
    $get_addresses = apply_filters('woocommerce_my_account_get_addresses', $cleanAddresses, $customer_id);
} else {
    $page_title = apply_filters('woocommerce_my_account_my_address_title', __('My Address', 'woocommerce'));
    $get_addresses = apply_filters('woocommerce_my_account_get_addresses', array(
        'billing' => __('Billing Address', 'woocommerce')
            ), $customer_id);
}
$col = 1;
?>
<div class="my-account-content">
    <h2 class="my-account-title">Address Book</h2>

<!--<p class="myaccount_address">
    <?php //echo apply_filters('woocommerce_my_account_my_address_description', __('The following addresses will be used on the checkout page by default.', 'woocommerce')); ?>
</p>-->

    <?php //if (!wc_ship_to_billing_address_only() && get_option('woocommerce_calc_shipping') !== 'no') echo '<div class="col2-set addresses">'; ?>

    <?php $index = 0; ?>
    <div class="col-lg-6">
        <h3>Default Addresses</h3>
        <p class="address-book-discription">To setup different Billing and Shipping addresses please use the 'Add New Address" link.</p>
        <?php
        foreach ($get_addresses as $name => $title) :
            if ($index == 2):
                ?>
            </div><div class="col-lg-6">
            <?php endif;
            ?>

            <div class="col-<?php echo ( ( $col = $col * -1 ) < 0 ) ? 1 : 2; ?> address">
                <header class="title">
                    <h3><?php echo $title; ?></h3>
            <!--            <a href="<?php //echo wc_get_endpoint_url('edit-address', $name);                                  ?>" class="edit"><?php //_e('Edit', 'woocommerce');                                  ?></a>-->
                    <?php if ($name != 'billing' && $name != 'shipping'): ?>
                        <form method="post">
                            <input name="my-account-delete-address" type="hidden" value="my-account-delete-address" />
                            <input name="my-account-delete-address-name" type="hidden" value="<?php echo $name ?>" />
                            <button>Delete</button>
                        </form>
                    <?php endif; ?>
                    <form method="post">
                        <input name="my-account-edit-address" type="hidden" value="my-account-edit-address" />
                        <input name="my-account-delete-address-name" type="hidden" value="<?php echo $name ?>" />
                        <button>Edit</button>
                    </form>
                </header>
                <address>
                    <?php
                    $address = apply_filters('woocommerce_my_account_my_address_formatted_address', array(
                        'first_name' => get_user_meta($customer_id, $name . '_first_name', true),
                        'last_name' => get_user_meta($customer_id, $name . '_last_name', true),
                        'company' => get_user_meta($customer_id, $name . '_company', true),
                        'address_1' => get_user_meta($customer_id, $name . '_address_1', true),
                        'address_2' => get_user_meta($customer_id, $name . '_address_2', true),
                        'city' => get_user_meta($customer_id, $name . '_city', true),
                        'state' => get_user_meta($customer_id, $name . '_state', true),
                        'postcode' => get_user_meta($customer_id, $name . '_postcode', true),
                        'country' => get_user_meta($customer_id, $name . '_country', true)
                            ), $customer_id, $name);

                    $formatted_address = WC()->countries->get_formatted_address($address);

                    if (!$formatted_address)
                        _e('You have not set up this type of address yet.', 'woocommerce');
                    else
                        echo $formatted_address;
                    ?>
                </address>
            </div>
            <?php
            $index++;
        endforeach;
        ?>

        <form method="post">
            <input name="my-account-new-address" type="hidden" value="my-account-new-address" />
            <button class="add-new-address-button" type="submit">Add New Address</button>
        </form>
    </div>
</div>
<?php if (!wc_ship_to_billing_address_only() && get_option('woocommerce_calc_shipping') !== 'no') echo '</div>'; ?>
