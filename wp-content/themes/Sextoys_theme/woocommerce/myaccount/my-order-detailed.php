<?php
$order_id = $_POST["order_numnber"];
$customer_orders = get_posts(apply_filters('woocommerce_my_account_my_orders_query', array(
    'numberposts' => $order_count,
    'meta_key' => '_customer_user',
    'meta_value' => get_current_user_id(),
    'post_type' => wc_get_order_types('view-orders'),
    'post_status' => array_keys(wc_get_order_statuses())
        )));
$order_index = '';
foreach ($customer_orders as $index => $order):
    if ($order->ID == $order_id):
        $order_index = $index;
    endif;
endforeach;
$order_total = count($customer_orders);
$order = wc_get_order($order_id);
$order_items = $order->get_items();
$order_total_price = $order->get_total();
$order_shipping = $order->get_total_shipping();
$order_tax = $order->get_cart_tax();
$order_discount = $order->get_total_discount();
$order_subtotal = $order->get_formatted_order_total();
$order_coupons = $order->get_used_coupons();
$order_notes = $order->get_customer_order_notes();
$order_payment_title = $order->$payment_method_title;
$order_payment = $order->$payment_method;
$order_subtotal_price = $order->get_subtotal();

switch ($order->get_status()):
    case "cancelled":
        $order_class = "my-account-single-order-status-cancelled";
        break;
    case "processing":
        $order_class = "my-account-single-order-status-processing";
        break;
    case "completed":
        $order_class = "my-account-single-order-status-completed";
        break;
    default:
        $order_class = '';
        break;
endswitch;
?>
<div class="my-account-content">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 my-account-single-subtitle-wrapper new-order-receipt-subtitle">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <p class="my-account-single-order-number-title">Order Number</p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <p class="my-account-single-order-number"><?php echo $order->get_order_number() ?></p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <a class="my-account-single-order-email"><img src="https://s3.amazonaws.com/adult-toys/images/site_images/mail_merlot.png"/></a>
            <div id="order-send-pdf"><div class="order-send-pdf-close overlay-close"><i class="fa fa-times" aria-hidden="true"></i></div><input type="hidden" id="order-send-pdf-order-id" value="<?php echo $order_id ?>"><p>Enter email address.</p><input type="email" required placeholder="Email Address" id="order-send-pdf-email"/><button type="button" id="order-send-pdf-button">Send</button><p class="order-email-error">Please enter a valid email address.</p></div>
            <form target="_blank" action="/receipt" method="post" class="my-account-single-order-print desktop-only"><input type="hidden" name="order_id" value="<?php echo $order_id ?>"/>
                <button type="submit"><img src="https://s3.amazonaws.com/adult-toys/images/site_images/print_merlot.png"/></button>
            </form>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 my-account-single-subtitle-wrapper">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <p class="my-account-single-order-status <?php echo $order_class ?>"><?php echo wc_get_order_status_name($order->get_status()); ?></p>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <p class="my-account-single-order-summary-title">Order Summary</p>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <p class="my-account-single-order-summary-date"><time datetime="<?php echo date('Y-m-d', strtotime($order->order_date)); ?>" title="<?php echo esc_attr(strtotime($order->order_date)); ?>"><?php echo date_i18n(get_option('date_format'), strtotime($order->order_date)); ?></time></p>
            <p class="my-account-single-order-summary-price"><?php echo sprintf(_n('%s for %s item', '%s  AUD for %s items', $item_count, 'woocommerce'), $order->get_formatted_order_total(), $item_count); ?></p>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding-mobile-450">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 delivery-details">
                <p>
                    Delivery Details
                </p>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 my-account-single-order-shipping-details">
                    <p class="my-account-single-order-shipping-to">Ship To:</p>
                    <p class="my-account-single-order-shipping-to-text">
                        <?php echo $order->get_formatted_shipping_address() ?>
                    </p>

                    <?php
                    if ($order->customer_message):
                        ?>
                        <p>Delivery Notes:</p>
                        <p class="my-account-single-order-shipping-to-text"><?php echo $order->customer_message ?></p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 my-account-single-order-shipping-details">
                <img src="https://s3.amazonaws.com/adult-toys/images/site_images/main-header-logo.png"/>
                <p>
                    <span>ABN:</span><br>
                    59  612 024 610
                </p>
                <p>
                    <span>Address:</span><br>
                    Level 2 608-612,<br>
                    Liverpool Road,<br>
                    South Strathfield,<br>
                    NSW 2136<br>
                </p>
                <p>
                    <span>email:</span><br>
                    <a href="mailto:info@meandmrsjones.com.au">info@mmj</a>
                </p>
            </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 item-summary">
            <p class="item-summary-title">
                Item Summary
            </p>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding item-summary-products">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <p>Product</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <p class="item-quantity">QTY</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <p>Total</p>
                </div>
            </div>
            <?php
            $total_items = 0;
            foreach ($order_items as $item):
                $total_items = $total_items + (int) $item["qty"];
                ?>
                <?php
                $product_id = $item["item_meta"]["_product_id"][0];
                ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <p class="my-account-single-order-product-title"><?php echo $item['name'] ?></p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <p class="item-quantity"><?php echo $item["qty"] ?></p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <p>$<?php echo money_format('%.2n', $item["line_subtotal"]) ?> AUD</p>
                </div>
            <?php endforeach; ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 my-account-single-order-summary-item-total">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <p>item total</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <p class="item-quantity"><?php echo $total_items ?></p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <p><?php echo '$' . money_format('%.2n', $order_subtotal_price) ?> AUD</p>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 my-account-single-order-summary-total-breakdown">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 first-col">
                        <p>Shipping</p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">
                        <p><?php echo (isset($order_shipping) && $order_shipping != 0) ? 'Flat rate shipping' : 'Free Shipping'; ?></p>
                    </div>
                    <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-6 last-col">
                        <p><?php echo (isset($order_shipping) && $order_shipping != 0) ? "$" . money_format('%.2n', $order_shipping) . ' AUD' : "$0.00"; ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-6 first-col">
                        <p>Disc.</p>
                    </div>
                    <div class = "col-lg-4 col-md-4 col-sm-4 hidden-xs">
                        <?php
                        if ($order_coupons):
                            foreach ($order_coupons as $coupon):
                                ?>
                                <p><?php echo $coupon ?></p>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </div>
                    <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-6 last-col">
                        <p><?php echo (isset($order_discount) && $order_discount != 0) ? "$" . money_format('%.2n', $order_discount) . ' AUD' : "$0.00"; ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-6 first-col">
                        <p>Taxes</p>
                    </div>
                    <div class = "col-lg-4 col-md-4 col-sm-4 hidden-xs">
                        <p>Includes GST*</p>
                    </div>
                    <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-6 last-col">
                        <p><?php echo (isset($order_tax) && $order_tax != 0) ? "$" . money_format('%.2n', $order_tax) . ' AUD' : "$0.00"; ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6 first-col total-paid">
                        <p>Total paid</p>
                    </div>
                    <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-6 col-lg-offset-2 total-paid last-col">
                        <p><span class="amount"><?php echo (isset($order_subtotal)) ? strip_tags($order_subtotal) . ' AUD' : '' ?></span></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 receipt-payment-breakdown no-padding-mobile-450">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding-mobile-450">
                    <p>Payment Type</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding-mobile-450">
                    <p><?php echo $order->payment_method_title ?></p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding-mobile-450">
                    <p><?php //echo $order->get_transaction_id()       ?>Purchase Date</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding-mobile-450">
                    <p><?php echo date('d-m-Y', strtotime($order->order_date)); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>