<?php
if (!defined('ABSPATH')) {
    exit;
}
require_once get_template_directory() . '/functions/reviews_class.php';
$review_class = new reviews_class();
$_pf = new WC_Product_Factory();
$_product = $_pf->get_product($product_id);
wp_enqueue_style('stars');
wp_enqueue_script('review-js');
$hasReview = reviews_class::checkForReview($_product->id, $current_user->ID);
if ($hasReview):
    $review = json_decode($hasReview->comment_content);
    echo "<input type='hidden' name='hasReviewStar' value='$review->stars'/>";
    $title = $review->title;
    $review = $review->review;
endif;
?>
<div class="ajax-response"></div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <form style="width:100%" method="POST" name="review-form">
        <p style="text-align: center;padding:5px;font-size: 28px;"><?php echo $_product->post->post_title ?></p>
        <p style="padding: 0; text-align: center;font-size: 20px;margin: 0">Select Star Rating</p>
        <div class="cont">
            <div class="stars">
                <input class="star star-5" id="star-5-2" value="5" type="radio" name="star"/>
                <label class="star star-5" for="star-5-2"></label>
                <input class="star star-4" id="star-4-2" value="4" type="radio" name="star"/>
                <label class="star star-4" for="star-4-2"></label>
                <input class="star star-3" id="star-3-2" value="3" type="radio" name="star"/>
                <label class="star star-3" for="star-3-2"></label>
                <input class="star star-2" id="star-2-2" value="2" type="radio" name="star"/>
                <label class="star star-2" for="star-2-2"></label>
                <input class="star star-1" id="star-1-2" value="1" type="radio" name="star"/>
                <label class="star star-1" for="star-1-2"></label>
            </div>
        </div>
        <p style="padding: 0;font-size: 18px;">Title</p>
        <input type="text" name='review-title' class="form-control" value="<?php echo $title ?>">
        <p style="padding: 0;font-size: 18px;">Review</p>
        <textarea class="form-control" style="width: 100%; height: 400px;border-radius: 5px;" name="rewiew-text"><?php echo (isset($review) && !empty($review)) ? $review : ''; ?></textarea>
        <input name='review_post_id' type="hidden" value="<?php echo $_product->id ?>"/>
        <button style="margin: 20px 0;background-color: #851D19;padding: 15px;font-size: 20px;font-style: normal;color: #FEFCD3;text-transform: uppercase;text-decoration: none; border: none;display: block;" class="btn btn-success save-review" name='save-review' value="<?php echo $_product->id ?>">Leave Review</button>
    </form>
</div>
