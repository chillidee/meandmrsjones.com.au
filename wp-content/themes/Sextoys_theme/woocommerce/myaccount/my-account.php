<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
do_action('woocommerce_before_my_account');
?>
<div class="row">
    <div class="col-lg-12 no-padding">
        <div class="sub-cat-background-image">
        </div>
        <div class="col-lg-12 no-padding">            
            <div class="col-lg-2 col-md-2 my-account-sidebar no-padding" id="my-account-sidebar">
                <ul>
                    <li <?php echo (!$_POST || isset($_POST['my_dashboard']) || isset($_POST['thankyouOrderId'])) ? 'class="my-account-active-button"' : '' ?>>
                        <form method="post">
                            <input type="hidden" name="my_dashboard" value="my_dashboard"/>
                            <button type="submit">My Dashboard</button>
                        </form>
                    </li>
                    <li <?php echo (isset($_POST['personal_details'])) ? 'class="my-account-active-button"' : '' ?>>
                        <form method="post">
                            <input type="hidden" name="personal_details" value="personal_details"/>
                            <button type="submit">Personal Details</button>
                        </form>
                    </li>
                    <li <?php echo (isset($_POST['address_book']) || isset($_POST['my-account-new-address']) || $_POST['my-account-edit-address'] || $_POST['my-account-delete-address']) ? 'class="my-account-active-button"' : '' ?>>
                        <form method="post">
                            <input type="hidden" name="address_book" value="address_book"/>
                            <button type="submit">Address Book</button>
                        </form>
                    </li>
                    <?php if (wc_get_customer_order_count(get_current_user_id()) > 0): ?>
                        <li <?php echo (isset($_POST['my_orders']) || isset($_POST['my-order-detailed'])) ? 'class="my-account-active-button"' : '' ?>>
                            <form method="post">
                                <input type="hidden" name="my_orders" value="my_orders"/>
                                <button type="submit">My Orders</button>
                            </form>
                        </li>
                    <?php endif; ?>
                    <li <?php echo (isset($_POST['customer_service'])) ? 'class="my-account-active-button"' : '' ?>>
                        <form method="post">
                            <input type="hidden" name="customer_service" value="customer_service"/>
                            <button  type="submit">Customer Service</button>
                        </form>
                    </li>
                </ul>
            </div>
            <div class="col-lg-7 col-md-10 my-account-content-wrapper" id="my-account-content-wrapper">
                <div class="col-lg-12">
                    <?php
                    wc_print_notices();
                    ?>
                    <?php
                    switch (true):
                        case $_POST['my_dashboard']:
                            wc_get_template('myaccount/my-account-summary.php', array('current_user' => $current_user));
                            break;
                        case $_POST['personal_details']:
                            wc_get_template('myaccount/form-edit-account.php', array('current_user' => $current_user));
                            break;
                        case $_POST['address_book']:
                            wc_get_template('myaccount/my-address.php');
                            break;
                        case $_POST['my_orders']:
                            wc_get_template('myaccount/my-orders.php', array('order_count' => $order_count));
                            break;
                        case $_POST['my-order-detailed']:
                            wc_get_template('myaccount/my-order-detailed.php', array('order_count' => $order_count));
                            break;
                        case $_POST['customer_service']:
                            wc_get_template('myaccount/my-account-customer-service.php');
                            break;
                        case $_POST['my-account-new-address']:
                            wc_get_template('myaccount/my-new-address.php', array('current_user' => $current_user));
                            break;
                        case $_POST['my-account-delete-address']:
                            wc_get_template('myaccount/my-address.php');
                            break;
                        case $_POST['my-account-edit-address']:
                            wc_get_template('myaccount/my-new-address.php', array('current_user' => $current_user));
                            break;
                        case $_POST['my-reviews']:
                        case $_GET['my-reviews']:
                            wc_get_template('myaccount/my-reviews.php', array('current_user' => $current_user));
                            break;
                        case $_POST['review_product_id']:
                            wc_get_template('myaccount/my-reviews-edit.php', array('current_user' => $current_user, 'product_id' => $_POST['review_product_id']));
                            break;
                        case $_POST['rewiew-text']:
                            echo '<script>window.location.replace("/my-account?my-reviews=true")</script>';
                            require_once get_template_directory() . '/functions/reviews_class.php';
                            reviews_class::addReview($_POST, $current_user);
                            break;
                        default:
                            wc_get_template('myaccount/my-account-summary.php', array('current_user' => $current_user));
                            break;
                    endswitch;
                    do_action('woocommerce_after_my_account');
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>