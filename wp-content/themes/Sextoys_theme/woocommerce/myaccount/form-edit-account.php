<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.1
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
?>

<?php wc_print_notices(); ?>



<div class="my-account-content">
    <h2 class="my-account-title">Personal Details</h2>

    <form class="edit-account" action="" method="post">
        <input type="hidden" name="personal_details" value="personal_details"/>
        <div class="col-lg-12">
            <div class="col-lg-12">
                <?php do_action('woocommerce_edit_account_form_start'); ?>
                <div class="col-lg-6">
                    <p>
                        <label for="account_first_name"><?php _e('First name', 'woocommerce'); ?> <span class="required">*</span></label>
                        <input type="text" class="input-text" name="account_first_name" id="account_first_name" value="<?php echo esc_attr($current_user->first_name); ?>" />
                    </p>
                </div>
                <div class="col-lg-6">
                    <p>
                        <label for="account_last_name"><?php _e('Last name', 'woocommerce'); ?> <span class="required">*</span></label>
                        <input type="text" class="input-text" name="account_last_name" id="account_last_name" value="<?php echo esc_attr($current_user->last_name); ?>" />
                    </p>
                </div>
                <div class="col-lg-6">
                    <p>
                        <label for="account_email"><?php _e('Email address', 'woocommerce'); ?> <span class="required">*</span></label>
                        <input readonly type="email" class="input-text" name="account_email" id="account_email" value="<?php echo esc_attr($current_user->user_email); ?>" />
                    </p>
                </div>
                <div class="col-lg-6">
                    <p>
                        <label for="account_phone"><?php _e('Mobile Number', 'woocommerce'); ?> <span class="required">*</span></label>
                        <input type="number" class="input-text" name="account_phone" id="account_phone" value="<?php echo get_user_meta($current_user->data->ID, 'billing_phone', true) ?>" />
                    </p>
                </div>
                <div class="col-lg-6 squaredFour">
                    <input id="my-account-change-password-toggle" name="change-password-toggle" type="checkbox" checked="false"/>
                    <label class="my-account-change-password-label" for="my-account-change-password-toggle"></label>
                    <p>Change password?</p>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="col-lg-12 my-account-change-password-container">
                <fieldset>
                    <div class="col-lg-12">
                        <legend><?php _e('Change my Password', 'woocommerce'); ?></legend>
                        <p class="my-account-password-change-info">Passwords are case sensitive and must be 8-20 characters in length, a combination of uppercase and lowercase letters and either numbers or symbols (eg. @&!*)</p>
                    </div>
                    <div class="col-lg-6">
                        <p class="form-row form-row-wide">
                            <label for="password_current"><?php _e('Current Password', 'woocommerce'); ?></label>
                            <input type="password" class="input-text" name="password_current" id="password_current" />
                        </p>
                    </div>
                    <div class="col-lg-6">
                        <p class="form-row form-row-wide">
                            <label for="password_1"><?php _e('New Password', 'woocommerce'); ?></label>
                            <input type="password" class="input-text" name="password_1" id="password_1" />
                        </p>
                    </div>
                    <div class="col-lg-6">
                        <p class="form-row form-row-wide">
                            <label for="password_2"><?php _e('Confirm New Password', 'woocommerce'); ?></label>
                            <input type="password" class="input-text" name="password_2" id="password_2" />
                        </p>
                    </div>
                </fieldset>
                <div class="clear"></div>

                <?php do_action('woocommerce_edit_account_form'); ?>
                <?php do_action('woocommerce_edit_account_form_end'); ?>
            </div>
        </div>
        <div class="col-lg-6 col-lg-offset-3">
            <p>
                <?php wp_nonce_field('save_account_details'); ?>
                <input  type="submit" id="my-account-save-personals-button" class="button" name="save_account_details" value="<?php esc_attr_e('Save', 'woocommerce'); ?>" />
                <input type="hidden" name="action" value="save_account_details" />
            </p>
        </div>
</div>
</form>
</div>