<?php
if (!defined('ABSPATH')) {
    exit;
}
require_once get_template_directory() . '/functions/reviews_class.php';
$review_class = new reviews_class();
?>
<div class="my-account-content">
    <h2 class="my-account-title">My Reviews</h2>
    <?php
    $items = $review_class->getItemsBroughtByCustomer($current_user->ID);
    if (isset($items) && !empty($items)):
        foreach ($items as $id => $title):
            if (reviews_class::checkIfProductExists($id)):
                ?>
                <div style="padding: 10px 0" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 my-reviews-product-info">
                        <img src="<?php echo $review_class->getProductImageURL($id) ?>" height="50" width="50"/>
                        <a target="_blank" href="<?php the_guid($id) ?>"><p style="margin: 0;display: inline-block;margin-left: 15px;"><?php echo $title ?></p></a>
                    </div>
                    <?php $hasReview = reviews_class::checkForReview($id, $current_user->ID) ?>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 my-reviews-add-review-button">
                        <form method="POST" action="\my-account">
                            <button style="margin-top: 12px;" name='review_product_id' value="<?php echo $id ?>"><?php echo ($hasReview) ? 'Edit Review' : 'Leave Review' ?></button>
                        </form>
                    </div>
                </div>
                <?php
            endif;
        endforeach;
    endif;
    ?>

</div>

