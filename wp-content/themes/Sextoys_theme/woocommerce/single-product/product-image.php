<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.14
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $post, $woocommerce, $product;
?>
<div class="col-lg-5 col-lg-offset-1 col-xs-10 col-xs-offset-1 col-md-5 col-md-offset-1">
    <div class="images  product-image">
        <?php
        if (has_post_thumbnail()) {
            $image_caption = get_post(get_post_thumbnail_id())->post_excerpt;
            $image_link = wp_get_attachment_url(get_post_thumbnail_id());
            $image = get_the_post_thumbnail($post->ID, apply_filters('single_product_large_thumbnail_size', 'shop_single'), array(
                'title' => get_the_title(get_post_thumbnail_id())
            ));

            $attachment_count = count($product->get_gallery_attachment_ids());

            if ($attachment_count > 0) {
                $gallery = '[product-gallery]';
            } else {
                $gallery = '';
            }
            ?>
            <div class="zoom">
                <?php
                echo apply_filters('woocommerce_single_product_image_html', sprintf('<img src="%s" class="woocommerce-main-image main-product-image zoom" title="%s"/>', $image_link, $image_caption, $image), $post->ID);
                ?>
            </div>
            <?php
        } else {
            $main_image = get_post_meta($post->ID, 'main_image_link', true);
            if ($main_image) {
                ?>
                <img src="<?php echo $main_image ?>"alt="<?php the_title() ?>"/>
                <?php
            } else {
                echo apply_filters('woocommerce_single_product_image_html', sprintf('<img src="%s" alt="%s" />', wc_placeholder_img_src(), __('Placeholder', 'woocommerce')), $post->ID);
            }
            unset($main_image);
        }
        do_action('woocommerce_product_thumbnails');
        ?>
    </div>
    <div class="col-lg-12 col-xs-12 product-gallery-wrapper">
        <?php
        $_product_id = $post->ID;
        $gallery_array = st_get_products_gallery_ids($_product_id);
        foreach ($gallery_array as $image):
            if ($image):
                ?>
                <div class="col-lg-3 col-xs-3 gallery-image-container">
                    <?php echo st_get_products_gallery_image($image) ?>
                </div>
                <?php
            endif;
        endforeach;
        ?>
    </div>
    <div class="col-lg-12 col-xs-12 video-container">
        <?php echo st_get_product_youtube_link($_product_id) ?>
    </div>
    <?php
    if (st_get_number_of_products_in_category(st_get_related_cat_slug_from_id($_product_id), $_product_id) > 0):
        ?>
        <div class="col-lg-12 col-xs-12 full-product-related">
            <p>Customers also bought</p>
            <?php
            add_related_products_after_product(6, st_get_related_cat_slug_from_id($_product_id), 'product');
            ?>
        </div>
    <?php endif; ?>
</div>