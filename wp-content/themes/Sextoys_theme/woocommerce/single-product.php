<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
get_header();
$_product_id = $post->ID;
$_product = wc_get_product($_product_id);
$post = $_product->post;
$_product_colour = get_post_meta($_product->id, 'st_colour', true);
$_product_size = get_post_meta($_product->id, 'st_size', true);
$associated_products = st_get_associated_products($_product_id);
$cat_object = get_product_category($post->ID);
$offset = 0;
$apparelCatIdArrays = st_get_apparel_cats();
if (isset($cat_object)):
    $image_array = st_get_product_background_image($cat_object);
    $children = st_cat_get_children($cat_object);
    $parent_cat_object = st_get_cat_object($cat_object);
    $cat_id = $cat_object;
endif;
if (isset($cat_id)):
    $parent_cat = st_get_cat_object(st_cat_top_level_parent($cat_id));
endif;
if (isset($parent_cat)):
    $parent_children = st_cat_get_children($parent_cat->term_id);
endif;
?>
<div class = "col-lg-12 prodct-cat-background-image"></div>
<?php
unset($image_array);
$currentSlugs = st_parent_and_current_cat_slug($_product_id);
$parent_slug = $currentSlugs[0];
$slug = $currentSlugs[1];
unset($currentSlugs);
$prevoius_page = $_SERVER['HTTP_REFERER'];
require_once get_template_directory() . '/functions/reviews_class.php';
?>
<div id="archive-sidebar" class="col-lg-2 archive-sidebar for-her-sidebar title-sidebar desktop-only" style="position: relative">
    <?php include get_template_directory() . '/partials/all_products_sidebar.php'; ?>
</div>
<div class="col-xs-12 col-md-12 col-sm-12 col-lg-7 archive-back-button-wrapper no-padding">
    <a class='mobile-previous-page archive-mobile-back-button' onclick="window.location = document.referrer" style='display:block;'><i class='fa fa-chevron-left' aria-hidden='true'></i>Back</a>
</div>
<div id="archive-display-product" class="archive-display-product col-lg-10 col-md-12 col-xs-12 col-sm-12 no-padding-mobile-450 no-padding">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 full-product-breadcrumb"><?php echo st_get_breadcrumb($_product_id) ?></div>
    <input type="hidden" id="archive-product-right-col-id" value="<?php echo $post->ID ?>"/>
    <div class="archive-right_col_product_container col-lg-12 col-md-12 col-xs-12 col-sm-12 no-padding-mobile-450 no-padding">
        <div class="col-lg-12 archive-product-ajax-response"></div>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 no-padding-mobile-450 no-padding">
            <div class="full-product-mobile-title col-lg-10 col-md-10 col-sm-offset-1 col-sm-10 col-md-offset-1 col-xs-12 col-sm-12"><h2><?php echo $post->post_title ?></h2></div>
            <div class="full-product-mobile-price col-lg-10 col-md-10 col-sm-offset-1 col-sm-10 col-md-offset-1 col-md-12 col-xs-12 col-sm-12">
                <?php
                $retail_price = get_post_meta($_product_id, '_retail_price', true);
                if ($retail_price):
                    ?>
                    <span class="retail_price_full">Retail Price $<?php echo $retail_price ?> AUD</span>
                <?php endif; ?>
                <h4>Our Price <?php echo $_product->get_price_html(); ?> AUD</h4></div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 center-block image-box archive-right-col-product-image zoomImg">
                <?php
                $image = st_get_the_post_full_image($_product_id);
                if (!strpos($image, 'src')):
                    ?>
                    <img style="display: none" class="attachment-full size-full wp-post-image main_image" src='<?php echo $image ?>' alt="<?php echo $post->post_title ?>"/>
                    <?php
                else:
                    echo $image;
                endif;
                ?>
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 product-gallery-wrapper">
                    <?php
                    $gallery_array = get_post_meta($_product_id, 'gallery_image_link', true);
                    $gallery = get_post_gallery_images($_product_id);
                    $attachment_ids = $_product->get_gallery_attachment_ids();
                    $gallery_array = explode(' ', $gallery_array);
                    foreach ($attachment_ids as $image_url):
                        array_push($gallery_array, wp_get_attachment_url($image_url));
                    endforeach;
                    if ($gallery_array):
                        foreach ($gallery_array as $image):
                            if ($image && strlen($image) > 2):
                                ?>
                                <div class="col-lg-3 col-xs-6 col-md-6 col-xs-6 col-sm-6 gallery-image-container">
                                    <img style="display: none;" class="gallery-image" src="<?php echo $image ?>" alt="Gallery Image"/>
                                    <img src="https://s3.amazonaws.com/adult-toys/images/site_images/product-image-placeholder.jpg" class="image-placeholder" alt="Me &amp; Mrs Jones">
                                </div>
                                <?php
                            endif;
                        endforeach;
                    endif;
                    unset($gallery_array);
                    ?>
                </div>
                <?php
                /*include 'partials/full_product_mobile.php';*/
                ?>
                <div class="col-lg-12 col-xs-12 video-container">
                    <?php
                    $video_link = get_post_meta($_product_id, 'youtube_link', true);
                    if ($video_link):
                        echo $video_link;
                    endif;
                    unset($video_link);
                    ?>
                </div>
                <?php								
                if (st_get_number_of_products_in_category(st_get_related_cat_slug_from_id($_product_id), $_product_id) > 0):
                    ?>
                    <div class="col-lg-12 col-xs-12 full-product-related">
                        <p class="suggest-products">Customers also bought</p>
                        <?php
                        add_related_products_after_product(2, st_get_related_cat_slug_from_id($_product_id), $_product_id);
                        ?>
                    </div>
					<script>if($('.product-full-related').length == 0) { $('.suggest-products').hide(); }</script>
                <?php endif; ?>
            </div>
            <?php
            include 'partials/full_product_desktop.php';
            ?>
        </div>
    </div>
</div>
<?php
get_footer();
?>