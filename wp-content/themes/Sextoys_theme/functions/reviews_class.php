<?php

class reviews_class {

    private function getOrdersFromCustomerID($customer_id) {
        return get_posts(apply_filters('woocommerce_my_account_my_orders_query', array(
            'numberposts' => -1,
            'meta_key' => '_customer_user',
            'meta_value' => $customer_id,
            'post_type' => wc_get_order_types('view-orders'),
            'post_status' => "wc-completed"
        )));
    }

    public function getItemsBroughtByCustomer($customer_id) {
        $orders = array();
        foreach ($this->getOrdersFromCustomerID($customer_id) as $order):
            $order = new WC_Order($order->ID);
            $order = $order->get_items();
            foreach ($order as $item):
                $orders[$item["product_id"]] = $item["name"];
            endforeach;
        endforeach;
        return $orders;
    }

    public function getProductImageURL($product_id) {
        $image = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'single-post-thumbnail');
        $image = (!$image) ? get_post_meta($product_id, 'main_image_link', true) : $image[0];
        return (isset($image) && !empty($image)) ? $image : wc_placeholder_img_src();
    }

    public static function addReview($post, $current_user) {

        $review = strip_tags($post["rewiew-text"]);
        $stars = strip_tags($post["star"]);
        $title = strip_tags($post["review-title"]);
        $postId = strip_tags($post["review_post_id"]);

        if (self::checkForReview($postId, $current_user->ID)):
            self::deleteUserReviews($postId, $current_user->ID);
        endif;

        $time = current_time('mysql');

        $data = array(
            'comment_post_ID' => $postId,
            'comment_author' => $current_user->data->display_name,
            'comment_author_email' => $current_user->data->user_email,
            'comment_author_url' => '',
            'comment_content' => json_encode(array('stars' => $stars, 'review' => $review, 'title' => $title)),
            'comment_type' => 'review',
            'comment_parent' => 0,
            'user_id' => $current_user->ID,
            'comment_author_IP' => $_SERVER["HTTP_HOST"],
            'comment_agent' => $_SERVER['HTTP_USER_AGENT'],
            'comment_date' => $time,
            'comment_approved' => 0,
        );

        wp_insert_comment($data);
    }

    public static function checkForReview($post_id, $user_id) {
        global $wpdb;
        $query = "SELECT * FROM `wp_comments` WHERE `comment_post_ID` = $post_id AND `comment_type` LIKE 'review' AND `user_id` = $user_id";
        $result = $wpdb->get_results($query);
        return (isset($result) && !empty($result)) ? $result[0] : false;
    }

    public static function deleteUserReviews($post_id, $user_id) {
        global $wpdb;
        $wpdb->query("DELETE FROM `wp_comments` WHERE `comment_post_ID` = $post_id AND `user_id` = $user_id");
    }

    public static function getReviews($post_id) {
        global $wpdb;
        $query = "SELECT * FROM `wp_comments` WHERE `comment_post_ID` = $post_id AND `comment_type` LIKE 'review' AND `comment_approved` LIKE '1'";
        $result = $wpdb->get_results($query);
        return (isset($result) && !empty($result)) ? $result : false;
    }

    public static function checkIfProductExists($post_id) {
        global $wpdb;
        $query = "SELECT * FROM `wp_posts` WHERE `ID` = $post_id";
        $result = $wpdb->get_results($query);
        return (isset($result) && !empty($result)) ? true : false;
    }

    public static function getStarRating($post_id) {
        $reviews = self::getReviews($post_id);
        $numberOfReviews = count($reviews);
        $stars = 0;
        foreach ($reviews as $review):
            $review = json_decode($review->comment_content);
            $stars = $stars + (int) $review->stars;
        endforeach;
        return $stars / $numberOfReviews;
    }

    public static function hasProductReviews($post_id) {
        global $wpdb;
        $query = "SELECT comment_ID FROM `wp_comments` WHERE `comment_post_ID` = $post_id AND `comment_type` LIKE 'review' AND `comment_approved` LIKE '1'";
        $result = $wpdb->get_results($query);
        return (isset($result) && !empty($result)) ? true : false;
    }

    public static function getReviewsTextOnly($post_id) {
        global $wpdb;
        $query = "SELECT comment_content FROM `wp_comments` WHERE `comment_post_ID` = $post_id AND `comment_type` LIKE 'review' AND `comment_approved` LIKE '1'";
        $result = $wpdb->get_results($query);
        return (isset($result) && !empty($result)) ? $result : false;
    }

}
