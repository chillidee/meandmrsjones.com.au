<?php
/*
 * ------------------------------
 * Dectect if user is on a mobile
 * ------------------------------
 */

function st_is_mobile() {
    $detect = new Mobile_Detect;
    return $detect->isMobile();
}

/*
 * return string of logo path
 */

function get_logo_url() {
    $path = get_site_url() . '/logo.png';
    return $path;
}

/*
 * -------------------------------------
 * Remove admin bar for none admin users
 * -------------------------------------
 */

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

/*
 * ---------------------------------
 * Returns string truncated to limit
 * ---------------------------------
 */

function limit_text($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}

/*
 * ------------------------------
 * Var dump surounded in pre tags
 * ------------------------------
 */

function st_var_dump($element) {
    ?>
    <pre>
        <?php var_dump($element); ?>
    </pre>
    <?php
}
