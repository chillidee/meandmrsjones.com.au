<?php

//if (!defined('ABSPATH')) {
//    exit;
//}

class account_class {

    private function account_get_posts_for_days($previous_date, $today) {
        $args = array(
            'post_type' => 'shop_order',
            'posts_per_page' => -1,
            'post_status' => array('wc-completed', 'wc-processing'),
            'date_query' => array(
                array(
                    'after' => $previous_date,
                    'before' => $today
                ),
            ),
        );
        $query = new WP_Query($args);
        return $query->posts;
    }

    public function account_get_defualt_products() {
        $today = date('F j, Y');
        $previous_date = date('F j, Y', strtotime('-7 days'));
        return $this->accounts_get_table_data($previous_date, $today);
    }

    public function accounts_refine_products_ajax() {
        echo json_encode($this->accounts_get_table_data(htmlentities($_POST['end_date']), htmlentities($_POST['start_date']), htmlentities($_POST['vendor'])));
        wp_die();
    }

    public function accounts_get_column_header_array() {
        return array("Order ID", "Wholeseller", "Title", "Wholesale Price", "Sale Price", "Profit", "Qty", "Total Wholesale Cost", "Total Profit");
    }

    private function accounts_get_table_data($previous_date, $today, $vendor = 'all') {
        $returnArray = array();
        $orders = $this->account_get_posts_for_days($previous_date, $today);
        foreach ($orders as $order):
            $orderId = $order->ID;
            $order = new WC_Order($order->ID);
            $_items = $order->get_items();
            foreach ($_items as $value):
                $product_id = $value["item_meta"]["_product_id"][0];
                if ($vendor == 'all' || $vendor == get_post_meta($product_id, 'vendor', true)):
                    $_wholesale = (float) $value["item_meta"]["_wholesale"][0];
                    $_sale_price = (float) $value["item_meta"]["_line_total"][0];
                    $_profit = $_sale_price - $_wholesale;
                    $_qty = (int) $value["item_meta"]["_qty"][0];
                    $_t_wholesale = $_wholesale * $_qty;
                    $_t_profit = $_profit * $_qty;
                    array_push($returnArray, array($orderId, get_post_meta($product_id, 'vendor', true), html_entity_decode($value["name"]), $_wholesale, $_sale_price, $_profit, $_qty, $_t_wholesale, $_t_profit));
                endif;
            endforeach;
        endforeach;
        return $returnArray;
    }

}
