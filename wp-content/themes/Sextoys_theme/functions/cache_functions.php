<?php

class cache_functions {

    public function __construct() {
        set_time_limit(0);
        foreach ($this->cache_get_pages_to_cache() as $slug):
            foreach ($this->cache_get_genders() as $gender):
                $this->cache_save_contents($this->cache_download_pages($slug, $gender), $slug . '-' . $gender);
            endforeach;
        endforeach;
    }

    private function cache_get_pages_to_cache() {
        return array(
            'apparel',
            'toys',
            'apparel',
            'essentials',
            'gifts-games',
            'fetish'
        );
    }

    private function cache_download_pages($slug, $gender) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, get_site_url() . "/" . $slug . '/');
        $postFields = array('caching' => 'true', 'gender' => 'for-' . $gender);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postFields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $server_output = curl_exec($ch);
        curl_close($ch);
        $htmlArray = explode('<main class="' . $slug . '">', $server_output);
        unset($server_output);
        $htmlArray = explode('<div class="about-us-overlay footer-overlay">', $htmlArray[1]);
        return $htmlArray[0];
    }

    private function cache_save_contents($fileString, $slug) {
        $destFolder = get_template_directory() . '/cachedPages/';
        if (!file_exists($destFolder)) {
            mkdir($destFolder, 0775, true);
        }
        if (file_exists($destFolder . $slug . '.html')):
            unlink($destFolder . $slug . '.html');
        endif;
        file_put_contents($destFolder . $slug . '.html', $fileString);
    }

    private function cache_get_genders() {
        return array('her', 'him', 'us');
    }

}
