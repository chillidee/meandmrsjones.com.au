<?php

/*
 * ----------------------------------
 * function to get most sold products
 * ----------------------------------
 */

function get_most_popular_products($numberOfProducts) {
    $args = array(
        'post_type' => 'product',
        "post_status" => "publish",
        'posts_per_page' => $numberOfProducts,
        'meta_key' => 'total_sales',
        'orderby' => 'meta_value_num'
    );
    return new WP_Query($args);
}

/*
 * -------------------------------
 * function to get newest products
 * -------------------------------
 */

function get_newest_products($numberOfProducts) {
    $args = array(
        'post_type' => 'product',
        "post_status" => "publish",
        'posts_per_page' => $numberOfProducts,
        'orderby' => 'date',
        'order' => 'DESC'
    );
    return new WP_Query($args);
}

/*
 * ------------------------------
 * function to get taged products
 * ------------------------------
 */

function st_get_taged_products($numberOfProducts, $tag) {
    $args = array(
        'post_type' => 'product',
        "post_status" => "publish",
        'posts_per_page' => $numberOfProducts,
        'orderby' => 'rand',
        'product_tag' => $tag
    );
    return new WP_Query($args);
}

/*
 * --------------------------------------------
 * Get array of all apparel cats for size guide
 * --------------------------------------------
 */

function st_get_apparel_cats() {
    $array = array(70, 172, 67, 63, 78, 80, 75, 82, 73, 83, 74, 81, 66, 72, 71, 22);
    return $array;
}

/*
 * --------------------------------------------
 * Query args for getting related products
 * --------------------------------------------
 */

function add_related_products_after_product($number_of_products, $cat_slug, $page) {
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => $number_of_products,
        'post_status' => 'publish',
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'slug',
                'terms' => $cat_slug
            )
        )
    );
    st_run_query($args, $number_of_products, $page);
}

/*
 * -----------------------
 * Output related products
 * -----------------------
 */

function st_run_query($args, $number_of_products, $page) {
    $number_of_columns = '';
    switch ($number_of_products):
        case 2:
            $number_of_columns = '6';
            break;
        case 3:
            $number_of_columns = '4';
            break;
        case 4:
            $number_of_columns = '3';
            break;
        default :
            $number_of_columns = '6';
            break;
    endswitch;
    if (st_is_mobile()):
        $number_of_columns = '12';
    endif;
    $the_query = new WP_Query($args);
    if ($the_query->have_posts()) :
        while ($the_query->have_posts()) : $the_query->the_post();
            $id = get_the_ID();
            $title = get_the_title();
            $img_url = wp_get_attachment_url(get_post_thumbnail_id());
            if ($page == 'product'):
                $guid = get_the_guid($id);
                echo "<a href='{$guid}'>";
            endif;
            echo "<div class='col-lg-{$number_of_columns} col-xs-{$number_of_columns} product-full-related'>";
            echo '<div class="center-block image-box">';
            echo "<img class='product-full-related-img' data-id='{$id}' src='{$img_url}' alt='{$title}'/>";
            echo '</div>';
            echo '<h4>' . $title . '</h4>';
            echo '</div>';
            if ($page == 'product'):
                echo "</a>";
            endif;
        endwhile;
    else:
        return false;
    endif;
    wp_reset_query();
}

/*
 * -----------------------------
 * Get products from search query
 * -----------------------------
 */

function st_product_search($query) {
    $query = strtolower($query);
    $cleanArray = array();
    $priceArray = array();
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'fields' => 'ids',
    );
    $the_query = new WP_Query($args);
    unset($args);
    foreach ($the_query->posts as $id):
        $title = get_the_title($id);
        $pos = strpos(strtolower(trim($title)), $query);
        if ($pos !== false):
            array_push($cleanArray, $id);
        endif;
    endforeach;
    unset($the_query);
    $cats = get_all_categories();
    foreach ($cats as $cat):
        $pos = strpos(strtolower(trim($cat->name)), $query);
        if ($pos !== false):
            $args = array(
                'post_type' => 'product',
                'posts_per_page' => -1,
                'post_status' => 'publish',
                'fields' => 'ids',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'id',
                        'terms' => $cat->term_id
                    )
                )
            );
            $the_query = new WP_Query($args);
            unset($args);
            foreach ($the_query->posts as $id):
                if (st_not_in_array($id, $cleanArray)):
                    array_push($cleanArray, $id);
                endif;
            endforeach;
            unset($the_query);
        endif;
    endforeach;
    unset($cats);
    foreach ($cleanArray as $id):
        $priceArray[$id] = (float) get_post_meta($id, '_price', true);
    endforeach;
    unset($cleanArray);
    $cleanArray = array();
    arsort($priceArray);
    foreach ($priceArray as $index => $value):
        array_push($cleanArray, $index);
    endforeach;
    unset($priceArray);
    return $cleanArray;
}

/*
 * -----------------------------------
 * Remove item from cart ajax function
 * -----------------------------------
 */

function st_remove_item_callback() {
    $id = esc_attr(ucfirst($_POST['id']));
    $cart = WC()->instance()->cart;
    $cart_id = $cart->generate_cart_id($id);
    $cart_item_id = $cart->find_product_in_cart($cart_id);
    if ($cart_item_id) {
        $cart->set_quantity($cart_item_id, 0);
    }
    st_get_cart_contents();
    wp_die();
}
