<?php

class api_functions {

    public function st_check_secert_code($received_code) {
        return ($received_code === API_KEY) ? TRUE : FALSE;
    }

    private function api_get_product_ids() {
        $ids = new WP_Query(array(
            'post_type' => 'product',
            'posts_per_page' => -1,
            "post_status" => "publish",
            'fields' => 'ids',
        ));
        return $ids->posts;
    }

    private function api_get_meta_db_query($value) {
        global $wpdb;
        return $wpdb->get_results('SELECT `post_id`,`meta_value` FROM `wp_postmeta` WHERE `meta_key` = "' . $value . '"');
    }

    private function api_get_first_three_chars($string) {
        return substr($string, 0, 3);
    }

    public function api_get_cats_images($request) {
        $cleanArray = array();
        foreach ($this->api_get_all_cats() as $cat):
            if ($cat->term_id !== 177):
                $cleanArray[$cat->term_id] = st_get_category_image($cat->term_id, 'full');
            endif;
        endforeach;
        return $cleanArray;
    }

    public function api_image_urls($request) {
        $cleanArray = array();
        $ids = new WP_Query(array(
            'post_type' => 'product',
            'posts_per_page' => -1,
            "post_status" => "publish",
            'fields' => 'ids',
        ));
        $ids = $ids->posts;
        foreach ($ids as $id):
            //if (get_post_meta($id, 'vendor', true) == 'Wrapped Secrets'):
            array_push($cleanArray, get_post_meta($id, 'main_image_link', true));
            //endif;
        endforeach;
        return $this->api_prepare_response_object($cleanArray);
    }

    private function api_get_the_post_thumbnail_src($img) {
        return (preg_match('~\bsrc="([^"]++)"~', $img, $matches)) ? $matches[1] : '';
    }

    private function api_get_similar_products_array($code, $brand) {
        $code_sub = $this->api_get_first_three_chars($code);
        $simualr_array = array();
        $brands_array = array();
        $ids = array();
        $codes = $this->api_get_meta_db_query("_code");
        foreach ($codes as $product):
            if ($this->api_get_first_three_chars($product->meta_value) === $code_sub && !(boolean) get_post_meta($product->post_id, '_is_child', true)):
                array_push($simualr_array, $product->post_id);
            endif;
            unset($product);
        endforeach;
        unset($codes);
        $brands = $this->api_get_meta_db_query('_brand');
        foreach ($brands as $s_brand):
            if ($s_brand->meta_value === $brand):
                if (!(boolean) get_post_meta($s_brand->post_id, '_is_child', true)):
                    array_push($brands_array, $s_brand->post_id);
                    unset($s_brand);
                endif;
            endif;
        endforeach;
        foreach ($simualr_array as $id) {
//            $ids[$id] = get_the_title((int) $id);
//            unset($id);
            if (!in_array($id, $brands_array) && !(boolean) get_post_meta($id, '_is_child', true)):
                array_push($brands_array, $id);
            endif;
        }
        unset($simualr_array);
        foreach ($brands_array as $id):
            $id = (int) $id;
            if (!in_array($id, $ids) && get_post_meta($id, '_is_child', true) != 'true'):
                $main_image = get_post_meta($id, 'main_image_link', true);
                if (!$main_image):
                    $main_image = $this->api_get_the_post_thumbnail_src(get_the_post_thumbnail($id));
                endif;
                $ids[$id] = array();
                $ids[$id]['title'] = get_the_title($id);
                $ids[$id]['code'] = get_post_meta($id, '_code', true);
                $ids[$id]['image'] = $main_image;
                $ids[$id]['l_desc'] = get_post_field('post_content', $id);
            endif;
            unset($id);
        endforeach;
        unset($brands_array);
        return $ids;
    }

    private function api_get_users_id() {
        global $wpdb;
        return $wpdb->get_results("SELECT ID FROM wp_users");
    }

    private function api_prepare_response_object($data) {
        $response = new WP_REST_Response($data);
        $response->set_status(201);
        $response->header('Location', get_site_url());
        return $response;
    }

    private function api_get_all_cats() {
        return get_categories(array('show_option_none' => '',
            'hide_empty' => 0,
            'taxonomy' => 'product_cat'));
    }

    private function api_get_parent_name($child) {
        $cats = $this->api_get_all_cats();
        foreach ($cats as $cat):
            if ($cat->term_id === $child):
                return ' - ' . $cat->name;
            endif;
        endforeach;
        return '';
    }

    private function api_get_top_level_cat_names() {
        $cats = $this->api_get_all_cats();
        foreach ($cats as $cat):
            if (!$cat->parent):
                $parent_array[$cat->term_id] = $cat->name;
            endif;
        endforeach;
        return $parent_array;
    }

    private function api_update_associated_products($data) {
        update_post_meta($data['parent_product_id'], '_is_parent', true);
        update_post_meta($data['post_id'], '_is_child', true);
        update_post_meta($data['post_id'], '_parent_id', $data['parent_product_id']);
    }

    private function api_new_associated_products($id, $parent_id) {
        update_post_meta($parent_id, '_is_parent', true);
        update_post_meta($id, '_is_child', true);
        update_post_meta($id, '_parent_id', $parent_id);
    }

    private function insertProductTags($post_id, $data) {
        if (isset($post_id) && isset($data->tags)):
            $term_type = 'product_tag';
            $tags = explode(',', $data->tags);
            $tag_ids = array();
            $result = '';
            foreach ($tags as $tag):
                $term = term_exists($tag, $term_type);
                if (!$term):
                    $term_id = wp_insert_term($tag, $term_type);
                endif;
            endforeach;
            $result = wp_set_post_terms($post_id, $data->tags, $term_type);
            return implode(',', $result);
        endif;
    }

    private function api_update_post_meta($post_id, $data) {
        if (isset($data->barcode)):
            update_post_meta($post_id, '_sku', $data->barcode);
        endif;
        if (isset($data->brand)):
            update_post_meta($post_id, '_brand', $data->brand);
        endif;
        if (isset($data->wholesale_price)):
            update_post_meta($post_id, '_wholesale_price', $data->wholesale_price);
        endif;
        if (isset($data->code)):
            update_post_meta($post_id, '_code', $data->code);
        endif;
        if (isset($data->selling_price)):
            update_post_meta($post_id, '_regular_price', $data->selling_price);
            update_post_meta($post_id, '_price', $data->selling_price);
        endif;
        if (isset($data->barcode)):
            update_post_meta($post_id, '_barcode', $data->barcode);
        endif;
        if (isset($data->related_category)):
            update_post_meta($post_id, '_select_show_related_products_from_cat', $data->related_category);
        endif;
        if (isset($data->image_link)):
            update_post_meta($post_id, 'main_image_link', $data->image_link);
        endif;
        if (isset($data->image_link_thumb)):
            update_post_meta($post_id, 'image_link_thumb', $data->image_link_thumb);
        endif;
        if (isset($data->gallery_image)):
            update_post_meta($post_id, 'gallery_image_link', $data->gallery_image);
        endif;
        if (isset($data->youtube_link)):
            update_post_meta($post_id, 'youtube_link', $data->youtube_link);
        endif;
        if (isset($data->vendor)):
            update_post_meta($post_id, 'vendor', $data->vendor);
        endif;
        if (!empty($data->st_colour)):
            update_post_meta($post_id, 'st_colour', $data->st_colour);
        endif;
        if (!empty($data->st_size)):
            update_post_meta($post_id, 'st_size', $data->st_size);
        endif;
        if ($data->markup_option == '1'):
            update_post_meta($post_id, '_price_markup', $data->markup_amount);
        endif;
        if (isset($data->markup_option)):
            update_post_meta($post_id, '_select_price_save', $data->markup_option);
        endif;
        if (isset($data->seo_desc)):
            update_post_meta($post_id, '_yoast_wpseo_metadesc', $data->seo_desc);
        endif;
        if (isset($data->seo_title)):
            update_post_meta($post_id, '_yoast_wpseo_title', $data->seo_title);
        endif;
    }

    private function api_new_product_set_cats($post_id, $data) {
        if (isset($data->category)):
            $catsArray = array();
            foreach (explode(' ', $data->category) as $cat):
                array_push($catsArray, (int) $cat);
            endforeach;
            wp_set_object_terms($post_id, $catsArray, 'product_cat');
        endif;
    }

    private function api_add_product($data) {
        if ($data) {
            $post = array('post_status' => "publish", 'post_title' => $data->name, 'post_type' => "product", 'post_content' => $data->l_desc, 'comment_status' => 'closed', 'post_excerpt' => $data->s_desc);
            $post_id = wp_insert_post($post);
            if ($post_id):
                if (!empty($data->parent_product_id)):
                    $this->api_update_associated_products(array('post_id' => $post_id, 'parent_product_id' => (int) $data->parent_product_id));
                endif;
                $this->api_update_post_meta($post_id, $data);
                $this->api_new_product_set_cats($post_id, $data);
                if ($data->tags && strlen($data->tags) > 0):
                    $tags = $this->insertProductTags($post_id, $data);
                endif;
            endif;
        }
        if ($data && $post_id):
            return array('status' => 200, 'guid' => (string) $tags);
        else:
            return array('error', 500);
        endif;
    }

    public function getChildCats($request) {
        $cleanArray = array();
        foreach ($this->api_get_all_cats() as $cat):
            if ($cat->term_id !== 177):
                $cleanArray[$cat->term_id] = array('name' => $cat->name, 'parent' => $cat->parent);
            endif;
        endforeach;
        return $cleanArray;
    }

    public function api_get_wholesale_and_sale_prices($request) {
        $returnArray = array();
        $ids = new WP_Query(array(
            'post_type' => 'product',
            'posts_per_page' => -1,
            'post_status' => array('pending', 'draft', 'future', 'publish', 'trash')
        ));
        $ids = $ids->posts;
        foreach ($ids as $id):
            $wp = (float) get_post_meta($id->ID, '_wholesale_price', true);
            $rp = (float) get_post_meta($id->ID, '_regular_price', true);
            if ($wp >= $rp || !$wp || !$rp):
                $returnArray[$id->ID]['main_image_link'] = get_post_meta($id->ID, 'main_image_link', true);
                $returnArray[$id->ID]['title'] = $id->post_title;
                $returnArray[$id->ID]['status'] = $id->post_status;
                $returnArray[$id->ID]['guid'] = $id->guid;
                $returnArray[$id->ID]['wholesale_price'] = get_post_meta($id->ID, '_wholesale_price', true);
                $returnArray[$id->ID]['price'] = get_post_meta($id->ID, '_regular_price', true);
            endif;
        endforeach;
        return $this->api_prepare_response_object($returnArray);
    }

    private function api_unpubish_product_function($request) {
        if (get_post_meta($request['post_id'], '_is_parent', true)):
            update_post_meta($request['post_id'], '_is_parent', false);
            update_post_meta($request['post_id'], '_is_child', true);
            $changed_parent = false;
            $new_parent_id = 0;
            foreach ($this->api_get_product_ids() as $id):
                if (get_post_meta($id, '_parent_id', true) === $request['post_id']):
                    if (!$changed_parent && !$changed_parent):
                        update_post_meta($id, '_is_child', false);
                        update_post_meta($id, '_is_parent', true);
                        update_post_meta($id, '_parent_id', 0);
                        $changed_parent = true;
                        $new_parent_id = $id;
                    else:
                        update_post_meta($id, '_parent_id', $new_parent_id);
                    endif;
                endif;
                unset($id);
            endforeach;
            update_post_meta($request['post_id'], '_parent_id', $new_parent_id);
        endif;
        $result = wp_update_post(array('ID' => (int) $request['post_id'], 'post_status' => 'Draft'));
        return ($result) ? true : false;
    }

    public function api_out_out_stock_product($request) {
        $result = $this->api_unpubish_product_function($request);
        update_post_meta($request['post_id'], '_stock_status', 'outofstock');
        return $this->api_prepare_response_object($result);
    }

    public function api_unpubish_product($request) {
        $result = $this->api_unpubish_product_function($request);
        return $this->api_prepare_response_object($result);
    }

    public function api_unpubish_coupon($request) {
        $result = wp_update_post(array('ID' => $request['post_id'], 'post_status' => 'Draft'));
        return $this->api_prepare_response_object($result);
    }

    public function api_delete_product($request) {
        $result = wp_delete_post($request['post_id'], true);
        $result = (!$result) ? false : true;
        return $this->api_prepare_response_object($result);
    }

    public function api_get_all_coupons($request) {
        $coupons = new WP_Query(array(
            'post_type' => 'shop_coupon',
            'orderby' => 'date',
            'order' => 'DESC',
            'post_status' => array('pending', 'draft', 'future', 'publish', 'trash')
        ));
        $coupons = $coupons->posts;
        $returnArray = array();
        foreach ($coupons as $coupon):
            $returnArray[$coupon->ID] = array(
                'title' => $coupon->post_title,
                'status' => $coupon->post_status,
                'usage' => get_post_meta($coupon->ID, 'usage_count', true),
                'description' => $coupon->post_excerpt,
                'user' => get_post_meta($coupon->ID, 'created_by', true),
                'lastUser' => get_post_meta($coupon->ID, 'lastUser', true)
            );
        endforeach;
        if (count($returnArray) == 0):
            $returnArray = false;
        endif;
        return $this->api_prepare_response_object($returnArray);
    }

    public function api_save_edited_coupon($request) {
        $returnArray = array();
        $coupon_id = $request['edit_coupon_save'];
        $edit_post = array(
            'ID' => $coupon_id,
            'post_title' => $request['coupon_title'],
            'post_excerpt' => $request['description'],
            'post_name' => $request['post_name'],
        );
        update_post_meta($coupon_id, 'lastUser', $request['user']);
        $returnArray['wp_update_post'] = wp_update_post($edit_post);
        $returnArray = $this->update_coupon_meta($coupon_id, $request, $returnArray);
        return $this->api_prepare_response_object($returnArray);
    }

    public function api_save_new_coupon($request) {
        $returnArray = array();
        $newpost = array(
            'post_title' => $request['coupon_title'],
            'post_excerpt' => $request['description'],
            'post_name' => $request['post_name'],
            'post_status' => 'publish',
            'post_type' => 'shop_coupon'
        );
        $coupon_id = wp_insert_post($newpost);
        if ($coupon_id):
            update_post_meta($coupon_id, 'created_by', $request['user']);
            $returnArray = $this->update_coupon_meta($coupon_id, $request, $returnArray);
        else:
            $returnArray['err'] = 'Failure to insert post';
        endif;
        return $this->api_prepare_response_object($returnArray);
    }

    private function update_coupon_meta($coupon_id, $request, $returnArray) {
        $returnArray['discount_type'] = update_post_meta($coupon_id, 'discount_type', $request['discountType']);
        $returnArray['coupon_amount'] = update_post_meta($coupon_id, 'coupon_amount', $request['coupon_amount']);
        $individualuse = ($request['individual_use'] == 'on') ? 'yes' : 'no';
        $returnArray['individual_use'] = update_post_meta($coupon_id, 'individual_use', $individualuse);
        $returnArray['usage_limit'] = update_post_meta($coupon_id, 'usage_limit', $request['usage_limit']);
        $returnArray['usage_limit_per_user'] = update_post_meta($coupon_id, 'usage_limit_per_user', $request['usage_limit_per_user']);
        $returnArray['limit_usage_to_x_items'] = update_post_meta($coupon_id, 'limit_usage_to_x_items', $request['limit_usage_to_x_items']);
        $returnArray['expiry_date'] = update_post_meta($coupon_id, 'expiry_date', $request['expiry_date']);
        $freeshipping = ($request['free_shipping'] == 'on') ? 'yes' : 'no';
        $returnArray['free_shipping'] = update_post_meta($coupon_id, 'free_shipping', $freeshipping);
        $returnArray['minimum_amount'] = update_post_meta($coupon_id, 'minimum_amount', $request['minimum_spend']);
        $returnArray['maximum_amount'] = update_post_meta($coupon_id, 'maximum_amount', $request['maximum_spend']);
    }

    public function api_get_coupons_edit($request) {
        $returnArray = array();
        $id = (int) $request['post_id'];
        $post = get_post($id);
        if ($post && $id):
            $returnArray['title'] = $post->post_title;
            $returnArray['description'] = $post->post_excerpt;
            $returnArray['discount_type'] = get_post_meta($id, 'discount_type', true);
            $returnArray['coupon_amount'] = get_post_meta($id, 'coupon_amount', true);
            $returnArray['individual_use'] = get_post_meta($id, 'individual_use', true);
            $returnArray['usage_limit'] = get_post_meta($id, 'usage_limit', true);
            $returnArray['usage_limit_per_user'] = get_post_meta($id, 'usage_limit_per_user', true);
            $returnArray['limit_usage_to_x_items'] = get_post_meta($id, 'limit_usage_to_x_items', true);
            $returnArray['expiry_date'] = get_post_meta($id, 'expiry_date', true);
            $returnArray['free_shipping'] = get_post_meta($id, 'free_shipping', true);
            $returnArray['minimum_amount'] = get_post_meta($id, 'minimum_amount', true);
            $returnArray['maximum_amount'] = get_post_meta($id, 'maximum_amount', true);
        else:
            $returnArray['err'] = 'Post not found';
        endif;
        return $this->api_prepare_response_object($returnArray);
    }

    public function api_pubish_product($request) {
        $id = $request['post_id'];
        update_post_meta($id, '_stock_status', 'instock');
        $date = date("Y-m-d H:i:s");
        $result = wp_update_post(array('ID' => $id, 'post_status' => 'publish', 'post_date' => $date, 'post_date_gmt' => $date));
        $result = ($result) ? true : false;
        return $this->api_prepare_response_object($result);
    }

    public function api_get_subscriped_customers() {
        global $wpdb;
        foreach ($this->api_get_users_id() as $value):
            if (get_user_meta($value->ID, 'subscribed', true) == 'true'):
                $user = get_userdata($value->ID);
                $data[$value->ID] = array(
                    'name' => $user->display_name,
                    'email' => $user->user_email,
                    'dob' => get_user_meta($value->ID, 'dob', true),
                    'title' => get_user_meta($value->ID, 'title', true),
                    'phone' => get_user_meta($value->ID, 'billing_phone', true)
                );
            endif;
        endforeach;
        $emails = $wpdb->get_results("SELECT * FROM  `email_subscribers`");
        $index = 999999;
        foreach ($emails as $email):
            $data[$index] = array(
                'email' => $email->email
            );
            $index--;
        endforeach;
        return $this->api_prepare_response_object($data);
    }

    public function api_get_cats() {
        $cleanarray = array();
        $parent_array = $this->api_get_top_level_cat_names();
        foreach ($this->api_get_all_cats() as $cat):
            $cleanarray[$cat->term_id] = $parent_array[st_cat_top_level_parent($cat->term_id)] . $this->api_get_parent_name($cat->parent) . ' - ' . $cat->name;
        endforeach;
        return $this->api_prepare_response_object($cleanarray);
    }

    public function api_get_all_products($request) {
        if (isset($request['meta'])):
            $meta = array(
                array(
                    'key' => '_stock_status',
                    'value' => 'outofstock',
                    'compare' => '=',
                )
            );
        else:
            $meta = array();
        endif;
        if (isset($request['status'])):
            $status = $request['status'];
        else:
            $status = array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash');
        endif;
        if (isset($request['number_of_products'])):
            $number_of_products = $request['number_of_products'];
        else:
            $number_of_products = -1;
        endif;
        $ids = new WP_Query(array(
            'post_type' => 'product',
            'post_status' => $status,
            'orderby' => 'date',
            'order' => 'DESC',
            'posts_per_page' => $number_of_products,
            'fields' => 'ids',
            'meta_query' => $meta
        ));
        $ids = $ids->posts;
        $returnArray = array();
        if (isset($request['vendor'])):
            $vendorArray = array();
            foreach ($ids as $id):
                if (get_post_meta($id, 'vendor', true) == $request['vendor']):
                    array_push($vendorArray, $id);
                endif;
            endforeach;
            $returnArray = $this->get_product_information_for_list($vendorArray);
        else:
            $returnArray = $this->get_product_information_for_list($ids);
        endif;
        return $this->api_prepare_response_object($returnArray);
    }

    private function get_product_information_for_list($ids) {
        $productArray = array();
        foreach ($ids as $id):
            $image = get_post_meta($id, 'main_image_link', true);
            if (!$image):
                $image = wp_get_attachment_url(get_post_thumbnail_id($id));
            endif;
            $productArray[$id] = array(
                'image' => $image,
                'title' => get_the_title($id),
                'code' => get_post_meta($id, '_code', true),
                'vender' => get_post_meta($id, 'vendor', true),
                'brand' => get_post_meta($id, '_brand', true),
                'stockstatus' => get_post_meta($id, '_stock_status', true),
                'status' => get_post_status($id),
                's_description' => get_post_field('post_excerpt', $id),
                'l_description' => get_post_field('post_content', $id),
                'size' => get_post_meta($id, 'st_size', true),
                'colour' => get_post_meta($id, 'st_colour', true),
                'gallery' => get_post_meta($id, '_product_image_gallery', true),
                'sku' => get_post_meta($id, '_sku', true),
                'wholesale_price' => get_post_meta($id, '_wholesale_price', true),
                'price' => get_post_meta($id, '_regular_price', true),
                'youtube_link' => get_post_meta($id, 'youtube_link', true)
            );
        endforeach;
        return $productArray;
    }

    public function api_get_product_codes() {
        $cleanArray = array();
        foreach ($this->api_get_product_ids() as $value):
            $cleanArray[$value] = get_post_meta($value, '_code', true);
        endforeach;
        return $this->api_prepare_response_object($cleanArray);
    }

    public function api_get_similar_products($request) {
        return $this->api_prepare_response_object($this->api_get_similar_products_array($request['code'], $request['brand']));
    }

    public function api_import_product($request) {
        $data = json_decode($request->get_body());
        return $this->api_prepare_response_object($this->api_add_product($data));
    }

    public function update_meta($request) {
        return $this->api_prepare_response_object(update_post_meta((int) $request['post_id'], $request['key'], $request['value']));
    }

    public function api_get_product($request) {
        $post_id = $request['post_id'];
        $product_cat_id = array();
        foreach (get_the_terms($post_id, 'product_cat') as $term) {
            array_push($product_cat_id, $term->term_id);
        }
        $product_cat_id = implode(' ', $product_cat_id);
        $cleanArray = array();
        $cleanArray['post_id'] = $post_id;
        $tagsArray = array();
        foreach (get_the_terms($post_id, 'product_tag') as $term):
            array_push($tagsArray, $term->name);
        endforeach;
        $cleanArray['tags'] = $tagsArray;
        unset($tagsArray);
        $cleanArray['title'] = get_the_title($post_id);
        $cleanArray['sdescription'] = get_post_field('post_excerpt', $post_id);
        $cleanArray['ldescription'] = get_post_field('post_content', $post_id);
        $cleanArray['sku'] = get_post_meta($post_id, '_sku', true);
        $cleanArray['brand'] = get_post_meta($post_id, '_brand', true);
        $cleanArray['wholesale_price'] = get_post_meta($post_id, '_wholesale_price', true);
        $cleanArray['code'] = get_post_meta($post_id, '_code', true);
        $cleanArray['price'] = get_post_meta($post_id, '_regular_price', true);
        $cleanArray['barcode'] = get_post_meta($post_id, '_barcode', true);
        $cleanArray['select_show_related_products_from_cat'] = get_post_meta($post_id, '_select_show_related_products_from_cat', true);
        $cleanArray['main_image_link'] = get_post_meta($post_id, 'main_image_link', true);
        $cleanArray['gallery_image_link'] = get_post_meta($post_id, 'gallery_image_link', true);
        $cleanArray['youtube_link'] = get_post_meta($post_id, 'youtube_link', true);
        $cleanArray['vendor'] = get_post_meta($post_id, 'vendor', true);
        $cleanArray['st_colour'] = get_post_meta($post_id, 'st_colour', true);
        $cleanArray['st_size'] = get_post_meta($post_id, 'st_size', true);
        $cleanArray['price_markup'] = get_post_meta($post_id, '_price_markup', true);
        $cleanArray['select_price_save'] = get_post_meta($post_id, '_select_price_save', true);
        $cleanArray['category'] = $product_cat_id;
        $cleanArray['seo_title'] = get_post_meta($post_id, '_yoast_wpseo_title', true);
        $cleanArray['seo_desc'] = get_post_meta($post_id, '_yoast_wpseo_metadesc', true);
        $cleanArray['parent_id'] = get_post_meta($post_id, '_parent_id', true);
        $cleanArray['related_category'] = get_post_meta($post_id, '_select_show_related_products_from_cat', true);
        $cleanArray['sim_products'] = $this->api_get_similar_products_array($cleanArray['code'], $cleanArray['brand']);
        return $this->api_prepare_response_object($cleanArray);
    }

    public function api_test($request) {
        return $this->api_prepare_response_object($this->api_get_product_ids());
    }

    public function api_detele_cat($request) {
        $cat_id = (int) $request['category-id'];
        $cat_thumb_id = get_woocommerce_term_meta($cat_id, 'thumbnail_id', true);
        wp_delete_post($cat_thumb_id, true);
        $result = wp_delete_term($cat_id, 'product_cat', $args);
        return $this->api_prepare_response_object($cat_thumb_id);
    }

    public function api_new_cat($request) {
        $catName = $request['new-category-name'];
        $catSlug = str_replace(" ", "-", $catName);
        $result = wp_insert_term(
                $catName, 'product_cat', array(
            'slug' => $catSlug,
            'parent' => $request['new-category-parent']
                )
        );
        if (!is_wp_error($result)):
            $attchment_id = wp_insert_post(array('guid' => $request['image-link'], 'post_title' => $catName, 'post_excerpt' => '', 'post_content' => '', 'post_type' => 'attachment', 'post_status' => 'inherit', 'post_mime_type' => 'image/jpeg'), true);
            $meta = array(
                'width' => 450,
                'height' => 450,
                'file' => basename($request['image-link']),
                'sizes' => array(),
                'image_meta' => array(),
            );
            update_post_meta($attchment_id, '_wp_attached_file', $request['image-link']);
            update_post_meta($attchment_id, '_wp_attachment_metadata', $request['image-link']);
            if (!is_wp_error($attchment_id)):
                add_woocommerce_term_meta($result['term_id'], 'thumbnail_id', $attchment_id);
            endif;
        endif;
        return $this->api_prepare_response_object($attchment_id);
    }

    public function api_get_number_products($request) {
        $numbers = array();
        $vendors = array();
        $ids = new WP_Query(array(
            'post_type' => 'product',
            'posts_per_page' => -1,
            'post_status' => array('pending', 'draft', 'future', 'publish', 'trash'),
            'fields' => 'ids'
        ));
        foreach ($ids->posts as $id):
            $vendor = get_post_meta($id, 'vendor', true);
            $vendors[$vendor] = 0;
        endforeach;
        foreach ($ids->posts as $id):
            $vendor = get_post_meta($id, 'vendor', true);
            $i = $vendors[$vendor];
            $i++;
            $vendors[$vendor] = $i;
        endforeach;
        $numbers['vendors'] = $vendors;
        $numbers['totalProducts'] = count($ids->posts);
        unset($ids);
        $ids = new WP_Query(array(
            'post_type' => 'product',
            'posts_per_page' => -1,
            'post_status' => 'draft',
            'fields' => 'ids'
        ));
        foreach ($ids->posts as $id):
            if (get_post_meta($id, '_stock_status', true) == 'outofstock'):
                $numbers['outofstock'] ++;
            else:
                $numbers['pending'] ++;
            endif;
        endforeach;
        //$numbers['pending'] = count($ids->posts);
        $numbers['live'] = count($this->api_get_product_ids());
        return $this->api_prepare_response_object($numbers);
    }

    public function api_order_created($request) {
        $customer = array();
        $vendors = array();
        $orderContent = $request->get_body();
        $orderContent = json_decode($orderContent);
        $orderContent = $orderContent->order;
        $billing = $orderContent->billing_address;
        $customerDetails = $orderContent->customer;
        $billingAddress = array(
            'first_name' => $billing->first_name,
            'last_name' => $billing->last_name,
            'address_1' => $billing->address_1,
            'address_2' => $billing->address_2,
            'city' => $billing->city,
            'state' => $billing->state,
            'postcode' => $billing->postcode,
            'country' => $billing->country,
            'phone' => $billing->phone,
            'email' => $billing->email
        );
        $customer['billingAddress'] = $billingAddress;
        $billing = $orderContent->shipping_address;
        $billingAddress = array(
            'first_name' => $billing->first_name,
            'last_name' => $billing->last_name,
            'address_1' => $billing->address_1,
            'address_2' => $billing->address_2,
            'city' => $billing->city,
            'state' => $billing->state,
            'postcode' => $billing->postcode,
            'country' => $billing->country,
            'phone' => $billing->phone
        );
        $customer['shippingAddress'] = $billingAddress;
        $customer['details'] = array(
            'order_number' => $orderContent->order_number,
            'username' => $customerDetails->username
        );
        $items = $orderContent->line_items;
        foreach ($items as $index => $item):
            $wholeseller = get_post_meta($item->product_id, 'vendor', true);
            $customer['vendors'][$wholeseller][$item->product_id] = $item;
        endforeach;
        $this->sendVendorEmails($customer);
    }

    private function createOrderTextFile($vendor, $data) {
        $file = get_template_directory() . '/orders/2417.' . $data['details']['order_number'] . '.txt';
        $content = "001\t2417\t" . $data['shippingAddress']['first_name'] . " " . $data['shippingAddress']['last_name'] . "\tMe & Mrs Jones\tinfo@meandmrsjones.com.au\t1300 780 182\t" . $data['shippingAddress']['address_1'] . "," . $data['shippingAddress']['address_2'] . "\t" . $data['shippingAddress']['city'] . "\t" . $data['shippingAddress']['state'] . "\t" . $data['shippingAddress']['postcode'] . "\tDrop Ship\t" . $data['details']['order_number'] . "\t2417\r\n";
        foreach ($data['vendors'] as $_vendor => $item):
            if ($_vendor == $vendor):
                foreach ($item as $_item):
                    $content .= "002\t" . get_post_meta($_item->product_id, '_code', true) . "\t" . $_item->name . "\t\t\t" . $_item->quantity . "\t\r\n";
                endforeach;
            endif;
        endforeach;
        $content .= "003\tAcct\t\t\t\t\t\t\t\r\n";
        $content .= "999\r";
        file_put_contents($file, $content);
        return $file;
    }

    private function createSimpleOrderTextFile($vendor, $data) {
        $vendorTrim = strtolower(str_replace(' ', '', $vendor));
        $file = get_template_directory() . '/orders/' . $vendorTrim . "-" . $data['details']['order_number'] . '.txt';
        $content = "New Order From MMJ\r\n\r\n";
        $content .= "Order number " . $data['details']['order_number'] . "\r\n\r\n";
        $content .= "Customer Details\r\n\r\n";
        $content .= "Billing Address\r\n\r\n";
        $content .= "{$data['billingAddress']['first_name']} {$data['billingAddress']['last_name']}\r\n{$data['billingAddress']['email']}\r\n{$data['billingAddress']['address_1']}\r\n {$data['billingAddress']['address_2']}\r\n {$data['billingAddress']['city']}\r\n {$data['billingAddress']['state']}\r\n {$data['billingAddress']['postcode']}\r\n\r\n";
        $content .= "Shipping Address\r\n\r\n";
        $content .= "{$data['shippingAddress']['first_name']} {$data['shippingAddress']['last_name']}\r\n {$data['shippingAddress']['address_1']}\r\n {$data['shippingAddress']['address_2']}\r\n {$data['shippingAddress']['city']}\r\n {$data['shippingAddress']['state']}\r\n {$data['shippingAddress']['postcode']}\r\n\r\n";
        $content .= "Item Details\r\n\r\n";
        foreach ($data['vendors'] as $_vendor => $item):
            if ($_vendor == $vendor):
                foreach ($item as $_item):
                    $content .= "Code: " . get_post_meta($_item->product_id, '_code', true) . ", Name: " . $_item->name . ", Size: " . get_post_meta($_item->product_id, 'st_size', true) . ", Colour: " . get_post_meta($_item->product_id, 'st_colour', true) . ", Quantity: " . $_item->quantity . ", Barcode: " . get_post_meta($_item->product_id, '_barcode', true) . "\r\n";
                endforeach;
            endif;
        endforeach;
        file_put_contents($file, $content);
        return $file;
    }

    private function sendVendorEmails($data) {
        $email_address = 'matt@chillidee.com.au';
        wp_mail($email_address, 'Hook Fired', 'Hook fired on order created');
        $subject = 'MMJ Order Placed';
        $headers = 'From: MMJ <info@meandmrsjones.com.au>' . "\r\n";
        $content = '';
        require 'email_functions.php';
        $emails = new email_orders();
        $file = '';
        foreach ($data['vendors'] as $vendor => $items):
            switch ($vendor):
                case 'Windsor Wholeseller':
                    //$email_address = 'matt@chillidee.com.au';
                    //$email_address = array('weborders@windsorwholesale.com.au', 'info@meandmrsjones.com.au');
                    $file = $this->createOrderTextFile($vendor, $data);
                    //if (!$this->api_ftp_windsor_order($file)):
                    $emails->sendmail($email_address, $subject, 'MMJ New Order', $headers, $file);
                    //endif;
                    break;
                case 'Wrapped Secrets':
                    //$email_address = array('admin@wrappedsecrets.com.au', 'des@wrappedsecrets.com.au', 'wstracking@meandmrsjones.com.au', 'info@meandmrsjones.com.au');
                    $file = $this->createSimpleOrderTextFile($vendor, $data);
                    $emails->sendmail($email_address, $subject, 'MMJ New Order', $headers, $file);
                    break;
                default:
                    $file = $this->createSimpleOrderTextFile($vendor, $data);
                    $emails->sendmail('info@meandmrsjones.co.au', 'ALERT! Order not sent to vendor', 'ALERT! Order not sent to vendor', $headers, $file);
                    break;
            endswitch;
        endforeach;
    }

    private function api_ftp_windsor_order($orderFile) {
        $ftp_server = "windsorftp.dyndns.info";
        $ftp_user_name = "2417";
        $ftp_user_pass = "AP_2303";
        $remote_file = basename($orderFile);
        $destination_path = "webninja/";
        $destination_file = $destination_path . basename($orderFile);
        $conn_id = ftp_connect($ftp_server);
        $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
        if (!$login_result):
            return false;
        else:
            return ftp_put($conn_id, $destination_file, $orderFile, FTP_ASCII);
        endif;
    }

    public function api_save_product($request) {
        $data = json_decode($request->get_body());
        $post_id = (int) $data->save_post;
        if (isset($data->category)):
            $catsArray = array();
            foreach (explode(' ', $data->category) as $cat):
                array_push($catsArray, (int) $cat);
            endforeach;
            wp_set_object_terms($post_id, $catsArray, 'product_cat');
        endif;
        if (isset($data->barcode)):
            update_post_meta($post_id, '_sku', $data->barcode);
        endif;

        if (isset($data->brand)):
            update_post_meta($post_id, '_brand', $data->brand);
        endif;
        if (isset($data->wholesale_price)):
            update_post_meta($post_id, '_wholesale_price', $data->wholesale_price);
        endif;
        if (isset($data->code)):
            update_post_meta($post_id, '_code', $data->code);
        endif;
        if (isset($data->editProduct_price)):
            update_post_meta($post_id, '_regular_price', $data->editProduct_price);
            update_post_meta($post_id, '_price', $data->editProduct_price);
        endif;
        if (isset($data->barcode)):
            update_post_meta($post_id, '_barcode', $data->barcode);
        endif;
        if (isset($data->related_category)):
            update_post_meta($post_id, '_select_show_related_products_from_cat', $data->related_category);
        endif;
        if (isset($data->image_link)):
            update_post_meta($post_id, 'main_image_link', $data->image_link);
        endif;
        if (isset($data->image_link_thumb)):
            update_post_meta($post_id, 'image_link_thumb', $data->image_link_thumb);
        endif;
        if (isset($data->gallery_image)):
            update_post_meta($post_id, 'gallery_image_link', $data->gallery_image);
        endif;
        if (isset($data->youtube_link)):
            update_post_meta($post_id, 'youtube_link', $data->youtube_link);
        endif;
        if (!empty($data->colour)):
            update_post_meta($post_id, 'st_colour', $data->colour);
        endif;
        if (!empty($data->size)):
            update_post_meta($post_id, 'st_size', $data->size);
        endif;
        if (!empty($data->markup_amount)):
            update_post_meta($post_id, '_price_markup', $data->markup_amount);
        endif;
        if (!empty($data->seo_desc)):
            update_post_meta($post_id, '_yoast_wpseo_metadesc', $data->seo_desc);
        endif;
        if (!empty($data->seo_title)):
            update_post_meta($post_id, '_yoast_wpseo_title', $data->seo_title);
        endif;
        if (!empty($data->related_category)):
            update_post_meta($post_id, '_select_show_related_products_from_cat', $data->related_category);
        endif;
        if (isset($data->parent_id)):
            $this->api_new_associated_products($post_id, $data->parent_id);
        endif;
        $this->updateProductTags($post_id, $data);
        $my_post = array(
            'ID' => $post_id,
            'post_title' => $data->title,
            'post_content' => $data->l_description,
            'post_excerpt' => $data->s_description
        );
        wp_update_post($my_post);
        return $this->api_prepare_response_object(true);
    }

    private function updateProductTags($post_id, $data) {
        $term_type = 'product_tag';
        if (isset($post_id) && isset($data->tags)):
            $tags = explode(',', $data->tags);
            $tag_ids = array();
            foreach ($tags as $tag):
                $term = term_exists($tag, $term_type);
                if (!$term):
                    $term_id = wp_insert_term($tag, $term_type);
                endif;
            endforeach;
            wp_set_post_terms($post_id, $data->tags, $term_type);
        else:
            if (isset($post_id)):
                wp_set_object_terms($post_id, null, $term_type);
            endif;
        endif;
    }

    public function api_get_display_tags($request) {
        $cat_id = $request['cat_id'];
        $filters = array(
            'size' => get_term_meta($cat_id, 'size', true),
            'tags' => get_term_meta($cat_id, 'tags', true),
            'brand' => get_term_meta($cat_id, 'brand', true),
            'colour' => get_term_meta($cat_id, 'colour', true),
            'price' => get_term_meta($cat_id, 'price', true)
        );
        return $this->api_prepare_response_object($filters);
    }

    public function api_set_display_tags($request) {
        $cat_id = $request['cat_id'];
        update_term_meta($cat_id, 'size', $request['size']);
        update_term_meta($cat_id, 'tags', $request['tags']);
        update_term_meta($cat_id, 'brand', $request['brand']);
        update_term_meta($cat_id, 'colour', $request['colour']);
        update_term_meta($cat_id, 'price', $request['price']);
        return $this->api_prepare_response_object(true);
    }

    public function api_get_cat_tags($request) {
        $tagName = str_replace('&', '&amp;', $request['tag_name']);
        $termID = get_term_by('name', $tagName, 'product_tag');
        $termID = (int) $termID->term_id;
        return $this->api_prepare_response_object(array('cats' => get_term_meta($termID, 'show_in_cats', true)));
    }

    public function api_get_all_tags($request) {
        return $this->api_prepare_response_object(get_terms('product_tag'));
    }

    public function api_set_cat_tags($request) {
        $tagName = str_replace('&', '&amp;', $request['tag_name']);
        $termID = get_term_by('name', $tagName, 'product_tag');
        if (!$termID):
            $termID = wp_insert_term($request['tag_name'], 'product_tag');
        endif;
        $termID = $termID->term_id;
        update_term_meta($termID, 'show_in_cats', $request['cats']);
        return $this->api_prepare_response_object(array('result' => get_term_meta($termID, 'show_in_cats', true)));
    }

    public function api_get_shipping($request) {
        $results = get_option('woocommerce_flat_rate_2_settings');
        return $this->api_prepare_response_object($results);
    }

    public function api_set_shipping($request) {
        $results = update_option('woocommerce_flat_rate_2_settings', $request['value']);
        return $this->api_prepare_response_object($request['value']);
    }

    public function api_get_gst($request) {
        global $wpdb;
        $results = $wpdb->get_results('SELECT `tax_rate` FROM `wp_woocommerce_tax_rates` WHERE `tax_rate_id` = 1');
        return $this->api_prepare_response_object($results);
    }

    public function api_set_gst($request) {
        global $wpdb;
        $results = $wpdb->get_results("UPDATE `wp_woocommerce_tax_rates` SET `tax_rate` = '" . $request['value'] . "' WHERE `wp_woocommerce_tax_rates`.`tax_rate_id` = 1;");
        return $this->api_prepare_response_object($results);
    }

}
