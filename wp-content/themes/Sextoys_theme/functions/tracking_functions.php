<?php

function tracking_ww_tracking(WP_REST_Request $request) {
    if (isset($_FILES)):
        $tempFolder = st_get_site_root() . "/test_tracking/";
        foreach ($_FILES as $file):
            if (strpos($file["name"], '.txt') !== false):
                $tracking_number = false;
                $order_number = false;
                $target_file = $tempFolder . basename($file["name"]);
                move_uploaded_file($file["tmp_name"], $target_file);
                $linesArray = array();
                $file = fopen($tempFolder . $file["name"], "r");
                if ($file):
                    while (!feof($file)):
                        array_push($linesArray, fgets($file));
                    endwhile;
                endif;
                foreach ($linesArray as $line):
                    $linesArray = preg_split('/\s+/', $line);
                    if ($linesArray[0] == '001'):
                        $order_number = $linesArray[3];
                    endif;
                    if ($linesArray[0] == '004'):
                        $tracking_number = $linesArray[10];
                    endif;
                endforeach;
                fclose($file);
                unlink($target_file);
                if ($tracking_number && $order_number):
                    update_post_meta($order_number, 'windsor_tracking_number', 0);
                    $post = get_post($order_number);
                    if ($post->post_type == 'shop_order'):
                        $order = new WC_Order($order_number);
                        $billingEmail = $order->billing_email;
                        if (update_post_meta($order_number, 'windsor_tracking_number', $tracking_number) && $billingEmail):
                            $contentArray = array();
                            $subject = 'Your Me & Mrs Jones Order has been Shipped';
                            $contentArray[0] = 'Order Shipped';
                            $content = "Your order has been shipped\r\n\r\n";
                            $content .= "Your tracking number is <strong>" . $tracking_number . "</strong>\r\n\r\n";
                            $content .= "Please visit <a href='https://sttrackandtrace.startrack.com.au/'>https://sttrackandtrace.startrack.com.au/</a> to check delivery status.\r\n\r\n";
                            $content .= "Thank you for shopping with Me & Mrs Jones\r\n\r\n";
                            $contentArray[1] = $content;
                            $emailClass = st_get_email_class();
                            st_var_dump($emailClass->sendmail($billingEmail, $subject, $contentArray));
                        endif;
                    endif;
                endif;
            endif;
            if (strpos($file["name"], '.pdf') !== false):
                $uploadsDir = wp_upload_dir();
                $uploadsDir = $uploadsDir["basedir"] . '/wholesale_invoices/';
                $target_file = $uploadsDir . basename($file["name"]);
                move_uploaded_file($file["tmp_name"], $target_file);
            endif;
        endforeach;
    endif;
    echo 'Done';
}

function tracking_ws_tracking(WP_REST_Request $request) {
    
}

add_action('rest_api_init', function () {
    register_rest_route('tracking_api', 'ww_tracking', array(
        'methods' => 'POST',
        'callback' => 'tracking_ww_tracking',
    ));
    register_rest_route('tracking_api', 'ws_tracking', array(
        'methods' => 'POST',
        'callback' => 'tracking_ws_tracking',
    ));
});
?>