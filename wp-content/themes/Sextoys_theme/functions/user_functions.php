<?php

/*
 * ----------------------------------------------------------------------
 * Automatic login if the user has been authenticated from thr login form
 * ----------------------------------------------------------------------
 */

function custom_login_callback() {
    if (isset($_POST['login']) && isset($_POST['password'])):
        $cleanArray = array();
        $cleanArray['user_login'] = esc_attr($_POST['login']);
        $cleanArray['user_password'] = esc_attr($_POST['password']);
        $creds = array();
        $creds['user_login'] = $cleanArray['user_login'];
        $creds['user_password'] = $cleanArray['user_password'];
        $creds['remember'] = true;
        $user = wp_signon($creds, false);
        if (is_wp_error($user)) {
            echo $user->get_error_message();
        }
    endif;
}

add_action('after_setup_theme', 'custom_login_callback');

/*
 * -----------------------------------------------------------------------------
 * Authenticates the username and password from the login form, if authenticated
 * the page refreshes and user is logined in else error message is returned
 * -----------------------------------------------------------------------------
 */

function st_authenticate_user_callback() {
    $login = esc_attr($_POST['login']);
    $password = esc_attr($_POST['password']);
    $user = wp_authenticate($login, $password);
    if (is_wp_error($user)) {
        echo $user->get_error_message();
    } else {
        echo 'true';
    }
    wp_die();
}

add_action('wp_ajax_st_authenticate_user', 'st_authenticate_user_callback');
add_action('wp_ajax_nopriv_st_authenticate_user', 'st_authenticate_user_callback');

/*
 * -----------------
 * Create a new user
 * -----------------
 */

function st_create_user_callback() {
    $cleanArray = array(
        'first_name' => esc_attr($_POST['first_name']),
        'last_name' => esc_attr($_POST['last_name']),
        'user_login' => esc_attr($_POST['username']),
        'user_pass' => esc_attr($_POST['password']),
        'user_email' => esc_attr($_POST['email']),
        'subscribe' => esc_attr($_POST['subscribe'])
    );
    $user = wp_insert_user($cleanArray);
    if (is_wp_error($user)):
        echo 'Error 1';
    else:
        $creds = array(
            'user_login' => $cleanArray['user_login'],
            'user_password' => $cleanArray['user_pass'],
            'remember' => true
        );
        $userlogon = wp_signon($creds, false);
        update_user_meta($user, 'subscribed', $cleanArray['subscribe']);
        update_user_meta($user, "billing_first_name", $cleanArray['first_name']);
        update_user_meta($user, "billing_last_name", $cleanArray['last_name']);
        if (isset($_POST['after_order'])):
            update_user_meta($user, "billing_first_name", $cleanArray['first_name']);
            update_user_meta($user, "billing_last_name", $cleanArray['last_name']);
            update_user_meta($user, "billing_company", strip_tags($_POST["newAddressCompanyName"]));
            update_user_meta($user, "billing_address_1", strip_tags($_POST["billing_address1"]));
            update_user_meta($user, "billing_address_2", strip_tags($_POST["billing_address2"]));
            update_user_meta($user, "billing_city", strip_tags($_POST["billing_city"]));
            update_user_meta($user, "billing_postcode", strip_tags($_POST["billing_postcode"]));
            update_user_meta($user, "billing_state", strip_tags($_POST["billing_state"]));
            update_user_meta($user, "billing_phone", strip_tags($_POST["phone"]));
            update_post_meta($_POST['order_id'], '_customer_user', $user);
        endif;
        echo (is_wp_error($userlogon)) ? 'false' : 'true';
    endif;

    wp_die();
}

/*
 * ---------------------------
 * Check if username is in use
 * ---------------------------
 */

function check_user_name_callback() {
    $username = esc_attr($_POST['username']);
    echo (username_exists($username)) ? 'true' : 'false';
    wp_die();
}
