<?php

function api_get_new_products(WP_REST_Request $request) {
    if (checkAPIKey($request)):
        global $wpdb;
        $productArray = array();
        $cat_id = $request['cat_id'];
        $query = "SELECT wp_posts.ID "
                . "FROM wp_posts, wp_term_relationships "
                . "WHERE wp_posts.ID = wp_term_relationships.object_id "
                . "AND wp_term_relationships.term_taxonomy_id = $cat_id";
        foreach (json_decode($request['imported_products']) as $id):
            $query .= " AND wp_posts.ID != $id";
        endforeach;
        foreach ($wpdb->get_results($query, OBJECT) as $product):
            array_push($productArray, $product->ID);
        endforeach;
        return api_prepare_response_object($productArray);
    else:
        return notAuthResponse();
    endif;
}

function api_get_product_meta(WP_REST_Request $request) {
    if (checkAPIKey($request)):
        global $wpdb;
        $returnArray = array();
        $product_id = $request['product_id'];
        $meta = $wpdb->get_results("SELECT * FROM `wp_postmeta` WHERE `post_id` = $product_id");
        $post = get_post($product_id);
        array_push($returnArray, array(
            'post' => $post,
            'meta' => $meta
        ));
        return api_prepare_response_object($returnArray);
    else:
        return notAuthResponse();
    endif;
}

function api_get_attachment(WP_REST_Request $request) {
    if (checkAPIKey($request)):
        global $wpdb;
        $attachment_id = $request['attachment_id'];
        return api_prepare_response_object($wpdb->get_results("SELECT * FROM wp_posts WHERE ID = $attachment_id"));
    else:
        return notAuthResponse();
    endif;
}

function api_get_cats(WP_REST_Request $request) {
    if (checkAPIKey($request)):
        require_once ABSPATH . 'wp-content/plugins/warehouse/includes/warehouse_live_products_class.php';
        $live_products = new warehouse_live_products_class();
        return api_prepare_response_object($live_products->warehouse_get_cat_array());
    else:
        return notAuthResponse();
    endif;
}

add_action('rest_api_init', function () {
    register_rest_route('admin_api', 'get_new_products', array(
        'methods' => 'POST',
        'callback' => 'api_get_new_products'
    ));
    register_rest_route('admin_api', 'get_product_meta', array(
        'methods' => 'POST',
        'callback' => 'api_get_product_meta'
    ));
    register_rest_route('admin_api', 'get_attachment', array(
        'methods' => 'POST',
        'callback' => 'api_get_attachment'
    ));
    register_rest_route('admin_api', 'get_cats', array(
        'methods' => 'POST',
        'callback' => 'api_get_cats'
    ));
});

function api_prepare_response_object($data) {
    $response = new WP_REST_Response($data);
    $response->set_status(200);
    $response->header('Location', get_site_url());
    return $response;
}

function notAuthResponse() {
    $response = new WP_REST_Response();
    $response->set_status(403);
    $response->header('Location', get_site_url());
    return $response;
}

function checkAPIKey($request) {
    $api_key = 'g#Q&5RP!bNYv@G#kNomyaaTZj9grx16N%WH)ClXN(K7Ye_7#mt';
    return ($api_key == $request['api_key']) ? true : false;
}
