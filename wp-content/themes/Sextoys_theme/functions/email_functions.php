<?php

class email_orders {

    function sendmail($email_address, $subject, $content, $headers, $attachment) {
        $mails = new WC_Emails();
        if ($attachment):
            return $mails->send($email_address, $subject, $content, $headers, $attachment);
        else:
            return $mails->send($email_address, $subject, $content, $headers);
        endif;
    }

    function wrapContent($heading, $content) {
        $mails = new WC_Emails();
        return $mails->wrap_message($heading, $content);
    }

    function sendmailWithWrap($email_address, $subject, $content, $headers, $attachment) {
        $mails = new WC_Emails();
        $content = $mails->wrap_message($heading, $content);
        if ($attachment):
            return $mails->send($email_address, $subject, $content, $headers, $attachment);
        else:
            return $mails->send($email_address, $subject, $content, $headers);
        endif;
    }

}
