<?php

/*
 * -----------------------
 * Change image src to aws
 * -----------------------
 */

add_filter('wp_get_attachment_url', 'st_get_attachment_url');

function st_get_attachment_url($url) {
    $image_array = explode('/', $url);
    $image_name = $image_array[count($image_array) - 1];
    return 'https://s3.amazonaws.com/adult-toys/images/low-res/' . $image_name;
}

/*
 * ---------------------------------------------------------
 * Add image to aws upon upload in dashboard
 * ---------------------------------------------------------
 */

add_filter('wp_handle_upload_prefilter', 'st_aws_upload_filter');

function st_aws_upload_filter($file) {
    Aws_Wrapper::aw_put_file(null, 'images/low-res/', $file['name'], $file['tmp_name'], $file['type']);
    return $file;
}

/*
 * ---------------------------------------------------------
 * Delete image on aws upon deletion in dashboard
 * ---------------------------------------------------------
 */

add_action('delete_attachment', 'st_aws_delete_image');

function st_aws_delete_image($postid) {
    $result = wp_get_attachment_metadata($postid);
    $result = $result['file'];
    $file_exist = Aws_Wrapper::check_if_image_exists(null, $result, 'low-res');
    if ($file_exist):
        Aws_Wrapper::aw_delete_object($result, 'low-res');
    endif;
}
