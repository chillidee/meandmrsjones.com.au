<?php
/*
 * -----------------------------------
 * Get the number of items in the cart
 * -----------------------------------
 */

function number_of_items_in_cart() {
    return WC()->cart->get_cart_contents_count();
}

/*
 * -----------------------
 * Change button cart text
 * -----------------------
 */

function woo_custom_cart_button_text() {

    return __('Add To Cart', 'woocommerce');
}

add_filter('woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text');

/*
 * --------------------------------------------
 * Remove woocommerce actions from product page
 * --------------------------------------------
 */

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products');

/*
 * -----------------------------
 * Remove woocomerce added items
 * -----------------------------
 */

function woo_remove_product_tabs($tabs) {

    unset($tabs['description']);       // Remove the description tab
    unset($tabs['reviews']);    // Remove the reviews tab
    unset($tabs['additional_information']);   // Remove the additional information tab

    return $tabs;
}

add_filter('woocommerce_product_tabs', 'woo_remove_product_tabs', 98);

/*
 * ---------------------------------
 * Add description under cart button
 * ---------------------------------
 */

function add_description_to_product() {
    global $product;
    $productObject = $product->post;
    $description = $productObject->post_content;
    echo '<div class="col-lg-12 col-lg-offset-0 col-xs-12 col-xs-offset-0 product-description">'
    . '<h5 class="product-information-title">Product Information</h5>'
    . '<h4 class="product-description-title">Product Description</h4>'
    . '<p>' . $description . '<p>'
    . '</div>';
}

add_action('woocommerce_single_product_summary', 'add_description_to_product', 60);

/*
 * -----------------------------------------
 * Add breadcrumb before signle product page
 * -----------------------------------------
 */

function add_breadcrumb_before_product() {
    echo '<div class="row"><div class="col-lg-offset-1 col-xs-10 col-xs-offset-1 col-lg-10 product-breadcrumb">';
    woocommerce_breadcrumb();
    echo '</div></div>';
}

add_action('woocommerce_before_single_product', 'add_breadcrumb_before_product', 10);

/*
 * ------------------------------------
 * Get parents subcats from parent name
 * ------------------------------------
 */

function woocommerce_subcats_from_parentcat_by_NAME2($parent_cat_NAME) {
    $IDbyNAME = get_term_by('name', $parent_cat_NAME, 'product_cat');
    $product_cat_ID = $IDbyNAME->term_id;
    $args = array(
        'hierarchical' => 1,
        'show_option_none' => '',
        'hide_empty' => 0,
        'parent' => $product_cat_ID,
        'taxonomy' => 'product_cat'
    );
    $subcats = get_categories($args);
    return $subcats;
}

/*
 * ----------------------------------
 * Get parents subcats from parent id
 * ----------------------------------
 */

function st_subcats_from_parentcat_by_id($parent_cat_id) {
    $product_cat_ID = $parent_cat_id;
    $cleanArray = array();
    $args = array(
        'hierarchical' => 1,
        'show_option_none' => '',
        'hide_empty' => 0,
        'parent' => $product_cat_ID,
        'taxonomy' => 'product_cat'
    );
    $subcats = get_categories($args);
    foreach ($subcats as $cat):
        $order = (int) st_get_category_meta($cat->term_id, 'cat_order');
        if ($order == 0) {
            $order = 99;
        }
        $cleanArray[$cat->term_id] = $order;
    endforeach;
    $subcats = array();
    asort($cleanArray);
    foreach ($cleanArray as $index => $value):
        array_push($subcats, get_category((int) $index));
    endforeach;
    return $subcats;
}

/*
 * ------------------------------
 * Get woocommerce category image
 * ------------------------------
 */

function st_get_category_image($cat_id, $size) {
    $cat_thumb_id = get_woocommerce_term_meta($cat_id, 'thumbnail_id', true);
    $shop_catalog_img = wp_get_attachment_image_src($cat_thumb_id, $size);
    return $shop_catalog_img;
}

/*
 * ----------------------------
 * Get a list of all categories
 * ----------------------------
 */

function get_all_categories() {
    $taxonomy = 'product_cat';
    $orderby = 'name';
    $args = array(
        'taxonomy' => $taxonomy,
        'hide_empty' => 0,
        'orderby' => $orderby);
    $all_categories = get_categories($args);
    return $all_categories;
}

/*
 * ------------------------
 * Populates cart drop down
 * ------------------------
 */

function st_get_cart_contents() {
    global $woocommerce;
    $cart = $woocommerce->cart->cart_contents;
    $uploads = wp_upload_dir();
    if (count($cart) > 0):
        ?><i id="cart-scroll-up" class="fa fa-angle-up"></i>
        <div class="cart-items-container" id="items-container">
            <?php
            foreach ($cart as $item):
                $data = $item['data'];
                st_update_cart_subtotal($data->price, $item["quantity"]);
                ?>
                <div class="cart-item-container col-lg-10 col-lg-offset-1">
                    <img src="https://s3.amazonaws.com/adult-toys/images/low-res/close_merlot.png" class="remove-item-from-cart"  title="Remove Item" id="<?php echo $data->post->ID ?>"/>
                    <a href="<?php echo $data->post->guid; ?>">
                        <div class="cat-overlay-image">
                            <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($data->post->ID)); ?>"/>
                        </div>
                        <div class="cat-overlay-description">
                            <h4><?php echo $data->post->post_title ?></h4>
                            <?php
                            $color = st_get_product_color($data->post->ID);
                            $size = st_get_product_size($data->post->ID);
                            if ($color):
                                ?>
                                <p>Colour: <span><?php echo $color; ?></span></p>
                                <?php
                            endif;
                            if ($size):
                                ?>
                                <p>Size: <span><?php echo $size; ?></span></p>
                            <?php endif; ?>
                            <p>Quantity: <span><?php echo $item["quantity"]; ?></span></p>
                            <input id="cart-overlay-quantity" type="hidden" value="<?php echo $item["quantity"]; ?>"/>
                            <p>Price: <span>$<?php echo $data->price ?></span></p>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
        <i id="cart-scroll-down" class="fa fa-angle-down"></i>
        <?php
    else:
        ?>
        <h3>Your Cart Is Empty</h3>
    <?php
    endif;
}
