<?php

/*
 * -------------------------
 * Add styles and JS to site
 * -------------------------
 */

function enqueue_scripts_and_styles() {
    if (is_admin()) {
        wp_enqueue_media();
    }

    wp_enqueue_script('jquery-zoom', get_template_directory_uri() . '/js/jquery.zoom.min.js');
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js');
    wp_enqueue_script('custom', get_template_directory_uri() . '/js/custom.js');
    wp_enqueue_script('angular', get_template_directory_uri() . '/js/angular.min.js');
    wp_enqueue_script('menu', get_template_directory_uri() . '/js/menu.js');
    wp_enqueue_script('preloader', get_template_directory_uri() . '/js/jquery.preload.min.js');
    wp_enqueue_script('preloadImages', get_template_directory_uri() . '/js/preloadImages.js');

    wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style('stylesheet', get_template_directory_uri() . '/style.min.css');
    wp_enqueue_style('main', get_template_directory_uri() . '/css/main.css');
    wp_enqueue_style('fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css');
}

add_action('wp_enqueue_scripts', 'enqueue_scripts_and_styles', '500000');
