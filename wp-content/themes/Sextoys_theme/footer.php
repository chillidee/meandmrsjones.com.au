<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 */
$path = content_url() . '/uploads/2016/02/';
?>
<div class="about-us-overlay footer-overlay"><?php include_once 'partials/about-overlay.min.html'; ?></div>

<div class="privacy-overlay footer-overlay"><?php include_once 'partials/privacy-overlay.min.html'; ?></div>
<div class="faq-overlay footer-overlay"><?php include_once 'partials/faq-overlay.min.html'; ?></div>
<div class="tc-overlay footer-overlay"><?php include_once 'partials/t&c-overlay.min.html'; ?></div>

<?php if (!is_user_logged_in()): ?>
    <div id="mobile-signup-overlay" class="footer-overlay mob-only"><?php include_once 'partials/mobile-signup-overlay.php' ?></div>
<?php endif;
?>
</main>
<footer class="row">
    <?php include_once 'partials/footer-links.php'; ?>
</footer>
</div>
<link href='https://fonts.googleapis.com/css?family=Merriweather+Sans:400,300,300italic,400italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
<?php
wp_footer();
if (isset($_REQUEST['referal']) && !empty($_REQUEST['referal'])):
    ?>
    <script>
        ga('send', 'event', 'referal', '"<?php echo $_REQUEST['referal'] ?>"', {
            'referal': "<?php echo $_REQUEST['referal'] ?>"
        });
    </script>
    <?php
endif;
?>
</body>
</html>