<?php

class orders_email_orders {

    function orders_sendmail($email_address, $subject, $content, $headers, $attachment) {
        $mails = new WC_Emails();
        if ($attachment):
            return $mails->send($email_address, $subject, $content, $headers, $attachment);
        else:
            return $mails->send($email_address, $subject, $content, $headers);
        endif;
    }

    function orders_wrapContent($heading, $content) {
        $mails = new WC_Emails();
        return $mails->wrap_message($heading, $content);
    }

    function orders_sendmailWithWrap($email_address, $subject, $content, $headers, $attachment) {
        $mails = new WC_Emails();
        $content = $mails->wrap_message($heading, $content);
        if ($attachment):
            return $mails->send($email_address, $subject, $content, $headers, $attachment);
        else:
            return $mails->send($email_address, $subject, $content, $headers);
        endif;
    }

    function orders_no_reply_content($subject, $content) {
        $returnContent = '<html dir="ltr"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>Me &amp; Mrs Jones</title></head><body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><div id="wrapper" dir="ltr" style="background-color: #f5f5f5; margin: 0; padding: 70px 0 70px 0; -webkit-text-size-adjust: none !important; width: 100%;"><table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"><tbody><tr><td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container" style="box-shadow: 0 1px 4px rgba(0,0,0,0.1) !important; background-color: #fdfdfd; border: 1px solid #dcdcdc; border-radius: 3px !important;"><tbody><tr><td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="600" id="template_header" style="background-color: #851D19; border-radius: 3px 3px 0 0 !important; color: #ffffff; border-bottom: 0; font-weight: bold; line-height: 100%; vertical-align: middle; font-family: &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif;"><tbody><tr><td id="header_wrapper" style="padding: 36px 48px; display: block;"><a href="http://meandmrsjones.com.au" style="color: #557da1; font-weight: normal; text-decoration: none;"><img height="50px" src="https://s3.amazonaws.com/adult-toys/images/site_images/header-logo.png" alt="Me &amp; Mrs Jones" style="border: none; display: block; margin: 0 auto; font-size: 14px; font-weight: bold; height: 200px; line-height: 100%; outline: none; text-decoration: none; text-transform: capitalize;"><h1 style="color: #ffffff; font-family: &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; font-size: 30px; font-weight: 300; line-height: 150%; margin: 0; text-align: left; text-shadow: 0 1px 0 #7797b4; -webkit-font-smoothing: antialiased;">' . $subject . '</h1></a></td></tr></tbody></table></td></tr><tr><td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body"><tbody><tr><td valign="top" id="body_content" style="background-color: #fdfdfd;"><table border="0" cellpadding="20" cellspacing="0" width="100%"><tbody><tr><td valign="top" style="padding: 48px;"> <div id="body_content_inner" style="color: #737373; font-family: &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;"><p style="margin: 0 0 16px;">' . $content . '</div></td></tr></tbody></table></td></tr></tbody></table> </td></tr><tr><td align="center" valign="top"><table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer"><tbody><tr><td valign="top" style="padding: 0; -webkit-border-radius: 6px;"><table border="0" cellpadding="10" cellspacing="0" width="100%"><tbody><tr><td colspan="2" valign="middle" id="credit" style="padding: 0 48px 48px 48px; -webkit-border-radius: 6px; border: 0; color: #851D19; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;"><p>Me &amp; Mrs Jones ' . date('Y') . '</p></td></tr></tbody></table></td></tr></tbody></table> </td></tr></tbody></table></td></tr></tbody></table></div></body></html>';
        return $returnContent;
    }

}
