<ul class="parent-sidebar-sub-menu">
    <?php
    $slugSidebar = get_queried_object();
    $slugSidebar = $slugSidebar->post_name;
    ?>
    <li class="parent-sidebar-sub-menu-has-child parent-sidebar <?php echo (strtolower($parent_slug) == 'toys' || strtolower($slug) == 'toys' || strtolower($slugSidebar) == 'toys') ? ' sub-menu-open' : '' ?>">
        <span class="menuclosed"></span>
        <a class="archive-cat-title" href="<?php echo get_site_url(); ?>/toys">Toys</a>
        <ul class="sidebar-sub-menu">
            <li><a <?php echo (strtolower($slug) === 'vibrators') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/toys/vibrators/">Vibrators</a></li>
            <li><a <?php echo (strtolower($slug) == 'anal') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/toys/anal/">Anal</a></li>
            <li><a <?php echo (strtolower($slug) == 'cock-rings') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/toys/cock-rings/">Cock Rings</a></li>
            <li><a <?php echo (strtolower($slug) == 'dongs-dildos-strap-ons') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/toys/dongs-dildos-strap-ons/">Dongs, Dildos & Strap-Ons</a></li>
            <li><a <?php echo (strtolower($slug) == 'bullets-balls-eggs') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/toys/bullets-balls-eggs/">Bullets, Balls, Eggs</a></li>
            <li><a <?php echo (strtolower($slug) == 'masturbators') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/toys/masturbators/">Masturbators</a></li>
            <li><a <?php echo (strtolower($slug) == 'pumps') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/toys/pumps/">Pumps</a></li>
            <li><a <?php echo (strtolower($slug) == 'sleeves-extensions') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/toys/sleeves-extensions/">Sleeves & Extensions</a></li>
            <li><a <?php echo (strtolower($slug) == 'love-dolls') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/toys/love-dolls">Love Dolls</a></li>
            <li><a <?php echo (strtolower($slug) == 'swings-slings') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/toys/swings-slings/">Swings & Slings</a></li>
            <li><a <?php echo (strtolower($slug) == 'sex-furniture') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/toys/sex-furniture/">Sex Furniture</a></li>
            <li><a <?php echo (strtolower($slug) == 'accessories') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/toys/accessories/">Accessories</a></li>
        </ul>
    </li>
    <li class="parent-sidebar-sub-menu-has-child parent-sidebar <?php echo (strtolower($parent_slug) == 'fetish' || strtolower($slug) == 'fetish') ? ' sub-menu-open' : '' ?>">
        <span class="menuclosed"></span>
        <a class="archive-cat-title" href="<?php echo get_site_url(); ?>/fetish">Fetish</a>
        <ul class="sidebar-sub-menu">
            <li><a <?php echo (strtolower($slug) == 'blindfolds-hoods') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/fetish/blindfolds-hoods/">Blindfolds & Hoods</a></li>
            <li><a <?php echo (strtolower($slug) == 'cuffs-restraints-gags') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/fetish/cuffs-restraints-gags/">Cuffs, Restraints & Gags</a></li>
            <li><a <?php echo (strtolower($slug) == 'whips-paddles-ticklers') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/fetish/whips-paddles-ticklers/">Whips, Paddles & Ticklers</a></li>
            <li><a <?php echo (strtolower($slug) == 'nipple-stimulators') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/fetish/nipple-stimulators/">Nipple Stimulators</a></li>
            <li><a <?php echo (strtolower($slug) == 'accessories') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/fetish/accessories/">Accessories</a></li>
            <li><a <?php echo (strtolower($slug) == 'fifty-shades-of-grey-inspired') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/fetish/fifty-shades-of-grey-inspired/">Fifty Shades of Grey Inspired</a></li>
        </ul>
    </li>
    <li class="parent-sidebar-sub-menu-has-child parent-sidebar <?php echo (strtolower($parent_slug) == 'apparel' || strtolower($slug) == 'apparel') ? ' sub-menu-open' : '' ?>">
        <span class="menuclosed"></span>
        <a class="archive-cat-title" href="<?php echo get_site_url(); ?>/apparel">Apparel</a>
        <ul class="sidebar-sub-menu">
            <li><a <?php echo (strtolower($slug) == 'lingerie') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/apparel/lingerie/">Lingerie</a></li>
            <li><a <?php echo (strtolower($slug) == 'mens-range') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/apparel/mens-range/">Mens Range</a></li>
            <li><a <?php echo (strtolower($slug) == 'costumes') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/apparel/costumes/">Costumes</a></li>
            <li><a <?php echo (strtolower($slug) == 'bridal') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/apparel/bridal/">Bridal</a></li>
            <li><a <?php echo (strtolower($slug) == 'club-wear') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/apparel/club-wear/">Club Wear</a></li>
            <li><a <?php echo (strtolower($slug) == 'fetish-wear') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/fetish/fetish-wear/">Fetish Wear</a></li>
            <li><a <?php echo (strtolower($slug) == 'sexy-shoes') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/apparel/sexy-shoes/">Sexy Shoes</a></li>
            <li><a <?php echo (strtolower($slug) == 'accessories') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/apparel/accessories/">Accessories</a></li>
        </ul>
    </li>
    <li class="parent-sidebar-sub-menu-has-child parent-sidebar <?php echo (strtolower($parent_slug) == 'essentials' || strtolower($slug) == 'essentials') ? ' sub-menu-open' : '' ?>">
        <span class="menuclosed"></span>
        <a class="archive-cat-title" href="<?php echo get_site_url(); ?>/essentials">Essentials</a>
        <ul class="sidebar-sub-menu">
            <li><a <?php echo (strtolower($slug) == 'lubes-gels') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/essentials/lubes-gels/">Lubes & Gels</a></li>
            <li><a <?php echo (strtolower($slug) == 'massage') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/essentials/massage/">Massage</a></li>
            <li><a <?php echo (strtolower($slug) == 'body-paints') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/essentials/body-paints/">Body Paints</a></li>
            <li><a <?php echo (strtolower($slug) == 'cleaning-grooming') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/essentials/cleaning-grooming/">Cleaning & Grooming</a></li>
            <li><a <?php echo (strtolower($slug) == 'sex-toy-cleaners') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/essentials/sex-toy-cleaners/">Sex Toy Cleaners</a></li>
            <li><a <?php echo (strtolower($slug) == 'sexual-aids') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/essentials/sexual-aids/">Sexual Aids</a></li>
            <li><a <?php echo (strtolower($slug) == 'condoms') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/essentials/condoms/">Condoms</a></li>
        </ul>
    </li>
    <li class="parent-sidebar-sub-menu-has-child parent-sidebar <?php echo (strtolower($parent_slug) == 'gifts-games' || strtolower($slug) == 'gifts-games') ? ' sub-menu-open' : '' ?>">
        <span class="menuclosed"></span>
        <a class="archive-cat-title" href="<?php echo get_site_url(); ?>/gifts-games">Gifts &amp; Games</a>
        <ul class="sidebar-sub-menu">
            <li><a <?php echo (strtolower($slug) == 'sex-games') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/gifts-games/sex-games/">Sex Games</a></li>
            <li><a <?php echo (strtolower($slug) == 'naughty-novelties') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/gifts-games/naughty-novelties/">Naughty Novelties</a></li>
            <li><a <?php echo (strtolower($slug) == 'couples-kits') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/gifts-games/couples-kits/">Couples Kits</a></li>
            <li><a <?php echo (strtolower($slug) == 'hens-night-products') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/gifts-games/hens-night-products/">Hens Night Products</a></li>
            <li><a <?php echo (strtolower($slug) == 'bucks-night-products') ? 'class="active-category"' : ''; ?> href="<?php echo get_site_url(); ?>/gifts-games/bucks-night-products/">Bucks Night Products</a></li>
        </ul>
    </li>
    <?php if (is_user_logged_in()): ?>
        <li class="parent-sidebar-sub-menu-has-child parent-sidebar <?php echo ($_SERVER["REQUEST_URI"] == '/my-account/') ? ' sub-menu-open' : '' ?> visible-xs visible-sm visible-md">
            <span class="menuclosed"></span>
            <a class="archive-cat-title" href="<?php echo get_site_url(); ?>/my-account/">My Account</a>
            <ul class="sidebar-sub-menu">
                <li <?php echo (!$_POST || isset($_POST['my_dashboard']) || isset($_POST['thankyouOrderId'])) ? 'class="my-account-active-button"' : '' ?>>
                    <form method="post" action="/my-account">
                        <input type="hidden" name="my_dashboard" value="my_dashboard"/>
                        <button type="submit">My Dashboard</button>
                    </form>
                </li>
                <li <?php echo (isset($_POST['personal_details'])) ? 'class="my-account-active-button"' : '' ?>>
                    <form method="post" action="/my-account">
                        <input type="hidden" name="personal_details" value="personal_details"/>
                        <button type="submit">Personal Details</button>
                    </form>
                </li>
                <li <?php echo (isset($_POST['address_book']) || isset($_POST['my-account-new-address']) || $_POST['my-account-edit-address'] || $_POST['my-account-delete-address']) ? 'class="my-account-active-button"' : '' ?>>
                    <form method="post" action="/my-account">
                        <input type="hidden" name="address_book" value="address_book"/>
                        <button type="submit">Address Book</button>
                    </form>
                </li>
                <?php if (wc_get_customer_order_count(get_current_user_id()) > 0): ?>
                    <li <?php echo (isset($_POST['my_orders']) || isset($_POST['my-order-detailed'])) ? 'class="my-account-active-button"' : '' ?>>
                        <form method="post" action="/my-account">
                            <input type="hidden" name="my_orders" value="my_orders"/>
                            <button type="submit">My Orders</button>
                        </form>
                    </li>
                <?php endif; ?>
                <li <?php echo (isset($_POST['customer_service'])) ? 'class="my-account-active-button"' : '' ?>>
                    <form method="post" action="/my-account">
                        <input type="hidden" name="customer_service" value="customer_service"/>
                        <button  type="submit">Customer Service</button>
                    </form>
                </li>
                <li <?php echo (isset($_POST['my-reviews']) || isset($_POST['review_product_id'])) ? 'class="my-account-active-button"' : '' ?>>
                    <form method="post" action="/my-account">
                        <input type="hidden" name="my-reviews" value="my-reviews"/>
                        <button  type="submit">My Reviews</button>
                    </form>
                </li>
            </ul>
        </li>
    <?php endif; ?>
</ul>