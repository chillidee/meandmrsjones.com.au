<?php if ($page == 'my-account-page'): ?>
    <p>Your Account</p>
    <ul>
        <li <?php echo (!$_POST || isset($_POST['my_dashboard']) || isset($_POST['thankyouOrderId'])) ? 'class="my-account-active-button"' : '' ?>>
            <form method="post">
                <input type="hidden" name="my_dashboard" value="my_dashboard"/>
                <button type="submit">My Dashboard</button>
            </form>
        </li>
        <li <?php echo (isset($_POST['personal_details'])) ? 'class="my-account-active-button"' : '' ?>>
            <form method="post">
                <input type="hidden" name="personal_details" value="personal_details"/>
                <button type="submit">Personal Details</button>
            </form>
        </li>
        <li <?php echo (isset($_POST['address_book']) || isset($_POST['my-account-new-address']) || $_POST['my-account-edit-address'] || $_POST['my-account-delete-address']) ? 'class="my-account-active-button"' : '' ?>>
            <form method="post">
                <input type="hidden" name="address_book" value="address_book"/>
                <button type="submit">Address Book</button>
            </form>
        </li>
        <?php if (wc_get_customer_order_count(get_current_user_id()) > 0): ?>
            <li <?php echo (isset($_POST['my_orders']) || isset($_POST['my-order-detailed'])) ? 'class="my-account-active-button"' : '' ?>>
                <form method="post">
                    <input type="hidden" name="my_orders" value="my_orders"/>
                    <button type="submit">My Orders</button>
                </form>
            </li>
        <?php endif; ?>
        <li <?php echo (isset($_POST['customer_service'])) ? 'class="my-account-active-button"' : '' ?>>
            <form method="post">
                <input type="hidden" name="customer_service" value="customer_service"/>
                <button  type="submit">Customer Service</button>
            </form>
        </li>
    </ul>
    <?php
else:
    $url = explode('/', $_SERVER["REQUEST_URI"]);
    $all_cats = get_all_categories();
    $parent_cat = '';
    foreach ($all_cats as $cat):
        if (strtolower($cat->slug) == strtolower($page)):
            $parent_cat = $cat;
        endif;
    endforeach;
    unset($all_cats);
    $parent_children = st_cat_get_children($parent_cat->term_id);
    ?>
    <p><?php echo $page ?></p>
    <div class="<?php
    echo ($parent_cat->slug) ? $parent_cat->slug . '-sidebar' : '';
    echo ($children && !st_is_third_tier_cat($cat_object) && !isset($_POST["cat-number"])) ? ' title-sidebar' : '';
    ?>">
             <?php
             if ($parent_children):
                 ?>
            <ul class="parent-sidebar-sub-menu" id="sidebar-menu-wrapper">
                <?php
                foreach ($parent_children as $child):
                    $child_children = st_cat_get_children($child->term_id);
                    ?>
                    <li class="<?php
                    echo ($child_children) ? 'parent-sidebar-sub-menu-has-child' : 'parent-sidebar-sub-menu-no-child';
                    echo ($cat_object->parent == $child->term_id || $parent_cat_object->parent == $child->term_id || strtolower($url[count($url) - 2]) == strtolower($child->slug)) ? ' sub-menu-open' : '';
                    ?> parent-sidebar"><a class="archive-cat-title" href="<?php echo st_get_category_guid($child->term_id); ?>">
                        <?php echo $child->name; ?>
                        <?php if ($child_children):
                            ?></a>
                            <ul class="sidebar-sub-menu">
                                <?php foreach ($child_children as $child_sub): ?>
                                    <li>
                                        <a <?php echo ($child_sub->name == $catSlug) ? 'class="active-category"' : ''; ?> href="<?php echo st_get_category_guid($child_sub->term_id) ?>"><?php echo $child_sub->name; ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif;
                        ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
<?php
endif;
unset($parent_children);
unset($parent_cat);
unset($children);
