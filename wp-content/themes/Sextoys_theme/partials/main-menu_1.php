<ul class="<?php echo "{$class_selector}"; ?>-menu <?php echo ($page_class) ? $page_class : '' ?>">
    <?php
    foreach ($main_menu as $menu_item):

        if (!$menu_item->menu_item_parent):
            ?>
            <li id='<?php echo $menu_item->post_name ?>-header-menu' class="<?php echo "{$class_selector}"; ?>-menu-item home-menu-parent" data-value="<?php echo $parent_menus[$menu_item->post_name]->term_id ?>">
                <a href="<?php echo $menu_item->url ?>"><?php echo $menu_item->post_title ?></a>
                <div class="hover-menu-wrapper">
                    <?php
                    foreach ($parent_menus as $cat):
                        if ($cat->name == $menu_item->post_title):
                            ?>
                            <div id="hover-sub-menu-<?php echo "{$cat->term_id}" ?>" class="hover-sub-menu">
                                <div class="col-lg-12 hover-menu-header">
                                    <p><?php echo $cat->name ?></p>
                                </div>
                                <?php foreach (st_subcats_from_parentcat_by_id($cat->term_id) as $subcat): ?>
                                    <div class="col-lg-2 hover-subcat-parent-child <?php echo (strtolower($subcat->name) == 'sale') ? 'sale_link' : '' ?>">
                                        <p><a href="<?php echo st_get_category_guid($subcat->term_id); ?>"><?php echo (strtolower($subcat->name) == 'sale') ? 'ON ' . $subcat->name . ' NOW' : $subcat->name ?></a></p>
                                        <ul>
                                            <?php
                                            $subsubcats = st_subcats_from_parentcat_by_id($subcat->term_id);
                                            if (st_get_category_meta($subsubcats[0]->term_id, 'cat_order')):
                                                $subsubcatsSorted = array();
                                                foreach ($subsubcats as $subsubcat):
                                                    $subsubcatsSorted[st_get_category_meta($subsubcat->term_id, 'cat_order')] = $subsubcat;
                                                endforeach;
                                                $subsubcats = $subsubcatsSorted;
                                                unset($subsubcatsSorted);
                                            endif;
                                            foreach ($subsubcats as $subsubcat):
                                                ?>
                                                <li>
                                                    <a href="<?php echo get_term_link($subsubcat->term_id, 'product_cat'); ?>"><?php echo $subsubcat->name ?></a>
                                                    <ul class="hover-menu-sub-root">
                                                        <?php
                                                        foreach (st_subcats_from_parentcat_by_id($subsubcat->term_id) as $sss_cat):
                                                            ?>
                                                            <li><a href="<?php echo st_get_category_guid($sss_cat->term_id); ?>"><?php echo $sss_cat->name ?></a></li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </li>
                                                <?php
                                            endforeach;
                                            unset($subsubcats);
                                            ?>
                                        </ul>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <?php
                        endif;
                    endforeach;
                    ?>
                </div>
            </li>
            <?php
        endif;
    endforeach;
    ?>
    <li class="header-search">
        <form action="/product-category/search" method="get">
            <span><input name="header-search" class="header-search-box" id="header-search" type="text" autocomplete="off" placeholder="search all products..."/></span>
        </form>
    </li>
    <li class="nav-header-cart-wrapper">
        <div class="nav-header-cart" data-total="0">
            <p class="nav-header-cart-total"><?php echo number_of_items_in_cart() ?></p>
        </div>
        <div class="cart-overlay">
            <div class="cart-contents">
                <?php st_get_cart_contents() ?>
            </div>
            <div class="cart-actions-wrapper col-lg-12 no-padding">
                <div class="cart-total-container col-lg-10 col-lg-offset-1">
                    <p class="amount price">Total<span class="cart_total_price"><?php echo st_get_cart_total() ?></span><span> AUD</span></p>
                </div>
                <div class="cart-buttons-container col-lg-12 no-padding">
                    <div class="col-lg-6"><a href="<?php echo WC_Cart::get_cart_url(); ?>" class="cart-overlay-cart-button">Cart</a></div>
                    <div class="col-lg-6"><a href="<?php echo WC_Cart::get_checkout_url(); ?>" class="cart-overlay-checkout-button">Checkout</a></div>
                    <div class="col-lg-12"><button class="cart-overlay-close">No Thanks, Continue Shopping</button></div>
                </div>
            </div>
        </div>
    </li>
    <li class="header-login-wrapper <?php echo (is_user_logged_in()) ? 'user-logged-in-header' : 'user-not-logged-in-header' ?>">
        <?php if (!is_user_logged_in()): ?>
            <div class="header-login"><p>Login</p></div>
        <?php else: ?>
            <?php $user = wp_get_current_user(); ?>
            <div class="header-loggedIn"><a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>">Account</a><span><a href="<?php echo wp_logout_url(get_permalink()); ?>">&lsqb;Logout&rsqb;</a></span>
            </div>
            <?php if (isset($user->display_name)): ?>
                <p class="header-user-welcome">Welcome, <?php echo $user->display_name ?></p>
            <?php endif; ?>
        <?php endif; ?>
    </li>
</ul>
<?php
unset($user);
unset($main_menu);
unset($parent_menus);
