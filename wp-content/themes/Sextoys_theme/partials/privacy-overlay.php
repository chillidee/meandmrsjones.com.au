<div class="col-lg-12 info-page">
    <div class="content-box">
        <div class="privacy-overlay-close overlay-close"><i class="fa fa-times" aria-hidden="true"></i></div>
        <div class="text-container">
            <h1>Me &amp; Mrs. Jones Privacy Policy</h1>
            <p class="p1">Me &amp; Mrs. Jones’ regards customer privacy as an important part of our relationship with our customers. The following privacy policy applies to all Me &amp; Mrs. Jones users, and conforms to Internet privacy standards. Any issues related to this site will be discreetly handled.<b> </b>In this policy, “us”, “we” or “our” means <b>Me &amp; Mrs. Jones&nbsp;</b>and its related bodies corporate.</p>
            <p class="p2">We are bound by the National Privacy Principles contained in the Privacy Act 1988 (Cth) (subject to exemptions that apply to us under that Act).</p>
            <p class="p2">We may, from time to time, review and update this policy, including taking account of new or amended laws, new technology and/or changes to our operations. All personal information held by us will be governed by the most recently updated policy.</p>
            <p class="p2">This policy was last updated on 2016-05-26</p>
            <p class="p2"><strong>This policy sets out:</strong></p>
            <ul>
                <li>Purchasing</li>
                <li>Payment</li>
                <li>Packaging</li>
                <li>Delivery</li>
                <li>Your details will be always be kept confidential</li>
                <li>We will not share, pass on, or sell your contact information to anyone outside of our business. Your contact details remain confidential with us.</li>
            </ul>
            <p><b>Billing </b><strong>Statements</strong><br>
                Your credit card statement or PayPal account will only show “”MMJ” [Me &amp; Mrs. Jones] and will not reference&nbsp;products purchased.</p>
            <p><strong>Packaging and Delivery</strong><br>
                Your parcels will be discreetly packaged to ensure your privacy.</p>
            <p><strong>Your Account</strong><br>
                If you choose to create an account, we will require users to provide us with contact information such as names, email addresses, and billing/shipping addresses. Your contact information is used for order processing and for delivery of purchases. Your account will save your billing and shipping address for ease of future purchases. We do not save your payment details. If you do not wish to have your information saved you may opt to shop as a guest without creating an account.</p>
            <p><strong>Communication</strong><br>
                We use your information only for communications regarding your purchase, unless you have requested to receive news, special offers &amp; discounts from Me &amp; Mrs. Jones.</p>
            <p class="p2">For more information, don’t hesitate to <span class="s1"><strong>contact us</strong>.</span></p>
        </div>
    </div>
</div>