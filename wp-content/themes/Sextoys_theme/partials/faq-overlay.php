<div class="col-lg-6 col-lg-offset-1 info-page">
    <div class="content-box">
        <div class="faq-overlay-close overlay-close"><i class="fa fa-times" aria-hidden="true"></i></div>
        <div class="text-container">
            <p>Me &amp; Mrs. Jones FAQ</p>
            <p class="p1"><strong>What is your return/exchange policy?<br>
                </strong>For health and hygiene reasons, we cannot exchange any adult products or apparel, unless the product is faulty. To return a faulty item for refund or replacement contact [Customer Service Email] for a return label or if you have an account with you can print a return label by logging into your account and viewing your previous orders. Some products in certain brands include a warranty. We honour all warranties with proof of purchase.</p>
            <p>*adult products and apparel include all products sold by Me &amp; Mrs. Jones.</p>
            <p><strong>What if I receive a faulty product? What do I do?<br>
                </strong>We always do our best to ensure that the items we ship to you are in top condition. However, if you feel as though you have received an item that is faulty, we will be happy to have the product exchanged free of charge or refund the full value of the product if it is in fact found faulty upon return inspection by our staff. The product must be returned within 30 days of the purchase date. For return labels …………..</p>
            <p class="p1"><strong>What should I except for shipping time?<br>
                </strong>Sydney, Adelaide, Brisbane, Melbourne, Australian Capital Territory – 1 Day<br>
                North Queensland, Perth, Darwin, Alice Springs – 2-3 Days<br>
                Rural – 3-5 Days</p>
            <p class="p2"><span class="s2"><strong>What should I expect for shipping cost?<br>
                    </strong></span>We have a flat rate shipping cost of $10.50 throughout Australia.</p>
            <p class="p1"><strong>Can I ship an item as a gift and include a gift message?<br>
                </strong>Yes, at checkout you may indicate that the order is a gift by checking the appropriate box provided, and entering a brief personal note if desired. Enter the gift recipient’s address in the shipping details and the billing details as your own address so the lucky person will receive your gift without seeing how much was spent on the order. They may use their receipt to return an item if necessary under the guidelines of our return/exchange policy.</p>
            <p class="p3"><span class="s3"><strong>I’m concerned about my privacy. Is the shipping discreet?<br>
                    </strong></span>Your parcels will not include any references to adult products and will be discreetly packaged to ensure your privacy.</p>
            <p class="p1"><strong>What payment methods do you accept?<br>
                </strong>We accept Visa, MasterCard, and PayPal.</p>
            <p class="p3"><span class="s3"><strong>How will my purchases appear on my credit card statement or Paypal account?<br>
                    </strong></span>Your privacy is important to us. Your credit card statement will only show “”MMJ” [Me &amp; Mrs. Jones].</p>
            <p class="p4"><strong>How do I know that my payment went through?<br>
                </strong>If the order confirmation page appeared after your payment and you received an order confirmation email from us then your payment has gone through. If you’re still unsure, please contact your bank for further information.</p>
            <p class="p1"><strong>How will I know when my parcel will arrive?<br>
                </strong>You will receive an email from us when your parcel ships with an estimated time of delivery and as well as details to track and trace your parcel.</p>
            <p><strong>I haven’t received my parcel yet. What do I do?<br>
                </strong>If your parcel has not arrived in the specified delivery time please contact customer service at [Customer Service Email].<br>
                <strong><br>
                    What are the benefits of having an account with MMJ?<br>
                </strong>MMJ will remember your shipping and billing addresses to speed up your purchase process. Also you may view past orders and benefit from the ease of re-ordering your favourite products. If you need to return or exchange an item, you can print a return label from your previous orders list. We will also keep you in the know about all of our fantastic deals and promotions.</p>
            <p class="p1"><strong>Do I need to create an account in order to shop?<br>
                </strong>You do not need to create an account in order to shop. You may opt to shop as a guest and none of your information will be saved.</p>
            <p class="p1"><strong>How do I update my account details?<br>
                </strong>To update any of your personal details, please log into your account. You can then edit your personal information such as billing/shipping address, email address, and password.</p>
            <p class="p1"><strong>I can’t remember my password. What do I do?<br>
                </strong>If you can’t remember your password, please click the ‘Forgotten password?’ link at login. An email will be sent to you with instructions on how to reset your password.</p>
            <p class="p1"><strong>How do I know what size I am in apparel?<br>
                </strong>We want all of your purchases to fit and look great. For help with choosing the best fit, please refer to our Size Guide which you can find on our apparel product pages.</p>
            <p class="p1"><strong>Didn’t find the answer you were looking for? Contact Us <span class="contact-us-link">Here</span>.</strong></p>
        </div>
    </div>
</div>