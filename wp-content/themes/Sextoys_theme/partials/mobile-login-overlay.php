<div class="mobile-login-menu-close"><i class="fa fa-times" aria-hidden="true"></i></div>
<?php if (!is_user_logged_in()): ?>
    <div class="mobile-login-overlay-content-wrapper">
        <div class="mobile-menu-header-logo">
            <img src="<?php echo get_template_directory_uri() . '/images/header-logo.png'?>"/>
        </div>
        <div class="mobile-login-overlay-content col-xs-12">
            <p>Sign into your account:</p>
            <form name="mobile_loginForm" id="mobile_loginForm" method="post" ng-app novalidate>
                <div class="col-xs-12 no-padding-mobile">
                    <input ng-model="mobile_username" id="mobile_login-username" placeholder="EMAIL" name="username" type="text" required/>
                    <p class="user_name_error" ng-show="mobile_loginForm.username.$touched && mobile_loginForm.username.$invalid">Please enter an valid email address</p>
                </div>
                <div class="col-xs-12 no-padding-mobile">
                    <input ng-model="mobile_password" id="mobile_login-password" placeholder="PASSWORD" name="password" type="password" required/>
                    <p class="user_name_error" ng-show="mobile_loginForm.password.$touched && mobile_loginForm.password.$invalid">Please enter an valid password</p>
                </div>
                <div class="col-xs-12"><p class="signin-error"></p></div>
                <div class="col-xs-12 col-sm-12 no-padding-mobile">
                    <button type="button" id="mobile_login-button" ng-disabled="!mobile_loginForm.$valid">Sign In</button>
                </div>
                <div class="col-xs-12 col-sm-12 no-padding-mobile forgotten-password-container">
                    <a href="/my-account/lost-password/" id="login-forgotten-password">Forgotten Password?</a>
                </div>
            </form>
        </div>
        <div class="mobile-login-overlay-bottom-content col-xs-12 no-padding">
            <div class="col-xs-12">
                <p>Don't have an account?</p>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2">
                <button id="mobile-create-account-btn" type="button">Create an account</button>
            </div>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2">
                <button id="mobile-guest-account-btn" type="button">Continue as guest</button>
            </div>
        </div>
    </div>
<?php endif; ?>