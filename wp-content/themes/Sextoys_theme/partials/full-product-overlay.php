<?php
require_once get_template_directory() . '/functions/reviews_class.php';
$_product_id = (int) $_POST['product_id'];
$_product = wc_get_product($_product_id);
$post = $_product->post;
$_product_colour = get_post_meta($_product->id, 'st_colour', true);
$_product_size = get_post_meta($_product->id, 'st_size', true);
$associated_products = st_get_associated_products($_product_id);
$cat_id = st_get_cat_id_from_slug(st_get_cat_slug_from_id($_product_id));
?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 full-product-breadcrumb"><?php echo st_get_breadcrumb($_product_id) ?></div>
<input type="hidden" id="archive-product-right-col-id" value="<?php echo $post->ID ?>"/>
<div class="archive-right_col_product_container col-lg-12 col-md-12 col-xs-12 col-sm-12 no-padding-mobile-450">
    <div class="col-lg-12 archive-product-ajax-response"></div>
    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 no-padding-mobile-450">
<!--        <div class="product_overlay_close"><i class="fa fa-times" aria-hidden="true"></i></div>-->
        <div class="full-product-mobile-title col-lg-10 col-md-10 col-sm-offset-1 col-sm-10 col-md-offset-1 col-xs-12 col-sm-12"><h1><?php echo $post->post_title ?></h1></div>
        <div class="full-product-mobile-price col-lg-10 col-md-10 col-sm-offset-1 col-sm-10 col-md-offset-1 col-md-12 col-xs-12 col-sm-12">
            <?php
            $retail_price = get_post_meta($_product->id, '_retail_price', true);
            if ($retail_price):
                ?>
                <span class="retail_price_full"><?php echo $retail_price ?></span>
            <?php endif; ?>
            <h4><?php echo $_product->get_price_html(); ?></h4>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 center-block image-box archive-right-col-product-image zoomImg">
            <?php
            $image = st_get_the_post_full_image($_product_id);
            if (!strpos($image, 'src')):
                ?>
                <img style="display: none" class="attachment-full size-full wp-post-image main_image" src='<?php echo $image ?>' alt="<?php echo $post->post_title ?>"/>
                <?php
            else:
                echo $image;
            endif;
            ?>
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 product-gallery-wrapper">
                <?php
                $gallery_array = get_post_meta($_product_id, 'gallery_image_link', true);
                $attachment_ids = $_product->get_gallery_attachment_ids();
                $gallery_array = explode(' ', $gallery_array);
                foreach ($attachment_ids as $image_url):
                    array_push($gallery_array, wp_get_attachment_url($image_url));
                endforeach;
                foreach ($gallery_array as $image):
                    if ($image && strlen($image) > 2):
                        ?>
                        <div class="col-lg-3 col-xs-6 col-md-6 col-xs-6 col-sm-6 gallery-image-container">
                            <img class="gallery-image" src="<?php echo $image ?>" alt="Gallery Image"/>
                            <img src="https://s3.amazonaws.com/adult-toys/images/site_images/product-image-placeholder.jpg" class="image-placeholder" alt="Me &amp; Mrs Jones">
                        </div>
                        <?php
                    endif;
                endforeach;
                unset($gallery_array);
                ?>
                <?php
                include get_stylesheet_directory() . '/woocommerce/partials/full_product_mobile.php';
                ?>
                <div class="col-lg-12 col-xs-12 video-container">
                    <?php
                    echo get_post_meta($_product_id, 'youtube_link', true);
                    ?>
                </div>
                <?php
                if (st_get_number_of_products_in_category(st_get_related_cat_slug_from_id($_product_id), $_product_id)):
                    ?>
                    <div class="col-lg-12 col-xs-12 full-product-related">
                        <p>Customers also bought</p>
                        <?php
                        add_related_products_after_product(2, st_get_related_cat_slug_from_id($_product_id), $_product_id);
                        ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php
        include get_stylesheet_directory() . '/woocommerce/partials/full_product_desktop.php';
        ?>
    </div>
</div>