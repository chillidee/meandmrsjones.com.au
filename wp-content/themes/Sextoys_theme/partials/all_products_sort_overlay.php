<div class="overlay-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
    <i class="archive-sort-overlay-close fa fa-times" aria-hidden="true"></i>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <p class="title">Sort &#47; Filter</p>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 overlay-gender-wrapper">
        <div class="col-lg-4 col-md-4 col-xs-4">
            <input readonly name="gender" value="her"/>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-4">
            <input readonly name="gender" value="him"/>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-4">
            <input readonly name="gender" value="us"/>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 archive-sort-overlay-filters-wrapper">
        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 no-padding archive-sort-overlay-sort-wrapper">
            <p class="archive-sort-overlay-sort-by">Sort By</p>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 archieve-sort-by-select-wrapper no-padding">
            <select id="archieve-sort-by-select">
                <option value="0">Price: High to Low</option>
                <option value="1">Price: Low to High</option>
                <option value="2">Title: Z to A</option>
                <option value="3">Title: A to Z</option>
            </select>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 archive-sort-overlay-sort-wrapper">
            <p class="archive-sort-overlay-sort-default">Clear</p>
        </div>
    </div>
    <?php if ($slug != 'search'): ?>
        <?php if (get_term_meta($products[1], 'tags', true) != 'no'): ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 archive-sort-overlay-filters-wrapper">                    
                    <?php
                    getProductsTags($products[1]);
                    ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if (get_term_meta($products[1], 'brand', true) != 'no'): ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding archive-sort-overlay-filter-wrapper">
                    <p class="archive-overlay-expland">Brand<i class="fa fa-plus" aria-hidden="true"></i></p>
                    <ul>
                        <?php
                        st_sort_overlay_meta_options($products[1], '_brand', 'brand');
                        ?>
                    </ul>
                </div>
            </div>
            <?php
        endif;
        $sizes = st_get_size_array($slug, $parent_slug);
        if (in_array($products[1], st_get_apparel_cats())):
            $sizes = array('One Size', 'Small', 'Medium', 'Large', 'X Large', 'Plus');
        endif;
        if (get_term_meta($products[1], 'size', true) != 'no'):
            ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding archive-sort-overlay-filter-wrapper">
                    <p class="archive-overlay-expland">Size<i class="fa fa-plus" aria-hidden="true"></i></p>
                    <ul>
                        <li class="squaredFour margin-5">
                            <p>All</p>
                            <input name="size" id="size-all-sort-label" type="checkbox" checked value="all"/>
                            <label for="size-all-sort-label"></label>
                        </li>
                        <?php
                        if ($sizes):
                            foreach ($sizes as $index => $size):
                                ?>
                                <li class="squaredFour margin-5">
                                    <p><?php echo $size; ?></p>
                                    <input name="size" type="checkbox" id="<?php echo trim(strtolower($size)); ?>-sort-label" value="<?php echo $index; ?>"/>
                                    <label for="<?php echo trim(strtolower($size)); ?>-sort-label"></label>
                                </li>
                                <?php
                            endforeach;
                        else:
                            st_sort_overlay_meta_options($products[1], 'st_size', 'size');
                        endif;
                        unset($sizes);
                        ?>
                    </ul>
                </div>
            </div>
            <?php
        endif;
        if (get_term_meta($products[1], 'colour', true) != 'no'):
            ?>
            <div <?php echo ($cat_object->term_id === 25) ? ' style="display:none" ' : '' ?> class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding archive-sort-overlay-filter-wrapper">
                    <p class="archive-overlay-expland">Colour<i class="fa fa-plus" aria-hidden="true"></i></p>
                    <ul>
                        <li class="squaredFour margin-5">
                            <p>All</p>
                            <input name="colour" id="colour-all" checked type="checkbox" value="all"/>
                            <label for="colour-all"></label>
                        </li>
                        <?php if (st_get_colour_array($slug, $parent_slug)): ?>
                            <li class="squaredFour margin-5">
                                <p>Black</p>
                                <input name="colour" id="black" type="checkbox" value="black"/>
                                <label for="black"></label>
                            </li>
                            <li class="squaredFour margin-5">
                                <p>Blue</p>
                                <input name="colour" id="blue" type="checkbox" value="blue"/>
                                <label for="blue"></label>
                            </li>
                            <li class="squaredFour margin-5">
                                <p>Brown</p>
                                <input name="colour" id="brown" type="checkbox" value="brown"/>
                                <label for="brown"></label>
                            </li>
                            <li class="squaredFour margin-5">
                                <p>Green</p>
                                <input name="colour" id="green" type="checkbox" value="green"/>
                                <label for="green"></label>
                            </li>
                            <li class="squaredFour margin-5">
                                <p>Grey</p>
                                <input name="colour" id="grey" type="checkbox" value="grey"/>
                                <label for="grey"></label>
                            </li>
                            <li class="squaredFour margin-5">
                                <p>Nude</p>
                                <input name="colour" id="nude" type="checkbox" value="nude"/>
                                <label for="nude"></label>
                            </li>
                            <li class="squaredFour margin-5">
                                <p>Orange</p>
                                <input name="colour" id="orange" type="checkbox" value="orange"/>
                                <label for="orange"></label>
                            </li>
                            <li class="squaredFour margin-5">
                                <p>Pink</p>
                                <input name="colour" id="pink" type="checkbox" value="pink"/>
                                <label for="pink"></label>
                            </li>
                            <li class="squaredFour margin-5">
                                <p>Purple</p>
                                <input name="colour" id="purple" type="checkbox" value="purple"/>
                                <label for="purple"></label>
                            </li>
                            <li class="squaredFour margin-5">
                                <p>Red</p>
                                <input name="colour" id="red" type="checkbox" value="red"/>
                                <label for="red"></label>
                            </li>
                            <li class="squaredFour margin-5">
                                <p>White</p>
                                <input name="colour" id="white" type="checkbox" value="white"/>
                                <label for="white"></label>
                            </li>
                            <li class="squaredFour margin-5">
                                <p>Yellow</p>
                                <input name="colour" id="yellow" type="checkbox" value="yellow"/>
                                <label for="yellow"></label>
                            </li>
                            <li class="squaredFour margin-5">
                                <p>Clear</p>
                                <input name="colour" id="clear" type="checkbox" value="clear"/>
                                <label for="clear"></label>
                            </li>
                            <?php
                        endif;
                        ?>
                    </ul>
                </div>
            </div>
            <?php
            unset($colours);
        endif;
        if (get_term_meta($products[1], 'price', true) != 'no'):
            ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding archive-sort-overlay-filter-wrapper">
                    <p class="archive-overlay-expland">Price Range<i class="fa fa-plus" aria-hidden="true"></i></p>
                    <ul id="archive-overlay-price-inputs">
                        <li class="squaredFour margin-5">
                            <p>Below $50</p>
                            <input type="checkbox" id="below_20_price" name="price" value="49.99"/>
                            <label for="below_20_price"></label>
                        </li>
                        <li class="squaredFour margin-5">
                            <p>$50 - $100</p>
                            <input type="checkbox" id="below_40_price" name="price" value="99.99"/>
                            <label for="below_40_price"></label>
                        </li>
                        <li class="squaredFour margin-5">
                            <p>Over $100</p>
                            <input type="checkbox" id="over_60_price" name="price" value="100.00"/>
                            <label for="over_60_price"></label>
                        </li>
                    </ul>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>