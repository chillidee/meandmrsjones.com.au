<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="mobile-menu-header-logo">
        <a href="/">
            <img src="<?php echo "{$aws_site}{$logo_name}" ?>-logo.png"/>
        </a>
    </div>

    <div class="mobile-menu-header-overlay col-xs-6 col-xs-offset-3">
        <p>Menu</p>
    </div>
    <div class="col-xs-12 header-search">
        <form action="/product-category/search" method="get">
            <span><input name="header-search" class="header-search-box" id="header-search" type="text" autocomplete="off" placeholder="search all products..."/></span>
        </form>
    </div>
    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
        <?php
        if (isset($main_menu)):
            foreach ($main_menu as $menu_item):
                if (!$menu_item->menu_item_parent):
                    ?>
                    <div class="col-xs-4 home-middle-buttons-container">
                        <a id="" href="<?php echo get_site_url() . $menu_item->url ?>"><?php echo $menu_item->post_title ?></a>
                    </div>
                    <?php
                endif;
            endforeach;
        endif;
        ?>
    </div>
    <div class="mobile-login-border col-xs-6 col-xs-offset-3"></div>
    <?php if (!is_user_logged_in()): ?>
        <div class="mobile-login col-xs-12"><p id="mobile-sign-up">Signup</p><p> &#47; </p><a id="mobile-login">Login</a></div>
    <?php else: ?>
        <div class="mobile-login col-xs-12"><a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>">Account</a><p> &#47; </p><a href="<?php echo wp_logout_url($_SERVER["REQUEST_URI"]); ?>">Logout</p></div>
    <?php endif; ?>
    <div class="mobile-footer col-xs-12">
        <ul>
            <li><a href="/guide-spot/" data-name="mobile-footer-guide">Guide Spot</a></li>
            <li><a id="about-overlay-mobile-link">About</a></li>
            <li><a id="faq-overlay-mobile-link">FAQ</a></li>
            <li><a id="terms-overlay-mobile-link">Terms &AMP; Conditions</a></li>
            <li><a id="privacy-overlay-mobile-link">Privacy</a></li>
            <li><a id="contact-overlay-mobile-link">Contact</a></li>
            <li><a href="/returns-1/" data-name="mobile-footer-returns">Returns &amp; Exchanges</a></li>
        </ul>
    </div>
    <div class="mobile-social-links col-xs-12 col-sm-12 col-md-12">
        <ul>
            <li><a data-name="desktop-facebook-footer" href="https://www.facebook.com/MMJtoysAU/" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
            <li><a data-name="desktop-instagram-footer" href="https://www.instagram.com/mmjtoysau/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            <li><a data-name="desktop-twitter-footer" href="https://twitter.com/MMJtoysAU" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
            <li><a data-name="desktop-twitter-pinterest" href="https://au.pinterest.com/mmjtoysau/" target="_blank"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a></li>
            <li><a data-name="desktop-twitter-youtube" href="https://www.youtube.com/channel/UC0GD9UO5434pAhznYw8_INw" target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
        </ul>
    </div>
    <div class="mobile-menu-close"><i class="fa fa-times" aria-hidden="true"></i></div>
</div>