<ul class="<?php echo "{$class_selector}"; ?>-menu <?php echo ($page_class) ? $page_class : '' ?>">
    <li class="<?php echo "{$class_selector}"; ?>-menu-item home-menu-parent">
        <a href="<?php echo get_site_url(); ?>/toys">Toys</a>
        <div class="hover-menu-wrapper">
            <ul>
                <li><a href="<?php echo get_site_url(); ?>/toys/vibrators">Vibrators</a></li>
                <li><a href="<?php echo get_site_url(); ?>/toys/anal/">Anal</a></li>
                <li><a href="<?php echo get_site_url(); ?>/toys/cock-rings/">Cock Rings</a></li>
                <li><a href="<?php echo get_site_url(); ?>/toys/dongs-dildos-strap-ons/">Dongs, Dildos & Strap-Ons</a></li>
                <li><a href="<?php echo get_site_url(); ?>/toys/bullets-balls-eggs/">Bullets, Balls, Eggs</a></li>
                <li><a href="<?php echo get_site_url(); ?>/toys/masturbators/">Masturbators</a></li>
                <li><a href="<?php echo get_site_url(); ?>/toys/pumps/">Pumps</a></li>
                <li><a href="<?php echo get_site_url(); ?>/toys/sleeves-extensions/">Sleeves & Extensions</a></li>
                <li><a href="<?php echo get_site_url(); ?>/toys/love-dolls">Love Dolls</a></li>
                <li><a href="<?php echo get_site_url(); ?>/toys/swings-slings/">Swings & Slings</a></li>
                <li><a href="<?php echo get_site_url(); ?>/toys/sex-furniture/">Sex Furniture</a></li>
                <li><a href="<?php echo get_site_url(); ?>/toys/accessories/">Accessories</a></li>
            </ul>
        </div>
    </li>
    <li class="<?php echo "{$class_selector}"; ?>-menu-item home-menu-parent">
        <a href="<?php echo get_site_url(); ?>/fetish">Fetish</a>
        <div class="hover-menu-wrapper">
            <ul>
                <li><a href="<?php echo get_site_url(); ?>/fetish/blindfolds-hoods/">Blindfolds & Hoods</a></li>
                <li><a href="<?php echo get_site_url(); ?>/fetish/cuffs-restraints-gags/">Cuffs, Restraints & Gags</a></li>
                <li><a href="<?php echo get_site_url(); ?>/fetish/whips-paddles-ticklers/">Whips, Paddles & Ticklers</a></li>
                <li><a href="<?php echo get_site_url(); ?>/fetish/nipple-stimulators/">Nipple Stimulators</a></li>
                <li><a href="<?php echo get_site_url(); ?>/fetish/accessories/">Accessories</a></li>
                <li><a href="<?php echo get_site_url(); ?>/fetish/fifty-shades-of-grey-inspired/">Fifty Shades of Grey Inspired</a></li>
            </ul>
        </div>
    </li>
    <li class="<?php echo "{$class_selector}"; ?>-menu-item home-menu-parent">
        <a href="<?php echo get_site_url(); ?>/apparel">Apparel</a>
        <div class="hover-menu-wrapper">
            <ul>
                <li><a href="<?php echo get_site_url(); ?>/apparel/lingerie/">Lingerie</a></li>
                <li><a href="<?php echo get_site_url(); ?>/apparel/mens-range/">Mens Range</a></li>
                <li><a href="<?php echo get_site_url(); ?>/apparel/costumes/">Costumes</a></li>
                <li><a href="<?php echo get_site_url(); ?>/apparel/bridal/">Bridal</a></li>
                <li><a href="<?php echo get_site_url(); ?>/apparel/club-wear/">Club Wear</a></li>
                <li><a href="<?php echo get_site_url(); ?>/fetish/fetish-wear/">Fetish Wear</a></li>
                <li><a href="<?php echo get_site_url(); ?>/apparel/sexy-shoes/">Sexy Shoes</a></li>
                <li><a href="<?php echo get_site_url(); ?>/apparel/accessories/">Accessories</a></li>
            </ul>
        </div>
    </li>
    <li class="<?php echo "{$class_selector}"; ?>-menu-item home-menu-parent">
        <a href="<?php echo get_site_url(); ?>/essentials">Essentials</a>
        <div class="hover-menu-wrapper">
            <ul>
                <li><a href="<?php echo get_site_url(); ?>/essentials/lubes-gels/">Lubes & Gels</a></li>
                <li><a href="<?php echo get_site_url(); ?>/essentials/massage/">Massage</a></li>
                <li><a href="<?php echo get_site_url(); ?>/essentials/body-paints/">Body Paints</a></li>
                <li><a href="<?php echo get_site_url(); ?>/essentials/cleaning-grooming/">Cleaning & Grooming</a></li>
                <li><a href="<?php echo get_site_url(); ?>/essentials/sex-toy-cleaners/">Sex Toy Cleaners</a></li>
                <li><a href="<?php echo get_site_url(); ?>/essentials/sexual-aids/">Sexual Aids</a></li>
                <li><a href="<?php echo get_site_url(); ?>/essentials/condoms/">Condoms</a></li>
            </ul>
        </div>
    </li>
    <li class="<?php echo "{$class_selector}"; ?>-menu-item home-menu-parent">
        <a href="<?php echo get_site_url(); ?>/gifts-games">Gifts &amp; Games</a>
        <div class="hover-menu-wrapper">
            <ul>
                <li><a href="<?php echo get_site_url(); ?>/gifts-games/sex-games/">Sex Games</a></li>
                <li><a href="<?php echo get_site_url(); ?>/gifts-games/naughty-novelties/">Naughty Novelties</a></li>
                <li><a href="<?php echo get_site_url(); ?>/gifts-games/couples-kits/">Couples Kits</a></li>
                <li><a href="<?php echo get_site_url(); ?>/gifts-games/hens-night-products/">Hens Night Products</a></li>
                <li><a href="<?php echo get_site_url(); ?>/gifts-games/bucks-night-products/">Bucks Night Products</a></li>
                <!--                <li><a href="/gifts-games/novelty-love-dolls/">Novelty Love Dolls</a></li>-->
            </ul>
    </li>
    <li class="header-search">
        <form action="<?php echo get_site_url(); ?>/product-category/search" method="get">
            <span><input name="header-search" class="header-search-box" id="header-search" type="text" autocomplete="off" placeholder="search all products..."/></span>
        </form>
    </li>
    <li class="nav-header-cart-wrapper">
        <div class="nav-header-cart" data-total="0">
            <p class="nav-header-cart-total"><?php echo number_of_items_in_cart() ?></p>
        </div>
        <div class="cart-overlay">
            <div class="cart-contents">
                <?php st_get_cart_contents() ?>
            </div>
            <div class="cart-actions-wrapper col-lg-12 no-padding">
                <div class="cart-total-container col-lg-10 col-lg-offset-1">
                    <p class="amount price">Total<span class="cart_total_price"><?php echo st_get_cart_total() ?></span><span> AUD</span></p>
                </div>
                <div class="cart-buttons-container col-lg-12 no-padding">
                    <div class="col-lg-6"><a href="<?php echo WC_Cart::get_cart_url(); ?>" class="cart-overlay-cart-button">Cart</a></div>
                    <div class="col-lg-6"><a href="<?php echo WC_Cart::get_checkout_url(); ?>" class="cart-overlay-checkout-button">Checkout</a></div>
                    <div class="col-lg-12"><button class="cart-overlay-close"></button></div>
                </div>
            </div>
        </div>
    </li>
    <li class="header-login-wrapper <?php echo (is_user_logged_in()) ? 'user-logged-in-header' : 'user-not-logged-in-header' ?>">
        <?php if (!is_user_logged_in()): ?>
            <div class="header-login"><p>Login</p></div>
        <?php else: ?>
            <?php $user = wp_get_current_user(); ?>
            <div class="header-loggedIn"><a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>">Account</a><span><a href="<?php echo wp_logout_url(get_permalink()); ?>">&lsqb;Logout&rsqb;</a></span>
            </div>
            <?php if (isset($user->display_name)): ?>
                <p class="header-user-welcome">Welcome, <?php echo $user->display_name ?></p>
            <?php endif; ?>
        <?php endif; ?>
    </li>
</ul>
<?php
unset($user);
unset($main_menu);
unset($parent_menus);
