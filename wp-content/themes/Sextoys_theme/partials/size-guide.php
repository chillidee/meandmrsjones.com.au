<div class="col-lg-12">
    <div draggable="true" class="size-gude_overlay_close"><i class="fa fa-times" aria-hidden="true"></i></div>
    <h4>Size Guide</h4>
    <p>De Namour</p>
    <table>
        <thead>
            <tr>
                <td>
                    De Namour Lingerie Size
                </td>
                <td>
                    AUS Dress size
                </td>
                <td>
                    Bust
                </td>
                <td>
                    Waist
                </td>
                <td>
                    Hips
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    XS
                </td>
                <td>
                    6 - 8
                </td>
                <td>
                    30" - 32"
                </td>
                <td>
                    23" - 25"
                </td>
                <td>
                    33" - 36"
                </td>
            </tr>
            <tr>
                <td>
                    Small
                </td>
                <td>
                    10 - 12
                </td>
                <td>
                    34" - 36"
                </td>
                <td>
                    25" - 28"
                </td>
                <td>
                    36" - 38"
                </td>
            </tr>
            <tr>
                <td>
                    Medium
                </td>
                <td>
                    12 - 14
                </td>
                <td>
                    38" - 40"
                </td>
                <td>
                    28" - 32"
                </td>
                <td>
                    38" - 40"
                </td>
            </tr>
            <tr>
                <td>
                    Large
                </td>
                <td>
                    18 - 20
                </td>
                <td>
                    42" - 44"
                </td>
                <td>
                    32" - 35"
                </td>
                <td>
                    40" - 44"
                </td>
            </tr>
            <tr>
                <td>
                    XL
                </td>
                <td>
                    22 - 24
                </td>
                <td>
                    46" - 48"
                </td>
                <td>
                    35" - 40"
                </td>
                <td>
                    44" - 47"
                </td>
            </tr>
            <tr>
                <td>
                    XXL
                </td>
                <td>
                    26 - 28
                </td>
                <td>
                    50" - 52"
                </td>
                <td>
                    40" - 45"
                </td>
                <td>
                    47" - 52"
                </td>
            </tr>
            <tr>
                <td>
                    XXXL
                </td>
                <td>
                    30 - 32
                </td>
                <td>
                    52" - 54"
                </td>
                <td>
                    45" - 50"
                </td>
                <td>
                    52" - 56"
                </td>
            </tr>
        </tbody>
    </table>
    <p>Fantasy Lingerie</p>
    <table>
        <thead>
            <tr>
                <td>

                </td>
                <td>
                    1 Size
                </td>
                <td>
                    S
                </td>
                <td>
                    M
                </td>
                <td>
                    L
                </td>
                <td>
                    S/M
                </td>
                <td>
                    M/L
                </td>
                <td>
                    1X
                </td>
                <td>
                    2X
                </td>
                <td>
                    3X
                </td>
                <td>
                    1X/2X
                </td>
                <td>
                    3X/4X
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    Aus Size
                </td>
                <td>
                    6-14
                </td>
                <td>
                    6-10
                </td>
                <td>
                    10-12
                </td>
                <td>
                    14-16
                </td>
                <td>
                    6-10
                </td>
                <td>
                    12-16
                </td>
                <td>
                    18-20
                </td>
                <td>
                    20-22
                </td>
                <td>
                    22-24
                </td>
                <td>
                    18-22
                </td>
                <td>
                    22-26
                </td>
            </tr>
            <tr>
                <td>
                    US Size
                </td>
                <td>
                    2-10
                </td>
                <td>
                    2-6
                </td>
                <td>
                    6-8
                </td>
                <td>
                    10-12
                </td>
                <td>
                    2-6
                </td>
                <td>
                    8-12
                </td>
                <td>
                    14-16
                </td>
                <td>
                    16-18
                </td>
                <td>
                    18-20
                </td>
                <td>
                    14-18
                </td>
                <td>
                    18-22
                </td>
            </tr>
            <tr>
                <td>
                    BUST (in)
                </td>
                <td>
                    32-38
                </td>
                <td>
                    32-34
                </td>
                <td>
                    34-36
                </td>
                <td>
                    36-38
                </td>
                <td>
                    32-34
                </td>
                <td>
                    36-38
                </td>
                <td>
                    40-42
                </td>
                <td>
                    44-46
                </td>
                <td>
                    44-46
                </td>
                <td>
                    40-46
                </td>
                <td>
                    44-49
                </td>
            </tr>
            <tr>
                <td>
                    HIPS (in)
                </td>
                <td>
                    34-38
                </td>
                <td>
                    33-35
                </td>
                <td>
                    36-38
                </td>
                <td>
                    39-41
                </td>
                <td>
                    34-36
                </td>
                <td>
                    38-41
                </td>
                <td>
                    44-46
                </td>
                <td>
                    46-49
                </td>
                <td>
                    49-51
                </td>
                <td>
                    44-49
                </td>
                <td>
                    49-52
                </td>
            </tr>
        </tbody>
    </table>
</div>