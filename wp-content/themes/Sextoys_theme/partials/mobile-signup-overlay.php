<script>
    $(document).ready(function() {

        var validForm = false;

        jQuery("input[name=signUpOverlayFirstName").last().blur(function () {            
            var value = jQuery(this).val().trim();
            if (value == "") {
                jQuery(".signUpOverlayFirstName_error").show();
                validForm = false;
            } else {
                jQuery(".signUpOverlayFirstName_error").hide();
                validForm = true;
            }
        });
        jQuery("input[name=signUpOverlaySurname").last().blur(function () {            
            var value = jQuery(this).val().trim();
            if (value == "") {
                jQuery(".signUpOverlaySurname_error").show();
                validForm = false;
            } else {
                jQuery(".signUpOverlaySurname_error").hide();
                validForm = true;
            }
        });
        jQuery("select[name=day").last().last().blur(function () {            
            var value = jQuery(this).val().trim().replace("Day","");
            if (value == "") {
                jQuery(".signUpOverlayDOB_error").show();
                validForm = false;
            } else {
                jQuery(".signUpOverlayDOB_error").hide();
                validForm = true;
            }
        });
        jQuery("select[name=month").last().last().blur(function () {            
            var value = jQuery(this).val().trim().replace("Month","");
            if (value == "") {
                jQuery(".signUpOverlayDOB_error").show();
                validForm = false;
            } else {
                jQuery(".signUpOverlayDOB_error").hide();
                validForm = true;
            }
        });
        jQuery("select[name=year").last().last().blur(function () {            
            var value = jQuery(this).val().trim().replace("Year","");
            if (value == "") {
                jQuery(".signUpOverlayDOB_error").show();
                validForm = false;
            } else {
                jQuery(".signUpOverlayDOB_error").hide();
                validForm = true;
            }
        });
        jQuery("input[name=signUpOverlayEmail").last().blur(function () {            
            var value = jQuery(this).val().trim();
            if (value == "" || (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value) == false)) {
                jQuery(".signUpOverlayEmail_error").show();
                validForm = false;
            } else {
                jQuery(".signUpOverlayEmail_error").hide();
                validForm = true;
            }
        });
        jQuery("input[name=signUpOverlayEmailConfirm").last().blur(function () {            
            var value = jQuery(this).val().trim();
            if (value == "" || (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value) == false)) {
                jQuery(".signUpOverlayEmailConfirm_error").show();
                validForm = false;
            } else {
                jQuery(".signUpOverlayEmailConfirm_error").hide();
                validForm = true;

                if(value != jQuery("input[name=signUpOverlayEmail").last().val().trim()) {
                    jQuery(".signUpOverlayEmailConfirm_error_match").show();
                    validForm = false;
                } else {
                    jQuery(".signUpOverlayEmailConfirm_error_match").hide();
                    validForm = true;
                }
            }            
        });
        jQuery("input[name=signUpOverlayPassword").last().blur(function () {            
            var value = jQuery(this).val().trim();
            if (value == "" || !/[A-Z]/.test(value) || !/[a-z]/.test(value) || !/[0-9]/.test(value) || value.length < 8) {
                jQuery(".signUpOverlayPassword_error").show();
                validForm = false;
            } else {
                jQuery(".signUpOverlayPassword_error").hide();
                validForm = true;
            }            
        });
        jQuery("input[name=signUpOverlayPasswordConfirm").last().blur(function () {            
            var value = jQuery(this).val().trim();
            if (value == "" || !/[A-Z]/.test(value) || !/[a-z]/.test(value) || !/[0-9]/.test(value) || value.length < 8) {
                jQuery(".signUpOverlayPasswordConfirm_error").show();
                validForm = false;
            } else {
                jQuery(".signUpOverlayPasswordConfirm_error").hide();
                validForm = true;

                if(value != jQuery("input[name=signUpOverlayPassword").last().val().trim()) {
                    jQuery(".signUpOverlayPassword_error_match").show();
                    validForm = false;
                } else {
                    jQuery(".signUpOverlayPassword_error_match").hide();
                    validForm = true;
                }
            }
        });
        jQuery("button[name=signup-form-button-mobile").click(function() {
            
            jQuery("input[name=signUpOverlayFirstName").last().focus();
            jQuery("input[name=signUpOverlaySurname").last().focus();
            jQuery("select[name=day").last().focus();
            jQuery("select[name=month").last().focus();
            jQuery("select[name=year").last().focus();
            jQuery("input[name=signUpOverlayEmail").last().focus();
            jQuery("input[name=signUpOverlayEmailConfirm").last().focus();
            jQuery("input[name=signUpOverlayPassword").last().focus();
            jQuery("input[name=signUpOverlayPasswordConfirm").last().focus();            
            jQuery("button[name=signup-form-button-mobile").focus();

            if(validForm) {
                jQuery(".signup-form-button-mobile_error").hide();
                jQuery("input[name=signUpOverlayFirstName").first().val(jQuery("input[name=signUpOverlayFirstName").last().val());
                jQuery("input[name=signUpOverlaySurname").first().val(jQuery("input[name=signUpOverlaySurname").last().val());
                jQuery("select[name=day").first().val(jQuery("select[name=day").last().val());
                jQuery("select[name=month").first().val(parseInt(jQuery("select[name=month").last().val())+1);
                jQuery("select[name=year").first().val(jQuery("select[name=year").last().val());
                jQuery("input[name=signUpOverlayEmail").first().val(jQuery("input[name=signUpOverlayEmail").last().val());
                jQuery("input[name=signUpOverlayEmailConfirm").first().val(jQuery("input[name=signUpOverlayEmailConfirm").last().val());
                jQuery("input[name=signUpOverlayPassword").first().val(jQuery("input[name=signUpOverlayPassword").last().val());
                jQuery("input[name=signUpOverlayPasswordConfirm").first().val(jQuery("input[name=signUpOverlayPasswordConfirm").last().val());                
                jQuery('#signupsubscribe').first().prop('checked', jQuery('#signupsubscribeMobile').is(':checked'));
                jQuery("button[name=signup-form-button-desktop").click();
            } else {
                jQuery(".signup-form-button-mobile_error").show();
            }
        });        
    });
    
</script>
<div class="signup-overlay-close"><i class="fa fa-times" aria-hidden="true"></i></div>
<form method="post" id="signupForm" name="signupForm" ng-app="signupform" novalidate>
    <input type="hidden" id="requestURL" value="<?php echo $_SERVER["REQUEST_URI"] ?>"/>
    <div class="col-lg-12 col-xs-12 col-sm-12">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <label>Create an account</label>
        </div>
    </div>
    <br/><br/>
    <p class="user-create-error user_name_error"></p>
    <div class="col-lg-6 col-xs-12 col-sm-12">
        <div class="col-xs-12 col-sm-12 col-sm-12">
            <div class="col-xs-12 col-sm-12 col-sm-12 no-padding">
                <label for="titleSelect">Title</label>
                <select class="titleSelect" name="titleSelect">
                    <option value="Miss">Miss</option>
                    <option value="Ms">Ms</option>
                    <option value="Mr">Mr</option>
                    <option value="Sir">Sir</option>
                    <option value="Mrs">Mrs</option>
                    <option value="Dr">Dr</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-xs-12 col-sm-12 col-sm-6">
            <label for="signUpOverlayFirstName">First Name</label>
            <input ng-model="signUpOverlayFirstName" tabindex="1" id="signUpOverlayFirstName" type="text" name="signUpOverlayFirstName" required/>
            <p class="user_name_error signUpOverlayFirstName_error" style="display:none">Please inform first name</p>
        </div>
        <div class="col-lg-6 col-xs-12 col-sm-12 col-sm-6">
            <label for="signUpOverlaySurname">Surname</label>
            <input ng-model="signUpOverlaySurname" tabindex="2" id="signUpOverlaySurname" type="text" name="signUpOverlaySurname" required/>
            <p class="user_name_error signUpOverlaySurname_error" style="display:none">Please inform surname</p>
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12 dob-container">
            <div class="col-lg-12 col-xs-12 col-sm-12 no-padding">
                <label for="signUpOverlayDOB">Date of birth</label>
                <?php
                $monthArray = array('January', 'Febuary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
                echo '<select tabindex="7" id="signUpOverlayDOBDay" name="day">';
                echo '<option>Day</option>';
                for ($i = 1; $i <= 31; $i++) {
                    $dayIndex = $i;
                    $i = str_pad($i, 2, 0, STR_PAD_LEFT);
                    echo "<option value='$dayIndex'>$i</option>";
                }
                echo '</select>';
                echo '<select tabindex="8" id="signUpOverlayDOBMonth" name="month">';
                echo '<option>Month</option>';
                for ($i = 1; $i <= 12; $i++) {
                    $monthIndex = $i;
                    $i = str_pad($i, 2, 0, STR_PAD_LEFT);
                    echo "<option value='$monthIndex'>$monthArray[$monthIndex]</option>";
                }
                echo '</select>';
                echo '<select tabindex="9" id="signUpOverlayDOBYear" name="year">';
                echo '<option>Year</option>';
                for ($i = date('Y'); $i >= date('Y', strtotime('-100 years')); $i--) {
                    echo "<option value='$i'>$i</option>";
                }
                echo '</select>';
                ?>
                <p class="signup-form-dateofbirth-error user_name_error signUpOverlayDOB_error" style="display:none">Please inform date of birth</p>
            </div>
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <label for="signUpOverlayEmail">E-mail</label>
            <input ng-model="signUpOverlayEmail" tabindex="3" id="signUpOverlayEmail" type="email" name="signUpOverlayEmail" required/>                      
            <p class="user_name_error signUpOverlayEmail_error" style="display:none">Please inform a valid email</p>
        </div>
    </div>
    <div class="col-lg-12 col-xs-12 col-sm-12">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <label for="signUpOverlayEmailConfirm">Confirm e-mail</label>
            <input ng-model="signUpOverlayEmailConfirm" tabindex="4" id="signUpOverlayEmailConfirm" type="email" name="signUpOverlayEmailConfirm" required em-check="signUpOverlayEmail"/>
            <p class="user_name_error signUpOverlayEmailConfirm_error" style="display:none">Please inform a valid email</p>
            <p class="user_name_error signUpOverlayEmailConfirm_error_match" style="display:none">Email addresses don't match</p>
        </div>
    </div>
    <div class="col-lg-6 col-xs-12 col-sm-12 right-section">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <label for="signUpOverlayPassword">Password</label>
            <input ng-model="signUpOverlayPassword" tabindex="5" id="signUpOverlayPassword" type="password" name="signUpOverlayPassword" required/>            
            <p class="user_name_error signUpOverlayPassword_error" style="display:none">Please inform valid password</p>
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <label for="signUpOverlayPasswordConfirm">Confirm Password</label>
            <input ng-model="signUpOverlayPasswordConfirm" tabindex="6" id="signUpOverlayPasswordConfirm" type="password" name="signUpOverlayPasswordConfirm" required/>
            <p class="user_name_error signUpOverlayPasswordConfirm_error" style="display:none">Please inform valid password</p>
            <p class="user_name_error signUpOverlayPassword_error_match" style="display:none">Passwords don't match</p>            
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <p class="create-account-password-text">
                Enter a combination of at least eight characters with 1 uppercase, 1 lowercase and 1 number.
            </p>
        </div>
    </div>    
    <div class="col-lg-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-xs-12 col-sm-12 signUpOverlaySubscribe">
            <div class="squaredFour">
                <input id="signupsubscribeMobile" type="checkbox" checked />
                <label for="signupsubscribeMobile"></label>
            </div>
            <p>
                I would like to receive news, special offers & discounts from Me & Mrs. Jones
            </p>
        </div>
        <div class="col-lg-6 col-xs-12 col-sm-12 col-xs-12 col-sm-12">
            <p class="user_name_error signup-form-button-mobile_error" style="display:none">Please inform required fields</p>    
            <button type="button" id="signup-form-button" name="signup-form-button-mobile">Create Account</button>               
        </div>
    </div>
</form>