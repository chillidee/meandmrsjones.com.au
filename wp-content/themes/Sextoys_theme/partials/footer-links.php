<?php
$is_home = ($_SERVER["REQUEST_URI"] == '/') ? true : false;
$footer_css = ($is_home) ? "footer-links-wrapper-home" : "footer-links-wrapper";
?>
<div class="<?php echo $footer_css ?>">
    <?php
    if (!$cat_object):
        $cat_object = get_queried_object();
    endif;
    ?>
    <ul>
        <div class="col-lg-3 col-md-12 text-center">                    
            <li><a href="/blog/" data-name="desktop-footer-guide">Blog</a></li>
            <li><a href="/faq/">FAQ</a></li>
            <li><a href="/guide-spot/" data-name="desktop-footer-guide">Guide Spot</a></li>
        </div>
        <div class="col-lg-3 col-md-12 text-center">
            <li><a href="/terms-conditions/">Terms &AMP; Conditions</a></li>
            <li><a href="/returns-1/" data-name="desktop-footer-returns">Returns &amp; Exchanges</a></li>
        </div>
        <div class="col-lg-3 col-md-12 text-center">
            <li><a href="/privacy/">Privacy</a></li>
            <li><a data-name="desktop-contact-footer" id="contact-us-button">Contact</a></li>
            <li><a href="/about/">About</a></li>   
            <li><a href="/sitemap/" data-name="desktop-footer-sitemap">Sitemap</a></li>
        </div>
        <div class="col-lg-3 col-md-12 text-center">
            <li><a data-name="desktop-facebook-footer" href="https://www.facebook.com/MMJtoysAU/" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
            <li><a data-name="desktop-instagram-footer" href="https://www.instagram.com/mmjtoysau/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            <li><a data-name="desktop-twitter-footer" href="https://twitter.com/MMJtoysAU" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
            <li><a data-name="desktop-twitter-pinterest" href="https://au.pinterest.com/mmjtoysau/" target="_blank"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a></li>
            <li><a data-name="desktop-twitter-youtube" href="https://www.youtube.com/channel/UC0GD9UO5434pAhznYw8_INw" target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
        </div>    
    </ul>
</div>