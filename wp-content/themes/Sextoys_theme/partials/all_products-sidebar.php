<?php
$url = explode('/', $_SERVER["REQUEST_URI"]);
?>
<div id="archive-sidebar" class="col-lg-2 archive-sidebar <?php
echo ($parent_cat->slug) ? $parent_cat->slug . '-sidebar' : '';
echo ($children && !st_is_third_tier_cat($cat_object) && !isset($_POST["cat-number"])) ? ' title-sidebar' : '';
?>">
         <?php if (isset($parent_cat->name)): ?>
        <h2><?php echo $parent_cat->name ?></h2>
        <?php
    endif;
    if ($parent_children):
        if (st_get_category_meta($parent_children[0]->term_id, 'cat_order')):
            $parent_cats_sorted = array();
            foreach ($parent_children as $child):
                $cat_order = (int) st_get_category_meta($child->term_id, 'cat_order');
                if ($cat_order):
                    $parent_cats_sorted[$cat_order] = $child;
                endif;
            endforeach;
            sort($parent_cats_sorted);
            $parent_children = $parent_cats_sorted;
            unset($parent_cats_sorted);
        endif;
        ?>
        <ul class="parent-sidebar-sub-menu" id="sidebar-menu-wrapper">
            <?php
            foreach ($parent_children as $child):
                $child_children = st_cat_get_children($child->term_id);
                if ($child_children && st_get_category_meta($child_children[0]->term_id, 'cat_order')):
                    $child_childrenSorted = array();
                    foreach ($child_children as $child_sub):
                        $order = (int) st_get_category_meta($child_sub->term_id, 'cat_order');
                        if ($order):
                            $child_childrenSorted[$order] = $child_sub;
                        endif;
                    endforeach;
                    sort($child_childrenSorted);
                    $child_children = $child_childrenSorted;
                    unset($child_childrenSorted);
                endif;
                ?>
                <li class="<?php
                echo ($child_children) ? 'parent-sidebar-sub-menu-has-child' : 'parent-sidebar-sub-menu-no-child';
                echo ($child_children && ($cat_object->parent == $child->term_id || $parent_cat_object->parent == $child->term_id || strtolower($url[count($url) - 2]) == strtolower($child->slug))) ? ' sub-menu-open' : '';
                ?> parent-sidebar"><a class="archive-cat-title" href="<?php echo st_get_category_guid($child->term_id); ?>">
                    <?php echo $child->name; ?>
                    </a>
                    <?php if ($child_children): ?>
                        <ul class="sidebar-sub-menu">
                            <?php
                            foreach ($child_children as $child_sub):
                                ?>
                                <li>
                                    <a <?php echo ($child_sub->name == $catSlug) ? 'class="active-category"' : ''; ?> href="<?php echo st_get_category_guid($child_sub->term_id) ?>"><?php echo $child_sub->name; ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif;
                    ?>
                </li>
                <?php
            endforeach;
            ?>
        </ul>
    <?php endif; ?>
</div>