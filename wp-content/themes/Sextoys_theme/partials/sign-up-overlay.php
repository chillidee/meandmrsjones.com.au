<div class="signup-overlay-close"><i class="fa fa-times" aria-hidden="true"></i></div>
<form method="POST" id="signupForm" name="signupForm" ng-app="signupform" novalidate>
    <input type="hidden" id="requestURL" value="<?php echo $_SERVER["REQUEST_URI"] ?>"/>
    <div class="col-lg-12 col-xs-12 col-sm-12">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <h2>Create an account</h2>
        </div>
    </div>
    <p class="user-create-error user_name_error"></p>
    <div class="col-lg-6 col-xs-12 col-sm-12">
        <div class="col-lg-12">
            <div class="col-lg-3 no-padding">
                <label for="titleSelect">Title</label>
                <select class="titleSelect" name="titleSelect">
                    <option value="Miss">Miss</option>
                    <option value="Ms">Ms</option>
                    <option value="Mr">Mr</option>
                    <option value="Sir">Sir</option>
                    <option value="Mrs">Mrs</option>
                    <option value="Dr">Dr</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-xs-12 col-sm-12 col-sm-6">
            <label for="signUpOverlayFirstName">First Name</label>
            <input ng-model="signUpOverlayFirstName" tabindex="1" id="signUpOverlayFirstName" type="text" name="signUpOverlayFirstName" required/>
            <p class="user_name_error" ng-show="signupForm.signUpOverlayFirstName.$touched && signupForm.signUpOverlayFirstName.$invalid">Please inform first name</p>
        </div>
        <div class="col-lg-6 col-xs-12 col-sm-12 col-sm-6">
            <label for="signUpOverlaySurname">Surname</label>
            <input ng-model="signUpOverlaySurname" tabindex="2" id="signUpOverlaySurname" type="text" name="signUpOverlaySurname" required/>
            <p class="user_name_error" ng-show="signupForm.signUpOverlaySurname.$touched && signupForm.signUpOverlaySurname.$invalid">Please inform surname</p>
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <label for="signUpOverlayEmail">E-mail</label>
            <input ng-model="signUpOverlayEmail" tabindex="6" id="signUpOverlayEmail" type="email" name="signUpOverlayEmail" required/>
            <p class="user_name_error" ng-show="signupForm.signUpOverlayEmail.$touched && signupForm.signUpOverlayEmail.$invalid">Please inform a valid email</p>
            <p class="user_name_error email-match-error">Email addresses don't match</p>
            <p class="user_name_error email-in-use">Email address in use</p>
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <label for="signUpOverlayEmailConfirm">Confirm e-mail</label>
            <input ng-model="signUpOverlayEmailConfirm" tabindex="7" id="signUpOverlayEmailConfirm" type="email" name="signUpOverlayEmailConfirm" required em-check="signUpOverlayEmail"/>
            <p class="user_name_error" ng-show="signupForm.signUpOverlayEmailConfirm.$touched && signupForm.signUpOverlayEmailConfirm.$invalid">Please inform a valid email</p>
        </div>        
    </div>
    <div class="col-lg-6 col-xs-12 col-sm-12 right-section">
        <div class="col-lg-12 col-xs-12 col-sm-12 dob-container">
            <label for="signUpOverlayDOB">Date of birth</label>
            <?php
            $monthArray = array('January', 'Febuary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
            echo '<select tabindex="3" id="signUpOverlayDOBDay" name="day">';
            echo '<option>Day</option>';
            for ($i = 1; $i <= 31; $i++) {
                $dayIndex = $i;
                $i = str_pad($i, 2, 0, STR_PAD_LEFT);
                echo "<option value='$dayIndex'>$i</option>";
            }
            echo '</select>';
            echo '<select tabindex="4" id="signUpOverlayDOBMonth" name="month">';
            echo '<option>Month</option>';
            for ($i = 1; $i <= 12; $i++) {
                $monthIndex = $i;
                $monthArrayIndex = $i - 1;
                $i = str_pad($i, 2, 0, STR_PAD_LEFT);
                echo "<option value='$monthIndex'>$monthArray[$monthArrayIndex]</option>";
            }
            echo '</select>';
            echo '<select tabindex="5" id="signUpOverlayDOBYear" name="year">';
            echo '<option>Year</option>';
            for ($i = date('Y'); $i >= date('Y', strtotime('-100 years')); $i--) {
                echo "<option value='$i'>$i</option>";
            }
            echo '</select>';
            ?>
            <p class="signup-form-dateofbirth-error user_name_error">Please inform date of birth</p>
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <label for="signUpOverlayPassword">Password</label>
            <input ng-model="signUpOverlayPassword" tabindex="8" id="signUpOverlayPassword" type="password" name="signUpOverlayPassword" required/>
            <p class="user_name_error" ng-show="signupForm.signUpOverlayPassword.$touched && signupForm.signUpOverlayPassword.$invalid">Please inform valid password</p>
            <p class="user_name_error password-match-error">Passwords don't match</p>
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <label for="signUpOverlayPasswordConfirm">Confirm Password</label>
            <input ng-model="signUpOverlayPasswordConfirm" tabindex="9" id="signUpOverlayPasswordConfirm" type="password" name="signUpOverlayPasswordConfirm" required/>
            <p class="user_name_error" ng-show="signupForm.signUpOverlayPasswordConfirm.$touched && signupForm.signUpOverlayPasswordConfirm.$invalid">Please inform valid password</p>
        </div>
        <div class="col-lg-12 col-xs-12 col-sm-12 no-padding">
            <p class="create-account-password-text">
                Enter a combination of at least eight characters with 1 uppercase, 1 lowercase and 1 number.
            </p>
        </div>
    </div>    
    <div class="col-lg-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-xs-12 col-sm-12 signUpOverlaySubscribe">
            <div class="squaredFour">
                <input id="signupsubscribe" type="checkbox" checked />
                <label for="signupsubscribe"></label>
            </div>
            <p>
                I would like to receive news, special offers & discounts from Me & Mrs. Jones
            </p>
        </div>
        <div class="col-lg-6 col-xs-12 col-sm-12 col-xs-12 col-sm-12">
            <button type="button" id="signup-form-button" name="signup-form-button-desktop" ng-disabled="!signupForm.$valid">Create Account</button>
        </div>
    </div>
</form>