<?php
$user = wp_get_current_user();
if ($user):
    $user = $user->data;
    $user_email = $user->user_email;
    $display_name = $user->display_name;
    $display_name = explode(" ", $display_name);
    $first_name = $display_name[0];
    $last_name = ($display_name[1]) ? $display_name[1] : ' ';
    unset($display_name);
    unset($user);
endif;
?>
<div class="contact-overlay-close"><i class="fa fa-times" aria-hidden="true"></i></div>
<form id="contactForm" name="contactForm" ng-app novalidate>
    <div class="col-lg-12">
        <div class="col-lg-12 contact-overlay-header">
            <p>
                Contact Us
            </p>
        </div>
        <div class="col-lg-12">
            <p>Please fill out the below form and one of our customer service agents will be in touch, If you are enquiring about an existing order, please make sure you include the order number in the details</p>
        </div>
        <div class="col-lg-6">
            <div class="col-lg-6">
                <label class="required" for="contactFormFirstName">First Name</label>
                <input ng-model="contactFormFirstName" id="contactFormFirstName" ng-init="contactFormFirstName = <?php echo ($first_name) ? "'$first_name'" : ' ' ?>" name="contactFormFirstName" required type="text">
            </div>
            <div class="col-lg-6">
                <label class="required" for="contactFormSurname">Surname</label>
                <input ng-model="contactFormSurname" id="contactFormSurname" ng-init="contactFormSurname = <?php echo ($last_name) ? "'$last_name'" : ' ' ?>" name="contactFormSurname" required type="text">
            </div>
            <div class="col-lg-12">
                <label class="required" for="contactEmail">Contact Email</label>
                <input ng-model="contactEmail" id="contactEmail" name="contactEmail" ng-init="contactEmail = <?php echo ($user_email) ? "'$user_email'" : ' ' ?>" required type="email">
            </div>
            <div class="col-lg-12">
                <label class="required" for="contactNumber">Contact Number</label>
                <input ng-model="contactNumber" id="contactNumber" name="contactNumber" required type="tel">
            </div>
            <div class="col-lg-12">
                <label for="contactProblemSelect">About</label>
                <select id="contactProblemSelect">
                    <option value="0">Please Select...</option>
                    <option value="1">An existing order</option>
                    <option value="2">Product enquiry</option>
                    <option value="3">General enquiry</option>
                    <option value="4">Other</option>
                </select>
                <p class="user_name_error contact-problem-error">Please select an option</p>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="col-lg-12">
                <label class="required" for="contactDetail">Details</label>
                <textarea ng-model="contactDetail" name="contactDetail" required id="contactDetail"></textarea>
            </div>
            <div class="col-lg-6 col-lg-offset-6">
                <button type="button" id="contactButton" ng-disabled="!contactForm.$valid">Send</button>
            </div>
        </div>
        <div class="col-lg-12 address-container">
            <div class="col-lg-12">
                <p><a href="mailto:info@meandmrsjones.com.au">info@meandmrsjones.com.au</a></p>
            </div>
        </div>
</form>
<?php
unset($user_email);
unset($first_name);
unset($last_name);
?>