<div class="col-lg-6 col-lg-offset-1 info-page">
    <div class="content-box">
        <div class="tc-overlay-close overlay-close"><i class="fa fa-times" aria-hidden="true"></i></div>
        <div class="text-container">
            <h1>Terms &amp; Conditions</h1>
            <p>These terms of use (Terms) govern your use of the Me &amp; Mrs. Jones’ Website and all related sites including but not limited to www.meandmrsjones.com.au (the Website). Please read these Terms carefully.</p>
            <ol>
                <li><strong> Ownership</strong></li>
            </ol>
            <p>The Website and all content is operated and owned by&nbsp;[insert here],&nbsp;[Address]</p>
            <ol start="2">
                <li><strong> Pricing</strong></li>
            </ol>
            <p>All prices on the Website are displayed in Australian dollars and you will be charged in Australian dollars. Pricing is subject to change without prior notice.</p>
            <ol start="3">
                <li><strong> Orders</strong></li>
            </ol>
            <p>All orders submitted through the Website are subject to acceptance by Me &amp; Mrs. Jones.</p>
            <p>After you submit your order, you will receive an email to confirm receipt of your order. This email confirmation does not constitute an acceptance of the order or an offer to sell products. Me &amp; Mrs. Jones reserves the right to refuse an order for any reason. We reserve the right to correct inadvertent errors, omissions or inaccuracies in relation to price, product availability, or product descriptions. Acceptance of all orders is subject to Me &amp; Mrs. Jones confirming availability and price for products ordered. If your order is rejected for any reason you will be given a full refund for any amount paid for the order.</p>
            <p>Me &amp; Mrs. Jones only accepts your offer, and concludes the contract of sale for the product(s) ordered by you, when we dispatch the product(s) and send an email confirming that the product(s) have been dispatched to you.<br>
                We may cancel your order (included orders accepted by Me &amp; Mrs. Jones) without any liability to you if any of the products in your order are unavailable, or there were any errors in price or product description on the Website at the time of placing your order, or due to technical errors in processing your order. If we cancel your order for any reason you will be given a full refund for any amount paid for the order.</p>
            <p>If you purchase a product or service from us, we may request certain personally identifiable information from you. You may be required to provide contact information (such as name, Email, and&nbsp;postal address) and financial information (such as credit card number, expiration date).</p>
            <ol start="4">
                <li><strong> Communications</strong></li>
            </ol>
            <p>We use personally identifiable information for billing purposes and to fill your orders. If we have trouble processing an order, we will use this information to contact you. We may also us your email to communicate with you about sales and promotions if you have requested such communication.</p>
            <ol start="5">
                <li><strong> Legal</strong></li>
            </ol>
            <p>We reserve the right to disclose your personally identifiable information as required by law and when we believe that disclosure is necessary to protect our right and/or comply with a judicial proceeding, court order, or legal process served on our Website.</p>
            <ol start="6">
                <li><strong> Refund and Return Policy</strong></li>
            </ol>
            <p>For health and hygiene reasons, we do not return or exchange our adult products or apparel unless the item is faulty. We will return or exchange any faulty item within 30 days of the shipping date. If&nbsp;a product is sold with warranty, we will honour it for the time specified in the warranty. Any purchase within warranty may be returned with proof of purchase and the terms of the warranty will be upheld.</p>
        </div>
    </div>
</div>