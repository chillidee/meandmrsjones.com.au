<?php
/**
 * The template for displaying all pages
 * Using an if statement to check for the thankyou page, and if not present, outputting the packages page
 */
get_header(); 
   if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); ?>
		<!--<h1><?php the_title();?></h1>-->
                <?php the_content();
	} // end while
    } // end if
get_footer(); ?>
