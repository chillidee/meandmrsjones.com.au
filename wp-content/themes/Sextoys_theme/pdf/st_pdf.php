<?php

class st_pdf {

    function st_pdf_html($order_id) {
        $order = wc_get_order($order_id);
        $order_items = $order->get_items();
        $total_items = 0;
        $order_shipping = (!empty($order->get_total_shipping()) ? $order->get_total_shipping() : '');
        $discount = (!empty($order->get_total_discount()) ? '$' . money_format('%.2n', $order->get_total_discount()) : '');
        $coupons = (!empty($order->get_used_coupons()) ? $order->get_used_coupons() : '');
        $tax = (!empty($order->get_cart_tax()) ? '$' . $order->get_cart_tax() : '');
        $subtotal = $order->get_subtotal();
        $order_subtotal = (!empty($order->get_formatted_order_total())) ? $order->get_formatted_order_total() : "";

        $html = '<style>.total-items td{border-top:1px solid #B19663;}table tr td{width:33.3%}table tr td{height:40px;}.center-align{text-align:center;}.right-align{text-align:right;}table{border-bottom:1px solid #B19663;border-top:1px solid #B19663;}table tr{width:100%;}h1,h2,p{font-family:roboto}h1,h2{color:#851D19;margin:0;}h2{margin:10px 0;}thead tr td {font-weight:bold;}thead tr td{border-bottom:1px solid #B19663}p{color:grey;}.total-paid td{font-weight:bold;font-size:18px;border-bottom:1px solid #B19663;border-top:1px solid #B19663;padding 10px 0;}table{cellspacing="0";border-collapse: collapse;}</style>';
        $html .= "<div style='width:50%;display:block;float:left'><img style='height:50px;display:block;margin:0 auto;width:auto;' height:50px; src='https://s3.amazonaws.com/adult-toys/images/site_images/main-header-logo.png'/></div>";
        $html .= '<div style="width:50%;float:left;display:block"><p style="text-align:right"><span>Address:</span><br>Level 2 608-612,<br>Liverpool Road,<br>South Strathfield,<br>NSW 2136<br></p><p style="text-align:right"><span>ABN:</span><br>59  612 024 610</p><p style="text-align:right"><span>info@meandmrsjones.com.au</span></p></div>';
        $html .= '<div style="width:100%;"><h1 style="text-align:center">Order Number</h1></div><div style="width:100%;"><h2 style="text-align:center">' . $order->get_order_number() . '</h2><p style="text-align:center">' . date('d-m-Y', strtotime($order->order_date)) . '<p></div>';
        $html .= '<div style="width:100%;"><table style="width:100%;"><thead><tr><td>Product</td><td class="center-align">QTY</td><td class="right-align">Total</td></tr></thead><tbody>';
        foreach ($order_items as $item):
            $total_items = $total_items + (int) $item["qty"];
            $html .= '<tr><td>' . $item["name"] . '</td><td class="center-align">' . $item["qty"] . '</td><td class="right-align">$' . $item["line_subtotal"] . '</td></tr>';
        endforeach;
        $html .= '<tbody><tfoot><tr class="total-items"><td>Items Total</td><td class="center-align">' . $total_items . '</td><td class="right-align">$' . $subtotal . '</td></tr></tfoot></table></div>';
        $html .= '<div style="width:100%;"><table style="width:100%;"><tr><td>SHIPPING</td><td class="center-align">Flat rate shipping</td><td class="right-align">$' . $order_shipping . '</td></tr>';
        $html .= '<tr><td>DISC</td><td class="center-align">' . $coupons . '</td><td class="right-align">' . $discount . '</td></tr>';
        $html .= '<tr><td>TAXES</td><td class="center-align">Includes GST*</td><td class="right-align">' . $tax . '</td></tr>';
        $html .= '<tr class="total-paid"><td>Total Paid</td><td class="center-align"></td><td class="right-align">' . $order_subtotal . '</td></tr>';
        $html .= '</table></div>';
        $html .= '<div style="width:100%;"><table style="width:100%;"><tr><td>Payment Type</td><td class="right-align">' . date('d-m-Y', strtotime($order->order_date)) . '</td></tr><tr><td>' . $order->payment_method_title . '</td><td class="right-align"></td></tr></table></div>';
        $html .= '<div style="width:100%;"><h2>Delivery Details</h2></div><div style="width:100%;">' . $order->get_formatted_shipping_address() . '</div>';

        if ($order->customer_message):
            $html .= '<div style="width:100%;"><h2>Delivery Notes</h2></div><div style="width:100%;">' . $order->customer_message . '</div>';
        endif;
        return $html;
    }

    function st_pdf_html_without_logo($order_id) {
        $order = wc_get_order($order_id);
        $order_items = $order->get_items();
        $total_items = 0;
        $order_shipping = (!empty($order->get_total_shipping()) ? $order->get_total_shipping() : '');
        $discount = (!empty($order->get_total_discount()) ? '$' . money_format('%.2n', $order->get_total_discount()) : '');
        $coupons = (!empty($order->get_used_coupons()) ? $order->get_used_coupons() : '');
        $tax = (!empty($order->get_cart_tax()) ? '$' . $order->get_cart_tax() : '');
        $subtotal = $order->get_subtotal();
        $order_subtotal = (!empty($order->get_formatted_order_total())) ? $order->get_formatted_order_total() : "";

        $html = '<style>.total-items td{border-top:1px solid #B19663;}table tr td{width:33.3%}.center-align{text-align:center;}.right-align{text-align:right;}table{border-bottom:1px solid #B19663;border-top:1px solid #B19663;}table tr{width:100%;}h1,h2,p{font-family:roboto}h1,h2{color:#851D19;margin:0;}h2{margin:10px 0;}thead tr td {font-weight:bold;}thead tr td{border-bottom:1px solid #B19663}p{color:grey;}.total-paid td{font-weight:bold;font-size:18px;border-bottom:1px solid #B19663;border-top:1px solid #B19663;padding 10px 0;}table{cellspacing="0";border-collapse: collapse;}</style>';
        $html .= '<div style="width:100%;float:left;display:block"><p style="text-align:right"><span>Address:</span><br>Level 2 608-612,<br>Liverpool Road,<br>South Strathfield,<br>NSW 2136<br></p><p style="text-align:right"><span>ABN:</span><br>59  612 024 610</p><p style="text-align:right"><span>info@meandmrsjones.com.au</span></p></div>';
        $html .= '<div style="width:100%;"><h1 style="text-align:center">Order Number</h1></div><div style="width:100%;"><h2 style="text-align:center">' . $order->get_order_number() . '</h2><p style="text-align:center">' . date('d-m-Y', strtotime($order->order_date)) . '<p></div>';
        $html .= '<div style="width:100%;"><table style="width:100%;"><thead><tr><td>Product</td><td class="center-align">QTY</td><td class="right-align">Total</td></tr></thead><tbody>';
        foreach ($order_items as $item):
            $total_items = $total_items + (int) $item["qty"];
            $html .= '<tr><td>' . $item["name"] . '</td><td class="center-align">' . $item["qty"] . '</td><td class="right-align">$' . $item["line_subtotal"] . '</td></tr>';
        endforeach;
        $html .= '<tbody><tfoot><tr class="total-items"><td>Items Total</td><td class="center-align">' . $total_items . '</td><td class="right-align">$' . $subtotal . '</td></tr></tfoot></table></div>';
        $html .= '<div style="width:100%;"><table style="width:100%;"><tr><td>SHIPPING</td><td class="center-align">Flat rate shipping</td><td class="right-align">$' . $order_shipping . '</td></tr>';
        $html .= '<tr><td>DISC</td><td class="center-align">' . $coupons . '</td><td class="right-align">' . $discount . '</td></tr>';
        $html .= '<tr><td>TAXES</td><td class="center-align">Includes GST*</td><td class="right-align">' . $tax . '</td></tr>';
        $html .= '<tr class="total-paid"><td>Total Paid</td><td class="center-align"></td><td class="right-align">' . $order_subtotal . '</td></tr>';
        $html .= '</table></div>';
        $html .= '<div style="width:100%;"><table style="width:100%;"><tr><td>Payment Type</td><td class="right-align">' . date('d-m-Y', strtotime($order->order_date)) . '</td></tr><tr><td>' . $order->payment_method_title . '</td><td class="right-align">' . $order->get_transaction_id() . '</td></tr></table></div>';
        $html .= '<div style="width:100%;"><h2>Delivery Details</h2></div><div style="width:100%;">' . $order->get_formatted_shipping_address() . '</div>';

        if ($order->customer_message):
            $html .= '<div style="width:100%;"><h2>Delivery Notes</h2></div><div style="width:100%;">' . $order->customer_message . '</div>';
        endif;
        return $html;
    }

}
