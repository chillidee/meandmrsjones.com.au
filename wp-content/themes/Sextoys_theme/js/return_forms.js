jQuery(document).ready(function ($) {
    if ($('form[name=apparel_return]').length > 0) {
        $('select[name=apparelReturnSelect]').change(function () {
            var option = $(this).val();
            if (option === '1' || option === '2' || option === '3') {
                if ($('.file-upload-wrapper').hasClass('hidden')) {
                    $('.file-upload-wrapper').removeClass('hidden');
                }
                if ($('.faulty-submit-button').hasClass('hidden')) {
                    $('.faulty-submit-button').removeClass('hidden');
                }

                if (!$('.return-payment-text').hasClass('hidden')) {
                    $('.return-payment-text').addClass('hidden');
                }
                if (!$('.cost-submit-button').hasClass('hidden')) {
                    $('.cost-submit-button').addClass('hidden');
                }
                if (!$('.cost_payment').hasClass('hidden')) {
                    $('.cost_payment').addClass('hidden');
                }
            }
            if (option === '4' || option === '5') {
                if (!$('.file-upload-wrapper').hasClass('hidden')) {
                    $('.file-upload-wrapper').addClass('hidden');
                }
                if (!$('.faulty-submit-button').hasClass('hidden')) {
                    $('.faulty-submit-button').addClass('hidden');
                }

                if ($('.return-payment-text').hasClass('hidden')) {
                    $('.return-payment-text').removeClass('hidden');
                }
                if ($('.cost-submit-button').hasClass('hidden')) {
                    $('.cost-submit-button').removeClass('hidden');
                }
                if ($('.cost_payment').hasClass('hidden')) {
                    $('.cost_payment').removeClass('hidden');
                }
            }
        });

        if ($('input[name=order_number]').val().length > 0) {
            $('input[name=order_number]').trigger('input').promise(function () {
                console.log(1);

            });
        }
        if ($('select[name=apparelReturnSelect]').val() !== '-1') {
            console.log(1);
            $('select[name=apparelReturnSelect]').trigger('change');
            $('select[name=product_select]').val($('input[name=product_select_post]').val());
        }

        $('button[name="submit_apparel_form_faulty_button"]').click(function () {
            var has_name = ($('input[name=customer_name]').val().length > 0) ? true : false;
            var has_order_no = ($('input[name=order_number]').val().length > 0) ? true : false;
            var has_email = ($('input[name=email]').val().length > 0) ? true : false;
            var has_phone = ($('input[name=phone]').val().length > 0) ? true : false;
            var has_product = ($('select[name=product_select]').val() !== '-1') ? true : false;
            if (has_name && has_order_no && has_email && has_phone && has_product) {
                $('button[name="submit_apparel_form_faulty_button"]').attr('disabled', true);
                $('button[name="submit_apparel_form_faulty_button"]').html('Submitted');
                $('button[name="submit_apparel_form_faulty_button"]').addClass('btn-success');
                $('form[name=apparel_return]').submit();
            } else {
                commonErrorMessageHandlers(has_name, has_email, has_order_no, has_phone, has_product);
                if (!has_image && $('input[name=productImage]').next().hasClass('hidden')) {
                    $('input[name=productImage]').next().removeClass('hidden');
                    $('input[name=productImage]').on('change', function () {
                        $('input[name=productImage]').next().addClass('hidden');
                    });
                }
            }
        });

        $('button[name="submit_apparel_form_cost_button"]').click(function () {
            var has_name = ($('input[name=customer_name]').val().length > 0) ? true : false;
            var has_order_no = ($('input[name=order_number]').val().length > 0) ? true : false;
            var has_email = ($('input[name=email]').val().length > 0) ? true : false;
            var has_phone = ($('input[name=phone]').val().length > 0) ? true : false;
            var has_product = ($('select[name=product_select]').val() !== '-1') ? true : false;
            var has_accepted = ($('input[name=cost_accept]').prop('checked')) ? true : false;
            if (has_name && has_order_no && has_email && has_phone && has_product && has_accepted) {
                $('button[name="submit_apparel_form_cost_button"]').attr('disabled', true);
                $('button[name="submit_apparel_form_cost_button"]').html('Submitted');
                $('button[name="submit_apparel_form_cost_button"]').addClass('btn-success');
                $('form[name=apparel_return]').submit();
            } else {
                commonErrorMessageHandlers(has_name, has_email, has_order_no, has_phone, has_product);
                if (!has_accepted && $('input[name=cost_accept]').next().next().next().hasClass('hidden')) {
                    $('input[name=cost_accept]').next().next().next().removeClass('hidden');
                    $('input[name=cost_accept]').on('click', function () {
                        $('input[name=cost_accept]').next().next().next().addClass('hidden');
                    });
                }
            }
        });

    }

    if ($('form[name=st_return]').length > 0) {
        $('input[name=warranty_yes]').on('click', function () {
            if ($('input[name=warranty_no]').prop('checked')) {
                $('input[name=warranty_no]').prop('checked', false);
            }
        });
        $('input[name=warranty_no]').on('click', function () {
            if ($('input[name=warranty_yes]').prop('checked')) {
                $('input[name=warranty_yes]').prop('checked', false);
            }
        });
        $('input[name=faulty_on_arrival_yes]').on('click', function () {
            if ($('input[name=faulty_on_arrival_no]').prop('checked')) {
                $('input[name=faulty_on_arrival_no]').prop('checked', false);
            }
        });
        $('input[name=faulty_on_arrival_no]').on('click', function () {
            if ($('input[name=faulty_on_arrival_yes]').prop('checked')) {
                $('input[name=faulty_on_arrival_yes]').prop('checked', false);
            }
        });

        $('button[name=submit_form_button]').click(function () {
            var has_name = ($('input[name=customer_name]').val().length > 0) ? true : false;
            var has_order_no = ($('input[name=order_number]').val().length > 0) ? true : false;
            var has_email = ($('input[name=email]').val().length > 0) ? true : false;
            var has_phone = ($('input[name=phone]').val().length > 0) ? true : false;
            var has_product = ($('select[name=product_select]').val() !== '-1') ? true : false;
            var has_faulty = ($('input[name=faulty_on_arrival_yes]').prop('checked') || $('input[name=faulty_on_arrival_no]').prop('checked')) ? true : false;
            var has_warranty = ($('input[name=warranty_yes]').prop('checked') || $('input[name=warranty_no]').prop('checked')) ? true : false;
            var has_image = ($('input[name=productImage]').val().length) ? true : false;
            if (has_name && has_order_no && has_email && has_phone && has_product && has_faulty && has_warranty && has_image) {
                $('button[name=submit_form_button]').attr('disabled', true);
                $('button[name=submit_form_button]').html('Submitted');
                $('button[name=submit_form_button]').addClass('btn-success');
                $('form[name=st_return]').submit();
            } else {
                commonErrorMessageHandlers(has_name, has_email, has_order_no, has_phone, has_product);
                if (!has_faulty && $('input[name=faulty_on_arrival_yes]').next().next().hasClass('hidden')) {
                    $('input[name=faulty_on_arrival_yes]').next().next().removeClass('hidden');
                    $('input[name=faulty_on_arrival_yes]').on('click', function () {
                        $('input[name=faulty_on_arrival_yes]').next().next().addClass('hidden');
                    });
                    $('input[name=faulty_on_arrival_no]').on('click', function () {
                        $('input[name=faulty_on_arrival_yes]').next().next().addClass('hidden');
                    });
                }
                if (!has_warranty && $('input[name=warranty_yes]').next().next().hasClass('hidden')) {
                    $('input[name=warranty_yes]').next().next().removeClass('hidden');
                    $('input[name=warranty_yes]').on('click', function () {
                        $('input[name=warranty_yes]').next().next().addClass('hidden');
                    });
                    $('input[name=warranty_no]').on('click', function () {
                        $('input[name=warranty_yes]').next().next().addClass('hidden');
                    });
                }
                if (!has_image && $('input[name=productImage]').next().hasClass('hidden')) {
                    $('input[name=productImage]').next().removeClass('hidden');
                    $('input[name=productImage]').on('change', function () {
                        $('input[name=productImage]').next().addClass('hidden');
                    });
                }
            }
        });
    }

    function commonErrorMessageHandlers(has_name, has_email, has_order_no, has_phone, has_product) {
        if (!has_name && $('input[name=customer_name]').next().hasClass('hidden')) {
            $('input[name=customer_name]').next().removeClass('hidden');
            $('input[name=customer_name]').on('input', function () {
                $('input[name=customer_name]').next().addClass('hidden');
            });
        }
        if (!has_email && $('input[name=email]').next().hasClass('hidden')) {
            $('input[name=email]').next().removeClass('hidden');
            $('input[name=email]').on('input', function () {
                $('input[name=email]').next().addClass('hidden');
            });
        }
        if (!has_phone && $('input[name=phone]').next().hasClass('hidden')) {
            $('input[name=phone]').next().removeClass('hidden');
            $('input[name=phone]').on('input', function () {
                $('input[name=phone]').next().addClass('hidden');
            });
        }
        if (!has_order_no && $('input[name=order_number]').next().hasClass('hidden')) {
            $('input[name=order_number]').next().removeClass('hidden');
            $('input[name=order_number]').on('input', function () {
                $('input[name=order_number]').next().addClass('hidden');
            });
        }
        if (!has_product && $('select[name=product_select]').next().hasClass('hidden')) {
            $('select[name=product_select]').next().removeClass('hidden');
            $('select[name=product_select]').on('change', function () {
                $('select[name=product_select]').next().addClass('hidden');
            });
        }
    }

});