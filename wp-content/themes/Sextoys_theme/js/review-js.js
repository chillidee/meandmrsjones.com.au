jQuery(window).ready(function ($) {

    const ajaxContainer = $('.ajax-response');

    $('button[name=save-review]').click(function (e) {
        var btn = $(this), stars = false, review = false, ajaxMessage = '', title = false;
        btn.attr('disabled', true);
        ajaxContainer.html('');
        e.preventDefault();
        if ($('input[name=star]:checked').val() === undefined) {
            ajaxMessage += '<p style="padding: 5px;margin: 0;text-align: center;color: #fff;font-size: 16px; font-family: Roboto Slab,serif;" class="star-error">Please select  a star rating</p>';
        } else {
            stars = true;
        }
        if ($('input[name=review-title]').val().length > 0) {
            title = true;
        } else {
            ajaxMessage += '<p style="padding: 5px;margin: 0;text-align: center;color: #fff;font-size: 16px; font-family: Roboto Slab,serif;" class="title-error">Please write a title</p>';
        }
        if ($('textarea[name=rewiew-text]').val().length > 0) {
            review = true;
        } else {
            ajaxMessage += '<p style="padding: 5px;margin: 0;text-align: center;color: #fff;font-size: 16px; font-family: Roboto Slab,serif;" class="review-error">Please write a review</p>';
        }
        if (stars && review && title) {
            $('form[name="review-form"]').submit();
        } else {
            ajaxContainer.addClass('btn-danger');
            ajaxContainer.html(ajaxMessage);
            btn.attr('disabled', false);
            $('body').scrollTop(0);
        }
    });

    $('textarea[name=rewiew-text]').on('input', function () {
        if ($('.review-error').length > 0) {
            $('.review-error').remove();
            checkIfErrorMessage();
        }
    })

    $('input[name=review-title]').on('input', function () {
        if ($('.title-error').length > 0) {
            $('.title-error').remove();
            checkIfErrorMessage();
        }
    })

    $('.star').on('click', function () {
        if ($('.star-error').length > 0) {
            $('.star-error').remove();
            checkIfErrorMessage();
        }
    })

    if ($('input[name=hasReviewStar]').length > 0) {
        var star = $('input[name=hasReviewStar]').val();
        $('.star-' + star).click();
    }

    function checkIfErrorMessage() {
        var review_error = true, star_error = true, title_error = true, title_error_p = $('.title-error').html().length, review_error_p = $('.review-error').html().length, star_error_p = $('.star-error').html().length;
        if (review_error_p > 0) {
            review_error = false;
        }
        if (star_error_p > 0) {
            star_error = false;
        }
        if (title_error_p > 0) {
            title_error = false;
        }
        if (star_error && review_error && title_error) {
            ajaxContainer.html('');
            ajaxContainer.removeClass('btn-danger');
        }
    }

});