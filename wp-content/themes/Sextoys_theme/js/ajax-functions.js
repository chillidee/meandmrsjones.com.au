var ajaxurl = window.location.origin + '/wp-admin/admin-ajax.php';

jQuery(document).ready(function ($) {
    var updatingProducts = false;
    if ($('button[name=discount_signup_submit]').length > 0) {
        $('button[name=discount_signup_submit]').click(function () {
            var btn = $(this), email = $('input[name=discount_signup]');
            if (email.val().length > 0) {
                btn.prop('disabled', true);
                $.post(ajaxurl, { email: email.val(), action: 'st_home_subscribe' }, function (response) {
                    if (response === 'Error') {
                        $('.front_page_email_error').addClass('merlot');
                        $('.front_page_email_error').html('Email address already entered');
                        document.cookie = "enteredemail=true";
                        email.remove();
                        btn.remove();
                    } else {
                        if (response) {
                            email.remove();
                            btn.remove();
                            $('.home-popup').remove();
                            document.cookie = "enteredemail=true";
                            $('.front_page_email_error').html("Success! Please check your inbox for coupon code.");
                        } else {
                            btn.prop('disabled', false);
                        }
                    }
                });
            } else {
                $('.front_page_email_error').html('Please enter an email address');
            }
        });
        $('button[name=discount_signup_submit_popup]').click(function () {
            var btn = $(this), email = $('input[name=discount_signup_popup]');
            if (email.val().length > 0) {
                btn.prop('disabled', true);
                $.post(ajaxurl, { email: email.val(), action: 'st_home_subscribe' }, function (response) {
                    if (response === 'Error') {
                        $('.front_page_popup_email_error').addClass('merlot');
                        $('.front_page_popup_email_error').html('Email address already entered');
                        document.cookie = "enteredemail=true";
                        setTimeout(function () {
                            $('.home-popup').remove();
                        }, 2000);
                    } else {
                        if (response) {
                            document.cookie = "enteredemail=true";
                            $('.front_page_popup_email_error').html("Success! Please check your inbox for coupon code.");
                            setTimeout(function () {
                                $('.home-popup').remove();
                            }, 2000);
                        } else {
                            btn.prop('disabled', false);
                        }
                    }
                });
            } else {
                $('.front_page_popup_email_error').html('Please enter an email address');
            }
        });
        $('input[name=discount_signup]').on('input', function () {
            $('.front_page_email_error').html('');
        });
        $('input[name=discount_signup_popup]').on('input', function () {
            $('.front_page_popup_email_error').html('');
        });
    }

    if ($('input[name=order_number]').length > 0) {
        $('input[name=order_number]').on('input', function () {
            var ordernumber = $(this).val();
            var data = {
                action: 'st_is_valid_order_number',
                value: ordernumber
            };
            $.post(ajaxurl, data, function (response) {
                if (response) {
                    data = {
                        action: 'st_get_order_items',
                        value: ordernumber
                    }
                    $.post(ajaxurl, data, function (items) {
                        $('select[name=product_select]').html('');
                        items = JSON.parse(items);
                        var options;
                        for (var index in items) {
                            $('select[name=product_select]').append('<option value="' + index + '">' + items[index] + '</option>');
                        }
                    });
                }
            });
        });
    }

    var width = $('html').innerWidth();
    if ($('.my-account-single-order-email').length > 0) {
        $('#order-send-pdf-button').click(function () {
            $('.order-email-error').css('display', 'none');
            $(this).prop('disabled', true);
            if ($('#order-send-pdf-email').val().length > 0) {
                var data = {
                    'action': 'st_create_pdf',
                    'order_id': $('#order-send-pdf-order-id').val(),
                    'order_email': $('#order-send-pdf-email').val()
                };
                $.post(ajaxurl, data, function (response) {
                    if (response === '0') {
                        $('.order-email-error').html('There has been a problem please try again.');
                    } else {
                        $('#order-send-pdf').css('display', 'none');
                    }
                    $('#order-send-pdf-button').prop('disabled', false);
                });
            } else {
                $('.order-email-error').html('Please enter a valid email address.').promise().done(function () {
                    $('.order-email-error').css('display', 'block');
                    $('#order-send-pdf-button').prop('disabled', false);
                });
            }
        });
    }

    if ($('#checkout-change-address').length > 0) {

        $('#checkout-change-address').change(function () {
            var selected = $('#checkout-change-address option:selected').val();
            var userID = document.getElementById('checkout-user-id').value;
            var data = {
                'action': 'st_get_address',
                'user_id': userID,
                'address_name': selected
            };
            $.post(ajaxurl, data, function (response) {
                var addressArray = JSON.parse(response);
                document.getElementById('shipping_first_name').value = addressArray.firstName;
                document.getElementById('shipping_last_name').value = addressArray.lastName;
                document.getElementById('shipping_address_1').value = addressArray.address1;
                document.getElementById('shipping_address_2').value = addressArray.address2;
                document.getElementById('shipping_city').value = addressArray.city;
                document.getElementById('shipping_postcode').value = addressArray.postcode;
                var options = document.getElementById('shipping_state').getElementsByTagName('option');
                for (var i = 0; i < options.length; i++) {
                    if (options[i].value === addressArray.state) {
                        options[i].selected = 'true';
                    }
                }
                $('#shipping_state').trigger('change');
            });
        });
    }
    if ($('.archive-sort-overlay').length > 0) {

        if ($('#archive-sort-overlay').length > 0) {
            $('.archive-sort-overlay-sort-default').click(function () {
                resetSearchOverlay();
                sortFilterApply($('input[name=refined_gender]').val(), true);
            });
            $('.archive-sort-overlay-sort-clear').click(function () {
                resetSearchOverlay();
                sortFilterApply($('input[name=refined_gender]').val(), true);
            });
        }

        function resetSearchOverlay() {
            var inputs = document.getElementById('archive-sort-overlay').getElementsByTagName('input');
            $('input[name=refined_gender]').val('');
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type === 'checkbox') {
                    inputs[i].checked = false;
                    if (inputs[i].value === 'all') {
                        inputs[i].checked = true;
                    }
                }
            }
        }

        function replacePlaceholderImage(element) {
            var real_src = element.data('src');
            if (real_src) {
                element.attr('src', real_src);
                element.removeData('src');
            }
        }

        $("input[name=gender]").click(function () {
            sortFilterApply($(this).val());
        });
        $("input:checkbox[name=price]").click(function () {
            sortFilterApply();
        });
        $("input:checkbox[name=brand]").click(function () {
            sortFilterApply();
        });
        $("input:checkbox[name=colour]").click(function (event) {
            if ($(this).val() === 'all') {
                $('input[name=colour]').each(function () {
                    if ($(this).val() !== 'all') {
                        $(this).prop('checked', false);
                    }
                });
            } else {
                $('#colour-all').prop('checked', false);
            }
            sortFilterApply();
        });
        $("input:checkbox[name=size]").click(function () {
            if ($(this).val() === 'all') {
                $('input[name=size]').each(function () {
                    if ($(this).val() !== 'all') {
                        $(this).prop('checked', false);
                    }
                });
            } else {
                $('#size-all-sort-label').prop('checked', false);
            }
            sortFilterApply();
        });
        $("input:checkbox[name=tag]").click(function () {
            sortFilterApply();
        });
        $('#archieve-sort-by-select').change(function () {
            sortFilterApply();
        });
        function sortFilterApply(gender, defaultValues) {
            updatingProducts = false;
            if ($('.thinking').hasClass('hidden')) {
                $('.thinking').removeClass('hidden');
            }
            var values = [];
            $("input:checkbox[name=price]:checked").each(function () {
                values.push($(this).val());
            });
            if (values.length === 0) {
                $("input:checkbox[name=price]:not(:checked)").each(function () {
                    values.push($(this).val());
                });
            }
            var brands = [];
            $("input:checkbox[name=brand]:checked").each(function () {
                brands.push($(this).val());
            });
            if (brands.length === 0 || brands[0] === 'all') {
                $("input:checkbox[name=brand]:not(:checked)").each(function () {
                    brands.push($(this).val());
                });
            }
            var colour = [];
            $("input:checkbox[name=colour]:checked").each(function () {
                colour.push($(this).val());
            });
            if (colour.length === 0 || colour[0] === 'all') {
                $("input:checkbox[name=colour]:not(:checked)").each(function () {
                    colour.push($(this).val());
                });
            }
            var sizes = [];
            $("input:checkbox[name=size]:checked").each(function () {
                sizes.push($(this).val());
            });
            if (sizes.length === 0 || sizes[0] === 'all') {
                $("input:checkbox[name=size]:not(:checked)").each(function () {
                    sizes.push($(this).val());
                });
            }
            var tags = [];
            $("input:checkbox[name=tag]:checked").each(function () {
                tags.push($(this).val());
            });
            var selected = $('#archieve-sort-by-select option:selected').val();
            var refined = true;
            if (defaultValues) {
                refined = null;
            }
            var data = {
                action: 'st_new_sort_products',
                numberOfProducts: $('input[name=offset_number_orginial]').val(),
                cats: document.getElementById('archive-cat-id').value,
                selected: selected,
                prices: values.join() || '49.99,99.99,100.00',
                brands: brands.join() || 'all',
                colours: colour.join() || 'all',
                sizes: sizes.join() || 'all',
                tags: tags.join(','),
                offset: 0,
                refined: refined,
                parent: $('#archive-cat-parent').val(),
                slug: $('#archive-cat-id').val(),
                is_search: $('input[name=is_search]').val(),
                archive_loaded_products: $('#archive_loaded_products').val() || false
            };
            if (typeof (gender) !== 'undefined' && gender.length > 0) {
                data['gender'] = gender;
            } else {
                if ($('input[name=refined_gender]').val().length > 0) {
                    data['gender'] = $('input[name=refined_gender]').val();
                }
            }
            $.post(ajaxurl, data, function (response) {
                $('input[name=refined]').val(true);
                if (typeof (gender) !== 'undefined' && gender.length > 0) {
                    $('input[name=refined_gender]').val(gender);
                }
                $('.archive-product-wrapper').html('');
                if (response) {
                    $('.archive-product-wrapper').html(response).promise().done(function () {
                        if (!$('.thinking').hasClass('hidden')) {
                            $('.thinking').addClass('hidden');
                        }
                        $('.archive-product-wrapper').scrollTop(0);
                        archive_scroll_handler();
                        if (width <= 1200) {
                            //                                    $('#archive-sort-overlay').removeClass('overlay_open');
                            //                                    $('body').removeClass('modal-open');
                        }
                        $('.archive-product-image').children().each(function (index, element) {
                            if ($(element).hasClass('image-placeholder')) {
                                $(element).remove();
                            } else {
                                $(element).css('display', 'block');
                            }
                        });
                        $('body').removeClass('thinking');
                        $('.archive-product-image img').each(function () {
                            if ($(this).parents('.archive-product-container').offset().top < innerHeight) {
                                replacePlaceholderImage($(this));
                            }
                        });
                        var offset = 48;
                        $('input[name=product_offset]').each(function () {
                            offset = $(this).val();
                        });
                        $('input[name=offset_number]').val(offset);
                    });
                } else {
                    $('.archive-product-wrapper').html('<div class="col-lg-12 no-products"><h3>No products found, please refine your filters</h3></div>').promise().done(function () {
                        if (!$('.thinking').hasClass('hidden')) {
                            $('.thinking').addClass('hidden');
                        }
                    });
                }
                if (innerWidth < 767) {
                    $('.archive-sort-overlay-close').trigger('click');
                }
            });
            updatingProducts = false;
        }
    }

    if ($('#thankYouCreateAccountForm').length > 0) {
        $('#thankYouCreateEmailAddress').on('input', function () {
            var username = $('#thankYouCreateEmailAddress').val();
            var data = {
                'action': 'check_user',
                'username': username
            };
            $.post(ajaxurl, data, function (response) {
                if (response === 'true') {
                    $('.email-in-use').css('display', 'block');
                } else {
                    $('.email-in-use').css('display', 'none');
                }
            });
        });
        $('#thankYouCreateButton').click(function () {
            $(this).prop('disabled', true);
            var title = $('.titleSelectTY option:selected').val();
            var passwordErrorHtml = $('.thank-you-password-error');
            var password = $('#thankYouCreatePassword');
            var email = document.getElementById('thankYouCreateEmailAddress').value;
            var subscribe = document.getElementById('thankYouCreateSubscribe').value;
            subscribe = (subscribe === 'on') ? true : false;
            password.click(function () {
                passwordErrorHtml.css('display', 'none');
            });
            var day = $('.signUpOverlayDOBDay option:selected').val();
            var month = $('.signUpOverlayDOBMonth option:selected').val();
            var year = $('.signUpOverlayDOBYear option:selected').val();
            var dobnotvalid = (day === 'Day') || (month === 'Month') || (year === 'Year');
            var dobvalid = false;
            if (dobnotvalid) {
                $('.signup-form-dateofbirth-error').html('Please enter your date of birth');
                $('.signup-form-dateofbirth-error').css('display', 'block');
            } else {
                $('.signup-form-dateofbirth-error').css('display', 'none');
                var dobDate = new Date(year + ',' + month + ',' + day);
                var dob = dobDate.getDay() + ',' + dobDate.getMonth() + ',' + dobDate.getFullYear();
                if ((new Date().getYear() - dobDate.getYear()) < 18) {
                    $('.signup-form-dateofbirth-error').html('You must be over 18 to signup');
                    $('.signup-form-dateofbirth-error').css('display', 'block');
                } else {
                    dobvalid = true;
                }
            }
            var passwordComfirm = $('#thankYouCreatePasswordConfirm');
            var passwordMatch = password.val() === passwordComfirm.val();
            var passwordValid = false;
            var usernameValid = false;
            if (passwordMatch && dobvalid) {
                if (!password.val().match(/^(?=.{8,})(?=.*[a-z])(?=.*[A-Z]).*$/)) {
                    passwordErrorHtml.html('Passwords must have at least 8 characters, contain 1 uppercase letter and 1 lowercase letter.');
                    passwordErrorHtml.css('display', 'block');
                } else {
                    passwordValid = true;
                    var data = {
                        'action': 'check_user',
                        'username': email
                    };
                    $.post(ajaxurl, data, function (response) {
                        if (response === 'true') {
                            $('.email-in-use').css('display', 'block');
                            usernameValid = false;
                        } else {
                            $('.email-in-use').css('display', 'none');
                            usernameValid = true;
                            if (passwordValid && usernameValid) {
                                var data = {
                                    'action': 'create_user',
                                    'first_name': document.getElementById('thankYouCreateFirstName').value,
                                    'last_name': document.getElementById('thankYouCreateLastName').value,
                                    'username': email,
                                    'password': password.val(),
                                    'email': email,
                                    'phone': document.getElementById('thankYouCreatePhone').value,
                                    'subscribe': subscribe,
                                    'after_order': true,
                                    'order_id': document.getElementById('thankyouOrderId').value,
                                    'billing_address1': document.getElementById('thankyouBillingAddress1').value,
                                    'billing_address2': document.getElementById('thankyouBillingAddress2').value,
                                    'billing_city': document.getElementById('thankyouBillingAddressCity').value,
                                    'billing_country': document.getElementById('thankyouBillingAddressCountry').value,
                                    'billing_postcode': document.getElementById('thankyouBillingAddressPostcode').value,
                                    'billing_state': document.getElementById('thankyouBillingAddressState').value,
                                    'dob': dob,
                                    'title': title
                                };
                                $.post(ajaxurl, data, function (response) {
                                    if (response === 'true') {
                                        window.location = $('#requestURL').val();
                                    }
                                });
                            }
                            ;
                        }
                    });
                }
            } else {
                passwordErrorHtml.html('Passwords don\'t match');
                passwordErrorHtml.css('display', 'block');
            }
            $(this).prop('disabled', false);
        });
    }

    if ('#contact-form-overlay') {

        if ($('#contactForm')) {

            $('#contactButton').click(function () {
                var contactButton = $(this);
                contactButton.prop('disabled', true);
                var problem = $('#contactProblemSelect option:selected').val();
                if (problem !== '0') {
                    var firstName = document.getElementById('contactFormFirstName').value;
                    var surname = document.getElementById('contactFormSurname').value;
                    var contactNumber = document.getElementById('contactNumber').value;
                    var contactProblemSelect = document.getElementById('contactProblemSelect').value;
                    var details = document.getElementById('contactDetail').value;
                    var contactEmail = document.getElementById('contactEmail').value;
                    var data = {
                        'action': 'send_contact_email',
                        'firstName': firstName,
                        'surname': surname,
                        'contactNumber': contactNumber,
                        'contactProblemSelect': contactProblemSelect,
                        'details': details,
                        'contactEmail': contactEmail
                    };
                    $.post(ajaxurl, data, function (response) {
                        if (response) {
                            $('#contact-form-overlay').removeClass('overlay_open');
                        }
                    });
                } else {
                    $('.contact-problem-error').css('display', 'block');
                }
                contactButton.prop('disabled', false);
            });
        }
    }



    if ($('#my-account-subscribe-toggle')) {
        $('#my-account-subscribe-toggle').click(function () {
            $(this).prop('disabled', true);
            var value = $('#myonoffswitch:checked').val();
            if (value === 'on') {
                value = 'true';
            } else {
                value = 'false';
            }
            var customer_id = $('#my-account-customer-id').val();
            var data = {
                'action': 'change_subscribe_status',
                'customer_id': customer_id,
                'status': value
            };
            $.post(ajaxurl, data, function (response) {
                data = null;
                $('#my-account-subscribe-toggle').prop('disabled', false);
                if (response === '1') {
                    if (value === 'true') {
                        $('.my-account-subscribe-status').html('You are currently subscribed to Me & Mrs Jones Emails. -We hope you enjoy all the offers from us and our affiliates.');
                    } else {
                        $('.my-account-subscribe-status').html('You are currently not subscribed to Me & Mrs Jones Emails. -Subcribe now for offers from us and our affiliates.');
                    }
                }
            });
        });
    }

    if ($('#signupForm').length > 0) {

        $('#signUpOverlayEmail').on('input', function () {
            var username = $('#signUpOverlayEmail').val();
            var data = {
                'action': 'check_user',
                'username': username
            };
            $.post(ajaxurl, data, function (response) {
                if (response === 'true') {
                    $('.email-in-use').css('display', 'block');
                } else {
                    $('.email-in-use').css('display', 'none');
                }
            });
        });
        $('#signup-form-button').click(function () {
            $(this).prop('disabled', true);
            if ($('.user-create-error').css('display') === 'block') {
                $('.user-create-error').css('display', 'none');
            }
            var day = $('#signUpOverlayDOBDay option:selected').val();
            var month = $('#signUpOverlayDOBMonth option:selected').val();
            var year = $('#signUpOverlayDOBYear option:selected').val();
            var dobnotvalid = (day === 'Day') || (month === 'Month') || (year === 'Year');
            var dobvalid = false;
            if (dobnotvalid) {
                $('.signup-form-dateofbirth-error').html('Please enter your date of birth');
                $('.signup-form-dateofbirth-error').css('display', 'block');
            } else {
                $('.signup-form-dateofbirth-error').css('display', 'none');
                var dobDate = new Date(year + ',' + month + ',' + day);
                var dob = dobDate.getDay() + ',' + dobDate.getMonth() + ',' + dobDate.getFullYear();
                if ((new Date().getYear() - dobDate.getYear()) < 18) {
                    $('.signup-form-dateofbirth-error').html('You must be over 18 to signup');
                    $('.signup-form-dateofbirth-error').css('display', 'block');
                } else {
                    dobvalid = true;
                }
            }
            var title = $('.titleSelect option:selected').val();
            var firstEmail = $('#signUpOverlayEmail').val();
            var secondEmail = $('#signUpOverlayEmailConfirm').val();
            var emailMatch = firstEmail === secondEmail;
            var firstPassword = $('#signUpOverlayPassword').val();
            var secondPassword = $('#signUpOverlayPasswordConfirm').val();
            var passwordMatch = firstPassword === secondPassword;
            var passwordValid = false;
            var subscribe = $('#signupsubscribe:checked').val();
            subscribe = (subscribe === 'on') ? true : false;
            if (!passwordMatch) {
                $('.password-match-error').html('Passwords don\'t match');
                $('.password-match-error').css('display', 'block');
            } else {
                $('.password-match-error').css('display', 'none');
                if (!firstPassword.match(/^(?=.{8,})(?=.*[a-z])(?=.*[A-Z]).*$/)) {
                    $('.password-match-error').html('Passwords must have at least 8 characters, contain 1 uppercase letter and 1 lowercase letter.');
                    $('.password-match-error').css('display', 'block');
                } else {
                    $('.password-match-error').css('display', 'none');
                    passwordValid = true;
                }
            }
            if (!emailMatch) {
                $('.email-match-error').css('display', 'block');
            } else {
                $('.email-match-error').css('display', 'none');
            }
            var username = $('#signUpOverlayEmail').val();
            var usernamevalid = false;
            var data = {
                'action': 'check_user',
                'username': username
            };
            $.post(ajaxurl, data, function (response) {
                if (response === 'false') {
                    usernamevalid = true;
                    if (emailMatch && passwordMatch && passwordValid && dobvalid && usernamevalid) {
                        data = {
                            'action': 'create_user',
                            'first_name': $('#signUpOverlayFirstName').val(),
                            'last_name': $('#signUpOverlaySurname').val(),
                            'username': username,
                            'password': $('#signUpOverlayPassword').val(),
                            'email': username,
                            'subscribe': subscribe,
                            'dob': dob,
                            'title': title
                        };
                        $.post(ajaxurl, data, function (response) {
                            if (response === 'true') {
                                if (typeof fbq !== 'undefined') {
                                    fbq('track', 'CompleteRegistration');
                                }
                                window.location = $('#requestURL').val();
                            } else {
                                if (response === 'Error 1') {
                                    $('.user-create-error').html('Error creating user please try again');
                                    $('.user-create-error').css('display', 'block');
                                }

                            }
                        });
                    }
                } else {
                    usernamevalid = false;
                }
            });
            $(this).prop('disabled', false);
        });
    }

    function removePlaceHolderImages() {
        var placeholdler = $('.image-placeholder');
        if (placeholdler.length > 0) {
            placeholdler.each(function () {
                var mainImage = $(this).siblings('img');
                mainImage = $(mainImage);
                if (mainImage.complete) {
                    placeholdler.css('display', 'none');
                    mainImage.css('display', 'block');
                    placeholdler.remove();
                }
                mainImage.load(function () {
                    placeholdler.css('display', 'none');
                    mainImage.css('display', 'block');
                    placeholdler.remove();
                });
            });
        }
    }

    function change_full_product() {
        $('.product-option-dropdown-st_colour').change(function () {
            $('.add-to-cart-ajax').prop('disabled', true);
            if (width > 1200) {
                $('body').scrollTop(0);
            }
            var product_id = $(this).val();
            var data = {
                action: 'st_get_full_archive_product',
                product_id: product_id
            };
            $.post(ajaxurl, data, function (response) {
                $('.add-to-cart-ajax').prop('disabled', false);
                $('.archive-display-product').html(response).promise().done(function () {
                    removePlaceHolderImages();
                    $('.main_image').css('display', 'block');
                    if (width > 1200) {
                        add_image_zoom('.attachment-full');
                    }
                    archive_click_handlers();
                    change_full_product();
                });
            });
        });
        $('.product-option-dropdown-st_size').change(function () {
            $('.add-to-cart-ajax').prop('disabled', true);
            if (width > 1200) {
                $('body').scrollTop(0);
            }
            var product_id = $(this).val();
            var data = {
                'action': 'st_get_full_archive_product',
                'product_id': product_id
            };
            $.post(ajaxurl, data, function (response) {
                $('.add-to-cart-ajax').prop('disabled', false);
                $('.archive-display-product').html(response);
                removePlaceHolderImages();
                archive_click_handlers();
                change_full_product();
                $('.main_image').css('display', 'block');
                if (width > 1200) {
                    add_image_zoom('.attachment-full');
                }
            });
        });
    }

    $('.varible-product-image').click(function () {
        $(this).unbind();
        var id = $(this).attr('id');
        id = id.split('_');
    });
    function archive_scroll_handler() {
    }
    function archive_click_handlers() {
        if ($('.gallery-image').length > 0) {
            $('.gallery-image').click(function () {
                $('img').trigger('zoom.destroy');
                var clicked_image = ($(this).get(0).currentSrc);
                var main_image = $('.main_image').get(0).currentSrc;
                $('.main_image').attr('src', clicked_image);
                $(this).attr('src', main_image);
                if (width > 1200) {
                    add_image_zoom('.main_image');
                }
            });
        }
        if ($('.size-guide-link').length > 0) {
            $('.size-guide-link').click(function () {
                hide_show_overlay($('.archive-size-guide-overlay'));
            });
            $('.size-gude_overlay_close').click(function () {
                hide_show_overlay($('.archive-size-guide-overlay'));
            });
        }
        $('.quantity-decrease').click(function () {
            $('.add-to-cart-ajax').each(function (index, element) {
                if ($(element).prop('disabled')) {
                    $(element).prop('disabled', false)
                }
            });
            var total = parseInt($('#quantity-total').val());
            total--;
            if (total >= 1) {
                $('.quantity-total').val(total);
            }
        }
        );
        var archiveIncart = $('.full-product-added-cart');
        if (archiveIncart.length > 0) {
            archiveIncart.click(function () {
                var cartOverlay = $('.cart-overlay');
                hide_show_overlay(cartOverlay)
            });
        }
        $('.quantity-increase').click(function () {
            $('.add-to-cart-ajax').each(function (index, element) {
                if ($(element).prop('disabled')) {
                    $(element).prop('disabled', false)
                }
            });
            var total = parseInt($('#quantity-total').val());
            total++;
            $('.quantity-total').val(total);
        });
        $('.add-to-cart-ajax').click(function () {

            var button = $(this);
            if (typeof fbq !== 'undefined') {
                fbq('track', 'AddToCart');
            }
            var quantity = parseInt($('.quantity-total').val());
            var productID = $('#archive-product-right-col-id').val();
            var data = {
                'action': 'st_add_to_cart',
                'quantity': quantity,
                'ID': productID
            };
            $.post(ajaxurl, data, function (response) {
                if (response) {
                    $('.nav-header-cart-total').html(response);
                    $('.add-to-cart-ajax').html('Added To Cart');
                    if (width > 1200) {
                        $('.full-product-added-cart').css('display', 'block');
                        var data = {
                            'action': 'st_populate_cart_overlay'
                        };
                        $.post(ajaxurl, data, function (response) {
                            $('.cart-contents').html(response).promise().done(function () {
                                remove_cart_item_ajax();
                                var data = {
                                    'action': 'st_get_cart_total_price'
                                };
                                $.post(ajaxurl, data, function (response) {
                                    $('.cart_total_price').html(response);
                                });
                            });
                        });
                    } else {
                        $('main').scrollTop(0);
                        $('body').scrollTop(0);
                    }
                    $('.archive-product-ajax-response').css('padding', '10px');
                    $('.archive-product-ajax-response').html('Your item has been added to your cart, view your cart <a href="/cart/">here</a>');
                } else {
                    $('.archive-product-ajax-response').html('There has been an error please try again');
                }
            });
        });
        change_full_product();
    }

    if ($('#archive-display-product').length > 0) {
        archive_click_handlers();
    }

    $('#login-button').click(function () {
        sign_request_new('#loginForm', '#login-button', '#login-username', '#login-password');
    }
    );
    $('#mobile_login-button').click(function () {
        sign_request_new('#mobile_loginForm', '#mobile_login-button', '#mobile_login-username', '#mobile_login-password');
    });
    function sign_request_new(loginForm, loginButton, usernameInput, passwordInput) {
        var login_form = $(loginForm);
        var login_button = $(loginButton);
        login_button.addClass('disabled');
        if (login_form.hasClass('ng-valid')) {
            var username = $(usernameInput).val();
            var password = $(passwordInput).val();
            var data = {
                'action': 'st_authenticate_user',
                'login': username,
                'password': password
            };
            jQuery.post(ajaxurl, data, function (response) {
                if (response === 'true') {
                    login_button.attr({
                        type: 'submit',
                        value: 'submit'
                    });
                    login_form.submit();
                } else {
                    $('.signin-error').html('Invalid username or password');
                    login_button.removeClass('disabled');
                }
            });
        }
    }

    if ($('#newAddressFrom').length > 0) {
        $('.newAddressAddressSuburb').append('<ul class="postcode_autocomplete"></ul>');
        $('input[name=newAddressAddressSuburb]').on('input', function () {
            if ($(this).val().length > 3) {
                $.post(ajaxurl, { action: 'st_get_postcodes', input: $(this).val() }, function (response) {
                    response = JSON.parse(response);
                    if (response) {
                        var postcode_list = '';
                        for (var i in response) {
                            postcode_list += '<li>' + response[i][0] + ' - ' + response[i][2] + ' - ' + response[i][1] + '</li>';
                        }
                        $('.postcode_autocomplete').html(postcode_list);
                        if (postcode_list.length > 0) {
                            $('.postcode_autocomplete').css('display', 'block');
                        }
                        postcode_list_handlers();
                    }
                });
            }
        });
        function postcode_list_handlers() {
            if (!click_handler_check('.postcode_autocomplete li')) {
                $('.postcode_autocomplete li').click(function () {
                    $('.postcode_autocomplete').css('display', 'none');
                    var address_obj = $(this).html().split('-');
                    $('input[name=newAddressAddressSuburb]').val(address_obj[0]);
                    $('input[name=newAddressAddressPostcode]').val(address_obj[2]);
                    $('select[name=new-address-address-state] option').each(function () {
                        if ($(this).val().toLowerCase() === address_obj[1].toString().replace(/ /g, '').toLowerCase()) {
                            //console.log($(this).val());
                            $('select[name=new-address-address-state]').val($(this).val());
                        }
                    });
                    $('.checkout').trigger('change');
                });
            }
        }
    }

    if ($('.checkout').length > 0) {
        $('#billing_city_field').append('<ul class="postcode_autocomplete"></ul>');
        $('#shipping_city_field').append('<ul class="postcode_autocomplete2"></ul>');
        $('#billing_city').on('input', function () {
            if ($(this).val().length > 3) {
                $.post(ajaxurl, { action: 'st_get_postcodes', input: $(this).val() }, function (response) {
                    response = JSON.parse(response);
                    if (response) {
                        var postcode_list = '';
                        for (var i in response) {
                            postcode_list += '<li>' + response[i][0] + ' - ' + response[i][2] + ' - ' + response[i][1] + '</li>';
                        }
                        $('.postcode_autocomplete').html(postcode_list);
                        if (postcode_list.length > 0) {
                            $('.postcode_autocomplete').css('display', 'block');
                        }
                        postcode_list_handlers();
                    }
                });
            }
        });
        $('#shipping_city').on('input', function () {
            if ($(this).val().length > 3) {
                $.post(ajaxurl, { action: 'st_get_postcodes', input: $(this).val() }, function (response) {
                    response = JSON.parse(response);
                    if (response) {
                        var postcode_list = '';
                        for (var i in response) {
                            postcode_list += '<li>' + response[i][0] + ' - ' + response[i][2] + ' - ' + response[i][1] + '</li>';
                        }
                        $('.postcode_autocomplete2').html(postcode_list);
                        if (postcode_list.length > 0) {
                            $('.postcode_autocomplete2').css('display', 'block');
                        }
                        postcode_list_handlers();
                    }
                });
            }
        });
        function postcode_list_handlers() {
            if (!click_handler_check('.postcode_autocomplete li')) {
                $('.postcode_autocomplete li').click(function () {
                    $('.postcode_autocomplete').css('display', 'none');
                    var address_obj = $(this).html().split('-');
                    $('#billing_city').val(address_obj[0]);
                    $('#billing_postcode').val(address_obj[2]);
                    $('#billing_state option').each(function () {
                        if ($(this).val().toLowerCase() === address_obj[1].toString().replace(/ /g, '').toLowerCase()) {
                            $('#billing_state').val($(this).val());
                        }
                    });
                    $('.checkout').trigger('change');
                });
            }
            if (!click_handler_check('.postcode_autocomplete2 li')) {
                $('.postcode_autocomplete2 li').click(function () {
                    $('.postcode_autocomplete2').css('display', 'none');
                    var address_obj = $(this).html().split('-');
                    $('#shipping_city').val(address_obj[0]);
                    $('#shipping_postcode').val(address_obj[2]);
                    $('#shipping_state option').each(function () {
                        if ($(this).val().toLowerCase() === address_obj[1].toString().replace(/ /g, '').toLowerCase()) {
                            $('#shipping_state').val($(this).val());
                        }
                    });
                    $('.checkout').trigger('change');
                });
            }
        }
    }

    remove_cart_item_ajax();
    function add_image_zoom(imageName, imagePath) {
        $(imageName).wrap('<span style="display:block"></span>').css('display', 'block').parent().zoom({ magnify: 2 });
    }

    function hide_show_overlay(overlayClass) {
        enableDisableScroll(false);
        if (overlayClass) {
            var is_open = overlayClass.hasClass('overlay_open');
        }
        $('div').each(function (index, element) {
            if ($(element).hasClass('overlay_open')) {
                $(element).removeClass('overlay_open');
            }
        });
        if (!is_open && overlayClass) {
            overlayClass.addClass('overlay_open');
            enableDisableScroll(true);
        }
    }
    function enableDisableScroll(selector) {
        //if (innerWidth < 1200) {
        if (selector) {
            $("body").addClass("modal-open");
        } else {
            $("body").removeClass("modal-open");
        }
        // }
    }

    if ($('input[name=offset_number_orginial]').length > 0) {
        $('.archive-product-wrapper').scroll(function () {
            var scrollTrigger = $('.archive-product-wrapper').prop('scrollHeight') / 2;
            if ($('.archive-product-wrapper').scrollTop() > scrollTrigger && !updatingProducts) {
                updatingProducts = true;
                var numberOfProducts = parseInt($('input[name=offset_number_orginial]').val());
                var offset = parseInt($('input[name=offset_number]').val());
                var data = {
                    action: 'st_get_more_products',
                    offset: offset,
                    numberOfProducts: numberOfProducts,
                    cats: $('#archive-cat-id').val(),
                    parent: $('#archive-cat-parent').val(),
                    is_search: $('input[name=is_search]').val(),
                    archive_loaded_products: $('#archive_loaded_products').val() || false
                }
                if ($('input[name=refined_gender]').val().length > 0) {
                    data['gender'] = $('input[name=refined_gender]').val();
                }
                if ($('input[name=featured_products]').length > 0) {
                    data['featured_products'] = $('input[name=featured_products]').val();
                }
                if ($('input[name=refined]').val() === 'true') {
                    data['refined'] = true;
                    var values = [];
                    $("input:checkbox[name=price]:checked").each(function (index, element) {
                        if (element.value !== 'all') {
                            values.push($(this).val());
                        }
                    });
                    if (values.length === 0) {
                        $("input:checkbox[name=price]:not(:checked)").each(function () {
                            values.push($(this).val());
                        });
                    }
                    var brands = [];
                    $("input:checkbox[name=brand]:checked").each(function (index, element) {
                        if (element.value !== 'all') {
                            brands.push($(this).val());
                        }
                    });
                    if (brands.length === 0) {
                        $("input:checkbox[name=brand]:not(:checked)").each(function () {
                            brands.push($(this).val());
                        });
                    }
                    var colour = [];
                    $("input:checkbox[name=colour]:checked").each(function (index, element) {
                        colour.push($(this).val());
                    });
                    if (colour.length === 0) {
                        $("input:checkbox[name=colour]:not(:checked)").each(function () {
                            colour.push($(this).val());
                        });
                    }
                    var sizes = [];
                    $("input:checkbox[name=size]:checked").each(function (index, element) {
                        sizes.push($(this).val());
                    });
                    if (sizes.length === 0) {
                        $("input:checkbox[name=size]:not(:checked)").each(function () {
                            sizes.push($(this).val());
                        });
                    }
                    var tags = [];
                    $("input:checkbox[name=tag]:checked").each(function () {
                        tags.push($(this).val());
                    });
                    data['selected'] = $('#archieve-sort-by-select option:selected').val();
                    data['prices'] = values.join() || '19.99,39.99,59.99,60.00';
                    data['brands'] = brands.join() || 'all';
                    data['colours'] = colour.join() || 'all';
                    data['sizes'] = sizes.join() || 'all';
                    data['tags'] = tags.join();
                    data['slug'] = $('#archive-cat-id').val();
                }
                $.post(ajaxurl, data, function (response) {
                    if (response) {
                        $('.archive-product-wrapper').append(response);
                        $('.archive-product-wrapper img').each(function () {
                            element = $(this);
                            if (element.hasClass('wp-post-image')) {
                                if (this.complete) {
                                    $($(this).siblings()).css('display', 'none');
                                    $(this).css('display', 'block');
                                    $($(this).siblings()).remove();
                                }
                                element.load(function () {
                                    $($(this).siblings()).css('display', 'none');
                                    $(this).css('display', 'block');
                                    $($(this).siblings()).remove();
                                });
                            }
                        });
                        var offset = 48;
                        $('input[name=product_offset]').each(function () {
                            offset = $(this).val();
                        });
                        $('input[name=offset_number]').val(offset);
                        updatingProducts = false;
                    } else {
                        //                                $('.archive-product-wrapper').append('<p class="archive-no-more-products-message">No More Products</p>');
                    }
                });
            }
        });
    }
    function click_handler_check(element) {
        var result = false;
        if ($(element).length > 0 && $(element).data().length !== undefined) {
            $.each($(element).data('events'), function (i, event) {
                $.each(event, function (i, handler) {
                    if (handler.type.toString() === 'click') {
                        result = true;
                    }
                });
            });
        }
        return result;
    }
    if ($('.accounts-login-container').length > 0) {
        var login_button = $('button[name=accounts-login]');
        var login_form = $('form[name=accounts-login-form]');
        login_button.click(function () {
            login_button.addClass('disabled');
            var data = {
                'action': 'st_authenticate_user',
                'login': $('input[name=username]').val(),
                'password': $('input[name=password]').val()
            };
            jQuery.post(ajaxurl, data, function (response) {
                if (response === 'true') {
                    login_button.attr({
                        type: 'submit',
                        value: 'submit'
                    });
                    login_form.submit();
                } else {
                    login_button.removeClass('disabled');
                    $('.signin-error').removeClass('hidden');
                }
            });
        });
    }

    if ($('.full-product-reviews-container').length > 0) {
        var data = {
            action: 'st_get_product_reviews',
            post_id: $('#archive-product-right-col-id').val()
        }
        $.post(ajaxurl, data, function (response) {
            var reviews = JSON.parse(response), reviewText = '';
            if (reviews.error) {
                reviewText = '<h6>' + reviews.error + '</h6>';
            } else {
                for (var index in reviews) {
                    var starRating = reviews[index].stars;
                    reviewText += '<div class="full-product-review-container">';
                    reviewText += '<div class="stars">';
                    for (var i = 0; i < starRating; i++) {
                        var starNumber = i + 1;
                        reviewText += '<input class="star star-' + starNumber + '" type="radio" name="star"/><label class="star star-' + starNumber;
                        if (i === 0) {
                            reviewText += ' first-star';
                        }
                        reviewText += '" for="star-5-' + starNumber + '"></label>'
                    }
                    reviewText += '</div>';
                    reviewText += '<h6>' + reviews[index].title + '</h6>';
                    reviewText += '<p>' + reviews[index].review + '</p>';
                    reviewText += '</div>';
                }
            }
            $('.full-product-reviews-container').html(reviewText);
        })
    }

});

function addToCartAjax(button, url = '') {
    if (typeof fbq !== 'undefined') {
        fbq('track', 'AddToCart');
    }
    var data = {
        'action': 'st_add_to_cart',
        'quantity': 1,
        'ID': $(button).prop('id')
    };
    $.post(ajaxurl, data, function (response) {
        $('.nav-header-cart-total').html(response);
        $(button).html('Added To Cart');
        var data = {
            'action': 'st_populate_cart_overlay'
        };
        $.post(ajaxurl, data, function (response) {
            $('.cart-contents').html(response).promise().done(function () {
                remove_cart_item_ajax();
                var data = {
                    'action': 'st_get_cart_total_price'
                };
                $.post(ajaxurl, data, function (response) {
                    $('.cart_total_price').html(response);
                });
            });
            if (url != '')
                window.location.href = url;
        });
    });
}

function remove_cart_item_ajax() {
    $('.remove-item-from-cart').click(function () {
        var id = $(this).attr('id');
        var data = {
            'action': 'st_remove_item',
            'id': id
        };
        jQuery.post(ajaxurl, data, function (response) {
            $('.cart-contents').html(response);
            var data = {
                'action': 'st_get_cart_total'
            };
            $.post(ajaxurl, data, function (response) {
                $('.nav-header-cart-total').html(response);
            });
            var data = {
                'action': 'st_get_cart_total_price'
            };
            $.post(ajaxurl, data, function (response) {
                $('.cart_total_price').html(response);
            });
            remove_cart_item_ajax();
        });
    });
}