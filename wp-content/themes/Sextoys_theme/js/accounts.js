jQuery(document).ready(function ($) {

    var ajaxurl = $('input[name=ajax-url]').val();

    $('select[name=date-filter-select]').change(function () {
        var select = $(this);
        if (select.val() === 'd-r') {
            $('.date-range-container').removeClass('hidden');
        } else {
            if (!$('.date-range-container').hasClass('hidden')) {
                $('.date-range-container').addClass('hidden');
            }
        }
    });

    if ($('#accounts-items-table').length > 0) {
        var datatable = $('#accounts-items-table').DataTable({
            dom: 'lBfrtip',
            renderer: "bootstrap",
            select: true,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            footerCallback: function (row, data, start, end, display) {
                var api = this.api(), data;

                var intVal = function (i) {
                    return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                };

                for (var i = 3; i <= 8; i++) {

                    total = api
                            .column(i)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                    if (i !== 6) {
                        total = parseFloat(total).toFixed(2);
                    }
                    $('.accounts-total-' + i).html(total);
                }
            }
        });

        $('.filter-products').click(function () {
            var overlay = $('.loading-overlay');
            overlay.removeClass('hidden');
            var btn = $(this);
            btn.prop('disabled', true);
            var date = $('select[name=date-filter-select]').val();
            date = get_dates_object(date);
            var vendor = $('select[name=vendors]').val();
            var data = {
                action: 'accounts_filter_products',
                vendor: vendor,
                start_date: date.start_date,
                end_date: date.end_date
            }
            $.post(ajaxurl, data, function (response) {
                if (response && response.length > 0) {
                    datatable.clear();
                    JSON.parse(response).forEach(function (row) {
                        datatable.row.add(row);
                    })
                    datatable.draw();
                    btn.prop('disabled', false);
                    overlay.addClass('hidden');
                } else {
                    btn.prop('disabled', false);
                    overlay.addClass('hidden');
                }
            });
        });

    }

    function get_dates_object(data_selection) {
        var returnObj = {};
        if (data_selection === '7' || data_selection === '14' || data_selection === '31') {
            data_selection = parseInt(data_selection);
            var d = new Date();
            returnObj['start_date'] = d.toDateString();
            d.setDate(d.getDate() - data_selection);
            returnObj['end_date'] = d.toDateString();
        } else {
            if (data_selection === 'd-r') {
                returnObj['end_date'] = new Date($(".from-datepicker").datepicker("getDate").toString()).toDateString();
                returnObj['start_date'] = new Date($(".to-datepicker").datepicker("getDate").toString()).toDateString();
            }
        }
        return returnObj;
    }
})