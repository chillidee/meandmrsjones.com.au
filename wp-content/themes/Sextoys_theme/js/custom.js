jQuery(document).ready(function ($) {

    var innerHeight = document.documentElement.clientHeight;
    var innerWidth = document.documentElement.clientWidth;
    if (document.getElementById('my-account-sidebar') !== null && innerWidth > 1200) {
        if (innerWidth > 1600) {
            document.getElementById('my-account-sidebar').style.height = (innerHeight - 230) + 'px';
            document.getElementById('my-account-content-wrapper').style.height = (innerHeight - 230) + 'px';
        }
    }
    if (document.getElementById('archive-sidebar') !== null) {
        if (innerWidth > 1600) {
            document.getElementById('archive-sidebar').style.height = (innerHeight - 180) + 'px';
        } else {
            document.getElementById('archive-sidebar').style.height = (innerHeight - 165) + 'px';
        }
        if (document.getElementById('archive-products') !== null) {
            if (innerWidth > 1600) {
                if (document.getElementById('all_products_list') === null) {
                    document.getElementById('archive-products').style.height = (innerHeight - 292) + 'px';
                } else {
                    document.getElementById('archive-products').style.height = (innerHeight - 210) + 'px';
                }
            } else {
                if (innerWidth <= 1200) {
                    if (innerWidth <= 767) {
                        document.getElementById('archive-products').style.height = (innerHeight - 140) + 'px';
                    } else {
                        document.getElementById('archive-products').style.height = (innerHeight - 170) + 'px';
                    }
                } else {
                    document.getElementById('archive-products').style.height = (innerHeight - 195) + 'px';
                }
            }
        }
    }
    if ($('#archive-sort-overlay').length > 0 && innerWidth > 1200) {
        if (innerWidth > 1600) {
            $('#archive-sort-overlay').css('height', (innerHeight - 210) + 'px');
        } else {
            $('#archive-sort-overlay').css('height', (innerHeight - 195) + 'px');
        }
    }

    var screenwidth = $('html').innerWidth();
    var screenheight = $('html').innerHeight();
    var screenRatio = (screenwidth / screenheight);
    if ($('.parent-sidebar').length > 0 && !click_handler_check('.parent-sidebar')) {
        $('.parent-sidebar').click(function (event) {
            if (event.eventPhase === 2) {
                event.preventDefault();
                window.location = event.target.firstChild.href;
            }
        });
    }
    var notScrollPages = [''];
    if ($('nav').hasClass('home-nav') && notScrollPages.indexOf(window.location.pathname) === -1) {
        $(window).scroll(function () {
            var nav = $('nav'), menu = 'scrolling-menu';
            if ($('body').scrollTop() > 20) {
                nav.addClass(menu);
                $('.home-header-logo-container').css('display', 'block');
            } else {
                if (nav.hasClass(menu)) {
                    nav.removeClass(menu);
                    $('.home-header-logo-container').css('display', 'none');
                }
            }
        });
    }
    if ($('.archive-product-wrapper').length > 0) {
        if (document.referrer.toString().indexOf('/product/') === -1) {
            $('.archive-product-wrapper img').each(function () {
                element = $(this);
                if (element.hasClass('wp-post-image')) {
//                    if (this.complete) {
//                        $($(this).siblings()).css('display', 'none');
//                        $(this).css('display', 'block');
//                        $($(this).siblings()).remove();
//                    }
//                    element.load(function () {
//                        $($(this).siblings()).css('display', 'none');
//                        $(this).css('display', 'block');
//                        $($(this).siblings()).remove();
//                    });
                }
            });
        } else {
            $('.archive-product-wrapper img').each(function () {
//                element = $(this);
//                $($(this).siblings()).css('display', 'none');
//                $(this).css('display', 'block');
//                $($(this).siblings()).remove();
            });
        }
    }
    if ($('.new-address-save-button-container').length > 0) {
        $('.new-address-save-button-container').click(function () {
            if ($('#newAddressFrom').hasClass('ng-invalid')) {
                $('.new-address-save-button-container p').css('display', 'block');
            }
        });
    }
    if ($('#archive-products').length > 0) {

        $(document).keydown(function (e) {
            if (e.keyCode === 40) {
                var scrollTop = $('.archive-product-wrapper').scrollTop();
                $('.archive-product-wrapper').scrollTop(scrollTop + 50);
            }
            if (e.keyCode === 38) {
                var scrollTop = $('.archive-product-wrapper').scrollTop();
                $('.archive-product-wrapper').scrollTop(scrollTop - 50);
            }
        });

        function replacePlaceholderImage(element) {
            var real_src = element.data('src');
            if (real_src) {
                element.attr('src', real_src);
                element.removeData('src');
            }
        }
        var MOUSE_OVER = false, MOUSE_IN_FILTER = false, productContainer = $('.archive-product-wrapper'), sidebar = $('#archive-sidebar'), sort_overlay = $('#archive-sort-overlay');
        $('.archive-product-image img').each(function () {
            if ($(this).parents('.archive-product-container').offset().top < innerHeight) {
                //replacePlaceholderImage($(this));
            }
        });
        if (innerWidth > 1200) {
            productContainer.scroll(function () {
//                $('.archive-product-image img').each(function () {
//                    if ($(this).parents('.archive-product-container').offset().top > 0 && $(this).parents('.archive-product-container').offset().top < 2500) {
//                        replacePlaceholderImage($(this));
//                    }
//                });
            });
            sort_overlay.mouseenter(function () {
                MOUSE_IN_FILTER = true;
            });
            sort_overlay.mouseleave(function () {
                MOUSE_IN_FILTER = false;
            });
            productContainer.mouseenter(function () {
                MOUSE_OVER = true;
            });
            sidebar.mouseleave(function () {
                MOUSE_OVER = false;
            });
            sidebar.mouseenter(function () {
                MOUSE_OVER = true;
            });
            productContainer.mouseleave(function () {
                MOUSE_OVER = false;
            });
            if (!MOUSE_OVER && !MOUSE_IN_FILTER) {
                $('body').on('wheel', function (e) {
                    var isFirefox = typeof InstallTrigger !== 'undefined';
                    var scrollAmount = e.originalEvent.deltaY;
                    if (isFirefox) {
                        scrollAmount = (scrollAmount < 0) ? scrollAmount - 100 : scrollAmount + 100;
                    }
                    if (scrollAmount && !MOUSE_OVER && !MOUSE_IN_FILTER) {
                        e.preventDefault();
                        productContainer.scrollTop(productContainer.scrollTop() + scrollAmount);
                    }
                });
            }

            $('body').click(function (e) {
                if (!MOUSE_IN_FILTER) {
                    var sortButtonClciked = false;
                    if ($(e.target).html() === 'Sort / ' || $(e.target).attr('id') === 'product-sort-by-button') {
                        sortButtonClciked = true;
                    }
                    var sortOverlay = $('#archive-sort-overlay');
                    if (sortOverlay.hasClass('overlay_open') && !sortButtonClciked) {
                        sortOverlay.removeClass('overlay_open');
                    }
                }
            });
        }
    }

    if (screenwidth <= 1200) {

        window.mobilecheck = function () {
            var check = false;
            (function (a) {
                if (/(android|bb\d+|meego).+mobile|android|ipad|playbook|silk|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))
                    check = true;
            })(navigator.userAgent || navigator.vendor || window.opera);
            return check;
        };
        if (window.mobilecheck() === true) {
            var newHeight = $(window).height() + 170;
            $(".home-background-container").css("height", newHeight);
        }

        $('.content-box').each(function (index, element) {
            $(element).height(innerHeight);
        });
        if ($('#mobile-guest-account-btn').length > 0) {
            $('#mobile-guest-account-btn').click(function () {
                hide_show_overlay($('#mobile-login-overlay'));
            });
        }

        if ($('#mobile-sign-up').length > 0) {
            $('#mobile-sign-up').click(function () {
                closeAllOverlays();
                $('#mobile-signup-overlay').addClass('overlay_open');
            });
            if ($('.signup-overlay-close').length > 0) {
                $('.signup-overlay-close').click(function () {
                    closeAllOverlays();
                    enableDisableScroll();
                });
            }
        }
        if ($('#contact-overlay-mobile-link').length > 0) {
            $('#contact-overlay-mobile-link').click(function () {
                $('#contact-form-overlay').height(innerHeight);
                hide_show_overlay($('#contact-form-overlay'));
            });
        }
    }
    if ($('#continue-to-customer-service-button').length > 0) {
        $('#continue-to-customer-service-button').click(function () {
            switch ($("input:radio[name=group]:checked").val()) {
                case 'order':
                    window.location.href = '/returns-1/';
                    break;
                case 'return':
                    window.location.href = '/returns-1/';
                    break;
                case 'question':                    
                    hide_show_overlay($('#contact-form-overlay'));
                    break;
                case 'else':                    
                    hide_show_overlay($('#contact-form-overlay'));
                    break;
            }
        });
    }

    if ($('.my-account-single-order-email').length > 0) {
        $('.my-account-single-order-email').click(function () {
            if ($('#order-send-pdf').css('display') === 'none') {
                $('#order-send-pdf').css('display', 'block');
            } else {
                $('#order-send-pdf').css('display', 'none');
            }
        });
        $('.order-send-pdf-close').click(function () {
            if ($('#order-send-pdf').css('display') === 'none') {
                $('#order-send-pdf').css('display', 'block');
            } else {
                $('#order-send-pdf').css('display', 'none');
            }
        });
    }

    if ($('#create_account_button')) {
        $('#create_account_button').click(function () {
            hide_show_overlay($('.login-overlay-signup'));
        });
    }

    if ('#billing_address_1_field') {
    }
    setTimeout(function () {
        if ($('.checkout').length > 0) {
            $('#cybersource_accountNumber').on('input', function (event) {
                if (event.target.value.length < 2) {
                    if (event.target.value === '4') {
                        $('select[name=cybersource_cardType]').val('001');
                    }
                    if (event.target.value === '5') {
                        $('select[name=cybersource_cardType]').val('002');
                    }
                }
            });
        }
    }, 1500);
    if ($('.archive-sort-overlay').length > 0) {
        $('.archive-overlay-expland').click(function () {
            if ($(this).next().get(0).style.display === 'block') {
                $(this).next().get(0).style.display = 'none';
                $(this).children().addClass('fa-plus');
                $(this).children().removeClass('fa-minus');
            } else {
                $(this).next().get(0).style.display = 'block';
                $(this).children().removeClass('fa-plus');
                $(this).children().addClass('fa-minus');
            }
        });
    }

    if ($('#thankYouCreateAccountForm').length > 0) {
        var element = angular.element(document.querySelector('#thankYouCreateAccountForm'));
        var isInitialized = element.injector();
        if (!isInitialized) {
            angular.bootstrap(element);
        }
    }

    if ($('#mobile-login').length > 0) {
        $('#mobile-login').click(function () {
            hide_show_overlay($('#mobile-login-overlay'));
            var element = angular.element(document.querySelector('#mobile_loginForm'));
            var isInitialized = element.injector();
            if (!isInitialized) {
                angular.bootstrap(element);
            }
        });
    }

    if ($('.mobile-login-menu-close')) {
        $('.mobile-login-menu-close').click(function () {
            hide_show_overlay($('#mobile-login-overlay'));
        });
    }

    if ($('.signup-overlay-close')) {
        $('.signup-overlay-close').click(function () {
            if (innerWidth > 1200) {
                hide_show_overlay($('.login-overlay-signup'));
            }
        });
    }

    if ('#contact-form-overlay') {

        if ($('#contactForm').length > 0) {
            var element = angular.element(document.querySelector('#contactForm'));
            var isInitialized = element.injector();
            if (!isInitialized) {
                angular.bootstrap(element);
            }
        }

        $('.contact-overlay-close').click(function () {
            hide_show_overlay($('#contact-form-overlay'));
        });
        if ($('.contact-us-link').length > 0) {
            $('.contact-us-link').click(function () {
                hide_show_overlay($('#contact-form-overlay'));
            });
        }

        if ($('.privacy-overlay').length > 0) {
            $('.privacy-overlay-close').click(function () {
                hide_show_overlay($('.privacy-overlay'));
            });
            $('.about-overlay-close').click(function () {
                hide_show_overlay($('.about-us-overlay'));
            });
            $('.faq-overlay-close').click(function () {
                hide_show_overlay($('.faq-overlay'));
            });
            $('.tc-overlay-close').click(function () {
                hide_show_overlay($('.tc-overlay'));
            });
            $('.footer-about-link').click(function () {
                hide_show_overlay($('.about-us-overlay'));
            });
            $('.footer-privacy-link').click(function () {
                hide_show_overlay($('.privacy-overlay'));
            });
            $('.footer-faq-link').click(function () {
                hide_show_overlay($('.faq-overlay'));
            });
            $('.footer-tc-link').click(function () {
                hide_show_overlay($('.tc-overlay'));
            });
        }

        $('#contact-us-button').click(function () {
            hide_show_overlay($('#contact-form-overlay'));
        });
        $('#contactProblemSelect').change(function () {
            $('.contact-problem-error').css('display', 'none');
        });
    }

    if ($('#signupForm').length > 0) {
        var element = angular.element(document.querySelector('#signupForm'));
        var isInitialized = element.injector();
        if (!isInitialized) {
            angular.bootstrap(element);
        }


        $('#signUpOverlayPasswordConfirm').on('input', function () {
            if ($('.password-match-error').css('display') === 'block') {
                $('.password-match-error').css('display', 'none');
            }
        });
        $('#signUpOverlayPassword').on('input', function () {
            if ($('.password-match-error').css('display') === 'block') {
                $('.password-match-error').css('display', 'none');
            }
        });
        $('#signUpOverlayEmail').on('input', function () {
            if ($('.email-match-error').css('display') === 'block') {
                $('.email-match-error').css('display', 'none');
            }
        });
        $('#signUpOverlayEmailConfirm').on('input', function () {
            if ($('.email-match-error').css('display') === 'block') {
                $('.email-match-error').css('display', 'none');
            }
        });
        $('#signUpOverlayDOBDay').on('change', function () {
            if ($('.signup-form-dateofbirth-error').css('display') === 'block') {
                $('.signup-form-dateofbirth-error').css('display', 'none');
            }
        });
        $('#signUpOverlayDOBMonth').on('change', function () {
            if ($('.signup-form-dateofbirth-error').css('display') === 'block') {
                $('.signup-form-dateofbirth-error').css('display', 'none');
            }
        });
        $('#signUpOverlayDOBYear').on('change', function () {
            if ($('.signup-form-dateofbirth-error').css('display') === 'block') {
                $('.signup-form-dateofbirth-error').css('display', 'none');
            }
        });
    }

    if ($('#product-sort-by-button').length > 0) {
        function minOptions() {
            $('.archive-overlay-expland').each(function () {
                if ($(this).next().get(0).style.display === 'block') {
                    $(this).next().get(0).style.display = 'none';
                    $(this).children().addClass('fa-plus');
                    $(this).children().removeClass('fa-minus');
                }
            });
        }
        var animating = false;
        var animatingWidth = '450px';
        if (innerWidth <= 450) {
            animatingWidth = '100%';
        }
        $('.archive-sort-overlay-close').click(function () {
            minOptions();
            if ($('.archive-sort-overlay').css('width') === '0' && !animating) {
                enableDisableScroll(true);
                animating = true;
                $('.archive-sort-overlay').animate({
                    width: animatingWidth,
                }, 500, function () {
                    animating = false;
                });
            } else {
                enableDisableScroll(false);
                animating = true;
                $('.archive-sort-overlay').animate({
                    width: '-=' + animatingWidth,
                }, 500, function () {
                    animating = false;
                });
            }
        });
        $('#product-sort-by-button').click(function () {
            minOptions();
            if ($('.archive-sort-overlay').css('width') === '0px' && !animating) {
                $('#archive-sort-overlay').scrollTop(0);
                animating = true;
                enableDisableScroll(true);
                $('.archive-sort-overlay').animate({
                    width: animatingWidth,
                }, 500, function () {
                    animating = false;
                });
            } else {
                enableDisableScroll(false);
                animating = true;
                $('.archive-sort-overlay').animate({
                    width: '-=' + animatingWidth,
                }, 500, function () {
                    animating = false;
                });
            }
        });
    }

    if ($('.login-overlay-close')) {
        $('.login-overlay-close').click(function () {
            hide_show_overlay($('.login-overlay'));
        });
    }
    if ($('.login-button')) {
        $('.login-button').click(function () {
            if (innerWidth > 1200) {
                hide_show_overlay($('.login-overlay'));
            } else {
                hide_show_overlay($('#mobile-login-overlay'));
            }
        });
    }

    if ($('#my-account-change-password-toggle').length > 0) {
        document.getElementById('my-account-change-password-toggle').checked = false;
        $('#my-account-change-password-toggle').click(function () {
            if ($('.my-account-change-password-container').css('opacity') === '0.3') {
                $('.my-account-change-password-container').css('opacity', '1');
            } else {
                $('.my-account-change-password-container').css('opacity', '0.3');
            }
        });
    }

    if ($('.header-login') && $('.login-overlay')) {
        $('.header-login').click(function () {
            hide_show_overlay($('.login-overlay'));
            if ($('.login-overlay-signup').css('display') === 'block') {
                $('.login-overlay-signup').css('display', 'none');
            }
        });
    }

    if ($('#guest_account_button')) {
        $('#guest_account_button').click(function () {
            hide_show_overlay($('.login-overlay'));
        });
    }

    if ($('.nav-header-cart')) {
        $('.nav-header-cart').click(function () {
            hide_show_overlay($('.cart-overlay'));
        });
    }
    if ($('.cart-overlay-close')) {
        $('.cart-overlay-close').click(function () {
            hide_show_overlay($('.cart-overlay'));
        });
    }

    if ($('.archive-right_col_product_container')) {
        $('.archive-right_col_product_container img').each(function () {
            if (this.complete) {
                $($(this).siblings('.image-placeholder')).css('display', 'none');
                $(this).css('display', 'block');
                $($(this).siblings('.image-placeholder')).remove();
                if (innerWidth > 1200) {
                    add_image_zoom('.attachment-full');
                }
            } else {
                $(this).load(function () {
                    if ($(this).hasClass('wp-post-image')) {
                        $(this).siblings('.image-placeholder').remove();
                        $(this).css('display', 'block');
                        if (innerWidth > 1200) {
                            add_image_zoom('.attachment-full');
                        }
                    }
                    if ($(this).hasClass('gallery-image') || $(this).hasClass('product-full-related-img')) {
                        $(this).siblings('.image-placeholder').remove();
                        $(this).css('display', 'block');
                    }
                });
            }
        });
    }

    function add_image_zoom(imageName, imagePath) {
        $(imageName).wrap('<span style="display:block"></span>').css('display', 'block').parent().zoom({magnify: 2});
    }


    function hide_show_overlay(overlayClass) {
        enableDisableScroll();
        if (overlayClass) {
            var is_open = overlayClass.hasClass('overlay_open');
        }
        $('div').each(function (index, element) {
            if ($(element).hasClass('overlay_open')) {
                $(element).removeClass('overlay_open');
            }
        });
        if (!is_open && overlayClass) {
            overlayClass.addClass('overlay_open');
            enableDisableScroll();
        }
    }

    function closeAllOverlays() {
        $('div').each(function (index, element) {
            if ($(element).hasClass('overlay_open')) {
                $(element).removeClass('overlay_open');
            }
        });
    }

    function click_handler_check(element) {
        var result = false;
        if ($(element).length > 0 && $(element).data().length !== undefined) {
            $.each($(element).data('events'), function (i, event) {
                $.each(event, function (i, handler) {
                    if (handler.type.toString() === 'click') {
                        result = true;
                    }
                });
            });
        }
        return result;
    }
    if ($('.archive-cat-title').length > 0) {
        $('.parent-sidebar span').click(function (e) {
            if (!$(this).parent().hasClass('sub-menu-open')) {
                e.preventDefault();
                $('.sidebar-sub-menu').each(function (index, element) {
                    if ($(element).css('display') === 'block') {
                        $(element).parent().removeClass('sub-menu-open');
                        $(element).css('display', 'none');
                    }
                });
                $(this).parent().addClass('sub-menu-open');
                $(this).parent().children('ul').css('display', 'block');
            } else {
                $('.sidebar-sub-menu').each(function (index, element) {
                    if ($(element).css('display') === 'block') {
                        $(element).parent().removeClass('sub-menu-open');
                        $(element).css('display', 'none');
                    }
                });
            }
        });
        $('.archive-cat-title').click(function (e) {
            if (!$(this).parent().hasClass('sub-menu-open')) {
                e.preventDefault();
                $('.sidebar-sub-menu').each(function (index, element) {
                    if ($(element).css('display') === 'block') {
                        $(element).parent().removeClass('sub-menu-open');
                        $(element).css('display', 'none');
                    }
                });
                $(this).parent().addClass('sub-menu-open');
                $(this).parent().children('ul').css('display', 'block');
            }
        });
    }
    if (innerWidth < 1200) {

        if ($('.mobile-login-overlay-content-wrapper').length > 0) {
            $('#mobile_login-username').focus(function () {
                if ($('.signin-error').html().length > 0) {
                    $('.signin-error').html('');
                }
            });
            $('#mobile_login-password').focus(function () {
                if ($('.signin-error').html().length > 0) {
                    $('.signin-error').html('');
                }
            });
        }

        function background_fix(element) {
            if ($('main').height() <= innerHeight) {
                $('html').height(innerHeight);
                $('main').height(innerHeight);
            }
        }
        if ($('#about-overlay-mobile-link').length > 0 && !click_handler_check('#about-overlay-mobile-link')) {
            $('#about-overlay-mobile-link').click(function () {
                hide_show_overlay($('.about-us-overlay'));
            });
        }

        if ($('#faq-overlay-mobile-link').length > 0 && !click_handler_check('#faq-overlay-mobile-link')) {
            $('#faq-overlay-mobile-link').click(function () {
                hide_show_overlay($('.faq-overlay'));
            });
        }

        if ($('#terms-overlay-mobile-link').length > 0 && !click_handler_check('#terms-overlay-mobile-link')) {
            $('#terms-overlay-mobile-link').click(function () {
                hide_show_overlay($('.tc-overlay'));
            });
        }

        if ($('#privacy-overlay-mobile-link').length > 0 && !click_handler_check('#privacy-overlay-mobile-link')) {
            $('#privacy-overlay-mobile-link').click(function () {
                hide_show_overlay($('.privacy-overlay'));
            });
        }

        if ($('.mobile-menu-tigger').length > 0) {
            $('.mobile-menu-tigger').click(function () {
                if ($('#mobile-product-menu-wrapper').css('width') === innerWidth + 'px') {
                    $('#mobile-product-menu-wrapper').css('width', '0%');
                    $('#mobile-product-menu p').removeClass('mobile-product-menu-open');
                    enableDisableScroll();
                }
                if ($('.mobile-menu-overlay').hasClass('overlay_open')) {
                    $('.mobile-menu-overlay').removeClass('overlay_open');
                    enableDisableScroll();
                } else {
                    closeAllOverlays();
                    $('.mobile-menu-overlay').addClass('overlay_open');
                    enableDisableScroll();
                }
            });
            $('.mobile-menu-close').click(function () {
                if ($('.mobile-menu-overlay').hasClass('overlay_open')) {
                    $('.mobile-menu-overlay').removeClass('overlay_open');
                    enableDisableScroll();
                }
            });
        }

        if ($('#mobile-product-menu-wrapper').length > 0 && !click_handler_check('#mobile-product-menu-wrapper')) {
            var wrapper = $('#mobile-product-menu-wrapper');
            var trigger = $('#mobile-product-menu p');
            var size = "50%";
            if (window.matchMedia('(max-width: 767px)').matches) {
                size = "70%";
            }
            trigger.click(function () {
                enableDisableScroll();
                (trigger.hasClass('mobile-product-menu-open')) ? (trigger.removeClass('mobile-product-menu-open'), wrapper.animate({
                    width: "0%"
                }, 'fast')) : (trigger.addClass("mobile-product-menu-open"), wrapper.animate({
                    width: size
                }, 'fast'));
            });
        }

        function close_product_menu(trigger, wrapper) {
            (trigger.hasClass('mobile-product-menu-open')) ? (trigger.removeClass('mobile-product-menu-open'), wrapper.animate({
                width: "0%"
            }, 'fast'), function () {
                enableDisableScroll(false);
            }) : '';
        }

    }

    if ($('#get_below_fold_content').length > 0) {
        var body = $('body');
        var html = $('html');
        var main = $('main');
        if (body.innerWidth() > 1500) {
            html.css('overflow', 'hidden');
        }
        $('#get_below_fold_content').click(function () {
            changeArrow($(this), true);
        });
        function changeArrow(arrow, click) {
            if (arrow.hasClass('fa-chevron-down')) {
                arrow.removeClass('fa-chevron-down');
                arrow.addClass('fa-chevron-up');
                if (click) {
                    main.animate({scrollTop: $('.below-fold-content').offset().top}, 500);
                    body.animate({scrollTop: $('.below-fold-content').offset().top}, 500);
                    html.animate({scrollTop: $('.below-fold-content').offset().top - 100}, 500);
                    html.css('overflow', 'auto');
                }
            } else {
                arrow.removeClass('fa-chevron-up');
                arrow.addClass('fa-chevron-down');
                if (click) {
                    main.animate({scrollTop: 0}, 500);
                    body.animate({scrollTop: 0}, 500);
                    html.animate({scrollTop: 0}, 500);
                    html.css('overflow', 'hidden');
                }
            }
        }
    }
    if ($('.archive-product-wrapper').length > 0) {
        if (document.referrer.toString().indexOf('/product/') !== -1) {
            $('.archive-product-wrapper').scrollTop(parseInt(sessionStorage.getItem('scrollPosition')));
        } else {
            sessionStorage.setItem('scrollPosition', 0);
        }

        $('.archive-product-container a').click(function () {
            sessionStorage.setItem('scrollPosition', $('.archive-product-wrapper').scrollTop());
        });
    }
    if ($('.product-delivery-information-new-wrapper').length > 0) {
        $('.show-dropdown-info').click(function () {
            if ($(this).next().children().hasClass('hidden')) {
                $(this).removeClass('fa-caret-down');
                $(this).addClass('fa-caret-up');
                $(this).next().children().removeClass('hidden')
            } else {
                $(this).next().children().addClass('hidden');
                $(this).removeClass('fa-caret-up');
                $(this).addClass('fa-caret-down');
            }
        });
    }

    function enableDisableScroll(selector) {
        if (innerWidth < 1200) {
            ($("body").hasClass("modal-open") ? $("body").removeClass("modal-open") : $("body").addClass("modal-open"));
        }
    }

    $('.shipping-packaging-link').mouseenter(function () {
        $('.product_shipping-example').removeClass('hidden');
    }).mouseleave(function () {
        $('.product_shipping-example').addClass('hidden');
    })

    if ($('.close-home-popup').length > 0) {
        $('.close-home-popup').click(function () {
            $(this).parent().addClass('hidden');
        });
        setTimeout(function () {
            $('.home-popup').removeClass('hidden')
        }, 6000);
    }

});