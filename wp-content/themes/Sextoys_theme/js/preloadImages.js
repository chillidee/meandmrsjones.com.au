jQuery(document).ready(function ($) {

    var width = $('html').width();

    if ($('.home-middle-buttons-container').length > 0) {
        if (width > 1400) {
            $('<img/>').attr('src', 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/home-background.jpg').load(function () {
                $(this).remove();
                $('#image-placeholder').fadeOut('fast');
                $('#image-placeholder').remove();
            });
        }
        if (width > 1240 && width < 1400) {
            $('<img/>').attr('src', 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-lan-1400.jpg').load(function () {
                $(this).remove();
                $('#image-placeholder').fadeOut('fast');
                $('#image-placeholder').remove();
            });
        }
        if (width >= 600 && width <= 768) {
            $('<img/>').attr('src', 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-port.jpg').load(function () {
                $(this).remove();
                $('<img/>').attr('src', 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-lan-1024.jpg').load(function () {
                    $(this).remove();
                    $('#image-placeholder').fadeOut('fast');
                    $('#image-placeholder').remove();
                });
            });
        }
        if (width <= 320) {
            $('<img/>').attr('src', 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-port-iphone320.jpg').load(function () {
                $(this).remove();
                $('#image-placeholder').fadeOut('fast');
                $('#image-placeholder').remove();
            });
        }
        if (width >= 321 && width <= 375) {
            $('<img/>').attr('src', 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-port-iphone375.jpg').load(function () {
                $(this).remove();
                $('#image-placeholder').fadeOut('fast');
                $('#image-placeholder').remove();
            });
        }
        if (width >= 376 && width <= 414) {
            $('<img/>').attr('src', 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-port-iphone375.jpg').load(function () {
                $(this).remove();
                $('#image-placeholder').fadeOut('fast');
                $('#image-placeholder').remove();
            });
        }
        if (width > 414 && width <= 600) {
            $('<img/>').attr('src', 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-port-600.jpg').load(function () {
                $(this).remove();
                $('<img/>').attr('src', 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-lan-480.jpg').load(function () {
                    $(this).remove();
                    $('#image-placeholder').fadeOut('fast');
                    $('#image-placeholder').remove();
                });
            });
        }

        if (width >= 769 && width <= 1200) {
            $('<img/>').attr('src', 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-port-1200.jpg').load(function () {
                $(this).remove();
                $('<img/>').attr('src', 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-lan-1024.jpg').load(function () {
                    $(this).remove();
                    $('#image-placeholder').fadeOut('fast');
                    $('#image-placeholder').remove();
                });
            });
        }
    }
    $.preload(
            'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/for-her-toys.jpg',
            'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/for-her-apparel.jpg',
            'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/for-her-fetish.jpg',
            'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/for-her-essentials.jpg',
            'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/for-her-gg.jpg',
            'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/subcat-for-him-toys.jpg',
            'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/subcat-for-him-apparel.jpg',
            'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/subcat-for-him-fetish.jpg',
            'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/subcat-for-him-essentials.jpg',
            'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/subcat-for-him-gg.jpg',
            'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/subcat-us-toys.jpg',
            'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/subcat-us-fetish.jpg',
            'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/subcat-us-ess.jpg',
            'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/subcat-us-games-1.jpg'
            );
    if (width > 1200) {
        $.preload(

                );
    }
    if (width >= 768 && width <= 1200) {
        $.preload('https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-her-product-background-port-768.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/account-background-port-768.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/checkout-port-768.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-her-background-port-768.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-him-port-768.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-him-product-port-768.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-us-port-768.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-us-product-port-768.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-her-lan-1024.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-her-banner-mobile-lan.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-him-lan-1024.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-us-product-lan-1024.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-us-lan-1024.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/account-background-lan-1024.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/checkout-lan-1024.jpg'
                );
    }
    if (width <= 767) {
        $.preload(
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-her-port-450.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-her-product-port-450.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/account-port-450.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/checkout-port-450.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-him-port-450.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-him-product-port-450.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-us-port-450.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-us-product-port-450.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/account-lan-450.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/checkout-lan-450.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-her-product-lan-450.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-her-lan-450.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-him-product-lan-450.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-him-lan-450.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-us-product-lan-450.jpg',
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-us-lan-450.jpg'
                );
    }
    if (width <= 1200) {
        $.preload(
                'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/mobile-menu-icon.png'
                );
    }
});