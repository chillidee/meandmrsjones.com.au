jQuery(document).ready(function ($) {
    if ($('.type-product').length > 0) {
        $('.type-product').each(function (index, element) {
            var wholesalePrice = parseFloat(($(element).children('._wholesale_price').html()));
            var salePrice = $(element).children().find('.amount').html().toString().split('<span class="woocommerce-Price-currencySymbol">$</span>');
            salePrice = parseFloat(salePrice[salePrice.length - 1]);
            var markup1 = (wholesalePrice / 100);
            if (salePrice <= ((markup1 * 100) + wholesalePrice)) {
                $(this).css('background-color', '#f0ad4e');
            }
            if (salePrice <= ((markup1 * 50) + wholesalePrice)) {
                $(this).css('background-color', '#d9534f');
            }
        });
    }
});