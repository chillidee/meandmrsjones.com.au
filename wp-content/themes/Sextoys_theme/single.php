<?php
/**
 * The Template for displaying all single posts
 *
 */
/* -Redirect if user has no packages--- */
get_header();
?>
<div class="sub-cat-background-image">    
</div>
<div class="blog-single col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class = "col-lg-12 prodct-cat-background-image"></div>
    <?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
            ?>            
            <div class="col-lg-7 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 main-content">
                <div class="cart-checkout-header col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h1><p><?php the_title(); ?></p></h1>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-content-wrapper">
                    <?php the_content(); ?>
                </div>
            </div>
            <?php
        } // end while
    } // end if
    ?>
</div>
<?php get_footer();
?>
