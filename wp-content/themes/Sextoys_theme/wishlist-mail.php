<?php
require_once('../../../wp-blog-header.php');
if (!isset($_POST['action'])):
    ?>
    <body style="padding:0;margin:0;">
        <div style="width:100%;height:100%" class="wishlist-email-container">
            <i class="fa fa-times" id="email-wishlist-box-close"></i>
            <img style="width:100%;height:auto;" src="/test123/wp-content/uploads/2016/03/sign_in_image.png">
            <div id="send-wishlist-text-container" style="width:80%;height:65%;margin:0 10%">
                <h2>EMAIL WISHLIST</h2>
                <form name="wishlist_email_form" ng-app novalidate>
                    <h3>From</h3>
                    <p>Please enter your name this will be used for the email.</p>
                    <input ng-model="wishlistName" id="send-wishlist-name" name="name" required/>
                    <h3>To</h3>
                    <p>Love it? Share it! Enter up to 10 email addresses of friends or family, separated by a comma.</p>
                    <input ng-model="wishlistEmails" id="send-wishlist-emails" name="emails" required/>
                    <h3>Message</h3>
                    <textarea id="send-wishlist-message" name="message"></textarea>
                    <?php
                    for ($i = 0; $i <= count($_POST['ids']) - 1; $i++):
                        $id = $_POST['ids'][$i];
                        $id = esc_attr($id);
                        $id = explode('-', $id);
                        $id = $id[1];
                        $quantity = $_POST['quantitys'][$i];
                        $quantity = esc_attr($quantity);
                        ?>
                        <input id="product-<?php echo $id; ?>" class="email-product-id" type="hidden" value="<?php echo $id; ?>" />
                        <input id="quantity-<?php echo $id; ?>" class="email-product-quantity" type="hidden" value="<?php echo $quantity ?>" />
                        <?php
                    endfor;
                    ?>
                    <button ng-disabled="!wishlist_email_form.$valid" id="send-wishlist-button" type="button">Send</button>
                </form>
            </div>
        </div>
        <script>
            jQuery(document).ready(function ($) {

                $('#email-wishlist-box-close').click(function () {
                    $('#email-wishlist-frame').hide();
                });

                $('#send-wishlist-button').click(function () {
                    //$('#send-wishlist-button').attr('disabled', 'true');
                    $('#send-wishlist-button').addClass('disabled');
                    var firstName = $('#send-wishlist-name').val();
                    var emails = $('#send-wishlist-emails').val();
                    var message = $('#send-wishlist-message').val();
                    var ids = [];
                    var quantitys = [];
                    $('.email-product-id').each(function (index) {
                        ids[index] = $(this).val();
                    });
                    $('.email-product-quantity').each(function (index) {
                        quantitys[index] = $(this).val();
                    });
                    var data = {
                        'action': 'send',
                        'first-name': firstName,
                        'emails': emails,
                        'wishlist-message': message,
                        'ids': ids,
                        'quantitys': quantitys
                    };
                    $.post('/test123/wp-content/themes/Sextoys_theme/wishlist-mail.php', data, function (response) {
                        if (response) {
                            $('#send-wishlist-text-container').html("<p class='wishlist-email-response'>Thanks " + firstName + " your WishList has been sent.</p>");
                        } else {
                            $('#send-wishlist-text-container').html("<p class='wishlist-email-response'>Sorry " + firstName + " there has been a problem please close and try again.</p>");
                        }
                    });
                });
            });
        </script>
    </body>
    <?php
else:
    $name = esc_attr($_POST['first-name']);
    $nameString = '&fname=' . $name;
    $ids = $_POST['ids'];
    $quantitys = $_POST['quantitys'];
    $cartString = 'checkout/?';
    $cartString1 = 'wishlist-cart/?';
    $emails = $_POST['emails'];
    $send_wishlist_message = strip_tags($_POST['wishlist-message']);
    $emails = explode(',', $emails);
    $noOfEmails = count($emails);
    $subject = $name . ' has emailed you their wishlist';

    $headers = array();
    array_push($headers, 'MIME-Version: 1.0');
    array_push($headers, 'Content-Type: text/html');
    array_push($headers, 'charset=UTF-8');
    array_push($headers, 'From: SexToyss.com.au <shop@sextoyss.com.au>');

    $message = '<html>';
    $message .= '<head><title>Wishlist from Sextoyss</title></head>';
    $message .= '<img src="http://dev.sextoyss.com.au/test123/logo.png" style="width:300px;margin:20px 150px">';
    $message .= '<body><p>Here are the items ' . $name . ' would like</p>';
    $message .= '<p>';
    $message .= '<p>' . $send_wishlist_message . '</p>';
    $message .= '</p>';
    $message .= '<table style="margin-bottom:20px;width:600px;"><tbody>';
    global $woocommerce;
    $_pf = new WC_Product_Factory();
    $i = 0;
    $getIds = 'id=';
    $getQuan = '&quantity=';
    foreach ($ids as $id):
        $message .= '<tr style="width:600px">';
        $_product = $_pf->get_product(esc_attr($id));
        $message .= '<td style="width:150px">';
        $message .= $_product->get_image();
        $message .= '</td>';
        $message .= '<td style="width:150px;text-align: center;"><a href="' . $_product->get_permalink() . '">';
        $message .= $_product->get_title();
        $message .= '</a>';
        $message .= '</td>';
        $message .= '<td style="width:150px;text-align: center;">Quantity ';
        $message .= esc_attr($quantitys[$i]);
        $message .= '</td>';
        $message .= '<td style="width:150px;text-align: center;">Price ';
        $message .= $_product->get_price_html();
        $message .= '</td>';
        $message .= '</tr>';
        if (count($ids) - 1 != $i):
            $getIds .= esc_attr($id);
            $getIds .= ',';
            $getQuan .= esc_attr($quantitys[$i]);
            $getQuan .= ',';
        else:
            $getIds .= esc_attr($id);
            $getQuan .= esc_attr($quantitys[$i]);
        endif;
        $i++;
    endforeach;
    $cartString1 .= $getIds;
    $cartString1 .= $getQuan;
    $cartString1 .= $nameString;
    $message .= '<tr style="width:600px"><td style="width:300px; margin-left:150px"><a style="text-decoration: none;color:#fff;width:300px;background-color: rgb(57,159,205);padding: 10px 5px;" href="http://dev.sextoyss.com.au/test123/';
    $message .= $cartString1;
    $message .= '">Buy Now</a></td></tr>';
    $message .= '</tbody></table>';
    $message .= '</body></html>';
    $result = wp_mail($emails, $subject, $message, $headers);
    echo($result);
endif;
?>