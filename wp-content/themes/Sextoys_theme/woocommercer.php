<?php
/**
 * The template for displaying all pages
 * Using an if statement to check for the thankyou page, and if not present, outputting the packages page
 */
get_header(); ?>
		   <?php woocommerce_content(); ?>
<?php get_footer(); ?>