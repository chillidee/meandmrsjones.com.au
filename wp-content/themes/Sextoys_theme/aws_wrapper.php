<?php

require 'aws/aws-autoloader.php';
require 'aws-config.php';

use Aws\Common\Aws;
use Guzzle\Http\EntityBody;
use Aws\Ses\SesClient;

class Aws_Wrapper {

    private static function get_credentials() {
        return aws_get_config();
    }

    public static function test() {
        st_var_dump(Aws_Wrapper::get_s3_insance());
    }

    private static function get_s3_insance() {
        try {
            $aws = Aws::factory(Aws_Wrapper::get_credentials());
            $s3client = $aws->get('s3');
            return $s3client;
        } catch (Exception $e) {
            st_var_dump($e);
        }
    }

    public function aw_get_file() {
        $s3 = get_s3_insance();
        $result = $s3->getObject([
            'Bucket' => 'my-adult-toys',
            'Key' => 'adult_toys_key'
        ]);
        st_var_dump($result);
    }

    public static function aw_put_file($s3, $location, $object_name, $object, $type) {
        if (!$s3):
            $s3 = Aws_Wrapper::get_s3_insance();
        endif;
        $result = $s3->putObject([
            'ACL' => 'public-read',
            'Bucket' => 'adult-toys',
            'Key' => $location . $object_name,
            'Body' => EntityBody::factory(fopen($object, 'r+')),
            'ContentType' => $type
        ]);
        return $result;
    }

    public function aw_get_image_path($image_name, $imageType) {
        if (is_array($image_name)):
            return 'https://adult-toys.s3.amazonaws.com/images/' . $imageType . '/' . $image_name[count($image_name) - 1];
        else:
            return 'https://adult-toys.s3.amazonaws.com/images/' . $imageType . '/' . $image_name;
        endif;
    }

    public function aw_get_150_image_path($image_name, $imageType) {
        if (is_array($image_name)):
            $image_explode = explode('.jpg', $image_name[count($image_name) - 1]);
            return 'https://adult-toys.s3.amazonaws.com/images/' . $imageType . '/' . $image_explode[0] . '-150x150.jpg';
        else:
            $image_explode = explode('.jpg', $image_name);
            return 'https://adult-toys.s3.amazonaws.com/images/' . $imageType . '/' . $image_explode[0] . '-150x150.jpg';
        endif;
    }

    public function aw_get_300_image_path($image_name, $imageType) {
        if (is_array($image_name)):
            $image_explode = explode('.jpg', $image_name[count($image_name) - 1]);
            return 'https://adult-toys.s3.amazonaws.com/images/' . $imageType . '/' . $image_explode[0] . '-300x300.jpg';
        else:
            $image_explode = explode('.jpg', $image_name);
            return 'https://adult-toys.s3.amazonaws.com/images/' . $imageType . '/' . $image_explode[0] . '-300x300.jpg';
        endif;
    }

    public function aw_get_root_path() {
        return 'https://s3.amazonaws.com/adult-toys/';
    }

    public function aw_get_low_res_path() {
        return 'https://s3.amazonaws.com/adult-toys/images/low-res/';
    }

    public static function aw_move_image($imagePath, $origin, $desination) {

        $image_path_array = explode('/', $imagePath);
        $imageName = $image_path_array[count($image_path_array) - 1];

        $sourceBucket = 'adult-toys';
        $sourceKeyname = 'images/' . $origin . '/' . $imageName;
        $targetBucket = 'adult-toys';
        $targetKeyname = 'images/' . $desination . '/' . $imageName;
        $s3 = Aws_Wrapper::get_s3_insance();
        $result = '';
        try {
            $result = $s3->copyObject(array(
                'ACL' => 'public-read',
                'Bucket' => $targetBucket,
                'Key' => $targetKeyname,
                'CopySource' => "{$sourceBucket}/{$sourceKeyname}",
            ));
        } catch (Exception $e) {
            $result = $e;
        }
        return $result;
    }

    public static function aw_move_array_images($imageArray, $origin, $desination) {
        foreach ($imageArray as $image):
            $sourceBucket = 'adult-toys';
            $sourceKeyname = 'images/' . $origin . '/' . $image;
            $targetBucket = 'adult-toys';
            $targetKeyname = 'images/' . $desination . '/' . $image;
            $s3 = Aws_Wrapper::get_s3_insance();
            try {
                $result = $s3->copyObject(array(
                    'ACL' => 'public-read',
                    'Bucket' => $targetBucket,
                    'Key' => $targetKeyname,
                    'CopySource' => "{$sourceBucket}/{$sourceKeyname}",
                ));
            } catch (Exception $e) {
                $result = $e;
            }
        endforeach;
    }

    public static function get_list_of_images($s3Client, $location) {
        $keysArray = array();
        $imageNames = array();
        if (!$s3Client):
            $s3Client = Aws_Wrapper::get_s3_insance();
        endif;
        $bucket = 'adult-toys';
        $objects = $s3Client->getIterator('ListObjects', array(
            'Bucket' => $bucket,
            'Prefix' => 'images/' . $location . '/'
        ));
        foreach ($objects as $object) {
            array_push($keysArray, $object['Key']);
        }
        unset($objects);
        foreach ($keysArray as $key):
            $imageArray = explode('/', $key);
            array_push($imageNames, $imageArray[count($imageArray) - 1]);
        endforeach;
        return $imageNames;
    }

    public static function check_if_image_exists($s3, $imageName, $type) {
        if (!$s3):
            $s3 = Aws_Wrapper::get_s3_insance();
        endif;
        return $s3->doesObjectExist(aws_config_get_bucket(), 'images/' . $type . '/' . $imageName);
    }

    public static function aw_put_high_res_image($imageName, $ftpPath, $newName) {
        $s3 = Aws_Wrapper::get_s3_insance();
        if (check_if_image_exists($s3, $imageName, 'high-res')):
            st_var_dump($s3->putObject(array(
                        'Bucket' => 'adult-toys',
                        'Key' => 'images/high-res/' . $newName . '.jpg',
                        'Body' => EntityBody::factory(fopen($ftpPath, 'r+'))
                    ))
            );
        endif;
    }

    public static function aw_delete_object($imageName, $type) {
        $s3 = Aws_Wrapper::get_s3_insance();

        $bucket = 'adult-toys';
        $keyname = 'images/' . $type . '/ ' . $imageName . '.jpg';

        $result = $s3->deleteObject(array(
            'Bucket' => $bucket,
            'Key' => $keyname
        ));

        return $result;
    }

    private function get_ses_client() {
        try {
            return SesClient::factory(array(
                        'key' => AWS_ID,
                        'secret' => AWS_SECERT,
                        'region' => AWS_REGION
            ));
        } catch (Exception $e) {
            st_var_dump($e);
        }
    }

    function aws_sendemail($to, $from, $subject, $email, $attachment) {
        $client = $this->get_ses_client();
        $msg = array();
        $msg['Source'] = "matt@chillidee.com.au";
        $msg['SourceArn'] = "arn:aws:ses:us-east-1:964628428796:identity/matt@chillidee.com.au";
        $msg['Destination']['ToAddresses'][] = $to;

        $msg['Message']['Subject']['Data'] = $subject;
        $msg['Message']['Subject']['Charset'] = "UTF-8";

        $msg['Message']['Body']['Html']['Data'] = $email;
        $msg['Message']['Body']['Html']['Charset'] = "UTF-8";

        $msg['Message']['Body']['Text']['Data'] = $email;
        $msg['Message']['Body']['Text']['Charset'] = "UTF-8";

        try {
            $result = $client->sendEmail($msg);
        } catch (Exception $e) {
            st_var_dump($e);
        }
        return $result;
    }

}
