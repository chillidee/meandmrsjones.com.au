<?php
/**
 * Template Name: Accounts
 */
wp_head();
if ($_SERVER['REMOTE_ADDR'] == '::1' || $_SERVER['REMOTE_ADDR'] == '127.0.0.1' || $_SERVER['REMOTE_ADDR'] == '203.27.178.78' || $_SERVER['REMOTE_ADDR'] == '139.162.41.102'):
    ?>
    <?php
    if (is_user_logged_in() && current_user_can('manage_options')):
        require get_template_directory() . '/functions/account_class.php';
        $account_class = new account_class();
        require WP_PLUGIN_DIR . '/warehouse/includes/vendors_classes.php';
        $vendor_class = new vendor_functions();
        $vendors = $vendor_class->vendor_get_vendors();
        $totalsArray = array();
        ?>
        <input name="ajax-url" type="hidden" value="<?php echo admin_url('admin-ajax.php') ?>"/>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 accounts-container">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 accounts-top-bar">
                <div class="col-l-5 col-md-5 col-xs-5 col-sm-5 summary-container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p class="items-summary-trigger summary-text-active">Summary</p>
                        <p>/</p>
                        <p class="orders-summary-trigger">Detailed</p>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p class="date-range-text">Last 7 Days</p>
                    </div>
                </div>
                <div class="col-l-5 col-md-5 col-xs-5 col-sm-5 filter-container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4">
                            <label for="vendors">Vendor</label>
                            <select name="vendors">
                                <option value="all">All</option>
                                <?php foreach ($vendors as $vendor): ?>
                                    <option value="<?php echo $vendor->post_title ?>"><?php echo $vendor->post_title ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4">
                            <label for="date-filter-select">Date</label>
                            <select name="date-filter-select">
                                <option value="7">Last 7 Days</option>
                                <option value="14">Last 14 Days</option>
                                <option value="31">Last Month</option>
                                <option value="d-r">Date Range</option>
                            </select>
                            <div class="date-range-container col-lg-12 hidden">
                                <p>From: <input type="text" class="datepicker from-datepicker"></p>
                                <p>To:   <input type="text" class="datepicker to-datepicker"></p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4">
                            <button class="btn btn-info filter-products">Filter</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-xs-2 col-sm-2 logout-container">
                    <a href="<?php echo wp_logout_url(get_permalink()); ?>">Logout</a>
                </div>
            </div>
            <main>
                <div class="loading-overlay hidden">
                    <i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>
                    <span class="sr-only">Loading...</span>
                </div>
                <table id="accounts-items-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <?php
                            foreach ($account_class->accounts_get_column_header_array() as $value):
                                echo "<td>$value</td>";
                            endforeach;
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($account_class->account_get_defualt_products() as $item):
                            echo '<tr>';
                            foreach ($item as $_item):
                                echo "<td>$_item</td>";
                            endforeach;
                            echo '</tr>';
                        endforeach;
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="3">Totals:</th>
                            <th>$<span class="accounts-total-3"></span></th>
                            <th>$<span class="accounts-total-4"></span></th>
                            <th>$<span class="accounts-total-5"></span></th>
                            <th><span class="accounts-total-6"></span> items</th>
                            <th>$<span class="accounts-total-7"></span></th>
                            <th>$<span class="accounts-total-8"></span></th>
                        </tr>
                    </tfoot>
                </table>
            </main>
        </div>
        <?php
    else:
        ?>
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 accounts-login-container">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 accounts-login-background"></div>
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 accounts-login-wrapper">
                <img src="https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/header-logo.png" alt="Me &amp; Mrs Jones">
                <form method="POST" name="accounts-login-form">
                    <label for="username">Username</label>
                    <input type="text" name="username"/>
                    <label for="password">Password</label>
                    <input name="password" type="password"/>
                    <button type="button" value="Login" name="accounts-login">Login</button>
                </form>
                <p class="signin-error hidden">Incorrect Username or Password</p>
            </div>
        </div>
        <?php
        wp_footer();
    endif;
else:
    echo '<script type="text/javascript">
           window.location = "/"
      </script>';
endif;
?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
<link href="<?php echo get_template_directory_uri() . '/css/accounts.css' ?>" rel="stylesheet"/>
<script src="<?php echo get_template_directory_uri() . '/js/accounts.js' ?>"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.js"></script>
<script>
    $(function () {
        $(".datepicker").datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true
        });
    });
</script>