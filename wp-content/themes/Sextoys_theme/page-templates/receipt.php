<?php

/**
 * Template Name: Receipt
 */
st_get_pdf_plugin();
st_get_pdf_class();
$order_id = strip_tags($_POST['order_id']);
$pdf_class = new st_pdf();
$html = $pdf_class->st_pdf_html($order_id);
$pdf = new mPDF();
$pdf->WriteHTML($html);
$pdf->Output();
