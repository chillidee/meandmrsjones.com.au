<?php
/**
 * Template Name: Guide Page
 */
get_header();
?>
<div class="guide about-page">
    <div class="sub-cat-background-image">
    </div>
    <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 sub-page-cat-container">
        <div class="cart-checkout-header col-lg-7 col-lg-offset-1 col-xs-12 col-sm-12 col-md-12 no-padding">
            <h1><?php the_field('page_header_text'); ?></h1>
        </div>
        <div class="col-lg-7 col-lg-offset-1 col-xs-12 col-sm-12 col-md-12 empty-cart-container no-padding">
            <?php the_content(); ?>
        </div>
    </div>
</div>



<?php get_footer(); ?>