<?php
/**
 * Template Name: Links Page
 */
get_header();
?>
<style>
    a{
        color: #851D19;
        cursor: pointer;
    }
</style>

<div class="guide about-page">
    <div class="sub-cat-background-image">
    </div>
    <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 sub-page-cat-container" style="padding-bottom:20px">
        <div class="cart-checkout-header col-lg-7 col-lg-offset-1 col-xs-12 col-sm-12 col-md-12 no-padding">
            <h1><?php the_title(); ?></h1>
        </div> 
        <?php $index = 0; ?>
        <?php foreach (explode(',', get_post_meta($post->ID, '_product_image_gallery', true)) as $attachment_id): ?>
            <?php if($index % 2 == 0): ?>
                <div style="margin-top:20px" class="col-lg-4 col-lg-offset-1 col-md-6 col-xs-12 col-sm-12">
            <?php else: ?>
                <div style="margin-top:20px" class="col-lg-4 col-md-6 col-xs-12 col-sm-12">
            <?php endif; ?>            
                <a href="http://<?php echo get_the_title($attachment_id) ?>" alt="<?php echo get_the_title($attachment_id) ?>" target="_blank">
                    <img alt="<?php echo get_the_title($attachment_id) ?>" style="width:100%;max-width:100%;box-shadow:0 10px 16px 0 rgba(0,0,0,.2),0 6px 20px 0 rgba(0,0,0,.19)!important" src="<?php echo wp_get_attachment_url($attachment_id); ?>"/>
                </a>
            </div>
            <?php $index++; ?>
        <?php endforeach; ?>
        <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
        <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
    </div>
</div>

<?php get_footer();
?>



