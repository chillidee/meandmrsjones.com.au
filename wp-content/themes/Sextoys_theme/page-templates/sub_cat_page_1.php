<?php
/**
 * Template Name: Sub Cat Page
 */
get_header('main');

$aws = 'https://s3.amazonaws.com/adult-toys/images/low-res/';

$slug = explode('/', $_SERVER["REQUEST_URI"]);
$slug = $slug[count($slug) - 2];

if ($slug):

    $cat = get_term_by('slug', $slug, 'product_cat');

    $cat_id = $cat->term_id;

    $cat_name = $cat->name;

endif;
$cat_id = 9;
$slug = "for-her";
if ($cat_id):
    ?>
    <div class="sub-cat-background-image" style="background-image: url(<?php echo "{$aws}{$slug}-background.jpg" ?>)">
    </div>

    <?php
endif;
get_footer('home');
?>
