<?php
/**
 * Template Name: Sub Cat Page 1
 */
get_header();

$aws = 'https://s3.amazonaws.com/adult-toys/images/low-res/';

$slug = explode('/', $_SERVER["REQUEST_URI"]);
$slug = $slug[count($slug) - 2];

if ($slug):

    $cat = get_term_by('slug', $slug, 'product_cat');

    $cat_id = $cat->term_id;

    $cat_name = explode(' ', $cat->name);

endif;
if ($cat_id):
    ?>
    <div class="sub-cat-background-image <?php echo ($cat->slug) ? 'sub-cat-' . $cat->slug : '' ?>" >
    </div>
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 sub-page-cat-container" >
        <div class="col-lg-7 col-sm-7 col-md-7 col-xs-12">
            <?php
            foreach (st_subcats_from_parentcat_by_id($cat_id) as $subcat):
                if ($subcat->name == 'Gifts & Games' || $subcat->name == 'Gifts &amp; Games'):
                    $catHref = 'gifts-games';
                else:
                    $catHref = strtolower($subcat->name);
                endif;
                if (isset($title_number) && $title_number == 1):
                    ?>
                    <div class="row for-us-title-row">
                    <?php endif;
                    ?>
                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-8 sub-cat-page-cat-wapper">
                        <form method="POST" action="<?php echo get_site_url() . '/' . $catHref . '/' ?>">
                            <button name="gender" value="<?php echo $slug ?>">
                                <div class="sub-cat-page-cat-container">
                                    <?php
                                    $child_image_array = st_get_category_image($subcat->term_id, 'full');
                                    if ($child_image_array):
                                        ?>
                                        <img src="<?php echo $child_image_array[0] ?>" alt="<?php echo $subcat->name ?>"/>
                                    <?php endif; ?>
                                    <p class="subcat-page-text"><?php
                                        if (strtolower($subcat->name) == 'sale'):
                                            echo 'Promotions';
                                        else:
                                            echo $subcat->name;
                                        endif;
                                        ?></p>
                                    <?php if (strtolower($subcat->name) != 'sale'): ?>
                                        <p class="subcat-page-text-contains">
                                            <?php
                                            $subcat_string = array();
                                            foreach (st_cat_get_children($subcat->term_id) as $subcat_child):
                                                array_push($subcat_string, $subcat_child->name);
                                            endforeach;
                                            $subcat_string = implode(', ', $subcat_string);
                                            if ($subcat_string != ''):
                                                echo $subcat_string;
                                            endif;
                                            ?>
                                        </p>
                                    <?php else:
                                        ?>
                                        <p class="subcat-page-text-contains">
                                            Hottest sale products
                                        </p>
                                    <?php endif; ?>
                                </div>
                            </button>
                        </form>
                    </div>
                    <?php
                    if (isset($title_number) && $title_number == 1):
                        $title_number = 0;
                        ?>
                    </div>
                    <?php
                else:
                    if (isset($title_number)):
                        $title_number++;
                    endif;
                endif;
            endforeach;
            ?>

        </div>
        <div class="cat-page-text col-lg-5 col-sm-5 col-md-5 col-xs-12">
            <p>
                <?php echo "<span>{$cat_name[0]}</span></br>{$cat_name[1]}" ?>
            </p>
        </div>
    </div>
    <!--<div class="col-lg-10 col-lg-offset-1 col-md-offset-1 col-md-10 col-sm-12 col-xs-12 below-fold-content" title="About Us">
        <div class="home-middle-logo-container">
            <img src="https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/header-logo.png" alt="Me &amp; Mrs Jones"/>
            <p class="home-page-slogan">Adult Boutique</p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 below-fold-content-text-wrapper">
            <h1><?php the_field('column_1_title') ?></h1>
            <p><?php the_field('column_1_text'); ?></p>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 below-fold-content-text-wrapper">
            <h2><?php the_field('column_2_title') ?></h2>
            <p><?php the_field('column_2_text'); ?></p>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 below-fold-content-text-wrapper">
            <h3><?php the_field('column_3_title') ?></h3>
            <p><?php the_field('column_3_text'); ?></p>
        </div>
    </div>-->
    <?php
endif;
get_footer();
?>
