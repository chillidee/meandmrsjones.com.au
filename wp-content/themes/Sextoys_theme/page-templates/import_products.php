<?php
global $cats;
global $imageURLPath;
global $fields;
require 'import_functions.php';
?>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet"/>
<div class="loading-screen"></div><input type="hidden" value="false" id="is_excluded_product"/> <?php
$feed = productCsvToArray();
$exisitingProducts = getExisitingProductIDS();
$excluded_products = st_get_excluded_products();
$fields = $feed[0];
unset($feed[0]);
$sorted_feed = array();
foreach ($feed as $product) {
    if ((in_array($product[0], $excluded_products, true)) || (in_array($product[0], $exisitingProducts, true))):
        unset($product);
    else:
        array_push($sorted_feed, $product);
    endif;
    unset($product);
}
unset($feed);
unset($exisitingProducts);
unset($excluded_products);
$cats = array();
foreach ($sorted_feed as $product):
    if (!in_array($product[6], $cats)):
        array_push($cats, $product[6]);
    endif;
endforeach;
sort($cats);
if (empty($_POST)):
    display_cats();
else:
    $cat = $_POST['cat'];
    display_products($sorted_feed, $cat);
endif;

function displayCatDropdown($barcode) {
    $cats = array();
    $subcats = array();
    $args = array(
        'taxonomy' => 'product_cat',
        'orderby' => 'name'
    );

    $all_categories = get_categories($args);
    foreach ($all_categories as $cat):
        if (!$cat->parent):
            $catInfo = array(
                'cat_id' => $cat->cat_ID,
                'cat_name' => $cat->name
            );
            array_push($cats, $catInfo);
        endif;
        if ($cat->parent):
            $catInfo2 = array(
                'parent' => $cat->parent,
                'cat_id' => $cat->cat_ID,
                'cat_name' => $cat->name
            );
            array_push($subcats, $catInfo2);
        endif;
    endforeach;
    ?>
    <select class="import-category-select <?php echo $barcode ?>_dropdown">
        <option selected="true">None</option>
        <?php foreach ($cats as $index): ?>
            <option value="<?php echo $index['cat_id'] ?>"><?php echo $index['cat_name'] ?></option>
            <?php
            foreach ($subcats as $subcat):
                if ($subcat['parent'] === $index['cat_id']):
                    ?>
                    <option value="<?php echo $subcat['cat_id'] ?>|<?php echo $subcat['parent'] ?>"> - <?php echo $subcat['cat_name'] ?></option>
                    <?php
                endif;
            endforeach;
            ?>
        <?php endforeach; ?>
    </select>
    <?php
}

function product_set_price($barcode) {
    ?>
    <select id="<?php echo $barcode ?>_price_select" class="import-price-dropdown">
        <option value="0">Category Markup</option>
        <option value="1">Manual Markup</option>
        <option value="2">Manual Price</option>
    </select>
    <?php
}

function display_cats() {
    global $cats;
    ?>
    <div class="col-lg-12"><h1 style="text-align: left">Category</h1></div>
    <?php
    for ($i = 1; $i < count($cats); $i++):
        ?>
        <form method="POST" action="">
            <div class="col-lg-4">
                <input name="cat" type="hidden" value="<?php echo $i ?>"/>
                <button class="btn-link" type="submit"><?php echo $cats[$i] ?></button>
            </div>
        </form>
        <?php
    endfor;
}

function display_fields() {
    global $fields;
    ?>
    <div class="col-lg-12 import-product-row">
        <div class="col-lg-2"><h2>Image</h2></div>
        <div class="col-lg-1"><h2><?php echo $fields[1] ?></h2></div>
        <div class="col-lg-2"><h2><?php echo $fields[5] ?></h2></div>
        <div class="col-lg-2"><h2><?php echo $fields[2] ?></h2></div>
        <div class="col-lg-2"><h2>Import Options</h2></div>
        <div class="col-lg-1"><h2><?php echo $fields[4] ?></h2></div>
        <div class="col-lg-2"><h2>Actions</h2></div>
    </div>
    <?php
}

function display_products($s_feed, $category) {
    global $cats;
    $category = (int) $category;
    ?>
    <div class="col-lg-12">
        <?php
        foreach ($cats as $cat => $value):
            if (($cat !== 'Category') && ($cat === $category)):
                ?>
                <div class="col-lg-12 import-category-row link"><h3><?php echo $value ?></h3></div>
                <?php
                foreach ($s_feed as $product):
                    if ($value === $product[6]):
                        product_row($product, $value);
                        unset($s_feed[$product]);
                    endif;
                endforeach;
            endif;
        endforeach;
        ?>
    </div>
    <?php
}

function product_row($product, $cat) {
    global $fields;
    global $imageURLPath;
    $imageURL = $imageURLPath . $product[8];
    $index = 0;
    import_functions::if_display_product_edit_box($product);
    ?>
    <div class="col-lg-12 import-product-row  <?php echo $product[10] ?>_row">
        <p class="<?php echo $product[10] ?>_ajax_result_message ajax_result_message"></p>
        <div class="col-lg-2"><div class="import-image-container"><img src="<?php echo Aws_Wrapper::aw_get_300_image_path(explode('/', $product[8]), 'temp') ?>"/></div></div>
        <div class="col-lg-2">
            <h6>Title</h6>
            <p id="<?php echo $product[0] ?>_import_product_title"><?php echo $product[1] ?></p>
            <h6>Brand</h6>
            <p id="<?php echo $product[0] ?>_import_product_brand"><?php echo $product[5] ?></p>
        </div>
        <div class="col-lg-2">
            <h6>Short Description</h6>
            <p id="<?php echo $product[0] ?>_import_product_short"><?php echo $product[2] ?></p>
        </div>
        <div class="col-lg-3">
            <h6>Long Description</h6>
            <p id="<?php echo $product[0] ?>_import_product_long"><?php echo strip_tags($product[3]) ?></p>
        </div>
        <div class="col-lg-1">
            <h6>Wholesale Price</h6>
            <h6 id="<?php echo $product[10] ?>_price_wholesale"><?php echo number_format($product[4], 2) ?></h6></div>
        <div class="col-lg-2">
            <form>
                <?php
                foreach ($fields as $field):
                    $id_field = str_replace(" ", "_", $field);
                    ?>
                    <input id="<?php echo $product[0] ?>_import_product_<?php echo $id_field ?>" type="hidden" class="import-input-<?php echo $field ?>"  value="<?php echo $product[$index] ?>" name="<?php echo $field ?>"/>
                    <?php
                    $index++;
                endforeach;
                $custom_input_arrays = import_functions::if_get_custom_attributes();
                foreach ($custom_input_arrays as $custom_attr):
                    ?>
                    <input type="hidden" class="<?php echo $product[0] ?>_import_product_custom_attr" value="false|<?php echo $custom_attr->attribute_name ?>|<?php echo $custom_attr->attribute_id ?>"/>
                <?php endforeach;
                ?>
                <input type="hidden" id="<?php echo $product[0] ?>_import_product_parent" value=""/>
                <div class="col-lg-12 center-block">
                    <button class="import-product btn btn-success" type="button">Import</button>
                </div>
                <div class="col-lg-12 center-block">
                    <button type="button" class="exclude-product btn btn-danger <?php echo $product[10] ?>_exclude_button">Exclude</button>
                </div>
                <div class="col-lg-12 center-block">
                    <button type="button" class="btn btn-default import-edit-product" id="<?php echo $product[0] ?>_edit_product_button">Edit Product</button>
                </div>
            </form>
        </div>
    </div>
    <?php
}
?>
</div>
<?php
import_functions::if_import_javascript();
import_functions::if_import_styles();
?>