<?php
/**
 * Template Name: Sub Cat Page 2
 */
get_header();

$aws = 'https://s3.amazonaws.com/adult-toys/images/low-res/';

$slug = explode('/', $_SERVER["REQUEST_URI"]);
$slug = $slug[count($slug) - 2];

if ($slug):

    $cat = get_term_by('slug', $slug, 'product_cat');

    $cat_id = $cat->term_id;

    /*$cat_name = explode(' ', $cat->name);*/

endif;
if ($cat_id):
    ?>
	<div class="sub-cat-background-image <?php echo ($cat->slug) ? 'sub-cat-' . $cat->slug : '' ?>">
	</div>
	<div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 sub-page-cat-container">
		<div class="cart-checkout-header col-lg-7 col-xs-12 col-sm-12 col-md-12 no-padding">
			<h1><p><?php echo ucwords(str_replace('-',' ',$cat->slug)) ?></p></h1>
		</div>
	</div>
	<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 sub-page-cat-container" >
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">			
			<div class="row guide about-page">
				<?php
				$subcat_count = 0;
				$subcat_name = "";
				foreach (st_subcats_from_parentcat_by_id($cat_id) as $subcat):
					if ($subcat->name == 'Gifts & Games' || $subcat->name == 'Gifts &amp; Games'):
						$catHref = 'gifts-games';
					else:
						$catHref = strtolower($subcat->name);
					endif;
					if (isset($title_number) && $title_number == 1):
						?>
						
						<?php endif;
						?>
						<div class="col-lg-4 col-md-6 sub-cat-page-cat-wapper">
							<?php
							switch ($subcat_count) {
								case 0:
									$subcat_name = "toys";
									break;
								case 1:
									$subcat_name = "fetish";
									break;
								case 2:
									$subcat_name = "apparel";
									break;
								case 3:
									$subcat_name = "essentials";
									break;
								case 4:
									$subcat_name = "gifts-games";
									break;											
							}
							?>
							<form method="POST" action="<?php echo get_site_url() . '/' . $subcat_name . '/' ?>">
								<button name="gender" value="<?php echo $slug ?>">						
									<div class="sub-cat-page-cat-container">					
										<img src="<?php echo "{$aws}subcat-{$slug}-{$subcat_name}.jpg" ?>"/>									
										<p class="subcat-page-text">				
											<?php echo "{$subcat_name}" ?>
										</p>					
									</div>
								</button>
							</form>
						</div>
						<?php
						if (isset($title_number) && $title_number == 1):
							$title_number = 0;
							?>
						
						<?php
					else:
						if (isset($title_number)):
							$title_number++;
						endif;
					endif;
					$subcat_count++;
				endforeach;
				?>
			</div>
		</div>
		<div class="cat-page-text col-lg-5 col-sm-5 col-md-5 col-xs-12">
			<p>
				<?php echo "<span>{$cat_name[0]}</span></br>{$cat_name[1]}" ?>
			</p>
		</div>
	</div>
    <!--<div class="col-lg-10 col-lg-offset-1 col-md-offset-1 col-md-10 col-sm-12 col-xs-12 below-fold-content" title="About Us">
        <div class="home-middle-logo-container">
            <img src="https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/header-logo.png" alt="Me &amp; Mrs Jones"/>
            <p class="home-page-slogan">Adult Boutique</p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 below-fold-content-text-wrapper">
            <h1><?php the_field('column_1_title') ?></h1>
            <p><?php the_field('column_1_text'); ?></p>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 below-fold-content-text-wrapper">
            <h2><?php the_field('column_2_title') ?></h2>
            <p><?php the_field('column_2_text'); ?></p>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 below-fold-content-text-wrapper">
            <h3><?php the_field('column_3_title') ?></h3>
            <p><?php the_field('column_3_text'); ?></p>
        </div>
    </div>-->
    <?php
endif;
get_footer();
?>
