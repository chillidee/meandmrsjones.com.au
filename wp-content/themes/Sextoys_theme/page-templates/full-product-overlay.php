<?php
$_product_id = (int) $_POST['product_id'];
$_product = wc_get_product($_product_id);
$post = $_product->post;
$_product_colour = get_post_meta($_product->id, 'st_colour', true);
$_product_size = get_post_meta($_product->id, 'st_size', true);
$associated_products = st_get_associated_products($_product_id);
?>
<input type="hidden" id="archive-product-right-col-id" value="<?php echo $post->ID ?>"/>
<div class="archive-right_col_product_container col-lg-12">
    <div class="col-lg-12 archive-product-ajax-response"></div>
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="product_overlay_close"><i class="fa fa-times" aria-hidden="true"></i></div>
        <div class="col-lg-12 col-sm-12 col-xs-12 mob-only"><h1><?php echo $post->post_title ?></h1></div>
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 center-block image-box archive-right-col-product-image zoomImg"><?php echo $_product->get_image('full'); ?>
            <div class="col-lg-12 col-xs-12 product-gallery-wrapper">
                <?php
                $gallery_array = st_get_products_gallery_ids($post->ID);
                foreach ($gallery_array as $image):
                    if ($image):
                        ?>
                        <div class="col-lg-3 col-xs-3 gallery-image-container">
                            <?php echo st_get_products_gallery_image($image) ?>
                        </div>
                        <?php
                    endif;
                endforeach;
                ?>
            </div>
            <div class="col-lg-12 col-xs-12 video-container">
                <?php echo st_get_product_youtube_link($_product_id) ?>
            </div>
            <?php
            if (st_get_number_of_products_in_category(st_get_cat_slug_from_id(st_get_related_cat_slug_from_id($_product_id)), $_product_id)):
                ?>
                <div class="col-lg-12 col-xs-12 full-product-related">
                    <p>Customers also bought</p>
                    <?php
                    add_related_products_after_product(2, st_get_related_cat_slug_from_id($_product_id), $_product_id);
                    ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-lg-6 col-xs-12 archive-right-col-product-text">
            <div class="col-lg-12 col-sm-12 col-xs-12"><h1><?php echo $post->post_title ?></h1></div>
            <div class="col-lg-12 col-sm-12 col-xs-12 price"><h4><?php echo $_product->get_price_html(); ?></h4></div>
            <?php
            if ($_product_colour != ''):
                $colour_array = array();
                $colour_array[$_product_id] = $_product_colour;
                ?>
                <div class="col-lg-12 col-md-12">

                    <div class="col-lg-8 product-size-container">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 product-persifics-label"><p>Colour</p></div>
                        <div class="col-lg-8 col-md-4 col-sm-8 col-xs-8 product-persifics-dropdown">
                            <?php
                            if (count($associated_products) > 0):
                                foreach ($associated_products as $product):
                                    $associated_product_colour = get_post_meta($product, 'st_colour', true);
                                    if ($associated_product_colour != $_product_colour && get_post_status($product) === 'publish'):
                                        $colour_array[$product] = $associated_product_colour;
                                    endif;
                                endforeach;
                            endif;
                            if (count($colour_array) > 1):
                                ?>
                                <select class="product-option-dropdown" id="product-option-dropdown-st_colour">
                                    <?php foreach ($colour_array as $index => $product): ?>
                                        <option value="<?php echo $index ?>"><?php echo $product ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php
                            else:
                                if ($colour_array[$_product_id] != ''):
                                    ?>
                                    <input class="single-attribute" type="text" readonly="true" value="<?php echo $colour_array[$_product_id] ?>" />
                                    <?php
                                endif;
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
                <?php
            endif;
            if ($_product_size != ''):
                $size_array = array();
                $size_array[$_product_id] = $_product_size;
                ?>
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="col-lg-8 col-xs-12 product-size-container">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 product-persifics-label"><p>Size</p></div>
                        <div class="col-lg-8 col-md-4 col-sm-8 col-xs-8 product-persifics-dropdown">
                            <?php
                            if (count($associated_products) > 0):
                                foreach ($associated_products as $product):
                                    $associated_product_size = get_post_meta($product, 'st_size', true);
                                    if ($associated_product_size != $_product_size && get_post_status($product) === 'publish'):
                                        $size_array[$product] = $associated_product_size;
                                    endif;
                                endforeach;
                            endif;
                            if (count($size_array) > 1):
                                ?>
                                <select class="product-option-dropdown" id="product-option-dropdown-st_size">
                                    <?php foreach ($size_array as $index => $product): ?>
                                        <option value="<?php echo $index ?>"><?php echo $product ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php
                            else:
                                if ($size_array[$_product_id] != ''):
                                    ?>
                                    <input class="single-attribute" type="text" readonly="true" value="<?php echo $size_array[$_product_id] ?>" />
                                    <?php
                                endif;
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="col-lg-8 col-sm-12 archive-add-to-cart-container">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 product-persifics-label"><p>QTY</p></div>
                    <div class="col-xs-8 col-lg-8">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 archive-quantity-container"><i id="quantity-decrease" class="fa fa-minus"></i></div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 quantity-total-container"><input id="quantity-total" value="1"/></div>
                        <div class="col-lg-4 col-md-4  col-sm-4 col-xs-4 archive-quantity-container"><i id="quantity-increase" class="fa fa-plus"></i></div>
                    </div>
                </div>

            </div>
            <div class="col-lg-5 col-xs-6 cart"><button id="<?php echo $post->ID ?>-ajax-to-cart" class="add-to-cart-ajax" type="button">Buy Me Now</button></div>
            <div class="col-lg-7 col-xs-6 full-product-added-cart"><span class="full-product-added-cart-total"></span><p>In cart</p></div>
            <div class="col-lg-12 col-sm-12"><h5>Product Information</h5></div>
            <div class="col-lg-12 col-sm-12"><p><?php echo $post->post_content ?></p></div>
        </div>
    </div>
</div>