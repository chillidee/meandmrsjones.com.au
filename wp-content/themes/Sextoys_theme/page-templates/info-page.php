<?php
/**
 * Template Name: Info Pages
 */
get_header();
?>
<div class="home-background-container"></div>
<?php if ($_SERVER["REQUEST_URI"] != '/contact/'): ?>
    <div class="col-lg-6 col-lg-offset-1 info-page">
        <h1><?php the_field('page_title'); ?></h1>
        <div class="content-box">
            <div class="text-container">
                <?php
                if (have_posts()) : while (have_posts()) : the_post();
                        the_content();
                    endwhile;
                endif;
                ?>
            </div>
        </div>
    </div>
<?php else: ?>

<?php endif; ?>
<?php
get_footer();
?>
