<?php
/**
 * Template Name: All Products
 */
get_header();
wp_enqueue_style('stars-products');
$slug = explode('/', $_SERVER["REQUEST_URI"]);
$parent_slug = $slug[count($slug) - 3];
$slug = $slug[count($slug) - 2];
$offset = 48;
if (!empty($_POST["gender"])):
    $gender = explode('for-', $_POST["gender"]);
    $gender = $gender[count($gender) - 1];
endif;
if (file_exists(get_template_directory() . '/cachedPages/' . $slug . '-' . $gender . '.html') && !isset($_POST['caching'])):
    echo file_get_contents(get_template_directory() . '/cachedPages/' . $slug . '-' . $gender . '.html');
else:
    $featured_products = json_decode(get_post_meta($post->ID, 'featured_products', true));
    $products = array();
    if (isset($featured_products) && !empty($featured_products)):
        foreach ($featured_products as $position => $product):
            if (isset($product) && !empty($product) && (get_post_meta($product, '_stock_status', true) != 'outofstock')):
                $products[$position - 1] = $product;
            endif;
        endforeach;
        $products = st_get_all_products_for_template($slug, $offset, $gender, $parent_slug, $products);
        ?>
        <input name="featured_products" value='<?php echo implode(',', $products[0]) ?>' type="hidden"/>
        <?php
    else:
        $products = st_get_all_products_for_template($slug, $offset, $gender, $parent_slug);
    endif;
    ?>
    <style>@media(max-width:1200px){main{position: fixed}}body,html{overflow: hidden}</style>
    <input name="offset_number_orginial" type="hidden" value="<?php echo $offset ?>"/>
    <input name="offset_number" type="hidden" value="<?php echo $offset ?>"/>
    <input name="refined" type="hidden" value=""/>
    <input name="refined_gender" type="hidden" value="<?php echo $gender ?>"/>
    <input id="archive-cat-id" type="hidden" value="<?php echo $slug ?>"/>
    <input id="archive-cat-parent" type="hidden" value="<?php echo $parent_slug ?>"/>
    <input name="is_search" type="hidden" value="false"/>
    <div class = "col-lg-12 prodct-cat-background-image"></div>
    <div id="all_products_list" class = "col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div id="archive-sidebar" class="col-lg-2 archive-sidebar for-her-sidebar title-sidebar">
            <?php include get_template_directory() . '/partials/all_products_sidebar.php'; ?>
        </div>
        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 no-padding-mobile-450 product-sort-bar-container">
            <?php if (isset($slug)): ?>
                <div class="col-lg-12 product-sort-bar">
                    <div class="col-lg-12 col-lg-offset-0 col-xs-12 archive-sort-bar-wrapper">                        
                        <div class="col-lg-12 col-xs-6 col-sm-6 col-md-6 product-sort-by-button-wrapper">
                            <p id="product-sort-by-button"></p>
                            <div id="archive-sort-overlay" class="archive-sort-overlay col-lg-5 col-lg-offset-12 no-padding">
                                <?php
                                include_once get_template_directory() . '/partials/all_products_sort_overlay.php';
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            endif;
            ?>
        </div>
        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
            <p class="product_overlay_close product_overlay_back_text">Back</p>
        </div>
        <div id="archive-products" class="col-lg-10 col-xs-12 archive-products"><?php ?>
            <div class="thinking hidden"><img class="center-block" src="https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/main-header-logo.png"/><p class="center-block">Loading...</p></div>
            <div class="archive-product-wrapper">
                <?php if (count($products[0]) > 0):
                    ?>
                    <?php
                    $product_number = 0;
                    foreach ($products[0] as $index => $id):
                        $product_number = st_archive_product_container($id, $product_number);
                    endforeach;
                else:
                    ?>
                    <div class="col-lg-12 no-products">
                        <h3>No products found, please refine your search</h3>
                    </div>
                    <div class="empty-cart-container empty-search-containter col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h3>Most Popular</h3>
                            <?php
                            $posts = get_most_popular_products(3);
                            foreach ($posts->posts as $post):
                                st_archive_product_container($post->ID);
                            endforeach;
                            ?>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h3>Newest</h3>
                            <?php
                            $posts = get_newest_products(3);
                            foreach ($posts->posts as $post):
                                st_archive_product_container($post->ID);
                            endforeach;
                            ?>
                        </div>
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                            <h3>Featured</h3>
                            <?php
                            $posts = st_get_taged_products(3, 'featured');
                            foreach ($posts->posts as $post):
                                st_archive_product_container($post->ID);
                            endforeach;
                            unset($posts);
                            ?>
                        </div>
                    </div>
                <?php
                endif;
                unset($products);
                ?>
            </div>
        </div>
    </div>
    <?php
    if (get_field('column_1_title', get_queried_object())):
        ?>
        <div class="col-lg-10 col-lg-offset-1 col-md-offset-1 col-md-10 col-sm-12 col-xs-12 below-fold-content" title="About Us">
            <div class="home-middle-logo-container">
                <img src="https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/header-logo.png" alt="Me &amp; Mrs Jones"/>
                <p class="home-page-slogan">Adult Boutique</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 below-fold-content-text-wrapper">
                <h1><?php the_field('column_1_title', get_queried_object()) ?></h1>
                <p><?php the_field('column_1_text', get_queried_object()); ?></p>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 below-fold-content-text-wrapper">
                <h2><?php the_field('column_2_title', get_queried_object()) ?></h2>
                <p><?php the_field('column_2_text', get_queried_object()); ?></p>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 below-fold-content-text-wrapper">
                <h3><?php the_field('column_3_title', get_queried_object()) ?></h3>
                <p><?php the_field('column_3_text', get_queried_object()); ?></p>
            </div>
        </div>
        <?php
    endif;
endif;
get_footer();
?>