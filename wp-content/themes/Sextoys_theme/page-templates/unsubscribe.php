<?php
/**
 * Template Name: Unsubscribe Page
 */
get_header();
?>
<div class = "col-lg-12 prodct-cat-background-image" style = "background-image: url(<?php echo st_get_random_image_background() ?>)"></div>
<div class="blog-header col-lg-7 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12">
    <h1><?php the_title(); ?></h1>
</div>
<div class="blog-posts-container col-lg-7 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12">
    <?php
    if (isset($_POST['unsubscribe_email']) && !empty($_POST['unsubscribe_email'])):
        global $wpdb;
        $email = htmlentities($_POST['unsubscribe_email']);
        $removed = false;
        $dbResults = $wpdb->get_results("SELECT * FROM `email_subscribers` WHERE `email` LIKE '" . $email . "'");
        if (!empty($dbResults)):
            $dbResults = $dbResults[0]->id;
            $removed = true;
            $wpdb->get_results("DELETE FROM `email_subscribers` WHERE `email_subscribers`.`id` = " . $dbResults);
        endif;
        $dbResults = $wpdb->get_results("SELECT `ID` FROM wp_users t1, wp_usermeta t2 WHERE t1.ID = t2.user_id AND t2.meta_key LIKE 'subscribed' AND t2.meta_value LIKE 'true' AND t1.user_email LIKE '" . $email . "'");
        if (!empty($dbResults)):
            $dbResults = $dbResults[0]->ID;
            $removed = true;
            update_user_meta($dbResults, 'subscribed', false);
        endif;
        if ($removed):
            ?>
            <p>You have been successfully removed from our records</p>
            <?php
        else:
            unsubscribe_email_form('Email not found in our records');
        endif;
    else:
        unsubscribe_email_form();
    endif;

    function unsubscribe_email_form($errorMessage = '') {
        the_content();
        ?>
        <form style="margin:20px 0;" method="post">
            <div class="form-group">
                <label for="unsubscribe_email">Email address</label>
                <input name="unsubscribe_email" <?php echo (isset($_POST['unsubscribe_email'])) ? 'value="' . htmlentities($_POST['unsubscribe_email']) . '"' : '' ?> class="form-control" placeholder="Email"/>
                <p style="color:red" class="form-error-message"><?php echo $errorMessage ?></p>
                <button style="margin:20px 0;" type="submit" name="unsubscribe_button" class="btn btn-danger" disabled>Unsubscribe</button>
            </div>
        </form>
    <?php }
    ?>
</div>
<?php
wp_enqueue_script('unsubscribe_form_js');
get_footer();
?>
