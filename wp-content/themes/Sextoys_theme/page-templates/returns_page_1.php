<?php
/**
 * Template Name: Returns Page 1
 */
get_header();
?>
<div class="sub-cat-background-image">
</div>
<div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 sub-page-cat-container">
    <div class="cart-checkout-header col-lg-7 col-lg-offset-1 col-xs-12 col-sm-12 col-md-12 no-padding">
        <h1><p>RETURNS & EXCHANGES</p></h1>
    </div>
    <div class="col-lg-7 col-lg-offset-1 col-xs-12 col-sm-12 col-md-12 empty-cart-container no-padding">
        <p>We always do our best to ensure that the items we ship to you are in top condition. However, if you feel as though you have received an item that is faulty, we will be happy to have the product replaced free of charge within 14 days of your receipt date. We do not accept any returns or exchanges on health & hygiene items or personal lubricants (Our Essentials).</p>
        <p>&nbsp</p>
        <p>Choose a Return or Exchange Option Below</p>
    </div>
    <div class="col-lg-11 col-lg-offset-1 col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
        <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 sub-cat-page-cat-wapper">
            <a href="/returns-2">
                <div class="sub-cat-page-cat-container">
                    <img src="<?php the_field('sextoys_image') ?>"/>
                    <p class="subcat-page-text subcat-page-text-returns-1">Sex Toy &amp; Adult Product Exchange</p>
                    <p class="subcat-page-text-contains subcat-page-text-returns-1">Sex Toys, Games, Sex Toy Accessories</p>
                </div>
            </a>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 sub-cat-page-cat-wapper">
            <a href="/returns-3">
                <div class="sub-cat-page-cat-container">
                    <img src="<?php the_field('apparel_image') ?>"/>
                    <p class="subcat-page-text subcat-page-text-returns-1">Garments &amp; Shoe Returns &amp; Exchange</p>
                    <p class="subcat-page-text-contains subcat-page-text-returns-1">Lingerie, Costumes, Bridal Lingerie, Club Wear, Fetish Wear, Mens Range, Shoes</p>
                </div>
            </a>
        </div>
    </div>
</div>
<?php
get_footer();
?>
