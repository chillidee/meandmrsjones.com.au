<?php
/**
 * Template Name: Blog
 */
get_header();
$the_query = new WP_Query(array('post_type' => 'post', 'posts_per_page' => -1));
?>
<div class="sub-cat-background-image">    
</div>
<div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 sub-page-cat-container">
    <div class="cart-checkout-header col-lg-7 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 no-padding">
        <h1><p>BLOG</p></h1>
    </div>
    <div class="blog-posts-container col-lg-11 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 no-padding">
        <?php
        if ($the_query->have_posts()):
        while ($the_query->have_posts()):
            $the_query->the_post($post->ID);
            $post = get_post($post->ID);
            ?>
            <div class="col-lg-4 col-md-6 col-xs-12 col-sm-12">
                <article class="col-lg-12">                
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blog-image-thumb">
                        <?php if ( has_post_thumbnail() ) {
                            the_post_thumbnail(small);
                        } else { ?>
                            <img src="<?php bloginfo('template_directory'); ?>/images/home-background.jpg" alt="<?php the_title(); ?>" />
                        <?php } ?>
                    </div>
                    <div class="blog-article-content">
                        <p class="blog-article-title"><?php the_title() ?></p>
                        <p class="text-justify">
                            <?php 
                            $limit = 25;
                            $excerpt = explode(' ', get_the_excerpt(), $limit);
                            if (count($excerpt)>=$limit) {
                                array_pop($excerpt);
                                $excerpt = implode(" ",$excerpt).' […]';
                            } else {
                                $excerpt = implode(" ",$excerpt);
                            }	
                            $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
                            echo $excerpt;                        
                            ?>
                        </p>
                    </div>
                    <a href="<?php the_guid() ?>" class="blog-article-read-more">Read More</a>
                </article>
            </div>
            <?php
        endwhile;        
        endif;
        wp_reset_postdata();
        ?>
    </div>
</div>
<?php
get_footer();
