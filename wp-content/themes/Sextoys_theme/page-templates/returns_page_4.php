<?php
/**
 * Template Name: Returns Page Thank You
 */
if (isset($_POST)):
    $returnArray = array();
    $_pf = new WC_Product_Factory();
    if ($_POST["form_name"] == 'form_apparel_return'):
        $returnArray['name'] = htmlspecialchars($_POST["customer_name"]);
        $returnArray['email'] = strip_tags($_POST["email"]);
        $returnArray['phone'] = htmlspecialchars($_POST["phone"]);
        $returnArray['order_number'] = (int) htmlspecialchars($_POST["order_number"]);
        $returnArray['exchange_info'] = htmlspecialchars($_POST["exchange_info"]);
        $returnArray['warranty'] = (htmlspecialchars($_POST["apparelReturnSelect"] == 1)) ? 'Yes' : 'No';
        $returnArray['product_id'] = (int) htmlspecialchars($_POST["product_select"]);
        if (isset($_POST["contact_email"])):
            $returnArray['contact_by'] = 'email';
        endif;
        if (isset($_POST["contact_phone"])):
            $returnArray['contact_by'] = 'phone';
        endif;
        if (!empty($_FILES["productImage"]["tmp_name"])):
            $target_dir = st_get_site_root() . "/return_images/";
            $target_file = $target_dir . basename($_FILES["productImage"]["name"]);
            move_uploaded_file($_FILES["productImage"]["tmp_name"], $target_file);
        endif;
        $order = new WC_Order($returnArray['order_number']);
        $_product = $_pf->get_product($returnArray['product_id']);
        $_product = $_product->post;
        if (isset($order)):
            $returnArray['order']['billing'] = $order->get_formatted_billing_address();
            $returnArray['order']['shipping'] = $order->get_formatted_billing_address();
            if (!empty($returnArray)):
                switch (get_post_meta($returnArray['product_id'], 'vendor', true)):
                    case 'Windsor Wholeseller':
                        $email_address = array('michaelf@windsorwholesale.com.au', 'WWexchange@meandmrsjones.com.au', $returnArray['email']);
                        break;
                    case 'Wrapped Secrets':
                        $email_address = array('WWexchange@meandmrsjones.com.au', 'mark@wrappedsecrets.com.au', $returnArray['email']);
                        break;
                    default:
                        $email_address = 'info@meandmrsjones.com.au';
                        break;
                endswitch;
                //$email_address = array($return_email, $returnArray['email']);
                $subject = 'Me and Mrs Jones Return Request';
                $headers = 'From: MMJ <info@meandmrsjones.com.au>' . "\r\n";
                $content = "<h1>Customer Details</h1>";
                $content .= "<p>Name: " . $returnArray['name'] . "</p>";
                $content .= "<p>Phone: " . $returnArray['phone'] . "</p>";
                $content .= "<p>Email: " . $returnArray['email'] . "</p>";
                $content .= "<p>Contact By: " . ucwords($returnArray['contact_by']) . "</p>";
                $content .= "<h1>Order Details</h1>";
                $content .= "<p>Order number: " . $returnArray['order_number'] . "</p>";
                $content .= "<p>Purchased on: " . $order->order_date . "</p>";
                $content .= "<p>Is faulty: " . $returnArray['warranty'] . "</p>";
                $content .= "<p>Billing Address:</p>";
                $content .= "<p>" . $returnArray['order']['billing'] . "</p>";
                $content .= "<p>Shipping Address:</p>";
                $content .= "<p>" . $returnArray['order']['shipping'] . "</p>";
                $content .= "<h1>Product Details</h1>";
                $content .= "<p>Product code: " . get_post_meta($returnArray['product_id'], '_code', true) . "</p>";
                $content .= "<p>Title: " . $_product->post_title . "</p>";
                $content .= "<p>Size: " . get_post_meta($returnArray['product_id'], 'st_size', true) . "</p>";
                $content .= "<p>Colour: " . get_post_meta($returnArray['product_id'], 'st_colour', true) . "</p>";
                $content .= "<p></p>";
                require_once get_template_directory() . '/functions/email_functions.php';
                $emails = new email_orders();
                $content = $emails->wrapContent($subject, $content);
                if (isset($_POST['cost_accept'])):
                    $target_dir = st_get_site_root() . "/return_images/" . $returnArray['order_number'] . '.pdf';
                    st_get_pdf_plugin();
                    $pdf = new mPDF();
                    $pdf->WriteHTML(file_get_contents(get_template_directory_uri() . '/css/pdf_css.css'), 1);
                    $pdf->WriteHTML(utf8_encode($content), 2);
                    $pdf = $pdf->Output('', 'S');
                    $target_file = fopen($target_dir, "w");
                    if ($target_file):
                        fwrite($target_file, $pdf);
                        fclose($target_file);
                    endif;
                    $target_file = $target_dir;
                endif;
                if ($target_file):
                    //foreach ($email_address as $email):
                    $emails->sendmail($email_address, $subject, $content, $headers, $target_file);
                //endforeach;
                else:
                    // foreach ($email_address as $email):
                    $emails->sendmail($email_address, $subject, $content, $headers);
                //endforeach;
                endif;
            endif;
        endif;
    endif;
    if ($_POST["form_name"] == 'form_st_return'):
        $returnArray['name'] = htmlspecialchars($_POST["customer_name"]);
        $returnArray['email'] = strip_tags($_POST["email"]);
        $returnArray['phone'] = htmlspecialchars($_POST["phone"]);
        $returnArray['order_number'] = (int) htmlspecialchars($_POST["order_number"]);
        $returnArray['exchange_info'] = htmlspecialchars($_POST["exchange_info"]);
        $returnArray['warranty_number'] = htmlspecialchars($_POST["warranty_number"]);
        $returnArray['product_id'] = (int) htmlspecialchars($_POST["product_select"]);
        if (isset($_POST["contact_email"])):
            $returnArray['contact_by'] = 'email';
        endif;
        if (isset($_POST["contact_phone"])):
            $returnArray['contact_by'] = 'phone';
        endif;
        if (isset($_POST["faulty_on_arrival_yes"])):
            $returnArray['faulty_on_arrival'] = 'Yes';
        else:
            $returnArray['faulty_on_arrival'] = 'No';
        endif;
        if (isset($_POST["warranty_yes"])):
            $returnArray['warranty'] = 'Yes';
        else:
            $returnArray['warranty'] = 'No';
        endif;
        if (!empty($_FILES["productImage"]["tmp_name"])):
            $target_dir = st_get_site_root() . "/return_images/";
            $target_file = $target_dir . basename($_FILES["productImage"]["name"]);
            move_uploaded_file($_FILES["productImage"]["tmp_name"], $target_file);
        endif;
        $order = new WC_Order($returnArray['order_number']);
        $_product = $_pf->get_product($returnArray['product_id']);
        $_product = $_product->post;
        if (isset($order)):
            $returnArray['order']['billing'] = $order->get_formatted_billing_address();
            $returnArray['order']['shipping'] = $order->get_formatted_billing_address();
            if (!empty($returnArray)):
                switch (get_post_meta($returnArray['product_id'], 'vendor', true)):
                    case 'Windsor Wholeseller':
                        $email_address = array('michaelf@windsorwholesale.com.au', 'WWexchange@meandmrsjones.com.au', $returnArray['email']);
                        break;
                    case 'Wrapped Secrets':
                        $email_address = array('WWexchange@meandmrsjones.com.au', 'mark@wrappedsecrets.com.au', $returnArray['email']);
                        break;
                    default:
                        $email_address = 'info@meandmrsjones.com.au';
                        break;
                endswitch;
                //$email_address = array($return_email, $returnArray['email']);
                $subject = 'Me and Mrs Jones Return Request';
                $headers = 'From: MMJ <info@meandmrsjones.com.au>' . "\r\n";
                $content = "<h1>Customer Details</h1>";
                $content .= "<p>Name: " . $returnArray['name'] . "</p>";
                $content .= "<p>Phone: " . $returnArray['phone'] . "</p>";
                $content .= "<p>Email: " . $returnArray['email'] . "</p>";
                $content .= "<p>Contact By: " . ucwords($returnArray['contact_by']) . "</p>";
                $content .= "<h1>Order Details</h1>";
                $content .= "<p>Order number: " . $returnArray['order_number'] . "</p>";
                $content .= "<p>Purchased on: " . $order->order_date . "</p>";
                $content .= "<p>In warranty: " . $returnArray['warranty'] . "</p>";
                $content .= "<p>Warranty number: " . $returnArray['warranty_number'] . "</p>";
                $content .= "<p>Faulty On Arrival: " . $returnArray['faulty_on_arrival'] . "</p>";
                $content .= "<p>Billing Address:</p>";
                $content .= "<p>" . $returnArray['order']['billing'] . "</p>";
                $content .= "<p>Shipping Address:</p>";
                $content .= "<p>" . $returnArray['order']['shipping'] . "</p>";
                $content .= "<h1>Product Details</h1>";
                $content .= "<p>Product code: " . get_post_meta($returnArray['product_id'], '_code', true) . "</p>";
                $content .= "<p>Title: " . $_product->post_title . "</p>";
                $content .= "<p>Size: " . get_post_meta($returnArray['product_id'], 'st_size', true) . "</p>";
                $content .= "<p>Colour: " . get_post_meta($returnArray['product_id'], 'st_colour', true) . "</p>";
                $content .= "<p></p>";
                require_once get_template_directory() . '/functions/email_functions.php';
                $emails = new email_orders();
                $content = $emails->wrapContent($subject, $content);
                if ($target_file):
                    //foreach ($email_address as $email):
                    $emails->sendmail($email_address, $subject, $content, $headers, $target_file);
                //endforeach;
                else:
                    //foreach ($email_address as $email):
                    $emails->sendmail($email_address, $subject, $content, $headers);
                //endforeach;
                endif;
            endif;
        endif;
    endif;
endif;
//if (isset($_POST['cost_accept'])):
//    $orderNumber = $returnArray['order_number'];
//    header("Content-type:application/pdf");
//    header("Content-Disposition: attachment; filename=$orderNumber");
//
//    readfile($target_file);
//endif;
get_header();
?>
<div class="sub-cat-background-image">
</div>
<div class="col-lg-12 sub-page-cat-container" >
    <div class="cart-checkout-header col-lg-7 col-lg-offset-1 col-xs-12 col-sm-12 col-md-12">
        <p>Thank you, we have received your return/exchange submission</p>
    </div>
    <div class="col-lg-7 col-lg-offset-1 col-xs-12 col-sm-12 col-md-12 empty-cart-container">
        <p>You will receive a confirmation of your return/exchange in your email for your convenience.  Exchanges will be shipped within approximately 10 days of acceptance by Me & Mrs. Jones. Remember that if you are sending any items back to us you must print your return/exchange form and include it inside your parcel.</p>
    </div>
</div>
<?php
get_footer();
?>
