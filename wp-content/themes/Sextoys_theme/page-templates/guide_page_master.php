<?php
/**
 * Template Name: Guide Page Master
 */
get_header();
?>
<div class="sub-cat-background-image">
</div>
<div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 sub-page-cat-container">
    <div class="cart-checkout-header col-lg-7 col-xs-12 col-sm-12 col-md-12 no-padding">
        <h1><p>GUIDE SPOT</p></h1>
    </div>
</div>
<div class="row guide about-page">
    <div class="col-lg-12"></div>
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 sub-page-cat-container">
        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
            <!-- <div class="col-lg-4 col-md-6 sub-cat-page-cat-wapper">
                <a href="<?php the_field('tile_9_link') ?>">
                    <div class="sub-cat-page-cat-container">
                        <img src="<?php //the_field('tile_9_image')      ?>" alt="<?php //the_field('tile_9_text')      ?>">
                        <p class="subcat-page-text"><?php //the_field('tile_9_text')      ?></p>
                    </div>
                </a>
            </div>-->
            <div class="col-lg-4 col-md-6 sub-cat-page-cat-wapper">
                <a href="<?php the_field('tile_1_link') ?>">
                    <div class="sub-cat-page-cat-container">
                        <img src="<?php the_field('tile_1_image') ?>" alt="<?php the_field('tile_1_text') ?>">
                        <p class="subcat-page-text"><?php the_field('tile_1_text') ?></p>
<!--                                <p class="subcat-page-text-contains"></p>-->
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 sub-cat-page-cat-wapper">
                <a href="<?php the_field('tile_2_link') ?>">
                    <div class="sub-cat-page-cat-container">
                        <img src="<?php the_field('tile_2_image') ?>" alt="<?php the_field('tile_2_text') ?>">
                        <p class="subcat-page-text"><?php the_field('tile_2_text') ?></p>
<!--                                <p class="subcat-page-text-contains"></p>-->
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 sub-cat-page-cat-wapper">
                <a href="<?php the_field('tile_3_link') ?>">
                    <div class="sub-cat-page-cat-container">
                        <img src="<?php the_field('tile_3_image') ?>" alt="<?php the_field('tile_3_text') ?>">
                        <p class="subcat-page-text"><?php the_field('tile_3_text') ?></p>
    <!--                                <p class="subcat-page-text-contains"></p>-->
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 sub-cat-page-cat-wapper">
                <a href="<?php the_field('tile_4_link') ?>">
                    <div class="sub-cat-page-cat-container">
                        <img src="<?php the_field('tile_4_image') ?>" alt="<?php the_field('tile_4_text') ?>">
                        <p class="subcat-page-text"><?php the_field('tile_4_text') ?></p>
    <!--                                <p class="subcat-page-text-contains"></p>-->
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 sub-cat-page-cat-wapper">
                <a href="<?php the_field('tile_5_link') ?>">
                    <div class="sub-cat-page-cat-container">
                        <img src="<?php the_field('tile_5_image') ?>" alt="<?php the_field('tile_5_text') ?>">
                        <p class="subcat-page-text"><?php the_field('tile_5_text') ?></p>
    <!--                                <p class="subcat-page-text-contains"></p>-->
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 sub-cat-page-cat-wapper">
                <a href="<?php the_field('tile_6_link') ?>">
                    <div class="sub-cat-page-cat-container">
                        <img src="<?php the_field('tile_6_image') ?>" alt="<?php the_field('tile_6_text') ?>">
                        <p class="subcat-page-text"><?php the_field('tile_6_text') ?></p>
    <!--                                <p class="subcat-page-text-contains"></p>-->
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 sub-cat-page-cat-wapper">
                <a href="<?php the_field('tile_7_link') ?>">
                    <div class="sub-cat-page-cat-container">
                        <img src="<?php the_field('tile_7_image') ?>" alt="<?php the_field('tile_7_text') ?>">
                        <p class="subcat-page-text"><?php the_field('tile_7_text') ?></p>
    <!--                                <p class="subcat-page-text-contains"></p>-->
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 sub-cat-page-cat-wapper">
                <a href="<?php the_field('tile_8_link') ?>">
                    <div class="sub-cat-page-cat-container">
                        <img src="<?php the_field('tile_8_image') ?>" alt="<?php the_field('tile_8_text') ?>">
                        <p class="subcat-page-text"><?php the_field('tile_8_text') ?></p>
    <!--                                <p class="subcat-page-text-contains"></p>-->
                    </div>
                </a>
            </div>
            <?php if (get_field('tile_10_link')): ?>
                <div class="col-lg-4 col-md-6 sub-cat-page-cat-wapper">
                    <a href="<?php the_field('tile_10_link') ?>">
                        <div class="sub-cat-page-cat-container">
                            <img src="<?php the_field('tile_10_image') ?>" alt="<?php the_field('tile_10_text') ?>">
                            <p class="subcat-page-text"><?php the_field('tile_10_text') ?></p>
                        </div>
                    </a>
                </div>
            <?php endif; ?>
        </div>        
    </div>
</div>
<?php get_footer(); ?>