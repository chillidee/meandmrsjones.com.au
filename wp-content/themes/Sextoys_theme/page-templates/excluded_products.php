<?php
/**
 * Template Name: excluded products
 */
//get_header();

global $imported_products;
global $fields;
global $cats;
?>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"/>
<div class="row">
    <div class="loading-screen"></div>
    <input type="hidden" value="true" id="is_excluded_product"/> <?php
    $excluded_products = st_get_excluded_products();
    $feed = productCsvToArray();
    $fields = $feed[0];
    $imported_products = getExisitingProductIDS();
    $products = array();
    foreach ($feed as $product):
        if (in_array($product[0], $excluded_products)):
            array_push($products, $product);
        endif;
    endforeach;
    foreach ($products as $product):
        if (in_array($product[0], $imported_products)):
            unset($product);
        endif;
    endforeach;
    unset($imported_products);
    unset($excluded_products);
    $cats = get_cats($products);
    if (empty($_POST)):
        display_cats($cats);
    else:
        ?>
        <div class="col-lg-12">
            <?php
            $cat = $_POST['cat'];
            display_fields($feed);
            unset($feed);
            foreach ($products as $product):
                if ($product[6] === $cats[$cat]):
                    product_row($product);
                endif;
            endforeach;
            ?>
        </div><?php
    endif;

    function get_cats($product_array) {
        $cats_array = array();
        foreach ($product_array as $product):
            if (!in_array($product[6], $cats_array)):
                array_push($cats_array, $product[6]);
            endif;
        endforeach;
        return $cats_array;
    }

    function displayCatDropdown($barcode) {
        $cats = array();
        $subcats = array();
        $args = array(
            'taxonomy' => 'product_cat',
            'orderby' => 'name'
        );

        $all_categories = get_categories($args);
        foreach ($all_categories as $cat):
            if (!$cat->parent):
                $catInfo = array(
                    'cat_id' => $cat->cat_ID,
                    'cat_name' => $cat->name
                );
                array_push($cats, $catInfo);
            endif;
            if ($cat->parent):
                $catInfo2 = array(
                    'parent' => $cat->parent,
                    'cat_id' => $cat->cat_ID,
                    'cat_name' => $cat->name
                );
                array_push($subcats, $catInfo2);
            endif;
        endforeach;
        ?>
        <select class="import-category-select <?php echo $barcode ?>_dropdown">
            <option selected="true">None</option>
            <?php foreach ($cats as $index): ?>
                <option value="<?php echo $index['cat_id'] ?>"><?php echo $index['cat_name'] ?></option>
                <?php
                foreach ($subcats as $subcat):
                    if ($subcat['parent'] === $index['cat_id']):
                        ?>
                        <option value="<?php echo $subcat['cat_id'] ?>|<?php echo $subcat['parent'] ?>"> - <?php echo $subcat['cat_name'] ?></option>
                        <?php
                    endif;
                endforeach;
                ?>
            <?php endforeach; ?>
        </select>
        <?php
    }

    function product_set_price($barcode) {
        ?>
        <select id="<?php echo $barcode ?>_price_select" class="import-price-dropdown">
            <option value="0">Category Markup</option>
            <option value="1">Manual Markup</option>
            <option value="2">Manual Price</option>
        </select>
        <?php
    }

    function display_cats($cats_array) {
        ?>
        <div class="col-lg-12"><h1 style="text-align: left">Category</h1></div>
        <?php
        for ($i = 0; $i < count($cats_array); $i++):
            ?>
            <form method="POST" action="">
                <div class="col-lg-4">
                    <input name="cat" type="hidden" value="<?php echo $i ?>"/>
                    <button class="btn-link" type="submit"><?php echo $cats_array[$i] ?></button>
                </div>
            </form>
            <?php
        endfor;
    }

    function display_fields($feed) {
        ?>
        <div class="col-lg-12 import-product-row">
            <div class="col-lg-2"><h2>Image</h2></div>
            <div class="col-lg-1"><h2><?php echo $feed[0][1] ?></h2></div>
            <div class="col-lg-2"><h2><?php echo $feed[0][5] ?></h2></div>
            <div class="col-lg-2"><h2><?php echo $feed[0][2] ?></h2></div>
            <div class="col-lg-2"><h2>Import Options</h2></div>
            <div class="col-lg-1"><h2><?php echo $feed[0][4] ?></h2></div>
            <div class="col-lg-2"><h2>Actions</h2></div>
        </div>
        <?php
    }

    function display_products($feed, $category) {
        global $cats;
        $category = (int) $category;
        set_time_limit(0);
        ini_set('memory_limit', '246M');
        ?>
        <div class="row">
            <div class="loading-screen"></div>
            <div class="col-lg-12">
                <?php
                foreach ($cats as $cat => $value):
                    if (($cat !== 'Category') && ($cat === $category)):
                        ?>
                        <div class="col-lg-12 import-category-row link"><h3><?php echo $value ?></h3></div>
                        <?php
                        display_fields($feed);
                        foreach ($feed as $product):
                            if ($value === $product[6]):
                                product_row($product, $value);
                                unset($feed[$product]);
                            endif;
                        endforeach;
                    endif;
                endforeach;
                ?>
            </div>
        </div>
        <?php
    }

    function product_row($product) {
        global $fields;
        global $imageURLPath;
        $imageURL = $imageURLPath . $product[8];
        $index = 0;
        ?>
        <div class="col-lg-12 import-product-row <?php echo $product[10] ?>_row">
            <p class="<?php echo $product[10] ?>_ajax_result_message ajax_result_message"></p>
            <div class="col-lg-2"><div class="import-image-container"><img src="<?php echo Aws_Wrapper::aw_get_image_path(explode('/', $product[8]), 'temp') ?>"/></div></div>
            <div class="col-lg-1"><h6><?php echo $product[1] ?></h6></div>
            <div class="col-lg-2"><h6><?php echo $product[5] ?></h6></div>
            <div class="col-lg-2"><h6><?php echo $product[2] ?></h6></div>
            <div class="col-lg-2"><h6 class="col-lg-12" style="text-align: left">Category:</h6><div class="col-lg-12"><?php displayCatDropdown($product[10]) ?></div><h6 class="col-lg-12" style="text-align: left">Set Price by:</h6><div class="col-lg-12"><?php product_set_price($product[10]) ?></div>
                <div style="display: none;" id="<?php echo $product[10] ?>_manual_price_container">
                    <h6 style="text-align: left" class="col-lg-12">Enter Price or Markup (Number only)</h6>
                    <input style="margin: 10px 5%;width: 40%" type="text" id="<?php echo $product[10] ?>_manual_price" class="col-lg-12 manual_price_imput"/>
                    <p style="margin: 10px 5%;width: 40%;float: left;" id="<?php echo $product[10] ?>_price_result"></p>
                    <input type="hidden" value="<?php echo $product[10] ?>" class="product_barcode"/>
                </div>
            </div>
            <div class="col-lg-1"><h6 id="<?php echo $product[10] ?>_price_wholesale"><?php echo number_format($product[4], 2) ?></h6></div>
            <div class="col-lg-2">
                <form>
                    <?php foreach ($fields as $field): ?>
                        <input type="hidden" class="import-input-<?php echo $field ?>" value="<?php echo $product[$index] ?>" name="<?php echo $field ?>"/>
                        <?php
                        $index++;
                    endforeach;
                    ?>
                    <div class="col-lg-12 center-block">
                        <button class="import-product btn btn-success" type="button">Import</button>
                    </div>
                </form>
            </div>
        </div>
        <?php
    }

    //get_footer();
    ?>
    <style>
        #wpfooter {
            position: relative;
        }
        .imported-product{
            border: 1px solid green;
        }
        .excluded-product{
            border: 1px solid red;
        }
        body{
            background-color: transparent;
        }
        a {
            color: #428bca;
        }
        h2{
            font-size: 22px;
        }
        h2, h6{
            text-align: center;
            width: 90%;
            margin: 10px 5%;
        }
        .import-product-row {
            margin: 20px 0;
        }
        h3 {
            font-size: 19px;
        }
        .import-image-container {
            height: 200px;
            width: 200px;
            overflow: hidden;
        }
        .import-image-container img {
            height: 100%;
        }
        .ajax_result_message {
            padding: 10px;
        }
    </style>
    <script>
        jQuery(document).ready(function ($) {

            $('.import-product').click(function () {
                $(this).prop('disabled', true);
                var details = [];
                var form = $(this).parent().parent();
                $(form).children().each(function (index) {
                    details[index] = $(this).val();
                });
                $('.' + details[10] + '_exclude_button').prop('disabled', true);
                var cat = $('.' + details[10] + '_dropdown').val();
                var price_markup_option = $('#' + details[10] + '_price_select').find(":selected").val();
                var exclude_product = $('#is_excluded_product').val();
                var manual_price_input = '';
                var manaul_markup = '';
                if (price_markup_option === '2') {
                    manual_price_input = $('#' + details[10] + '_manual_price').val();
                }
                if (price_markup_option === '1') {
                    manaul_markup = $('#' + details[10] + '_manual_price').val();
                }
                var data = {
                    'action': 'st_import_product',
                    'code': details[0],
                    'name': details[1],
                    's_desc': details[2],
                    'l_desc': details[3],
                    'price': details[4],
                    'brand': details[5],
                    'cat': details[6],
                    'thumb': details[7],
                    'image': details[8],
                    'keywords': details[9],
                    'barcode': details[10],
                    'category': cat,
                    'price_markup_option': price_markup_option,
                    'manual_price_input': manual_price_input,
                    'manaul_markup': manaul_markup,
                    'excluded_product': exclude_product
                };
                console.log(data);
                $.post(ajaxurl, data, function (response) {
                    if (response) {
                        $('.' + details[10] + '_ajax_result_message').html('Product imported <a href="/wp-admin/post.php?post=' + response + '&action=edit" target="_blank">View</a>');
                        $('.' + details[10] + '_row').addClass('imported-product');
                    } else {
                        $('.' + details[10] + '_ajax_result_message').html('Error creating product');
                    }
                });
            });

            $(".loading-screen").css('display', 'none');

            $('.import-price-dropdown').change(function () {
                var id = $(this).attr('id');
                id = id.split("_");
                id = id[0];
                var price_drop_val = $(this).find(":selected").val();
                if (price_drop_val === '1' || price_drop_val === '2') {
                    $('#' + id + '_manual_price_container').css('display', 'block');
                }
            });

            $('.manual_price_imput').on('input', function () {
                var input_id = $(this).attr('id');
                var id = input_id.split("_");
                id = id[0];
                var price_drop_val = $('#' + id + '_price_select').find(":selected").val();
                var input_text = $('#' + id + '_manual_price').val();
                if (price_drop_val === '2') {
                    $('#' + id + '_price_result').html('$' + input_text);
                }
                if (price_drop_val === '1') {
                    var wholesale_price = $('#' + id + '_price_wholesale').html();
                    wholesale_price = parseInt(wholesale_price);
                    input_text = parseInt(input_text);
                    var new_price = wholesale_price + ((wholesale_price / 100) * input_text);
                    new_price = Math.round(new_price) - 0.01;
                    $('#' + id + '_price_result').html('$' + new_price);
                }
            });

        });
    </script>
</div>