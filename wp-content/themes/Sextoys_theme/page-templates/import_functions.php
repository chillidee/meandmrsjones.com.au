<?php

class import_functions {

    public function if_get_exisitingProducts() {

        $args = array(
            'post_type' => 'product',
            'posts_per_page' => -1,
            'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash')
        );

        $existingProducts = get_posts($args);
        $existingSkus = array();

        foreach ($existingProducts as $product):
            array_push($existingSkus, get_post_meta($product->ID, '_code', true));
        endforeach;
        return $existingSkus;
    }

    public function if_check_for_simular($code) {
        global $wpdb;
        $code_sub = self::if_get_first_three_chars($code);
        $simualr_array = array();
        $exsiting_products = self::if_get_exisitingProducts();
        foreach ($exsiting_products as $product):
            array_push($simualr_array, self::if_get_first_three_chars($product));
            unset($product);
        endforeach;
        unset($exsiting_products);
        return in_array($code_sub, $simualr_array) ? true : false;
    }

    private function if_get_first_three_chars($string) {
        return substr($string, 0, 3);
    }

    public function if_get_simular_products($code, $brand) {
        $code_sub = self::if_get_first_three_chars($code);
        $simualr_array = array();
        $brands_array = array();
        $ids = array();
        $codes = self::if_get_meta_db_query("_code");
        foreach ($codes as $product):
            if (self::if_get_first_three_chars($product->meta_value) === $code_sub && get_post_meta($product->post_id, '_is_child', true) !== 'true'):
                array_push($simualr_array, $product->post_id);
            endif;
            unset($product);
        endforeach;
        unset($codes);
        $brands = self::if_get_meta_db_query('_brand');
        foreach ($brands as $s_brand):
            if ($s_brand->meta_value === $brand):
                if (get_post_meta($s_brand->post_id, '_is_child', true) !== 'true'):
                    array_push($brands_array, $s_brand->post_id);
                    unset($s_brand);
                endif;
            endif;
        endforeach;
        foreach ($simualr_array as $id) {
            array_push($ids, $id);
            unset($id);
        }
        unset($simualr_array);
        foreach ($brands_array as $id):
            if (!in_array($id, $ids)):
                array_push($ids, $id);
            endif;
            unset($id);
        endforeach;
        unset($brands_array);
        return $ids;
    }

    private function if_get_meta_db_query($value) {
        global $wpdb;
        $wpdb->flush();
        $wpdb->query('SELECT `post_id`,`meta_value` FROM `wp_postmeta` WHERE `meta_key` = "' . $value . '"');
        return $wpdb->last_result;
    }

    public function if_show_related_from_category_dropdown($code) {
        $cats_array = get_all_categories();
        ?>
        <select class="import-related-cat-dropdown" id="<?php echo $code ?>-related-cat-dropdown"> <?php foreach ($cats_array as $cat): ?>
                <option value="<?php echo $cat->term_id ?>"><?php echo $cat->name ?></option>
            <?php endforeach;
            ?> </select>
        <?php
    }

    public function if_add_custom_attr_box($code) {
        ?>
        <h6>Custom Attribute</h6>
        <form id="<?php echo $code ?>_import_custom_attr_input_form">
            <label class="col-lg-6" for="<?php echo $code ?>_import_custom_attr_input_key_0">Key</label>
            <label class="col-lg-6" for="<?php echo $code ?>_import_custom_attr_input_value_0">Value</label>
            <input class="col-lg-5" type="text" id="<?php echo $code ?>_import_custom_attr_input_key_0" class="import-custom-attr-input"/>
            <input class="col-lg-5" type="text" id="<?php echo $code ?>_import_custom_attr_input_value_0" class="import-custom-attr-input"/>
            <div class="col-lg-2" id="<?php echo $code ?>_import_custom_attr_row_0"><i class="fa fa-plus import-add-row" aria-hidden="true" id="<?php echo $code ?>_import_custom_attr_input_add_row_0" style="color: green"></i></div>
        </form>
        <?php
    }

    public function if_add_custom_attr_box_new($code) {
        $attr = self::if_get_custom_attributes();
        ?>
        <h6>Custom Attribute</h6>
        <form id="<?php echo $code ?>_import_custom_attr_input_form"> <?php foreach ($attr as $index => $att): ?>
                <label class="col-lg-6" for="<?php echo $code ?>_import_custom_attr_input_value_<?php echo $index; ?>"><?php echo $att->attribute_label ?></label>
                <input class="col-lg-6" type="text" id="<?php echo $code ?>_import_custom_attr_input_value_<?php echo $att->attribute_label; ?>" class="import-custom-attr-input"/>
            <?php endforeach;
            ?></form>
        <?php
    }

    function if_get_custom_attributes() {
        global $woocommerce;
        return wc_get_attribute_taxonomies();
    }

    function if_custom_attributes_dropdown($code) {
        $attr = self::if_get_custom_attributes();
        ?>
        <select> <?php foreach ($attr as $att): ?>
                <option value="<?php echo $att->attribute_id ?>|<?php echo $att->attribute_name ?>" id="<?php echo $code ?>_custom_attribute_dropdown" class="custom_attribute_dropdown"><?php echo ucwords($att->attribute_label) ?></option>
            <?php endforeach;
            ?> </select>
        <?php
    }

    function if_product_set_price($barcode) {
        ?>
        <select id="<?php echo $barcode ?>_price_select" class="import-price-dropdown">
            <option value="0">Category Markup</option>
            <option value="1">Manual Markup</option>
            <option value="2">Manual Price</option>
        </select>
        <?php
    }

    function if_display_product_edit_box($product) {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                tinymce.init({
                    selector: 'textarea#<?php echo $product[0] ?>_import_product_short_edit'
                });
                tinymce.init({
                    selector: 'textarea#<?php echo $product[0] ?>_import_product_long_edit'
                });
            });
        </script>
        <div id="<?php echo $product[0] ?>_import_product_edit_overlay" class="import-edit-box-overlay row">
            <i class="fa fa-times import_product_edit-close" aria-hidden="true" id="<?php echo $product[0] ?>_import_product_edit-close"></i>
            <h2>Edit Product</h2>

            <h6>Title</h6>
            <input type="text" id="<?php echo $product[0] ?>_import_product_title_edit" value="<?php echo $product[1] ?>"/>

            <h6>Brand</h6>
            <input type="text" id="<?php echo $product[0] ?>_import_product_brand_edit" value="<?php echo $product[5] ?>"/>

            <h6>Short Description</h6>
            <textarea id="<?php echo $product[0] ?>_import_product_short_edit"><?php echo $product[2] ?></textarea>

            <h6>Long Description</h6>
            <textarea id="<?php echo $product[0] ?>_import_product_long_edit"><?php echo $product[3] ?></textarea>

            <h6>Category:</h6>
            <?php self::if_displayCatDropdown($product[10]) ?>
            <h6>Set Price by:</h6>
            <?php self::if_product_set_price($product[10]) ?>
            <h6>Enter Price or Markup (Number only)</h6>
            <input type="text" id="<?php echo $product[10] ?>_manual_price" class="col-lg-12 manual_price_imput"/>
            <p id="<?php echo $product[10] ?>_price_result"></p>
            <input type="hidden" value="<?php echo $product[10] ?>" class="product_barcode"/>

            <label for="<?php echo $product[0] ?>_variable_input">Add as associated product?</label>
            <input type="checkbox" id="<?php echo $product[0] ?>_variable_input" class="import_variable_checkbox" name="<?php echo $product[0] ?>_variate_input">
            <select class="import-simular-products-dropdown" style="display: none" id="<?php echo $product[0] ?>_checkbox">
                <?php foreach (self::if_get_simular_products($product[0], $product[5]) as $id): ?>
                    <option value="<?php echo $id ?>|<?php echo get_the_guid($id) ?>"><?php echo ucwords(get_the_title($id)) ?></option>
                <?php endforeach; ?>
            </select>
            <a href="" style="display: none" id="<?php echo $product[0] ?>_view_product_button" target="_blank" class="import_view_simular_product_button btn col-lg-4">View product</a>
            <h6 style="text-align: left">Show related products from:</h6>
            <?php self::if_show_related_from_category_dropdown($product[0]) ?>
            <?php self::if_add_custom_attr_box_new($product[0]) ?>
            <div class="col-lg-12">
                <button class="import-edit-product-save-btn btn btn-default" id="<?php echo $product[0] ?>_import_edit_product_save_btn" type="button">Save</button>
            </div>
        </div>
        <?php
    }

    function if_displayCatDropdown($barcode) {
        $cats = array();
        $subcats = array();
        $args = array(
            'taxonomy' => 'product_cat',
            'orderby' => 'name'
        );

        $all_categories = get_categories($args);
        foreach ($all_categories as $cat):
            if (!$cat->parent):
                $catInfo = array(
                    'cat_id' => $cat->cat_ID,
                    'cat_name' => $cat->name
                );
                array_push($cats, $catInfo);
            endif;
            if ($cat->parent):
                $catInfo2 = array(
                    'parent' => $cat->parent,
                    'cat_id' => $cat->cat_ID,
                    'cat_name' => $cat->name
                );
                array_push($subcats, $catInfo2);
            endif;
        endforeach;
        ?>
        <select class="import-category-select <?php echo $barcode ?>_dropdown">
            <option selected="true">None</option>
            <?php foreach ($cats as $index): ?>
                <option value="<?php echo $index['cat_id'] ?>"><?php echo $index['cat_name'] ?></option>
                <?php
                foreach ($subcats as $subcat):
                    if ($subcat['parent'] === $index['cat_id']):
                        ?>
                        <option value="<?php echo $subcat['cat_id'] ?>|<?php echo $subcat['parent'] ?>"> - <?php echo $subcat['cat_name'] ?></option>
                        <?php
                    endif;
                endforeach;
                ?>
            <?php endforeach; ?>
        </select>
        <?php
    }

    function if_import_javascript() {
        ?>
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>
            jQuery(document).ready(function ($) {

                //                add_custom_attr_handler('.import-add-row');

                $('.import-edit-product-save-btn').click(function () {
                    $('html').css('overflow', 'scroll');
                    var code = ($(this).attr('id')).split("_");
                    code = code[0];
                    var title = $('#' + code + '_import_product_title_edit').val();
                    var brand = $('#' + code + '_import_product_brand_edit').val();
                    var short = tinyMCE.get(code + '_import_product_short_edit').getContent();
                    var long = tinyMCE.get(code + '_import_product_long_edit').getContent();


                    var cust_attr = [];

                    $('#' + code + '_import_custom_attr_input_form').find('input').each(function () {
                        var id = $(this).attr('id').split("_");
                        var attr = id[id.length - 1];
                        var value = $(this).val();
                        if (value) {
                            attr = 'st_' + attr;
                            id = id[0];
                            cust_attr[attr] = value;
                        }
                    });

                    $('.' + code + '_import_product_custom_attr').each(function () {
                        var input_value = $(this).val();
                        var input_value_array = input_value.split("|");
                        if (cust_attr[input_value_array[1]]) {
                            $(this).val('true|' + input_value_array[1] + '|' + input_value_array[2] + '|' + cust_attr[input_value_array[1]]);
                        }
                    });

                    $('#' + code + '_import_product_title').html(title);
                    $('#' + code + '_import_product_Name').val(title);

                    $('#' + code + '_import_product_brand').html(brand);
                    $('#' + code + '_import_product_Brand').val(brand);

                    $('#' + code + '_import_product_short').html(short);
                    $('#' + code + '_import_product_Short_Description').val(short);

                    $('#' + code + '_import_product_long').html(long);
                    $('#' + code + '_import_product_Long_Description').val(long);

                    var iframe = $('#' + code + '_import_product_edit_overlay');
                    if (iframe.css('display', 'block')) {
                        iframe.css('display', 'none');
                    }
                });

                //                function add_custom_attr_handler(idOrClass) {
                //                    $(idOrClass).click(function () {
                //                        var code = ($(this).attr('id')).split("_");
                //                        console.log(code);
                //                        var orginal_number = code[code.length - 1];
                //                        console.log(orginal_number);
                //                        var input_number = orginal_number + 1;
                //                        console.log(input_number);
                //                        orginal_number = code[code.length - 1];
                //                        input_number = parseInt(orginal_number) + 1;
                //                        code = code[0];
                //                        var html = '<input class="col-lg-5" type="text" id="' + code + '_import_custom_attr_input_key_' + input_number + '" class="import-custom-attr-input"/>' +
                //                                '<input class="col-lg-5" type="text" id="' + code + '_import_custom_attr_input_value_' + input_number + '" class="import-custom-attr-input"/><div class="col-lg-2">' +
                //                                '<i class="fa fa-plus import-add-row" aria-hidden="true" id="' + code + '_import_custom_attr_input_add_row_' + input_number + '" style="color: green"></i></div>';
                //                        $('#' + code + '_import_custom_attr_row_' + orginal_number).after(html).promise().done(function () {
                //                            $('#' + code + '_import_custom_attr_row_' + orginal_number).css('display', 'none');
                //                            $(this).unbind();
                //                            add_custom_attr_handler('#' + code + '_import_custom_attr_input_add_row_' + input_number);
                //                        });
                //                    });
                //                }

                $('.import_product_edit-close').click(function () {
                    var code = ($(this).attr('id')).split("_");
                    code = code[0];
                    var iframe = $('#' + code + '_import_product_edit_overlay');
                    if (iframe.css('display', 'block')) {
                        iframe.css('display', 'none');
                        $('html').css('overflow', 'scroll');
                    }
                });

                $('.import-edit-product').click(function () {
                    $('html').css('overflow', 'hidden');
                    var code = ($(this).attr('id')).split("_");
                    code = code[0];
                    var iframe = $('#' + code + '_import_product_edit_overlay');
                    if (iframe.css('display', 'none')) {
                        iframe.css('display', 'block');
                    }
                });

                $('.import-simular-products-dropdown').change(function () {
                    var code = ($(this).attr('id')).split("_");
                    code = code[0];
                    var selected = $(this).find(":selected").val();
                    selected = selected.split("|");
                    selected = selected[1];
                    $('#' + code + '_view_product_button').attr('href', selected);
                });

                $('.import_view_simular_product_button').click(function () {
                    var code = ($(this).attr('id')).split("_");
                    code = code[0];

                });

                $('.import_variable_checkbox').click(function () {
                    var code = ($(this).attr('id')).split("_");
                    code = code[0];
                    var guid = $('#' + code + '_checkbox').find(":selected").val();
                    guid = guid.split("|");
                    guid = guid[1];
                    $('#' + code + '_view_product_button').attr('href', guid);
                    if ($(this).attr('checked')) {
                        $('#' + code + '_import_product_parent').val('true');
                        $('#' + code + '_checkbox').css('display', 'block');
                        $('#' + code + '_view_product_button').css('display', 'block');
                    } else {
                        $('#' + code + '_import_product_parent').val('false');
                        $('#' + code + '_checkbox').css('display', 'none');
                        $('#' + code + '_view_product_button').css('display', 'none');
                    }
                });

                $('.import-product').click(function () {
                    $(this).prop('disabled', true);
                    var details = [];
                    var form = $(this).parent().parent();
                    $(form).children().each(function (index) {
                        details[index] = $(this).val();
                    });
                    $('.' + details[10] + '_exclude_button').prop('disabled', true);
                    $('.' + details[0] + '_edit_product_button').prop('disabled', true);
                    var cat = $('.' + details[10] + '_dropdown').val();
                    var price_markup_option = $('#' + details[10] + '_price_select').find(":selected").val();
                    var exclude_product = $('#is_excluded_product').val();
                    var manual_price_input = '';
                    var manaul_markup = '';
                    if (price_markup_option === '2') {
                        manual_price_input = $('#' + details[10] + '_manual_price').val();
                    }
                    if (price_markup_option === '1') {
                        manaul_markup = $('#' + details[10] + '_manual_price').val();
                    }
                    var related_category = $('#' + details[0] + '-related-cat-dropdown').find(":selected").val();

                    var parent_product = $('#' + details[0] + '_import_product_parent').val();

                    if (parent_product) {
                        var parent_product_id = $('#' + details[0] + '_checkbox').find(":selected").val();
                        parent_product_id = parent_product_id.split("|");
                        parent_product_id = parent_product_id[0];

                    }
                    var custom_attr_clean = {};
                    $('.' + details[0] + '_import_product_custom_attr').each(function () {
                        var input_value = $(this).val();
                        var input_value_array = input_value.split("|");
                        if (input_value_array[0] === 'true') {
                            custom_attr_clean[input_value_array[1]] = input_value_array[input_value_array.length - 1];
                        }
                    });

                    var data = {
                        'action': 'st_import_product',
                        'code': details[0],
                        'name': details[1],
                        's_desc': details[2],
                        'l_desc': details[3],
                        'price': details[4],
                        'brand': details[5],
                        'cat': details[6],
                        'thumb': details[7],
                        'image': details[8],
                        'keywords': details[9],
                        'barcode': details[10],
                        'category': cat,
                        'price_markup_option': price_markup_option,
                        'manual_price_input': manual_price_input,
                        'manaul_markup': manaul_markup,
                        'excluded_product': exclude_product,
                        'related_category': related_category,
                        'custom_attr': custom_attr_clean,
                        'parent_product': parent_product,
                        'parent_product_id': parent_product_id
                    };
                    $.post(ajaxurl, data, function (response) {
                        if (response) {
                            $('.' + details[10] + '_ajax_result_message').html('Product imported <a href="/wp-admin/post.php?post=' + response + '&action=edit" target="_blank">View</a>');
                            $('.' + details[10] + '_row').addClass('imported-product');
                        } else {
                            $('.' + details[10] + '_ajax_result_message').html('Error creating product');
                        }
                    });
                });
                $(".loading-screen").css('display', 'none');

                $('.import-price-dropdown').change(function () {
                    var id = $(this).attr('id');
                    id = id.split("_");
                    id = id[0];
                    var price_drop_val = $(this).find(":selected").val();
                    if (price_drop_val === '1' || price_drop_val === '2') {
                        $('#' + id + '_manual_price_container').css('display', 'block');
                    }
                });

                $('.manual_price_imput').on('input', function () {
                    var input_id = $(this).attr('id');
                    var id = input_id.split("_");
                    id = id[0];
                    var price_drop_val = $('#' + id + '_price_select').find(":selected").val();
                    var input_text = $('#' + id + '_manual_price').val();
                    if (price_drop_val === '2') {
                        $('#' + id + '_price_result').html('$' + input_text);
                    }
                    if (price_drop_val === '1') {
                        var wholesale_price = $('#' + id + '_price_wholesale').html();
                        wholesale_price = parseInt(wholesale_price);
                        input_text = parseInt(input_text);
                        var new_price = wholesale_price + ((wholesale_price / 100) * input_text);
                        new_price = Math.round(new_price) - 0.01;
                        $('#' + id + '_price_result').html('$' + new_price);
                    }
                });
                $('.exclude-product').click(function () {
                    var details = [];
                    var form = $(this).parent().parent();
                    $(form).children().each(function (index) {
                        details[index] = $(this).val();
                        $(this).prop('disabled', true);
                    });
                    $('.' + details[10] + '_exclude_button').prop('disabled', true);
                    var data = {
                        'action': 'st_put_excluded_product',
                        'code': details[0],
                        'barcode': details[10]
                    };
                    $.post(ajaxurl, data, function (response) {
                        if (response) {
                            $('.' + details[10] + '_row').addClass('excluded-product');
                            $('.' + details[10] + '_ajax_result_message').html('Product excluded');
                        } else {
                            $('.' + details[10] + '_ajax_result_message').html('Error excluding product');
                        }
                    });
                });
            });
        </script>
        <?php
    }

    function if_import_styles() {
        ?>
        <style>
            label{
                margin: 10px 5%;
            }
            select{
                width: 100%;
            }
            button{
                margin-top: 10px
            }
            #wpfooter {
                position: relative;
            }
            body{
                background-color: transparent;
            }
            a {
                color: #428bca;
            }
            h2{
                font-size: 22px;
            }
            h2, h6{
                text-align: center;
                width: 90%;
                margin: 10px 5%;
            }
            h6{
                text-align: left;
            }
            .imported-product {
                border: 1px solid green;
                padding: 20px;
            }
            h3 {
                font-size: 19px;
            }
            .import-image-container {
                overflow: hidden;
            }
            .import-image-container img {
                width: 100%;
            }
            .ajax_result_message {
                padding: 10px;
            }

            .imported-product{
                border: 1px solid green;
            }
            .excluded-product{
                border: 1px solid red;
            }
            .import-custom-attr-input{
                margin: 10px 0;
            }
            .fa{
                cursor: pointer;
            }
            .import-add-row,input{
                margin: 10px 0;
            }
            .import-edit-box-overlay {
                position: fixed;
                height: 75%;
                width: 75%;
                margin: 5% 0 0 15.5%;
                background-color: #fff;
                left: 0;
                top: 0;
                border: 1px solid #ccc;
                display: none;
                z-index: 1;
                overflow: auto;
            }
            .import_product_edit-close {
                color: black;
                position: absolute;
                right: 2%;
                top: 5%;
                font-size: 20px;
            }
            .import-edit-box-overlay h6,.import-edit-box-overlay input,.import-edit-box-overlay textarea,.import-edit-box-overlay select,.mce-panel,.import_view_simular_product_button {
                width: 90%;
                margin: 10px 5%;
            }
            .import_variable_checkbox {
                width: 2% !important;
                margin: 10px 0 !important;
            }
            button {
                width: 100%;
            }
            .import_view_simular_product_button {
                text-align: left;
                padding: 0;
            }
            .import-edit-product-save-btn {
                margin: 10px 5%;
                width: auto;
            }
            .btn-link{
                text-align: left;
            }
            .import-product-row {
                margin: 10px 0;
            }
        </style>
        <?php
    }

}
