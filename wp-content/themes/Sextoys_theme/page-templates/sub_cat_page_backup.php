<?php
/**
 * Template Name: Sub Cat Page
 */
get_header();
$parent_cat_ID = 9;
$args = array(
    'hierarchical' => 1,
    'show_option_none' => '',
    'hide_empty' => 0,
    'parent' => $parent_cat_ID,
    'taxonomy' => 'product_cat'
);
//$subcats = get_categories($args);
$urlArray = explode('/', $_SERVER['REQUEST_URI']);
//$subcats = woocommerce_subcats_from_parentcat_by_NAME2($urlArray[count($urlArray) - 2]);
$subcats = st_subcats_from_parentcat_by_id(9);
foreach ($subcats as $subcat):
    $imageArray = st_get_category_image($subcat->term_id, array(480, 480));
    ?>
    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 home-images">
        <div class="home2ImageContainer">
            <a href="<?php echo get_category_link($subcat->term_id) ?>">
                <img src="<?php echo $imageArray[0] ?>" title="<?php echo ucwords($subcat->slug) ?>" alt="<?php echo ucwords($subcat->slug) ?>"/>
                <div class="home-cat-hover-text"><h4><?php echo ucwords($subcat->slug) ?></h4></div>
            </a>
        </div>
    </div>
    <?php
endforeach;
?>
</div>
<?php
get_footer();
?>
