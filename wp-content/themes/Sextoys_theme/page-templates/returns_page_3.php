<?php
/**
 * Template Name: Returns Page Apparel
 */
get_header();
$payment = st_get_cyber_payment();
if (!empty($_POST)):
    $option = htmlentities($_POST['apparelReturnSelect']);
    if ($option == 4 || $option == 5):
        if ($payment->is_available() && !$payment->validate_fields()):
            apparelReturnForm($payment, 'Credit card validation failed');
        else:
            if ($payment->is_available()):
                if (!isset($_POST['order_id'])):
                    $order = st_create_return_order(strip_tags($POST["email"]));
                else:
                    $order = $_POST['order_id'];
                endif;
                $result = $payment->process_payment($order);
                if ($result["result"] == "success"):
                    return_send_emails($_POST, $_FILES);
                    return_thank_you_page();
                else:
                    apparelReturnForm($payment, 'An error has processing occured processing your payment try again');
                endif;
            else:
                apparelReturnForm($payment, 'An error has occured please refresh and try again');
            endif;
        endif;
    else:
        return_send_emails($_POST, $_FILES);
        return_thank_you_page();
    endif;
else:
    apparelReturnForm($payment);
endif;

function return_send_emails($POST, $FILES) {
    $target_file = null;
    require_once get_template_directory() . '/functions/email_functions.php';
    $emails = new email_orders();
    global $woocommerce;
    $returnArray = array();
    $_pf = new WC_Product_Factory();
    if ($POST["form_name"] == 'form_apparel_return'):
        $returnArray['name'] = htmlspecialchars($POST["customer_name"]);
        $returnArray['email'] = strip_tags($POST["email"]);
        $returnArray['phone'] = htmlspecialchars($POST["phone"]);
        $returnArray['order_number'] = (int) htmlspecialchars($POST["order_number"]);
        $returnArray['exchange_info'] = htmlspecialchars($POST["exchange_info"]);
        $returnArray['warranty'] = (htmlspecialchars($POST["apparelReturnSelect"] == 1)) ? 'Yes' : 'No';
        $returnArray['product_id'] = (int) htmlspecialchars($POST["product_select"]);
        if (isset($POST["contact_email"])):
            $returnArray['contact_by'] = 'email';
        endif;
        if (isset($POST["contact_phone"])):
            $returnArray['contact_by'] = 'phone';
        endif;
        if (!empty($FILES["productImage"]["tmp_name"])):
            $target_dir = st_get_site_root() . "/return_images/";
            $target_file = $target_dir . basename($FILES["productImage"]["name"]);
            move_uploaded_file($FILES["productImage"]["tmp_name"], $target_file);
        endif;
        $order = new WC_Order($returnArray['order_number']);
        $_product = $_pf->get_product($returnArray['product_id']);
        $_product = $_product->post;
        if (isset($order)):
            $returnArray['order']['billing'] = $order->get_formatted_billing_address();
            $returnArray['order']['shipping'] = $order->get_formatted_billing_address();
            if (!empty($returnArray)):
                switch (get_post_meta($returnArray['product_id'], 'vendor', true)):
                    case 'Windsor Wholeseller':
                        $email_address = array('michaelf@windsorwholesale.com.au', 'WWexchange@meandmrsjones.com.au', $returnArray['email']);
                        break;
                    case 'Wrapped Secrets':
                        $email_address = array('WWexchange@meandmrsjones.com.au', 'mark@wrappedsecrets.com.au', $returnArray['email']);
                        break;
                    default:
                        $email_address = 'info@meandmrsjones.com.au';
                        break;
                endswitch;
                //$email_address = array($return_email, $returnArray['email']);
                $subject = 'Me and Mrs Jones Return Request';
                $headers = 'From: MMJ <info@meandmrsjones.com.au>' . "\r\n";
                $content = "<h1>Customer Details</h1>";
                $content .= "<p>Name: " . $returnArray['name'] . "</p>";
                $content .= "<p>Phone: " . $returnArray['phone'] . "</p>";
                $content .= "<p>Email: " . $returnArray['email'] . "</p>";
                $content .= "<p>Contact By: " . ucwords($returnArray['contact_by']) . "</p>";
                $content .= "<h1>Order Details</h1>";
                $content .= "<p>Order number: " . $returnArray['order_number'] . "</p>";
                $content .= "<p>Purchased on: " . $order->order_date . "</p>";
                $content .= "<p>Is faulty: " . $returnArray['warranty'] . "</p>";
                $content .= "<p>Billing Address:</p>";
                $content .= "<p>" . $returnArray['order']['billing'] . "</p>";
                $content .= "<p>Shipping Address:</p>";
                $content .= "<p>" . $returnArray['order']['shipping'] . "</p>";
                $content .= "<h1>Product Details</h1>";
                $content .= "<p>Product code: " . get_post_meta($returnArray['product_id'], '_code', true) . "</p>";
                $content .= "<p>Title: " . $_product->post_title . "</p>";
                $content .= "<p>Size: " . get_post_meta($returnArray['product_id'], 'st_size', true) . "</p>";
                $content .= "<p>Colour: " . get_post_meta($returnArray['product_id'], 'st_colour', true) . "</p>";
                $content = $emails->wrapContent($subject, $content, false);
                if (isset($POST['cost_accept'])):
                    $target_file = createPDFContent($returnArray, $content);
                endif;
                //foreach ($email_address as $email):
                $emails->sendmail($email_address, $subject, $content, $headers, $target_file);
            //endforeach;
            endif;
        endif;
    endif;
}

function createContent() {

}

function createPDFContent($returnArray, $content) {
    $target_dir = st_get_site_root() . "/return_images/" . $returnArray['order_number'] . '.pdf';
    st_get_pdf_plugin();
    $pdf = new mPDF();
    $pdf->WriteHTML(file_get_contents(get_template_directory_uri() . '/css/pdf_css.css'), 1);
    $pdf->WriteHTML(utf8_encode($content), 2);
    $pdf = $pdf->Output('', 'S');
    $target_file = fopen($target_dir, "w");
    if ($target_file):
        fwrite($target_file, $pdf);
        fclose($target_file);
    endif;
    return $target_dir;
}

function apparelReturnForm($payment, $error_message) {
    ?>
    <div class="sub-cat-background-image">
    </div>
    <div class="col-lg-12 sub-page-cat-container" >
        <div class="cart-checkout-header col-lg-10 col-lg-offset-1 col-xs-12 col-sm-12 col-md-12 no-padding">
            <h1><p>Exchange policy for garments &amp; shoes</p></h1>
        </div>
        <div class="col-lg-7 col-lg-offset-1 col-xs-12 col-sm-12 col-md-12 empty-cart-container no-padding">
            <p>If you have received a faulty garment or incorrect size or colour than stated on your receipt, we will replace it with an identical garment. If the item is no longer available or unable to be replaced, we will provide you with a refund for the total value of the item. Please do not return faulty garments to Me and Mrs. Jones. You only need to fill out the form below and  upload a photo of the faulty garment or incorrect order in the form below to process your exchange. A replacement product will be sent to you within approximately 10 days of acceptance of your completed exchange form.</p>
            <p>&nbsp</p>
            <p>If you have ordered the wrong size or colour or have had a change of mind, you may exchange your garment, but you will be charged for the shipping fees. Standard postage fees will apply to return orders. Please refer to the size charts provided for all of our apparel to avoid ordering an incorrect size. To exchange, please fill out the form below. Print the form and include it in return package to Me & Mrs. Jones to be mailed to the address below. You will be responsible for the return shipping of your parcel and you will be charged for the replacement shipping.</p>
            <p>&nbsp</p>
            <p>You may not return items that have been worn or washed. All returns must have all original tags and packaging to be accepted. Items may only be returned within 14 days of receipt.</p>
            <p>&nbsp</p>
            <p>
                Me & Mrs. Jones<br>
                P.O. Box 5303<br>
                Algester, QLD 4115<br>
            </p>
            <p>&nbsp</p>
            <p>For support please contact <a href="mailto:info@meandmrsjones.com.au">info@meandmrsjones.com.au</a></p>
        </div>
        <div class="col-lg-7 col-lg-offset-1 col-xs-12 col-sm-12 col-md-12 empty-cart-container no-padding">
            <h2>Complete this form to return and/or exchange garments or shoes</h2>
            <form class="checkout woocommerce-checkout" name="apparel_return" method="post" action="" enctype="multipart/form-data">
                <?php if (isset($order)): ?>
                    <input type="hidden" name="order_id" value="<?php echo $order ?>"/>
                    <?php
                endif;
                if (isset($error_message)):
                    ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p class="error text-center"><?php echo $error_message ?></p>
                    </div>
                <?php endif;
                ?>
                <input type="hidden" value="form_apparel_return" name="form_name"/>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <p><strong>Contact</strong></p>
                    <div class="col-lg-6 col-xs-12 col-sm-12 col-md-12">
                        <label class="required" for="name">Name:</label><input value="<?php echo (isset($_POST['customer_name'])) ? $_POST['customer_name'] : '' ?>" name="customer_name"/>
                        <p class="error hidden">Please enter your name</p>
                        <p>I prefer to be contacted by:</p>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 no-padding squaredFour">
                            <p>Email:</p>
                            <input type="checkbox" <?php echo (isset($_POST['contact_email']) && $_POST['contact_email'] == "on") ? ' checked ' : '' ?> name="contact_email" id="contact_email"/><label for="contact_email"></label>
                        </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 no-padding squaredFour">
                            <p>Phone:</p>
                            <input type="checkbox" <?php echo (isset($_POST['contact_phone']) && $_POST['contact_phone'] == "on") ? ' checked ' : '' ?> name="contact_phone" id="contact_phone"/><label for="contact_phone"></label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12 col-sm-12 col-md-12">
                        <label class="required" for="email">Email:</label><input name="email" value="<?php echo (isset($_POST['email'])) ? $_POST['email'] : '' ?>"/>
                        <p class="error hidden">Please enter your email address</p>
                        <label class="required" for="phone">Phone:</label><input name="phone" value="<?php echo (isset($_POST['phone'])) ? $_POST['phone'] : '' ?>"/>
                        <p class="error hidden">Please enter your phone number</p>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <p><strong>Your Exchange</strong></p>
                    <div class="col-lg-6 col-xs-12 col-sm-12 col-md-12">
                        <label class="required" for="order_number">Order No:</label><input value="<?php echo (isset($_POST['order_number'])) ? $_POST['order_number'] : '' ?>" name="order_number"/>
                        <p class="error hidden">Please enter your phone number</p>
                        <p style="font-size: 14px;">Find your order number on the top of your receipt</p>
                    </div>
                    <div class="col-lg-6 col-xs-12 col-sm-12 col-md-12">
                        <label class="required" for="exchange_info">What are you exchanging?</label>
                        <input type="hidden" name="product_select_post" value="<?php echo $_POST['product_select'] ?>"/>
                        <select name="product_select">
                            <option value="-1">Enter a order number</option>
                        </select>
                        <p class="error hidden">Please select your product</p>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-xs-12 col-sm-12 col-md-12">
                        <select name="apparelReturnSelect">
                            <option value="-1">Please select an option</option>
                            <option value="1" <?php echo (isset($_POST['apparelReturnSelect']) && $_POST['apparelReturnSelect'] == "1") ? ' selected ' : '' ?>>Item was faulty upon arrival</option>
                            <option value="2" <?php echo (isset($_POST['apparelReturnSelect']) && $_POST['apparelReturnSelect'] == "2") ? ' selected ' : '' ?>>I received the wrong colour item</option>
                            <option value="3" <?php echo (isset($_POST['apparelReturnSelect']) && $_POST['apparelReturnSelect'] == "3") ? ' selected ' : '' ?>>I received an incorrect size</option>
                            <option value="4" <?php echo (isset($_POST['apparelReturnSelect']) && $_POST['apparelReturnSelect'] == "4") ? ' selected ' : '' ?>>I changed my mind</option>
                            <option value="5" <?php echo (isset($_POST['apparelReturnSelect']) && $_POST['apparelReturnSelect'] == "5") ? ' selected ' : '' ?>>I ordered the wrong size or colour</option>
                        </select>
                    </div>
                    <div class="col-lg-6 col-xs-12 col-sm-12 col-md-12 hidden return-payment-text">
                        <p>We only offer free exchange/replacement for items that are faulty upon arrival or if your received a size or colour that is different from your original order.Any other exchange will be at the shipping cost of the customer. Standard shipping rates will apply. You must include a printed copy of this form in your return parcel.</p>
                    </div>
                </div>
                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 hidden file-upload-wrapper">
                    <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 no-padding">
                        <p class="required">Upload a Photo of Faulty Item:</p>
                        <input type="file" name="productImage" id="fileToUpload">
                        <p class="error hidden">Please upload an image</p><label for="productImage">.jpeg max 8mb</label>
                    </div>
                </div>
                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 hidden faulty-submit-button">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <button name="submit_apparel_form_faulty_button" type="button">Submit</button>
                        <p class="text-center">You will be emailed a copy of this form for your convenience</p>
                    </div>
                </div>
                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 woocommerce hidden cost_payment">
                    <table class="col-lg-8 col-lg-offset-2 col-md-12 col-xs-12 col-sm-12 shop_table woocommerce-checkout-review-order-table">
                        <thead>
                            <tr>
                                <th class="product-name">Product</th>
                                <th class="product-total">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="cart_item">
                                <td class="product-name">
                                    <strong class="product-quantity"></strong>Return Shipping</td>
                                <td class="product-total">
                                    <span class="amount">Flat rate: $15.00 AUD</span></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr class="order-total">
                                <th>Total</th>
                                <td><strong><span class="amount">$15.00</span></strong>  AUD</td>
                            </tr>
                        </tfoot>
                    </table>
                    <div id="payment" class="woocommerce-checkout-payment payment_method_cybersource payment_box col-lg-8 col-lg-offset-2 col-md-12 col-xs-12 col-sm-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <p style="padding:5px;color: #B7A168 !important">All Transactions are processed in Australia</p>
                        </div>
                        <label for="payment_method_cybersource">Credit Card
                            <img src="/wp-content/plugins/woocommerce-gateway-cybersource/assets/images/card-001.png" alt="001">
                            <img src="/wp-content/plugins/woocommerce-gateway-cybersource/assets/images/card-002.png" alt="002">
                        </label>
                        <?php
                        if ($payment->is_available()):
                            $payment->payment_fields();
                        endif;
                        ?>
                        <div class="form-row place-order">
                            <noscript>
                            Since your browser does not support JavaScript, or it is disabled, please ensure you click the &lt;em&gt;Update Totals&lt;/em&gt; button before placing your order. You may be charged more than the amount stated above if you fail to do so.        &lt;br/&gt;&lt;input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="Update totals" /&gt;
                            </noscript>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 hidden cost-submit-button">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 squaredFour cost_payment_accept_wrapper">
                        <input type="checkbox" id="cost_accept" name="cost_accept"/><label for="cost_accept"></label>
                        <p style="display: inline-block;margin-left: 20px !important;">
                            I have read the return policy and I acknowledge that I will be charged shipping for returns.
                        </p>
                        <p class="error hidden">Please accept the return cost</p>
                        <p>Thanks for filling out the return/exchange form at Me & Mrs. Jones. We will do our best to get your order sorted for you as soon as possible. A copy of your submission has been attached for your convenience. Remember that if you are sending any items back to us you must print the attached form and include it inside your parcel.</p>
                        <button name="submit_apparel_form_cost_button" type="button">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php
    wp_enqueue_script('return_forms_js');
}

function return_thank_you_page() {
    ?>
    <div class="sub-cat-background-image">
    </div>
    <div class="col-lg-12 sub-page-cat-container" >
        <div class="cart-checkout-header col-lg-7 col-lg-offset-1 col-xs-12 col-sm-12 col-md-12">
            <p>Thank you. A copy of your submission will be emailed to you for your convenience.</p>
        </div>
        <div class="col-lg-7 col-lg-offset-1 col-xs-12 col-sm-12 col-md-12 empty-cart-container">
            <p>You will receive a confirmation of your return/exchange in your email for your convenience.  Exchanges will be shipped within approximately 10 days of acceptance by Me & Mrs. Jones. Remember that if you are sending any items back to us you must print your return/exchange form and include it inside your parcel.</p>
        </div>
    </div>
    <?php
}

get_footer();
?>
