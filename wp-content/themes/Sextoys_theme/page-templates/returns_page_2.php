<?php
/**
 * Template Name: Returns Page st
 */
get_header();
?>
<div class="sub-cat-background-image">
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sub-page-cat-container">
    <div class="cart-checkout-header col-lg-10 col-lg-offset-1 col-xs-12 col-sm-12 col-md-12 no-padding">
        <h1><p>Exchange policy for sex toys &amp; adult products</p></h1>
    </div>
    <div class="col-lg-7 col-lg-offset-1 col-xs-12 col-sm-12 col-md-12 empty-cart-container no-padding">
        <p>For health and hygiene reasons, we cannot exchange any sex toys, adult products or health & hygiene products, unless the product is faulty. We do not accept any returns or exchanges on health and hygiene items or personal lubricants (Our Essentials).We will replace any faulty item within 14 days of the receipt date. We always do our best to ensure that the items we ship to you are in top condition. However, if you feel as though you have received an item that is faulty, we will be happy to have the product replaced free of charge. To request a replacement of a faulty item, fill out the form below and upload a photograph of the faulty product. Once we have verified that the product is indeed faulty, you will be shipped a replacement. If we are unable to replace the product you will receive a refund for the total value of the item. There is no need to return faulty adult products to Me & Mrs. Jones. <strong>PLEASE DO NOT SEND BACK ANY SEX TOY OR ADULT PRODUCT to Me & Mrs. Jones</strong>. Some products in certain brands include a warranty. We honour the terms of all warranties with proof of purchase. You will receive your exchanged item within approximately 10 days of the acceptance of your exchange.</p>
        <p>&nbsp</p>
        <p><strong>Before Requesting An Exchange on Your Sex Toy:</strong>
        <p>&nbsp</p>
        <ul>
            <li>Check to make sure the batteries are the proper type and are inserted correctly.</li>
            <li>Try new batteries in case the batteries you are using are worn out</li>
            <li>Check to make sure battery connecters are not wet</li>
            <li>Check that the security seal was taken off of the battery compartment.</li>
            <li>If it is a rechargeable toy, make sure you have charged it properly.</li>
        </ul>
        </p>
        <p>&nbsp</p>
        <p>If you have received a faulty upon arrival item from Our Essentials (lubes, massage products, body paints, cleaning and grooming products, condoms, toy cleaners or sexual aides) please email us at <a href="mailto:info@meandmrsjones.com.au">info@meandmrsjones.com.au</a></p>
    </div>
    <div class="col-lg-7 col-lg-offset-1 col-xs-12 col-sm-12 col-md-12 empty-cart-container no-padding">
        <h2>Complete this form to exchange a faulty sex toy or adult product</h2>
        <form name="st_return" method="post" action="/return-4/" enctype="multipart/form-data">
            <input type="hidden" value="form_st_return" name="form_name"/>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mobile-no-padding">
                <p class="mobile-no-padding"><strong>Contact</strong></p>
                <div class="col-lg-6 col-xs-12 col-sm-12 col-md-12 mobile-no-padding">
                    <label class="required" for="name">Name:</label><input name="customer_name"/>
                    <p class="error hidden">Please enter your name</p>
                    <p>I prefer to be contacted by:</p>
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 no-padding squaredFour">
                        <p>Email:</p>
                        <input id="contact_email" type="checkbox" name="contact_email"/><label for="contact_email"></label>
                    </div>
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 no-padding squaredFour">
                        <p>Phone:</p>
                        <input id="contact_phone" type="checkbox" name="contact_phone"/><label for="contact_phone"></label>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12 col-sm-12 col-md-12 mobile-no-padding">
                    <label class="required" for="email">Email:</label><input name="email"/>
                    <p class="error hidden">Please enter your email address</p>
                    <label class="required" for="phone">Phone:</label><input name="phone"/>
                    <p class="error hidden">Please enter your phone number</p>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mobile-no-padding">
                <p class="mobile-no-padding"><strong>Your Exchange</strong></p>
                <div class="col-lg-6 col-xs-12 col-sm-12 col-md-12 mobile-no-padding">
                    <label class="required" for="order_number">Order No:</label><input name="order_number"/>
                    <p class="error hidden">Please enter your phone number</p>
                    <p style="font-size: 14px;">Find your order number on the top of your receipt</p>
                </div>
                <div class="col-lg-6 col-xs-12 col-sm-12 col-md-12 mobile-no-padding">
                    <label class="required" for="exchange_info">What are you exchanging?</label>
                    <select name="product_select">
                        <option value="-1">Enter a order number</option>
                    </select>
                    <p class="error hidden">Please select your product</p>
                </div>
            </div>
            <div class="col-lg-6 col-xs-12 col-sm-12 col-md-12 mobile-no-padding">
                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 mobile-no-padding">
                    <p class="required">Was the item faulty upon arrival:</p>
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 no-padding squaredFour">
                        <p>Yes:</p><input id="faulty_on_arrival_yes" type="checkbox" name="faulty_on_arrival_yes"/><label for="faulty_on_arrival_yes"></label>
                        <p class="error hidden">Please select an option</p>
                    </div>
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 no-padding squaredFour">
                        <p>No:</p><input id="faulty_on_arrival_no" type="checkbox" name="faulty_on_arrival_no"/><label for="faulty_on_arrival_no"></label>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xs-12 col-sm-12 col-md-12 mobile-no-padding">
                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 no-padding">
                    <p class="required">Upload a Photo of Faulty Item:</p>
                    <input type="file" name="productImage" id="fileToUpload">
                    <p class="error hidden">Please upload an image</p><label for="productImage">.jpeg&nbspmax&nbsp8mb</label>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mobile-no-padding">
                <p class="warranty-message mobile-no-padding">We only offer exchange for items that are faulty upon arrival unless the item is under warranty.</p>
                <div class="col-lg-6 col-xs-12 col-sm-12 col-md-12 mobile-no-padding">
                    <p style="margin: 20px 0" class="required">Is this item under warranty?</p>
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 no-padding squaredFour">
                        <p>Yes:</p><input id="warranty_yes" type="checkbox" name="warranty_yes"/><label for="warranty_yes"></label>
                        <p class="error hidden">Please select an option</p>
                    </div>
                    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 no-padding squaredFour">
                        <p>No:</p><input id="warranty_no" type="checkbox" name="warranty_no"/><label for="warranty_no"></label>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12 col-sm-12 col-md-12 mobile-no-padding">
                    <label for="warranty_number">Warranty No:</label><input name="warranty_number"/>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mobile-no-padding">
                <button name="submit_form_button" type="button">Submit</button>
                <p class="text-center">You will be emailed a copy of this form for your convenience</p>
            </div>
        </form>
    </div>
</div>
<?php
wp_enqueue_script('return_forms_js');
get_footer();
?>
