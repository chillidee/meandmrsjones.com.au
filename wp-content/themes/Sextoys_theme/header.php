<?php
require( 'head.php' );
$main_menu = wp_get_nav_menu_items('main');
$is_home = ($_SERVER["REQUEST_URI"] == '/') ? true : false;
$aws_site = 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/';
$logo_name = ($is_home) ? "header" : "main-header";
$class_selector = ($is_home) ? "home" : "main";
$parent_menus = array();
foreach (get_all_categories() as $cat):
    if (isset($cat) && !$cat->parent && $cat->name != ''):
        $parent_menus[$cat->slug] = $cat;
    endif;
endforeach;
$cat_object = get_queried_object();
if (isset($cat_object->term_id)):
    $parent_object = st_get_cat_object(st_cat_top_level_parent($cat_object->term_id));
endif;
$page = $_SERVER["REQUEST_URI"];
$pageArray = explode('/', $page);
$page = $pageArray[1];
$page_class = '';
$nav_class = '';
switch ($page):
    case '':
        $page = 'home-page';
        $nav_class = 'home-page';
        break;
    case 'for-her':
        $page = 'for-her';
        $page_class = 'for-her-subpage';
        $nav_class = 'for-her-nav';
        break;
    case 'for-him':
        $page = 'for-him';
        $page_class = 'for-him-subpage';
        $nav_class = 'for-him-nav';
        break;
    case 'for-us':
        $page = 'for-us';
        $page_class = 'for-us-subpage';
        $nav_class = 'for-us-nav';
        break;
    case 'about':
        $page = 'about-page';
        $class_selector = "home";
        $logo_name = "header";
        break;
    case 'faq':
        $page = 'faq-page';
        $class_selector = "home";
        $logo_name = "header";
        break;
    case 'terms-conditions':
        $page = 'terms-conditions-page';
        $class_selector = "home";
        $logo_name = "header";
        break;
    case 'privacy':
        $page = 'privacy-page';
        $class_selector = "home";
        $logo_name = "header";
        break;
    case 'contact':
        $page = 'contact-page';
        $class_selector = "home";
        $logo_name = "header";
        break;
    case 'cart':
        $page = 'cart-page';
        $class_selector = "home";
        $logo_name = "main-header";
        break;
    case 'guide':
        $class_selector = "home";
        break;
    case 'checkout':
        $page = 'checkout-page';
        $class_selector = "home";
        $logo_name = "main-header";
        break;
    case 'my-account':
        $page = 'my-account-page';
        $class_selector = "account-nav home";
        $logo_name = "header";
        $mobile_menu_name = 'Account';
        break;
    case 'product':
        $classes = st_get_class_selectors(st_cat_top_level_parent(st_get_product_cat_id($cat_object->ID)));
        $page = $classes['page'];
        $page_class = $classes['page_class'];
        $nav_class = $classes['nav_class'];
        unset($classes);
        break;
endswitch;
$page1 = $pageArray[2];
switch ($page1):
    case 'for-her':
        $page = 'for-her';
        $page_class = 'for-her-subpage';
        break;
    case 'for-him':
        $page = 'for-him';
        $page_class = 'for-her-subpage';
        break;
    case 'for-us':
        $page = 'for-us';
        $page_class = 'for-her-subpage';
        break;
    case 'order-received':
        $page = 'checkout-order-complete';
        $class_selector = "account-nav home";
        $page_class = 'home';
        break;
    case 'search':
        $page = 'search-page';
        break;
    case 'lost-password':
        $page = 'for-him my-account-page';
        $page_class = 'for-him-subpage';
        $nav_class = 'for-him-nav';
        break;
endswitch;
if (http_response_code() === 404):
    $page = 'cart-page';
    $class_selector = "home";
    $logo_name = "main-header";
endif;
?>
<body>
    <div class="container-fluid">
        <?php if (!is_user_logged_in()): ?>
            <div class="login-overlay desktop-only">
                <?php include_once 'partials/desktop-login.min.html'; ?>
            </div>
            <div class="desktop-only login-overlay-signup">
                <?php include 'partials/sign-up-overlay.php'; ?>
            </div>
        <?php endif; ?>

        <div class="row">
            <nav class="<?php echo "{$class_selector}"; ?>-nav <?php
            echo ($parent_object->slug) ? $parent_object->slug . '-nav' : '';
            echo ($nav_class) ? $nav_class : '';
            echo (explode('/', $_SERVER["REQUEST_URI"])[1] != '') ? 'main-nav' : '';
            ?>">
                <div id="mobile-product-menu" class="col-xs-0 col-sm-0 col-md-0 mobile-product-menu mob-only">                                       
                    <?php if (in_array(explode('/', $_SERVER["REQUEST_URI"])[1], array('toys','fetish','apparel','essentials','gifts-games')) || $page == 'search-page'): ?>
                        <p></p>
                        <div id="mobile-product-menu-wrapper">
                            <span class="archive-cat-title">Categories</span>
                            <p class="close-mobile-product-menu-wrapper"></p>
                            <?php include get_template_directory() . '/partials/all_products_sidebar.php'; ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="col-sm-4 col-xs-4 col-md-4 mob-only mobile-menu-tigger">
                    <p></p>
                </div>
                <div class="col-lg-2 col-xs-4 col-sm-4 col-md-4 logo-wrapper">
                    <div class="<?php echo "{$class_selector}"; ?>-header-logo-container">
                        <a href="<?php echo get_site_url() ?>">
                            <img src="<?php echo $aws_site . 'header-logo.png'?>" alt="Me &amp; Mrs Jones"/>
                        </a>
                    </div>
                </div>
                <?php
                ?>
                <div class="col-sm-4 col-xs-4 col-md-4 nav-header-cart-wrapper mob-only">
                    <?php
                    if ($page != 'home-page'):
                        if ($page != 'search-page'):
                            ?>
                            <a href="<?php echo WC_Cart::get_cart_url(); ?>">
                                <div class="nav-header-cart" data-total="0">
                                    <p class="nav-header-cart-total"><?php echo number_of_items_in_cart() ?></p>
                                </div>
                            </a>
                            <?php
                        endif;
                    endif;
                    ?>
                </div>                
                <div class="mobile-menu-overlay mob-only">
                    <?php include_once 'partials/mobile-menu-overlay.php'; ?>
                </div>
                <?php if (!is_user_logged_in()): ?>
                    <div id="mobile-login-overlay">
                        <?php include_once 'partials/mobile-login-overlay.php'; ?>
                    </div>
                    <?php
                endif;
                ?>
                <div class="col-lg-10 desktop-only">
                    <?php
                    if (isset($main_menu)):
                        include_once 'partials/main-menu.php';
                    endif;
                    unset($main_menu);
                    ?>
                </div>
                <div id="contact-form-overlay">
                    <?php if (is_user_logged_in()): ?>
                        <?php include_once 'partials/contact-form-overlay.php'; ?>
                    <?php else: ?>
                        <?php include_once 'partials/contact-form-overlay_not_logged_in.php'; ?>
                    <?php endif; ?>
                </div>
            </nav>
            <main class="<?php echo ($page) ? $page : '' ?>">
                <?php
                unset($is_home);
                unset($aws_site);
                unset($logo_name);
                unset($class_selector);
                unset($parent_menus);
                unset($cat_object);
                unset($pageArray);
                unset($page);
                unset($page_class);
                unset($nav_class);
