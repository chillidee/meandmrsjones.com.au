<?php
get_header();

$aws = 'https://s3.amazonaws.com/adult-toys/images/low-res/';

$main_menu = wp_get_nav_menu_items('main');
?>
<style>@media(min-width:1200px){body,html{overflow: hidden}}</style>
<div class="row">
    <div class="col-lg-12 home-background-container" style="position: fixed; width: 100%; background-image: url(<?php echo get_template_directory_uri() . '/images/home-background.jpg' ?>); background-position: right;"><img id="image-placeholder"/></div>
    <div class="col-lg-12 home-middle-content-wrapper">
        <div class="col-lg-3">
            <div class="home-quote-text">
                <p>"We've got a thing going on.."</p>
            </div>
        </div>
        <div class="col-lg-6">

            <div class="col-lg-12 home-middle-content-container">

                <div class="home-middle-logo-container">
                    <img src="<?php echo get_template_directory_uri() . '/images/header-logo.png'?>" alt="Me &amp; Mrs Jones"/>
                    <h2 class="home-page-slogan">Adult Boutique</h2>
                </div>

            </div>
			
           <div class="col-lg-12 home-middle-buttons-container no-padding-mobile">

                <div class="col-lg-12">
                    <?php
                    if (isset($main_menu)):
                        foreach ($main_menu as $menu_item):
                            if (!$menu_item->menu_item_parent):
                                ?>
                                <div class="col-lg-4 col-md-4">
                                    <a data-name="home-page-<?php echo str_replace(' ', '-', strtolower($menu_item->post_title)) ?>-button" id="" href="<?php echo get_site_url() . $menu_item->url ?>"><?php echo $menu_item->post_title ?></a>
                                </div>
                                <?php
                            endif;
                        endforeach;
                    endif;
                    ?>
                </div>
                <!--<?php if ($_COOKIE["enteredemail"] !== 'true'): ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 email-signup-front">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                            <input type="email" name="discount_signup" placeholder="Enter Email for Discounts"/>
                        </div>
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <button name="discount_signup_submit" type="button" class="btn">SUBMIT</button>
                        </div>
                        <p class="front_page_email_error"></p>
                    </div>
                <?php endif; ?>-->
            </div>
        </div>

        <div class="col-lg-3">

            <!--            <div class="home-quote-text">
                            <p>"And we know that it's wrong.."</p>
                        </div>-->

        </div>
    </div>    
</div>
<?php
$popupPage = get_page_by_path('home-popup');
if ($popupPage && $popupPage->post_status == 'publish' && $_COOKIE["enteredemail"] !== 'true'):
    $popupPage = $popupPage->ID;
    ?>
    <div class="home-popup hidden" style="background-image: url(<?php (get_field('background-image', $popupPage)) ? the_field('background-image', $popupPage) : '' ?>)">
        <i class="fa fa-times close-home-popup" aria-hidden="true" title="close"></i>
        <p class="popup-text" <?php echo (!get_field('background-image', $popupPage)) ? 'style="color:black"' : '' ?>><?php echo the_field('heading', $popupPage) ?></p>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <input type="email" name="discount_signup_popup" placeholder="Enter Email for Discounts"/>
            <p class="front_page_popup_email_error"></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <button class="btn btn-default" name="discount_signup_submit_popup" type="button" class="btn">SUBMIT</button>
        </div>
    </div>
    <?php
endif;
unset($popupPage);
?>
<link href = "https://fonts.googleapis.com/css?family=Cardo" rel = "stylesheet">
<script>jQuery(document).ready(function(e){var a=e("html").width();0<e(".home-middle-buttons-container").length&&(1400<a&&e("<img/>").attr("src","https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-background.jpg").load(function(){e(this).remove(),e("#image-placeholder").fadeOut("fast"),e("#image-placeholder").remove()}),1240<a&&a<1400&&e("<img/>").attr("src","<?php echo get_template_directory_uri() . '/images/home-banner-mobile-lan-1400.jpg' ?>").load(function(){e(this).remove(),e("#image-placeholder").fadeOut("fast"),e("#image-placeholder").remove()}),600<=a&&a<=768&&e("<img/>").attr("src","https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-port.jpg").load(function(){e(this).remove(),e("<img/>").attr("src","https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-lan-1024.jpg").load(function(){e(this).remove(),e("#image-placeholder").fadeOut("fast"),e("#image-placeholder").remove()})}),a<=320&&e("<img/>").attr("src","https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-port-iphone320.jpg").load(function(){e(this).remove(),e("#image-placeholder").fadeOut("fast"),e("#image-placeholder").remove()}),321<=a&&a<=375&&e("<img/>").attr("src","https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-port-iphone375.jpg").load(function(){e(this).remove(),e("#image-placeholder").fadeOut("fast"),e("#image-placeholder").remove()}),376<=a&&a<=414&&e("<img/>").attr("src","https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-port-iphone375.jpg").load(function(){e(this).remove(),e("#image-placeholder").fadeOut("fast"),e("#image-placeholder").remove()}),414<a&&a<=600&&e("<img/>").attr("src","https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-port-600.jpg").load(function(){e(this).remove(),e("<img/>").attr("src","https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-lan-480.jpg").load(function(){e(this).remove(),e("#image-placeholder").fadeOut("fast"),e("#image-placeholder").remove()})}),769<=a&&a<=1200&&e("<img/>").attr("src","https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-port-1200.jpg").load(function(){e(this).remove(),e("<img/>").attr("src","https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-banner-mobile-lan-1024.jpg").load(function(){e(this).remove(),e("#image-placeholder").fadeOut("fast"),e("#image-placeholder").remove()})}))});</script>
<?php
wp_enqueue_script('preloader');
//wp_enqueue_script('preloadImages');
get_footer();
?>
