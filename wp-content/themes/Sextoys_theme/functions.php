<?php
/*
 * Create class to handle attaching the logo in emails
 */

global $subtotal;
global $wishlistTotal;
global $woocommerce;

//require_once 'functions/general_functions.php';
//require_once 'functions/product_functions.php';
require_once 'functions/api_functions.php';
require_once 'functions/tracking_functions.php';

function custom_includes() {
    require_once('aws_wrapper.php');
    require_once 'plugins/Mobile_Detect.php';
    //require_once 'pdf/receipt.php';
}

add_action('after_setup_theme', 'custom_includes');

// Clean the up the image from wp_get_attachment_image()
add_filter('wp_get_attachment_image_attributes', function( $attr ) {
    if (isset($attr['sizes']))
        unset($attr['sizes']);

    if (isset($attr['srcset']))
        unset($attr['srcset']);

    return $attr;
}, PHP_INT_MAX);

// Override the calculated image sizes
add_filter('wp_calculate_image_sizes', '__return_false', PHP_INT_MAX);

// Override the calculated image sources
add_filter('wp_calculate_image_srcset', '__return_false', PHP_INT_MAX);

// Remove the reponsive stuff from the content
remove_filter('the_content', 'wp_make_content_images_responsive');

function st_get_pdf_class() {
    require_once 'pdf/st_pdf.php';
}

function st_get_pdf_plugin() {
    require_once 'plugins/mpdf60/mpdf.php';
}

function st_get_cyber_payment() {
    $pluginPath = ABSPATH . 'wp-content/plugins/';
    require_once $pluginPath . 'woocommerce-gateway-cybersource/classes/class-wc-gateway-cybersource.php';
    return new WC_Gateway_CyberSource();
}

function st_create_return_order($email) {
    global $woocommerce;
    $order = wc_create_order();
    $order->set_total(15.00);
    $order->set_address(array('email' => $email), 'billing');
    return $order->id;
}

/*
 *
 * Add custom classes to WYSWYG editior
 *
 */

function my_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}

add_filter('mce_buttons_2', 'my_mce_buttons_2');

function my_mce_before_init_insert_formats($init_array) {
    $style_formats = array(
        array(
            'title' => 'Guide Title h2',
            'block' => 'h2',
            'classes' => 'guide-title'
        ),
        array(
            'title' => 'Guide Title h3',
            'block' => 'h3',
            'classes' => 'guide-title'
        ),
        array(
            'title' => 'Guide Title h4',
            'block' => 'h4',
            'classes' => 'guide-title'
        ),
        array(
            'title' => 'Guide Title p',
            'block' => 'p',
            'classes' => 'guide-title'
        ),
        array(
            'title' => 'Guide Body',
            'block' => 'p',
            'classes' => 'guide-body'
        )
    );
    $init_array['style_formats'] = json_encode($style_formats);

    return $init_array;
}

add_filter('tiny_mce_before_init', 'my_mce_before_init_insert_formats');

/*
 *
 * Remove enhanced dropdown on checkout
 *
 */

add_action('wp_enqueue_scripts', 'agentwp_dequeue_stylesandscripts', 100);

function agentwp_dequeue_stylesandscripts() {
    if (class_exists('woocommerce')) {
        wp_dequeue_style('select2');
        wp_deregister_style('select2');

        wp_dequeue_script('select2');
        wp_deregister_script('select2');
    }
}

/*
 *
 * Add email address to database from homepage
 *
 */

function st_home_subscribe_callback() {
    global $wpdb;
    $email = htmlentities($_POST['email']);
    $inDB = $wpdb->query("SELECT * FROM  `email_subscribers` WHERE `email` LIKE  '" . $email . "'");
    if ($inDB):
        echo 'Error';
    else:
        echo $wpdb->query("INSERT INTO `" . DB_NAME . "`.`email_subscribers` (`id`, `email`,`phone`) VALUES (NULL, '" . $email . "','')");
    endif;
    require get_template_directory() . '/emails/email_templates.php';
    $emails = new email_templates();
    $subject = 'Thank you for signing up to Me & Mrs Jones';
    $content = array('Thank you for signing up to Me & Mrs Jones', '<p>Please find your coupon code below for an extra 10% off at checkout.</p><p>Code: <strong>joinMMJ</strong><p></p>');
    $emails->sendmail($email, $subject, $content);
    wp_die();
}

/*
 * ------------------------|
 * Get a products category |
 * ------------------------|
 */

function get_product_category($ID) {
    $terms = get_the_terms($ID, 'product_cat');
    foreach ($terms as $term) {
        $product_cat_id = $term->term_id;
        break;
    }
    return $product_cat_id;
}

/*
 * ----------------------------|
 * Email pdf from account page |
 * ----------------------------|
 */

function st_email_pdf_callback() {
    $order_id = strip_tags($_POST['order_id']);
    $order_email = strip_tags($_POST['order_email']);
    $subject = 'Me and Mrs Jones Receipt Number: ' . $order_id;
    $save_path = WP_CONTENT_DIR . '/uploads/' . $order_id . ".pdf";
    st_get_pdf_plugin();
    st_get_pdf_class();
    require_once 'emails/email_templates.php';
    $emails = new email_templates();
    $pdf_class = new st_pdf();
    $pdf = new mPDF();
    $html = $pdf_class->st_pdf_html($order_id);
    unset($pdf_class);
    $pdf->WriteHTML(utf8_encode($html));
    unset($html);
    $content = $pdf->Output('', 'S');
    unset($pdf);
    $attachment = fopen($save_path, "w");
    if ($attachment):
        fwrite($attachment, $content);
        unset($content);
        fclose($attachment);
        $attachments = array($save_path);
        $result = $emails->sendmail($order_email, $subject, array('Me & Mrs Jones Invoice', $emails->invoicecontent($order_id)), $attachments);
        unlink($save_path);
    else:
        $result = '0';
    endif;
    echo $result;
    wp_die();
}

function st_is_valid_order_number_callback() {
    if (isset($_POST['value'])):
        $order = new WC_Order(htmlspecialchars($_POST["value"]));
        echo ($order->post) ? true : false;
    else:
        echo false;
    endif;
    wp_die();
}

function st_get_order_items_callback() {
    if (isset($_POST['value'])):
        $returnArray = array();
        $order = new WC_Order(htmlspecialchars($_POST["value"]));
        foreach ($order->get_items() as $item):
            $returnArray[$item["product_id"]] = $item["name"];
        endforeach;
        echo json_encode($returnArray);
    else:
        echo false;
    endif;
    wp_die();
}

function st_sort_size_array_comparison($a, $b) {
    $sizes = array(
        "xxs" => 0,
        "xs" => 1,
        "s" => 2,
        "m" => 3,
        "l" => 4,
        "xl" => 5,
        "xxl" => 6,
        "1xl" => 7,
        "2xl" => 8,
        "3xl" => 9,
        '5' => 0,
        '6' => 1,
        '7' => 2,
        '8' => 3,
        '9' => 4,
        '10' => 5,
        '11' => 6,
        '12' => 7,
        '13' => 8,
        '14' => 9,
        '15' => 10,
        '16' => 11,
        '17' => 12,
        '18' => 13,
        '19' => 14,
        '20' => 15,
        'one size' => 0,
        'small' => 1,
        'medium' => 2,
        'large' => 3,
        'x large' => 4,
        '1xl' => 5,
        '2xl' => 6,
        '3xl' => 7,
        '1x' => 8,
        '2x' => 9,
        '3x' => 10,
        '4x' => 11,
        'queen' => 12,
        's/m' => 0,
        'l/xl' => 1,
        '32' => 0,
        '33' => 1,
        '34' => 2,
        '35' => 3,
        '36' => 4,
        '37' => 5,
        '38' => 6,
        '39' => 7,
        '40' => 8,
        '41' => 9,
        '42' => 10,
        '43' => 11,
        '44' => 12
    );

    $asize = $sizes[strtolower($a)];
    $bsize = $sizes[strtolower($b)];

    if ($asize == $bsize) {
        return 0;
    }
    return ($asize > $bsize) ? 1 : -1;
}

function st_is_mobile() {
    $detect = new Mobile_Detect;
    return $detect->isMobile();
}

function my_post_image_html($html, $post_id, $post_thumbnail_id, $size, $attr) {
    $imageURL = get_post_meta($post_id, '_image_low_res_link', true);
    if ($imageURL):
        $image_array = explode('/', $imageURL);
        $image_name = $image_array[count($image_array) - 1];
        $html = "<img src='{$imageURL}' class='attachment-medium size-medium wp-post-image zoomImg' alt='{$image_name}'></img>";
    else:
        $image_string = wp_get_attachment_url($post_thumbnail_id);
        $image_array = explode('/', $image_string);
        $image_name = $image_array[count($image_array) - 1];
        unset($image_array[count($image_array) - 1]);
        $image_string = implode('/', $image_array);
        if (get_post_type($post_id) === 'product'):
            $AWSBucket = 'https://s3.amazonaws.com/adult-toys/images/low-res';
            $html = str_replace($image_string, $imageURL, $html);
        endif;
    endif;
    return $html;
}

add_filter('wp_get_attachment_url', 'st_get_attachment_url');

function st_get_attachment_url($url) {
    $image_array = explode('/', $url);
    $image_name = $image_array[count($image_array) - 1];
    return 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/' . $image_name;
}

function st_filter_content($content) {
    $regex = '#src="https://s3.amazonaws.com/adult-toys/"';
    $replace = 'src="' . get_site_url() . '"';

    $output = preg_replace($regex, $replace, $content);

    return $output;
}

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

function st_load_script_from_cdn($url, $localFile) {
    $cdnIsUp = get_transient('cnd_is_up');
    if ($cdnIsUp) {
        $load_source = $url;
    } else {
        $cdn_response = wp_remote_get($url);
        if (is_wp_error($cdn_response) || wp_remote_retrieve_response_code($cdn_response) != '200') {
            $load_source = $localFile;
        } else {
            $cdnIsUp = set_transient('cnd_is_up', true, MINUTE_IN_SECONDS * 2000);
            $load_source = $url;
        }
    }
    return $load_source;
}

function st_get_site_root() {
    return ABSPATH;
}

function enqueue_scripts_and_styles() {
    if (is_admin()) {
        wp_enqueue_media();
    }
    global $wp_styles;

    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', st_load_script_from_cdn("https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js", get_template_directory_uri() . '/js/jquery.min.js'));
    wp_enqueue_script('jquery-zoom', st_load_script_from_cdn('https://cdnjs.cloudflare.com/ajax/libs/jquery-zoom/1.7.15/jquery.zoom.min.js', get_template_directory_uri() . '/js/jquery.zoom.min.js'));
    wp_enqueue_script('bootstrap', st_load_script_from_cdn('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0', true));
    wp_enqueue_script('custom', get_template_directory_uri() . '/js/custom.min.js?v=1.0', array(), '1.10', true);
    wp_enqueue_script('angular', st_load_script_from_cdn("https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js", get_template_directory_uri() . '/js/angular.min'));
    wp_register_script('return_forms_js', get_template_directory_uri() . '/js/return_forms.js', '', '', true);
    wp_register_script('unsubscribe_form_js', get_template_directory_uri() . '/js/unsubscribe.min.js', '', '', true);
    wp_register_script('review-js', get_template_directory_uri() . '/js/review-js.min.js', '', '', true);

    wp_register_script('preloader', get_template_directory_uri() . '/js/jquery.preload.min.js');
    wp_register_script('preloadImages', get_template_directory_uri() . '/js/preloadImages.min.js');
    wp_register_style('bootstrap-css', st_load_script_from_cdn('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css', get_template_directory_uri() . '/css/bootstrap.min.css', array()));
    wp_enqueue_style('bootstrap-css');
    wp_enqueue_style('stylesheet', get_template_directory_uri() . '/style.min.css');
    wp_enqueue_style('main', get_template_directory_uri() . '/css/main.css?v=1.13', array(), '1.13');
    wp_enqueue_style('fontawesome', st_load_script_from_cdn('https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', get_template_directory_uri() . '/css/font-awesome.min.css'));
    wp_register_style('accounts', get_template_directory_uri() . '/css/accounts.css');
    wp_register_style('stars', get_template_directory_uri() . '/css/starscss.min.css');
    wp_register_style('stars-products', get_template_directory_uri() . '/css/stars-product-css.min.css');
}

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
add_action('wp_enqueue_scripts', 'enqueue_scripts_and_styles', '0');

function _remove_query_strings_1($src) {
    $rqs = explode('?ver', $src);
    return $rqs[0];
}

if (is_admin()) {
// Remove query strings from static resources disabled in admin
} else {
    add_filter('script_loader_src', '_remove_query_strings_1', 15, 1);
    add_filter('style_loader_src', '_remove_query_strings_1', 15, 1);
}

function _remove_query_strings_2($src) {
    $rqs = explode('&ver', $src);
    return $rqs[0];
}

if (is_admin()) {
// Remove query strings from static resources disabled in admin
} else {
    add_filter('script_loader_src', '_remove_query_strings_2', 15, 1);
    add_filter('style_loader_src', '_remove_query_strings_2', 15, 1);
}

function register_my_menus() {
    register_nav_menus(
            array(
                'header-menu' => __('Header Menu'),
                'footer-menu-left' => __('Footer Menu Left'),
                'footer-menu-right' => __('Footer Menu Right')
            )
    );
}

add_action('init', 'register_my_menus');
/**
 * Featured image
 */
add_theme_support('post-thumbnails');
/* Security! */

function explain_less_login_issues() {
    return '<strong>ERROR</strong>: Entered credentials are incorrect.';
}

add_filter('login_errors', 'explain_less_login_issues');
define('DISALLOW_FILE_EDIT', true); /* !
  /* Hook into the 'init' action so that the function
 * Containing our post type registration is not
 * unnecessarily executed.
 */
add_action('init', 'custom_post_type', 0);

//flush_rewrite_rules( );
function low_prio() {
    return 'low';
}

add_filter('wpseo_metabox_prio', 'low_prio');

/*
 * function to get most popular products
 */

function get_most_popular_products($numberOfProducts) {
    $args = array(
        'post_type' => 'product',
        "post_status" => "publish",
        'posts_per_page' => $numberOfProducts,
        'meta_key' => 'total_sales',
        'orderby' => 'meta_value_num'
    );
    return new WP_Query($args);
}

/*
 * ---------------------------------------------------------
 * Add image to aws upon upload in dashboard
 * ---------------------------------------------------------
 */

add_filter('wp_handle_upload_prefilter', 'st_aws_upload_filter');

function st_aws_upload_filter($file) {
    Aws_Wrapper::aw_put_file(null, 'images/low-res/', $file['name'], $file['tmp_name'], $file['type']);
    return $file;
}

/*
 * ---------------------------------------------------------
 * Delete image on aws upon deletion in dashboard
 * ---------------------------------------------------------
 */

add_action('delete_attachment', 'st_aws_delete_image');

function st_aws_delete_image($postid) {
    $result = wp_get_attachment_metadata($postid);
    $result = $result['file'];
    $file_exist = Aws_Wrapper::check_if_image_exists(null, $result, 'low-res');
    if ($file_exist):
        Aws_Wrapper::aw_delete_object($result, 'low-res');
    endif;
}

/*
 * Drop down list for logged in user
 */

function logged_in_user() {
    if (is_user_logged_in()):
        $user = wp_get_current_user();
        ?>
        <div class = "btn-group">
            <button type = "button" class = "btn btn-default dropdown-toggle" id="logged-in-user-dropdown" data-toggle = "dropdown" aria-haspopup = "true" aria-expanded = "false">
                Hello <?php echo $user->display_name ?><span class = "caret"></span>
            </button>
            <ul class = "dropdown-menu">
                <li><a href = "/my-account">Account</a></li>
                <li role = "separator" class = "divider"></li>
                <li><a href = "<?php echo wp_logout_url(home_url()); ?>">Logout</a></li>
            </ul>
        </div>
        <?php
    endif;
}

function action_woocommerce_save_account_details($user_id) {
    //st_var_dump($user_id);
}

add_action('woocommerce_save_account_details', 'action_woocommerce_save_account_details', 10, 1);

/*
 * function to get newest products
 */

function get_newest_products($numberOfProducts) {
    $args = array(
        'post_type' => 'product',
        "post_status" => "publish",
        'posts_per_page' => $numberOfProducts,
        'orderby' => 'date',
        'order' => 'DESC'
    );
    return new WP_Query($args);
}

/*
 * function to get taged products
 */

function st_get_taged_products($numberOfProducts, $tag) {
    $args = array(
        'post_type' => 'product',
        "post_status" => "publish",
        'posts_per_page' => $numberOfProducts,
        'orderby' => 'rand',
        'product_tag' => $tag
    );
    return new WP_Query($args);
}

/*
 * Get the number of items in the cart
 */

function number_of_items_in_cart() {
    return WC()->cart->get_cart_contents_count();
}

/*
 * return string of logo path
 */

function get_logo_url() {
    $path = get_site_url() . '/logo.png';
    return $path;
}

function get_mobile_menu($menuTitle, $menuName) {
    $returnMenu = '<div class="' . $menuTitle . '-menu-container">';
    $returnMenu .= '<h3 class="col-xs-10">' . $menuTitle . '</h3>';
    $returnMenu .= '<div class="col-xs-2"><i id="' . $menuTitle . '-menu" class="fa fa-plus"></i></div>';
    if ($menuName):
        $menu = wp_get_nav_menu_object($menuName);
        if ($menu):
            $menu_items = wp_get_nav_menu_items($menu->term_id);
        endif;
        if ($menu_items):
            foreach ((array) $menu_items as $key => $menu_item):
                $returnMenu .= '<div class="col-xs-12 footer-sub-menu">';
                $returnMenu .= '<a href="' . $menu_item->url . '">' . $menu_item->title . '</a>';
                $returnMenu .= '</div>';
            endforeach;
        endif;
    endif;
    $returnMenu .= '<div class="clear-space-20 footer-sub-menu"></div></div>';
    return $returnMenu;
}

/*
 * Change cart text on product pages
 */

add_filter('woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text');

function woo_custom_cart_button_text() {
    return __('Buy Me Now', 'woocommerce');
    //return __('Add To Cart', 'woocommerce');
}

add_action('wp_enqueue_scripts', 'wcqi_enqueue_polyfill');

function wcqi_enqueue_polyfill() {
    wp_enqueue_script('wcqi-number-polyfill');
}

/*
 * Check if product is in stock and returns the html
 */

function md_is_in_stock($product) {
    global $product;
    $stock = $product->get_total_stock();
    $returnString = '<span style="';
    $returnString .= 'color:green">In Stock</span>';
    return $returnString;
}

function st_get_most_wished_products() {
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => 4,
        'orderby' => 'meta_value_num',
        'meta_key' => 'wished'
    );
    echo '<div class="row">';
    echo '<div class="col-lg-10 col-lg-offset-1 col-xs-10 col-xs-offset-1 col-md-12 col-md-offset-0 product-related-title"><h2>Other People Wished For</h2></div>';
    echo '<div class="col-lg-10 col-lg-offset-1 col-xs-12 col-xs-offset-0 col-md-12 col-md-offset-0 related-product">';
    $the_query = new WP_Query($args);
    if ($the_query->have_posts()) :
        while ($the_query->have_posts()) : $the_query->the_post();
            $product = new WC_Product(get_the_ID());
            echo '<div class="product-container col-lg-3 col-lg-offset-0 col-md-3 col-md-offset-0 col-xs-6 col-xs-offset-0"><a href="' . get_permalink() . '">';
            echo '<div class="center-block image-box">';
            the_post_thumbnail('full');
            echo '</div>';
            echo '<h4>' . get_the_title() . '</h4>';
            echo '<div class="price related-price">' . $product->get_price_html() . '</div>';
            echo '</a></div>';
        endwhile;
    endif;
    echo '</div></div>';
    wp_reset_query();
}

/*
 * Run a wordpress query for products
 */

function st_product_query($arguements) {
    $tax_query = '';
    $sort_by = '';
    $order_by = '';
    if (isset($arguements['sort_by'])):
        $sort_by = $arguements['sort_by'];
    endif;
    if (isset($arguements['order_by'])):
        $order_by = $arguements['order_by'];
    endif;
    if (isset($arguements['number_of_products'])):
        $number_of_products = $arguements['number_of_products'];
    else:
        $number_of_products = -1;
    endif;
    if (isset($arguements['cat_id'])):
        $tax_query = array(
            'taxonomy' => 'product_cat',
            'field' => 'id',
            'terms' => $arguements['cat_id']
        );
    endif;
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => $number_of_products,
        'post_status' => 'publish',
        'orderby' => $order_by,
        'order' => $sort_by,
        'fields' => 'ids',
        'tax_query' => $tax_query
    );
    $the_query = new WP_Query($args);
    return $the_query->posts;
}

function accounts_filter_products_callback() {
    require_once 'functions/account_class.php';
    $accounts_ajax = new account_class();
    $accounts_ajax->accounts_refine_products_ajax();
}

add_action('wp_ajax_accounts_filter_products', 'accounts_filter_products_callback');

function st_get_class_selectors($parent_cat_id) {
    $class = array();
    switch ($parent_cat_id):
        case 9:
            $class['page'] = 'for-her';
            $class['page_class'] = 'for-her-subpage';
            $class['nav_class'] = 'for-her-nav';
            break;
        case 10:
            $class['page'] = 'for-him';
            $class['page_class'] = 'for-him-subpage';
            $class['nav_class'] = 'for-him-nav';
            break;
        case 24:
            $class['page'] = 'for-us';
            $class['page_class'] = 'for-us-subpage';
            $class['nav_class'] = 'for-us-nav';
            break;
    endswitch;
    return $class;
}

/*
 * ------------------------|
 * Get a products category |
 * ------------------------|
 */

function st_get_product_cat_id($id) {
    $terms = get_the_terms($id, 'product_cat');
    if ($terms[0]):
        return $terms[0]->term_id;
    else:
        return false;
    endif;
}

/*
 * |----------------------------------------------|
 * | Get array of all apparel cats for size guide |
 * |----------------------------------------------|
 */

function st_get_apparel_cats() {
    $array = array(70, 172, 67, 63, 78, 80, 75, 82, 73, 83, 74, 81, 66, 72, 71, 22);
    return $array;
}

function add_related_products_after_product($number_of_products, $cat_slug, $page) {
    $exclude_ids = array($page);
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => $number_of_products,
        'post_status' => 'publish',
        'post__not_in' => $exclude_ids,
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'slug',
                'terms' => $cat_slug
            )
        ),
        'meta_query' => array(
            'relation' => 'OR',
            array(
                'key' => '_is_parent',
                'value' => 1,
                'compare' => 'LIKE'
            ),
            array(
                'key' => '_is_child',
                'compare' => 'NOT EXISTS',
            )
        ),
    );
    st_run_query($args, $number_of_products, $page);
}

function st_run_query($args, $number_of_products, $page) {
    $number_of_columns = '';
    switch ($number_of_products):
        case 2:
            $number_of_columns = '6';
            break;
        case 3:
            $number_of_columns = '4';
            break;
        case 4:
            $number_of_columns = '3';
            break;
        default :
            $number_of_columns = '6';
            break;
    endswitch;
    if (st_is_mobile()):
        $number_of_columns = '12';
    endif;
    $the_query = new WP_Query($args);
    if ($the_query->have_posts()) :
        while ($the_query->have_posts()) : $the_query->the_post();
            $id = get_the_ID();
            $title = get_the_title();
            $img_url = wp_get_attachment_url(get_post_thumbnail_id());
            if (!$img_url):
                $img_url = st_get_the_post_thumbnail($id);
            endif;
            $guid = get_the_guid($id);
            echo "<a href='{$guid}'>";
            echo "<div class='col-lg-6 col-xs-6 product-full-related' data-id='{$id}'>";
            echo '<div class="center-block image-box">';
            echo "<img style='display:none;' class='product-full-related-img' data-id='{$id}' src='{$img_url}' alt='{$title}'/>";
            echo '<img src="https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/product-image-placeholder.jpg" class="image-placeholder" alt="Me &amp; Mrs Jones">';
            echo '</div>';
            echo '<h4>' . $title . '</h4>';
            echo '</div>';
            echo "</a>";
        endwhile;
    else:
        return false;
    endif;
    wp_reset_query();
}

/*
 * -------------------------------------------------------|
 * Get postcodes from database for checkout autocomplete  |
 * -------------------------------------------------------|
 */

function st_get_postcodes_callback() {
    global $wpdb;
    $inputText = htmlentities($_POST[input]);
    $inputLength = strlen($inputText);
    $sql = "SELECT * FROM `postcodes`\n"
            . "ORDER BY `postcodes`.`suburb` ASC";
    $result = array();
    foreach ($wpdb->get_results($sql) as $postcode):
        if (substr(strtolower($postcode->suburb), 0, $inputLength) == strtolower($inputText)) {
            $result[$postcode->id][0] = $postcode->suburb;
            $result[$postcode->id][1] = $postcode->postcode;
            $result[$postcode->id][2] = $postcode->state;
        }
    endforeach;
    echo json_encode($result);
    wp_die();
}

/*
 * -----------------------------
 * Get product from search query
 * -----------------------------
 */

function st_product_search($query) {
    $query = strtolower($query);
    $queryArray = explode(' ', $query);
    $catArray = array();
    foreach ($queryArray as $queryString):
        $cats = get_terms(array('taxonomy' => 'product_cat'));
        foreach ($cats as $cat):
            if (stripos($cat->name, $query) !== false):
                array_push($catArray, $cat->term_id);
            endif;
        endforeach;
    endforeach;
    $cleanArray = array();
    $priceArray = array();
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'fields' => 'ids',
        'orderby' => 'rand'
    );
    if (!empty($catArray)):
        $taxArrray = array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'id',
                'terms' => $catArray
            ),
        );
        $args['tax_query'] = $taxArrray;
    endif;
    $the_query = new WP_Query($args);
    unset($args);
    foreach ($the_query->posts as $id):
        if (isset($catArray) && !empty($catArray)):
            array_push($cleanArray, $id);
        else:
            $specialCharacters = '/[^a-zA-Z0-9]/';
            $title = preg_replace($specialCharacters, '', strip_tags(get_the_title($id)));
            $excerpt = preg_replace($specialCharacters, '', strip_tags(get_post_field('post_excerpt', $id)));
            $description = preg_replace($specialCharacters, '', strip_tags(get_post_field('post_content', $id)));
            $query = preg_replace($specialCharacters, '', strip_tags($query));
//            $inTitle = true;
//            $inExcerpt = true;
//            $inDescription = true;
//            foreach ($queryArray as $query):
//                $query = strtolower($query);
            $inTitle = (stripos($title, $query) !== false) ? true : false;
            $inExcerpt = (stripos($excerpt, $query) !== false) ? true : false;
            $inDescription = (stripos($description, $query) !== false) ? true : false;
//            endforeach;
            if ($inTitle || $inExcerpt || $inDescription):
                array_push($cleanArray, $id);
            endif;
        endif;
    endforeach;
    unset($catArray);
    unset($the_query);
    $cleanArray = array_unique($cleanArray);
//    foreach ($cleanArray as $id):
//        $priceArray[$id] = (float) get_post_meta($id, '_price', true);
//    endforeach;
//    unset($cleanArray);
//    $cleanArray = array();
//    arsort($priceArray);
//    foreach ($priceArray as $index => $value):
//        array_push($cleanArray, $index);
//    endforeach;
//    unset($priceArray);
    return $cleanArray;
}

add_filter('woocommerce_product_tabs', 'woo_remove_product_tabs', 98);

function woo_remove_product_tabs($tabs) {

    unset($tabs['description']);       // Remove the description tab
    unset($tabs['reviews']);    // Remove the reviews tab
    unset($tabs['additional_information']);   // Remove the additional information tab

    return $tabs;
}

/*
 * Remove woocommerce actions from product page
 */

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products');

//remove_filter('woocommerce_product_tabs', 'woocommerce_default_product_tabs');
//remove_filter('woocommerce_product_tabs', 'woocommerce_sort_product_tabs');

/*
 * Add description under cart button
 */
function add_description_to_product() {
    global $product;
    $productObject = $product->post;
    $description = $productObject->post_content;
    echo '<div class="col-lg-12 col-lg-offset-0 col-xs-12 col-xs-offset-0 product-description">'
    . '<h5 class="product-information-title">Product Information</h5>'
    . '<h4 class="product-description-title">Product Description</h4>'
    . '<p>' . $description . '<p>'
    . '</div>';
}

add_action('woocommerce_single_product_summary', 'add_description_to_product', 60);

function add_breadcrumb_before_product() {
    echo '<div class="row"><div class="col-lg-offset-1 col-xs-10 col-xs-offset-1 col-lg-10 product-breadcrumb">';
    woocommerce_breadcrumb();
    echo '</div></div>';
}

add_action('woocommerce_before_single_product', 'add_breadcrumb_before_product', 10);

function get_sidebar_for_woo() {
    $args = array('taxonomy' => 'product_cat');
    $terms = get_terms('product_cat', $args);

    if (count($terms) > 0) {
        echo '<h2>Categories</h2>';
        echo '<p class="my_term-archive">';
        foreach ($terms as $term) {
            echo '<a href="/product-category/' . $term->slug . '" title="' . sprintf(__('View all products in %s', 'my_localization_domain'), $term->name) . '">' . $term->name . '</a>';
            echo '</br>';
        }
        echo '</p>';
    }
    ?>
    <?php
}

add_action('woocommerce_sidebar', 'get_sidebar_for_woo');

function woocommerce_subcats_from_parentcat_by_NAME2($parent_cat_NAME) {
    $IDbyNAME = get_term_by('name', $parent_cat_NAME, 'product_cat');
    $product_cat_ID = $IDbyNAME->term_id;
    $args = array(
        'hierarchical' => 1,
        'show_option_none' => '',
        'parent' => $product_cat_ID,
        'taxonomy' => 'product_cat'
    );
    $subcats = get_categories($args);
    return $subcats;
}

/*
 * Get woocommerce subcategories
 */

function st_subcats_from_parentcat_by_id($parent_cat_id) {
    $product_cat_ID = $parent_cat_id;
    $cleanArray = array();
    $args = array(
        'hierarchical' => 1,
        'show_option_none' => '',
        'parent' => $product_cat_ID,
        'taxonomy' => 'product_cat'
    );
    $subcats = get_categories($args);
    $i = 99;
    foreach ($subcats as $cat):
        $order = (int) st_get_category_meta($cat->term_id, 'cat_order');
        if (!$order) {
            $order = $i;
            $i--;
        }
        $cleanArray[$cat->term_id] = $order;
    endforeach;
    $subcats = array();
    asort($cleanArray);
    foreach ($cleanArray as $index => $value):
        $subcats[$value] = get_category((int) $index);
    endforeach;
    return $subcats;
}

/*
 * Get woocommerce category image
 */

function st_get_category_image($cat_id, $size) {
    $cat_thumb_id = get_woocommerce_term_meta($cat_id, 'thumbnail_id', true);
    $shop_catalog_img = wp_get_attachment_image_src($cat_thumb_id, $size);
    return $shop_catalog_img;
}

function st_remove_item_callback() {
    $id = esc_attr(ucfirst($_POST['id']));
    $cart = WC()->instance()->cart;
    $cart_id = $cart->generate_cart_id($id);
    $cart_item_id = $cart->find_product_in_cart($cart_id);
    if ($cart_item_id) {
        $cart->set_quantity($cart_item_id, 0);
    }
    st_get_cart_contents();
    wp_die();
}

function load_Menu_Box() {
    echo '<div>';
    echo '<img src="' . get_template_directory_uri() . '/wp-content/uploads/2016/03/sign_in_image.png"/>';
    echo '</div>';
}

function my_action_callback() {
    $cat = esc_attr(ucwords($_POST['whatever']));
    echo '<div class="col-lg-10 col-lg-offset-1">';
    echo '<div class="row">';
    $subCats = woocommerce_subcats_from_parentcat_by_NAME2($cat);
    foreach ($subCats as $sc) {
        $link = get_term_link($sc->slug, $sc->taxonomy);
        $thumbnail_id = get_woocommerce_term_meta($sc->term_id, 'thumbnail_id', true);
        $image = wp_get_attachment_url($thumbnail_id);
        $name = $sc->name;
        echo '<div class="col-lg-4" style="margin-top:10px">';
        echo '<a href="' . $link . '" title="' . $name . '">';
        echo '<img src="' . $image . '" style="width: 100%;height: 75px;" alt=' . $name . '/>';
        echo '<p style="position: absolute; bottom: 0;right: 15px; padding: 0;margin: 0;width: 50%;text-align: center;background-color: #706b6b;">' . $sc->name . '</p>';
        echo '</a>';
        echo '</div>';
    }
    wp_die();
}

/*
 * Get a list of all categories
 */

function get_all_categories() {
    $taxonomy = 'product_cat';
    $orderby = 'name';
    $args = array(
        'taxonomy' => $taxonomy,
        'orderby' => $orderby);
    $all_categories = get_categories($args);
    return $all_categories;
}

/*
 * Custom login for the signin overlay
 */

/*
 * Automatic login if the user has been authenticated from thr login form
 */

function custom_login_callback() {
    if (isset($_POST['login']) && isset($_POST['password'])):
        $cleanArray = array();
        $cleanArray['user_login'] = esc_attr($_POST['login']);
        $cleanArray['user_password'] = esc_attr($_POST['password']);
        $creds = array();
        $creds['user_login'] = $cleanArray['user_login'];
        $creds['user_password'] = $cleanArray['user_password'];
        $creds['remember'] = true;
        $user = wp_signon($creds, false);
        if (is_wp_error($user)) {
            echo $user->get_error_message();
        }
    endif;
}

add_action('after_setup_theme', 'custom_login_callback');

/*
 * Authenticates the username and password from the login form, if authenticated
 * the page refreshes and user is logined in else error message is returned
 */

function st_authenticate_user_callback() {
    $login = esc_attr($_POST['login']);
    $password = esc_attr($_POST['password']);
    $user = wp_authenticate($login, $password);
    if (is_wp_error($user)) {
        echo $user->get_error_message();
    } else {
        echo 'true';
    }
    wp_die();
}

/*
 * Create a new user
 */

function st_create_user_callback() {
    $cleanArray = array(
        'first_name' => esc_attr($_POST['first_name']),
        'last_name' => esc_attr($_POST['last_name']),
        'user_login' => esc_attr($_POST['username']),
        'user_pass' => esc_attr($_POST['password']),
        'user_email' => esc_attr($_POST['email']),
        'subscribe' => esc_attr($_POST['subscribe']),
        'dob' => esc_attr($_POST['dob']),
        'title' => esc_attr($_POST['title'])
    );
    $user = wp_insert_user($cleanArray);
    if (is_wp_error($user)):
        echo 'Error 1';
    else:
        $creds = array(
            'user_login' => $cleanArray['user_login'],
            'user_password' => $cleanArray['user_pass'],
            'remember' => true
        );
        $userlogon = wp_signon($creds, false);
        update_user_meta($user, 'dob', $cleanArray['dob']);
        update_user_meta($user, 'title', $cleanArray['title']);
        update_user_meta($user, 'subscribed', $cleanArray['subscribe']);
        update_user_meta($user, "billing_first_name", $cleanArray['first_name']);
        update_user_meta($user, "billing_last_name", $cleanArray['last_name']);
        if (isset($_POST['after_order'])):
            update_user_meta($user, "billing_first_name", $cleanArray['first_name']);
            update_user_meta($user, "billing_last_name", $cleanArray['last_name']);
            update_user_meta($user, "billing_company", strip_tags($_POST["newAddressCompanyName"]));
            update_user_meta($user, "billing_address_1", strip_tags($_POST["billing_address1"]));
            update_user_meta($user, "billing_address_2", strip_tags($_POST["billing_address2"]));
            update_user_meta($user, "billing_city", strip_tags($_POST["billing_city"]));
            update_user_meta($user, "billing_postcode", strip_tags($_POST["billing_postcode"]));
            update_user_meta($user, "billing_state", strip_tags($_POST["billing_state"]));
            update_user_meta($user, "billing_phone", strip_tags($_POST["phone"]));
            update_post_meta($_POST['order_id'], '_customer_user', $user);
        endif;
        do_action('woocommerce_created_customer', $user, null, false);
        echo (is_wp_error($userlogon)) ? 'false' : 'true';
    endif;

    wp_die();
}

/*
 * ------------------------------------|
 * Check to see if user already exists |
 * ------------------------------------|
 */

function check_user_name_callback() {
    $username = esc_attr($_POST['username']);
    echo (username_exists($username)) ? 'true' : 'false';
    wp_die();
}

/*
 * Populates drop menu
 */

function st_get_cart_contents() {
    global $woocommerce;
    $cart = $woocommerce->cart->cart_contents;
    $uploads = wp_upload_dir();
    if (count($cart) > 0):
        ?><i id="cart-scroll-up" class="fa fa-angle-up"></i>
        <div class="cart-items-container" id="items-container">
            <?php
            foreach ($cart as $item):
                $data = $item['data'];
                st_update_cart_subtotal($data->price, $item["quantity"]);
                ?>
                <div class="cart-item-container col-lg-10 col-lg-offset-1">                    
                    <a href="<?php echo $data->post->guid; ?>">
                        <div class="cat-overlay-image">
                            <?php if (wp_get_attachment_url(get_post_thumbnail_id($data->post->ID))): ?>
                                <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($data->post->ID)); ?>" alt="<?php echo $data->post->post_title ?>"/>
                            <?php else: ?>
                                <img class="attachment-full size-full wp-post-image" src="<?php echo get_post_meta($data->post->ID, 'main_image_link', true); ?>" alt="<?php echo $data->post->post_title ?>"/>
                            <?php endif; ?>
                        </div>
                        <div class="cat-overlay-description">
                            <h4><?php echo $data->post->post_title ?></h4>
                            <?php
                            $color = st_get_product_color($data->post->ID);
                            $size = st_get_product_size($data->post->ID);
                            if ($color):
                                ?>
                                <p>Colour: <span><?php echo $color; ?></span></p>
                                <?php
                            endif;
                            if ($size):
                                ?>
                                <p>Size: <span><?php echo $size; ?></span></p>
                            <?php endif; ?>
                            <p>Quantity: <span><?php echo $item["quantity"]; ?></span></p>
                            <input id="cart-overlay-quantity" type="hidden" value="<?php echo $item["quantity"]; ?>"/>
                            <p>Price: <span>$<?php echo $data->price ?> AUD</span></p>
                        </div>
                    </a>
                    <img src="https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/remove-item-cart.png" class="remove-item-from-cart" alt="Remove Item"  title="Remove Item" id="<?php echo $data->post->ID ?>"/>
                </div>
            <?php endforeach; ?>
        </div>
        <i id="cart-scroll-down" class="fa fa-angle-down"></i>
        <?php
    else:
        ?>
        <h3>Your Cart Is Empty</h3>
    <?php
    endif;
}

/*
 * Getter and setter functions for the cart subtotal varible
 */

function st_get_cart_subtotal() {
    global $subtotal;
    return number_format((float) $subtotal, 2, '.', '');
}

function st_update_cart_subtotal($price, $quantity = 1) {
    global $subtotal;
    $calPrice = ($price * $quantity);
    $subtotal += $calPrice;
}

/*
 * Get number of items in cart for ajax request
 */

function st_number_of_items_ajax_callback() {
    echo number_of_items_in_cart();
    wp_die();
}

add_action('init', 'my_setcookie', 0);

//add_action('init', 'st_add_action_wishlist', 10);

function my_setcookie() {
    if (!isset($_COOKIE['sextoys-wishlist'])):
        $previousArray = array();
        $json = json_encode($previousArray);
        setcookie('sextoys-wishlist', $json, time() + 60 * 60 * 24 * 365, COOKIEPATH, COOKIE_DOMAIN);
    else:
        st_remove_item_from_wishlist();
        st_wishlist_post_data();
        st_add_wishlist_to_cart();
        st_clear_wishlist();
    endif;
}

/*
 * Add to wishlist container
 */

function st_add_to_wishlist($id) {
    ?>
    <div class="wishlist-container-page">
        <form action="" method="POST" enctype="multipart/form-data">
            <input name="add-to-wishlist" value="<?php echo $id; ?>" type="hidden">
            <button class="wishlist-button center-block" type="submit">
                <div id="wishlist-click-area" class="heart-box">
                    <i id="wishlist-heart-icon" class="fa fa-heart"></i>
                </div>
            </button>
        </form>
        <p class="wishlist-text-page">Add To WishList</p>
    </div>
    <?php
}

function st_add_to_wishlist_no_icon($id) {
    ?>
    <div class="wishlist-container-page">
        <form action="" method="POST" enctype="multipart/form-data">
            <input name="add-to-wishlist" value="<?php echo $id; ?>" type="hidden">
            <button class="wishlist-button_no_icon center-block" type="submit">Add To WishList
            </button>
        </form>
    </div>
    <?php
}

/*
 * Add item to cookie wishlist
 */

function st_add_item_to_cookie_wishlist_callback() {
    global $wishlistTotal;
    if (!empty($_POST['id'])):
        $id = (int) esc_attr($_POST['id']);
        st_add_item_to_cookie_wishlist($id);
        echo true;
    else:
        echo false;
    endif;
    wp_die();
}

function st_add_item_to_cookie_wishlist($id) {
    global $wishlistTotal;
    $previousArray = array();
    $previousArray = st_get_items_on_wishlist();
    if (!in_array($id, $previousArray)):
        array_push($previousArray, $id);
    endif;
    $json = json_encode($previousArray);
    $result = setcookie('sextoys-wishlist', $json, time() + 60 * 60 * 24 * 365, COOKIEPATH, COOKIE_DOMAIN);
    if ($result):
        $wishlistTotal++;
    endif;
}

/*
 * Get the numnber of items in wishlist
 */

function st_get_number_of_items_in_wishlist() {
    global $wishlistTotal;
    return $wishlistTotal;
}

function st_get_number_of_items_in_wishlist_startup() {
    $wishlistTotal = st_get_items_on_wishlist();
    return count($wishlistTotal);
}

/*
 * Get items on wishlist
 */

function st_get_items_on_wishlist() {
//global $wishlistTotal;
    $previousArray = array();
    if (isset($_COOKIE['sextoys-wishlist'])) {
        $cookie = stripcslashes($_COOKIE['sextoys-wishlist']);
        $previousArray = json_decode($cookie);
    }
    return (array) $previousArray;
}

/*
 * Clear items on wishlist page
 */

function st_clear_wishlist() {
    if (isset($_POST["clear-all-wishlist"])):
        $previousArray = array();
        $json = json_encode($previousArray);
        setcookie('sextoys-wishlist', $json, time() + 60 * 60 * 24 * 365, COOKIEPATH, COOKIE_DOMAIN);
    endif;
}

/*
 * Add item on wishlist to cart
 */

function st_add_wishlist_to_cart() {
    if (isset($_POST['wishlist-cart-id']) && isset($_POST['wishlist-cart-quantity'])):
        $id = (int) esc_attr($_POST['wishlist-cart-id']);
        $quantity = (int) esc_attr($_POST['wishlist-cart-quantity']);
        global $woocommerce;
        $woocommerce->cart->add_to_cart($id, $quantity);
    endif;
}

function st_get_number_of_items_on_wishlist_callback() {
    $cookie = stripcslashes($_COOKIE['sextoys-wishlist']);
    $previousArray = json_decode($cookie);
    echo count($previousArray);
    wp_die();
}

function st_get_cart_contents_callback() {
    $id = esc_attr($_POST['id']);
    $cookieArray = st_get_items_on_wishlist();
    if (isset($cookieArray)):
        foreach ($cookieArray as $element):
            if ($element == $id) {
                unset($element);
            }
        endforeach;
        $json = json_encode($cookieArray);
        $result = setcookie('sextoys-wishlist', $json, time() + 60 * 60 * 24 * 365, COOKIEPATH, COOKIE_DOMAIN);
        if ($result):

        endif;
    endif;
    wp_die();
}

function st_remove_item_from_wishlist() {
    global $wishlistTotal;
    if (isset($_POST['remove-from-wishlist'])):
        $id = esc_attr($_POST['remove-from-wishlist']);
        $cookieArray = st_get_items_on_wishlist();
        $cleanArray = array();
        if (isset($cookieArray)):
            foreach ($cookieArray as $element):
                if ($element != $id):
                    array_push($cleanArray, $element);
                endif;
            endforeach;
            $wishlistTotal = count($cleanArray);
            $json = json_encode($cleanArray);
            setcookie('sextoys-wishlist', $json, time() + 60 * 60 * 24 * 365, COOKIEPATH, COOKIE_DOMAIN);
        endif;
    endif;
}

function st_wishlist_post_data() {
    if (isset($_POST['add-to-wishlist'])):
        $id = esc_attr($_POST['add-to-wishlist']);
        st_add_item_to_cookie_wishlist($id);
        $meta_data = get_post_meta($id, 'wished');
        if ($meta_data[0]):
            $number = (int) $meta_data[0];
            $number++;
            $result = update_post_meta($id, 'wished', $number);
        else:
            $number = 1;
            $result = update_post_meta($id, 'wished', $number);
        endif;
    endif;
}

function st_add_wishlist_to_cart_callback() {
    $idArray = array();
    $valuesArray = array();
    $ids = $_POST['ids'];
    $values = $_POST['values'];
    if (isset($_POST['ids'], $_POST['values'])):
        foreach ($ids as $id):
            $id = esc_attr($id);
            $id = explode('-', $id);
            $id = $id[1];
            array_push($idArray, $id);
        endforeach;
        foreach ($values as $value):
            $value = esc_attr($value);
            array_push($valuesArray, $value);
        endforeach;
        global $woocommerce;
        for ($i = 0; $i <= count($valuesArray) - 1; $i++):
            $productID = $idArray[$i];
            $quantity = $valuesArray[$i];
            $woocommerce->cart->add_to_cart($productID, $quantity);
        endfor;
        echo true;
    endif;
    wp_die();
}

function get_ids_from_link() {
    if (isset($_GET['id'])):
        $idString = $_GET['id'];
        $ids = explode(',', $idString);
        $quantityString = $_GET['quantity'];
        $quantities = explode(',', $quantityString);
        for ($i = 0; $i <= count($ids) - 1; $i++):
            global $woocommerce;
            $id = esc_attr($ids[$i]);
            $quantity = esc_attr($quantities[$i]);
            $woocommerce->cart->add_to_cart($id, $quantity);
        endfor;
        wp_redirect(home_url());
    endif;
}

/*
 * Returns string truncated to limit
 */

function limit_text($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}

add_action('woocommerce_process_product_meta', 'st_save_post', 5);
add_action('publish_post', 'st_save_post', 5);

function st_save_post($post_ID) {
    $saveType = (int) $_POST['_select_price_save'];
    $wholePrice = ($_POST['_wholesale_price']) ? (float) $_POST['_wholesale_price'] : (float) get_post_meta($post_ID, '_wholesale_price', true);
    if ($saveType == 2):
        update_post_meta($post_ID, '_select_price_save', 2);
        $newPrice = (float) $_POST['_price'];
        st_update_price_in_database($post_ID, $newPrice);
    endif;
    if ($saveType == 1):
        update_post_meta($post_ID, '_select_price_save', 1);
        $price_markup = (int) $_POST['_price_markup'];
        update_post_meta($post_ID, '_price_markup', $price_markup);
        st_update_price_in_database($post_ID, st_calculate_price_plus_round($wholePrice, $price_markup));
    endif;
    if ($saveType == 0):
        $catMarkup = (int) $_POST['_cat_markup'];
        st_update_price_in_database($post_ID, st_calculate_price_plus_round($wholePrice, $catMarkup));
        update_post_meta($post_ID, '_select_price_save', 0);
    endif;
    update_post_meta($post_ID, '_update_automaticaly', $_POST['_update_automaticaly']);
    update_post_meta($post_ID, '_select_show_related_products_from_cat', $_POST['_select_show_related_products_from_cat']);
    update_post_meta($post_ID, '_youtube_video', $_POST['_youtube_video']);
    update_post_meta($post_ID, 'st_size', $_POST['_st_size']);
    update_post_meta($post_ID, 'st_colour', $_POST['_st_colour']);
    update_post_meta($post_ID, '_retail_price', $_POST['_retail_price']);
    update_post_meta($post_ID, '_code', $_POST['_code']);
    update_post_meta($post_ID, '_wholesale_price', $wholePrice);
}

function st_update_price_in_database($post_id, $new_price) {
    global $wpdb;
    $wpdb->query("UPDATE `wp_postmeta` SET `meta_value` = " . $new_price . " WHERE `post_id` = " . $post_id . " AND meta_key = '_regular_price'");
    $wpdb->flush();
    $wpdb->query("UPDATE `wp_postmeta` SET `meta_value` = " . $new_price . " WHERE `post_id` = " . $post_id . " AND meta_key = '_price'");
    $wpdb->flush();
}

function st_calculate_price_plus_round($wholesale_price, $percentage) {
    return number_format(round($wholesale_price + (($wholesale_price / 100) * $percentage), 0), 2);
}

/*
 * Add email address to database from footer form
 */

function st_enter_email_into_database($email) {
    global $wpdb;
    $wpdb->query("INSERT INTO `admin_sextoys`.`email_subscribers` (`id`, `email`) VALUES (NULL, '" . $email . "')");
}

function st_get_product_image($productID, $productTitle) {
    if (has_post_thumbnail($productID)):
        $image = get_the_post_thumbnail($productID, $size, $attr);
    else:
        $image = '<img class="attachment-thumb size-thumb wp-post-image" src="' . get_post_meta($productID, 'main_image_link', true) . '"alt="' . $productTitle . '"/>';
    endif;
    return $image;
}

/*
 *
 * ----------------------------|
 * Add extra product data menu |
 * ----------------------------|
 */

add_filter('woocommerce_product_data_tabs', 'st_parent_child_product_data_tab', 99, 1);

function st_parent_child_product_data_tab($product_data_tabs) {
    $product_data_tabs['parent_child'] = array(
        'label' => __('Parent/Child', 'my_text_domain'),
        'target' => 'my_custom_product_data',
    );
    return $product_data_tabs;
}

add_action('woocommerce_product_data_panels', 'add_my_custom_product_data_fields');

function add_my_custom_product_data_fields() {
    global $woocommerce, $post;
    ?>
    <div id="my_custom_product_data" class="panel woocommerce_options_panel">
        <?php
        $post_id = $post->ID;
        if (get_post_meta($post_id, '_is_parent', true)):
            ?>
            <div class="col-lg-12" style="background-color:#5cb85c">
                <p style="padding:20px;text-align: center;font-size: 18px;color:white">This product is a parent, see below for its children</p>
            </div>
            <style>img{max-width:150px;width:150px;height: auto;display:block;margin: 10px auto;}</style>
            <?php
            foreach (st_get_products_children_array($post_id) as $_post):
                $_post_id = $_post->ID;
                if ($_post_id != $post_id):
                    ?>
                    <div class="col-lg-3" style="border:3px solid #ccc;padding:10px;width: 33.3%;">
                        <p style="text-align: center"><?php echo $_post->post_title ?></p>
                        <p style="text-align: center">Code: <?php echo get_post_meta($_post_id, '_code', true) ?></p>
                        <p style="text-align: center">Size: <?php echo get_post_meta($_post_id, 'st_size', true) ?></p>
                        <p style="text-align: center">Colour: <?php echo get_post_meta($_post_id, 'st_colour', true) ?></p>
                        <?php
                        $image = st_get_the_post_full_image($_post_id);
                        if (strpos($image, '<img') !== false):
                            echo $image;
                        else:
                            echo '<img width="150" class="center-block" src=' . $image . ' />';
                        endif;
                        ?>
                        <a style="text-align: center" target="_blank" href="<?php echo $_post->guid ?>">View</a>
                    </div>
                    <?php
                endif;
            endforeach;
        else:
            ?>
            <style>img{max-width:150px;width:150px;height: auto;display:block;margin: 10px auto;}.col-lg-3{display: block;float:left;width:33.3%;}</style>
            <?php
            if (get_post_meta($post_id, '_is_child', true)):
                $parent_id = get_post_meta($post_id, '_parent_id', true);
            endif;
            $products = st_get_similar_products_array(get_post_meta($post_id, '_code', true), get_post_meta($post_id, '_brand', true));
            array_push($products, get_post($parent_id));
            foreach ($products as $_post):
                $_post_id = $_post->ID;
                if ($_post_id != $post_id && !get_post_meta($_post_id, '_is_child', true)):
                    ?>
                    <div class="col-lg-3 parent-container <?php echo ($parent_id == $_post_id) ? 'admin-parent-selected' : '' ?>" style="border:3px solid #ccc;padding:10px;">
                        <input type="hidden" <?php echo ($parent_id == $_post_id) ? 'id="_product_parent" name="_product_parent"' : '' ?> class="parent-input" value="<?php echo $_post_id ?>"/>
                        <p style="text-align: center"><?php echo $_post->post_title ?></p>
                        <p style="text-align: center">Code: <?php echo get_post_meta($_post_id, '_code', true) ?></p>
                        <p style="text-align: center">Size: <?php echo get_post_meta($_post_id, 'st_size', true) ?></p>
                        <p style="text-align: center">Colour: <?php echo get_post_meta($_post_id, 'st_colour', true) ?></p>
                        <?php
                        $image = st_get_the_post_full_image($_post_id);
                        if (strpos($image, '<img') !== false):
                            echo $image;
                        else:
                            echo '<img width="150" class="center-block" src=' . $image . ' />';
                        endif;
                        ?>
                        <a style="text-align: center" target="_blank" href="<?php echo $_post->guid ?>">View</a>
                    </div>
                    <?php
                endif;
            endforeach;
        endif;
        ?>
        <style>
            .admin-parent-selected{
                border-color: #5cb85c !important;
            }
            .parent-container{
                box-sizing: border-box;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
            }
        </style>
        <script>
            jQuery('.parent-container').click(function () {
                var selecting = true;
                if (jQuery(this).hasClass('admin-parent-selected')) {
                    selecting = false;
                }
                jQuery('.parent-container').each(function () {
                    jQuery(this).removeClass('admin-parent-selected');
                    jQuery(this).children('.parent-input').removeAttr('id');
                    jQuery(this).children('.parent-input').removeAttr('name');
                });
                if (selecting) {
                    jQuery(this).addClass('admin-parent-selected');
                    jQuery(this).children('.parent-input').attr('id', '_product_parent');
                    jQuery(this).children('.parent-input').attr('name', '_product_parent');
                }
            });
        </script>
    </div>
    <?php
}

add_action('woocommerce_process_product_meta', 'woocommerce_process_product_meta_fields_save');

function woocommerce_process_product_meta_fields_save($post_id) {
    $woo_checkbox = (isset($_POST['_product_parent']) && !empty($_POST['_product_parent'])) ? $_POST['_product_parent'] : false;
    if ($woo_checkbox):
        update_post_meta($post_id, '_parent_id', $woo_checkbox);
        update_post_meta($woo_checkbox, '_is_parent', true);
        update_post_meta($post_id, '_is_child', true);
        delete_post_meta($post_id, '_is_parent');
    else:
        delete_post_meta($post_id, '_parent_id');
        delete_post_meta($post_id, '_is_parent');
        delete_post_meta($post_id, '_is_child');
    endif;
}

function st_get_similar_products_array($code, $brand) {
    $products = new WP_Query(array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'meta_query' => array(
            'relation' => 'OR',
            array(
                'key' => '_code',
                'value' => substr($code, 0, 4),
                'compare' => 'LIKE'
            ),
            array(
                'key' => '_brand',
                'value' => $brand,
                'compare' => 'LIKE'
            )
        )
    ));
    return $products->posts;
}

function st_get_products_children_array($parent_id) {
    $products = new WP_Query(array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
        'meta_query' => array(
            array(
                'key' => '_parent_id',
                'value' => (string) $parent_id,
                'compare' => 'LIKE'
            )
        )
    ));
    return $products->posts;
}

/*
 * Add product custom meta to product admin
 */

function woo_add_custom_general_fields() {
    global $woocommerce, $post;
    $cats_array = get_all_categories();
    $cats_options = array();
    foreach ($cats_array as $cat):
        $cats_options[$cat->term_id] = $cat->parent . ' - ' . $cat->name;
    endforeach;
    unset($cats_array);
    echo '<div class="options_group">';
    woocommerce_wp_text_input(
            array(
                'id' => '_price',
                'label' => __('Price ($)', 'woocommerce'),
                'type' => 'float'
            )
    );
    woocommerce_wp_text_input(
            array(
                'id' => '_wholesale_price',
                'label' => __('Wholesale Price ($)', 'woocommerce'),
                'type' => 'float'
            )
    );
    woocommerce_wp_text_input(
            array(
                'id' => '_retail_price',
                'label' => __('Retail Price ($)', 'woocommerce'),
                'type' => 'float'
            )
    );
    woocommerce_wp_text_input(
            array(
                'id' => '_brand',
                'label' => __('Brand', 'woocommerce'),
            )
    );
    woocommerce_wp_text_input(
            array(
                'id' => '_code',
                'label' => __('Code', 'woocommerce'),
            )
    );
    $post_ID = $post->ID;
    woocommerce_wp_text_input(
            array(
                'id' => '_price_markup',
                'label' => __('Percentage of markup on wholesale price (%)', 'woocommerce'),
                'type' => 'number',
            )
    );
    $terms = get_the_terms($post_ID, 'product_cat');
    $cat_id = $terms[0]->term_id;
    woocommerce_wp_text_input(
            array(
                'id' => '_cat_markup',
                'label' => __('Category Markup', 'woocommerce'),
                'value' => st_get_category_markup($cat_id)
            )
    );
    woocommerce_wp_text_input(
            array(
                'id' => '_st_size',
                'label' => __('Product Size', 'woocommerce'),
                'value' => get_post_meta($post_ID, 'st_size', true)
            )
    );
    woocommerce_wp_text_input(
            array(
                'id' => '_st_colour',
                'label' => __('Product Colour', 'woocommerce'),
                'value' => get_post_meta($post_ID, 'st_colour', true)
            )
    );
    woocommerce_wp_text_input(
            array(
                'id' => '_youtube_video',
                'label' => __('You Tube Video Link', 'woocommerce')
            )
    );
    woocommerce_wp_select(
            array(
                'id' => '_select_price_save',
                'label' => __('Set price by:', 'woocommerce'),
                'type' => 'Dropdown Select',
                'options' => array(
                    0 => __('Category markup', 'woocommerce'),
                    1 => __('Manual markup', 'woocommerce'),
                    2 => __('Manual Price', 'woocommerce'),
                )
            )
    );
    woocommerce_wp_checkbox(array(
        'id' => '_update_automaticaly',
        'label' => __('Update price if category markup changed', 'woocommerce'),
            )
    );
    woocommerce_wp_select(
            array(
                'id' => '_select_show_related_products_from_cat',
                'label' => __('Show related categories from:', 'woocommerce'),
                'type' => 'Dropdown Select',
                'options' => $cats_options
            )
    );
    echo '</div>';
}

add_action('product_cat_edit_form_fields', 'st_add_fields_to_cat_page', 10, 2);


/*
 * -------------------------------------------------|
 * Get the percentage markup from the category page |
 * -------------------------------------------------|
 */

function st_get_category_markup($catId) {
    if ($catId):
        $optionsArray = get_option("taxonomy_$catId");
        return $optionsArray["category_percentage_markup"];
    endif;
}

function st_get_category_meta($catId, $meta) {
    if ($catId):
        $optionsArray = get_option("taxonomy_$catId");
        return $optionsArray[$meta];
    endif;
}

/**
 * Add a details metabox to the Add New Product Category page.
 *
 * For adding a details metabox to the WordPress admin when
 * creating new product categories in WooCommerce.
 *
 */
function st_add_fields_to_cat_page($term) {
    $t_id = $term->term_id;
    $term_meta = get_option("taxonomy_$t_id");
    ?>
    <tr class="form-field">
        <th scope="row" valign="top"><label for="term_meta[category_percentage_markup]"><?php _e('Category Percentage Markup', 'tutorialshares'); ?></label></th>
        <td>
            <input type="number" name="term_meta[category_percentage_markup]" id="term_meta[category_percentage_markup]" value="<?php echo esc_attr($term_meta['category_percentage_markup']) ? esc_attr($term_meta['category_percentage_markup']) : ''; ?>">
            <p class="description"><?php _e('Enter a percentage markup for this category', 'tutorialshares'); ?></p>
        </td>
    </tr>
    <tr class="form-field">
        <th scope="row" valign="top"><label for="term_meta[cat_order]"><?php _e('Category Order', 'tutorialshares'); ?></label></th>
        <td>
            <input type="number" name="term_meta[cat_order]" id="term_meta[cat_order]" value="<?php echo esc_attr($term_meta['cat_order']) ? esc_attr($term_meta['cat_order']) : ''; ?>">
            <p class="description"><?php _e('Order which the categories appear', 'tutorialshares'); ?></p>
        </td>
    </tr>
    <?php
}

add_action('woocommerce_product_options_general_product_data', 'woo_add_custom_general_fields', 10, 2);

function save_taxonomy_custom_meta($term_id) {
    if (isset($_POST['term_meta'])):
        $t_id = $term_id;
        $term_meta = get_option("taxonomy_$t_id");
        $cat_keys = array_keys($_POST['term_meta']);
        $message = 'term meta:';
        $percentage = $_POST['term_meta']['category_percentage_markup'];
        $order_number = $_POST['term_meta']['cat_order'];
        foreach ($cat_keys as $key) {
            if (isset($_POST['term_meta'][$key])) {
                $term_meta[$key] = $_POST['term_meta'][$key];
            }
        }
        update_option("taxonomy_$t_id", $term_meta);
        $post_ids = get_posts(array(
            'numberposts' => -1,
            'post_type' => 'product',
            'fields' => 'ids',
        ));
        foreach ($post_ids as $id):
            $update = get_post_meta($id, '_update_automaticaly', true);
            if ($update):
                $wholePrice = (float) get_post_meta($id, '_wholesale_price', true);
                st_update_price_in_database($id, st_calculate_price_plus_round($wholePrice, $percentage));
                update_post_meta($id, '_select_price_save', 3);
            endif;
        endforeach;
        if (isset($order_number)):
        //update_post_meta();
        endif;
    endif;
}

add_action('edited_product_cat', 'save_taxonomy_custom_meta', 10, 2);
add_action('create_product_cat', 'save_taxonomy_custom_meta', 10, 2);

function st_var_dump($element) {
    ?>
    <pre>
        <?php var_dump($element); ?>
    </pre>
    <?php
}

/*
 * Get product id already in the database
 */

function getExisitingProductIDS() {
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash')
    );

    $existingProducts = get_posts($args);
    $existingSkus = array();

    foreach ($existingProducts as $product):
        array_push($existingSkus, get_post_meta($product->ID, '_code', true));
    endforeach;
    return $existingSkus;
}

function productCsvToArray() {
    $localSavePath = get_home_path() . 'products5.csv';
    if (file_exists($localSavePath)):
        $feed = array();
        $file = fopen($localSavePath, "r");

        while (!feof($file)) {
            $row = fgetcsv($file);
            array_push($feed, $row);
        }

        fclose($file);
    endif;
    return $feed;
}

/*
 * Returns an array of all aviaible brands from cat
 */

function st_get_brands_from_cat($id) {
    $terms = get_term($id, 'product_cat');
    $theslug = $terms->slug;
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'slug',
                'terms' => $theslug
            )
    ));
    $postsArray = new WP_Query($args);
    unset($args);
    $brandsArray = array();
    if ($postsArray->have_posts()):
        while ($postsArray->have_posts()) : $postsArray->the_post();
            $brand = get_post_meta(get_the_ID(), '_brand', true);
            if (!in_array($brand, $brandsArray)):
                array_push($brandsArray, $brand);
            endif;
        endwhile;
        unset($postsArray);
    endif;
    sort($brandsArray);
    wp_reset_postdata();
    return $brandsArray;
}

function st_get_product_meta_from_cat_array($idArray, $mateValue) {
    $postsArray = new WP_Query(array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'fields' => 'ids',
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'id',
                'terms' => $idArray
            )
    )));
    $brandsArray = array();
    $postsArray = $postsArray->posts;
    if (count($postsArray) > 0):
        foreach ($postsArray as $id) :
            $size = get_post_meta($id, $mateValue, true);
            if (!in_array(trim(strtolower($size)), $brandsArray)):
                array_push($brandsArray, trim(strtolower($size)));
            endif;
            unset($id);
        endforeach;
    endif;
    unset($postsArray);
    sort($brandsArray);
    return $brandsArray;
}

function st_sort_overlay_meta_options($post_cat_id_array, $meta, $name = '') {
    foreach (st_get_product_meta_from_cat_array($post_cat_id_array, $meta) as $size):
        ?>
        <li class="squaredFour margin-5">
            <p><?php echo $size; ?></p>
            <input name="<?php echo $name ?>" type="checkbox" id="<?php echo trim($size); ?>" value="<?php echo $size ?>"/>
            <label for="<?php echo trim($size); ?>"></label>
        </li>
        <?php
    endforeach;
}

function st_get_price_range_from_cat_array($idArray) {
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'id',
                'terms' => $idArray
            )
    ));
    return st_get_price_range_from_cat($args);
}

function st_get_brands_from_cat_array($idArray) {
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'id',
                'terms' => $idArray
            )
    ));
    $postsArray = new WP_Query($args);
    unset($args);
    $brandsArray = array();
    if ($postsArray->have_posts()):
        while ($postsArray->have_posts()) : $postsArray->the_post();
            $brand = get_post_meta(get_the_ID(), '_brand', true);
            if (!in_array($brand, $brandsArray)):
                array_push($brandsArray, $brand);
            endif;
        endwhile;
        unset($postsArray);
    endif;
    sort($brandsArray);
    wp_reset_postdata();
    return $brandsArray;
}

function st_get_products_from_cat($cat, $args) {
    $postsArray = new WP_Query($args);
    $productArray = array();
    if ($postsArray->have_posts()):
        while ($postsArray->have_posts()) : $postsArray->the_post();
            $product_cats = wp_get_post_terms(get_the_ID(), 'product_cat');
            $product_cats = $product_cats[0];
            if ($product_cats->term_id === $cat):
                array_push($productArray, get_the_ID());
            endif;
        endwhile;
        wp_reset_postdata();
    endif;
    return $productArray;
}

function st_get_price_range_from_cat($args) {
    global $post;
    $args = $args;
    $min_price;
    $max_price;
    $postsArray = new WP_Query($args);
    $priceArray = array();
    if ($postsArray->have_posts()):
        while ($postsArray->have_posts()) : $postsArray->the_post();
            $post_id = $post->ID;
            $_product = new WC_Product($post_id);
            if (!$min_price):
                $min_price = (int) $_product->price;
            endif;
            if ($min_price > (int) $_product->price):
                $min_price = (int) $_product->price;
            elseif ($max_price < (int) $_product->price):
                $max_price = (int) $_product->price;
            endif;
        endwhile;
        wp_reset_postdata();
    endif;
    array_push($priceArray, $min_price);
    array_push($priceArray, $max_price);
    return $priceArray;
}

function st_not_in_array($needle, $haystack) {
    $is_in = true;
    foreach ($haystack as $element):
        if ($needle == $element):
            $is_in = false;
            break;
        endif;
    endforeach;
    return $is_in;
}

/*
 * --------------------------------|
 * Get breadcrumb for product page |
 * --------------------------------|
 */

function st_get_breadcrumb($product_id) {
    $patterns = array();
    $patterns[0] = '/&amp;/';
    $patterns[1] = '/&/';
    $patterns[3] = '/,/';
    $patterns[4] = '/ /';
    $breadcrumb = '';
    $product_cat = st_get_cat_object(get_product_category($product_id));
    $parent = st_get_cat_object(st_cat_top_level_parent($product_cat->term_id));
    if ($parent->term_id !== $product_cat->parent):
        $sub_parent = st_get_cat_object($product_cat->parent);
    endif;
    //$breadcrumb .= ($parent->name) ? '<a href="' . get_category_link($parent->term_id) . '">' . $parent->name . '</a>/' : '';
    $breadcrumb .= ($sub_parent->name) ? '<a href="/' . strtolower(preg_replace($patterns, '-', $sub_parent->name)) . '/">' . $sub_parent->name . '</a>/' : '';
    $breadcrumb .= ($product_cat->name) ? '<a href="/' . strtolower(preg_replace($patterns, '-', $sub_parent->name)) . '/' . strtolower(preg_replace($patterns, '-', $product_cat->name)) . '/">' . $product_cat->name . '</a>' : '';
    return $breadcrumb;
}

function st_parent_and_current_cat_slug($product_id) {
    $cleanArray = array();
    $product_cat = st_get_cat_object(get_product_category($product_id));
    if ($parent->term_id !== $product_cat->parent):
        $sub_parent = st_get_cat_object($product_cat->parent);
    endif;
    $patterns = array();
    $patterns[0] = '/&amp;/';
    $patterns[1] = '/&/';
    $patterns[3] = '/,/';
    $patterns[4] = '/ /';
    $catName = strtolower(preg_replace($patterns, '-', $sub_parent->name));
    $slugName = strtolower(preg_replace($patterns, '-', $product_cat->name));
    array_push($cleanArray, $catName);
    array_push($cleanArray, $slugName);
    return $cleanArray;
}

function st_get_product_category($product_array) {
    $cleanArray = array();
    foreach ($product_array as $product) {
        $cat = get_product_category($product);
        if (!in_array($cat, $cleanArray)):
            array_push($cleanArray, $cat);
        endif;
    }
    return $cleanArray;
}

function st_get_product_metas($product_array, $meta) {
    $cleanArray = array();
    foreach ($product_array as $product) {
        $brand = get_post_meta($product, $meta, true);
        if (!in_array($brand, $cleanArray)):
            array_push($cleanArray, $brand);
        endif;
    }
    return $cleanArray;
}

/*
 * ---------------------------------|
 * Add out of stock on product list |
 * ---------------------------------|
 */

function st_get_category_ids($cats, $catOrIds) {
    if ($catOrIds == 'ids'):
        $ids = $cats;
    else:
        $args = array(
            'post_type' => 'product',
            'posts_per_page' => -1,
            'fields' => 'ids',
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'id',
                    'terms' => $cats
                )
            ),
        );
        $query = new WP_Query($args);
        $ids = $query->posts;
    endif;
    return $ids;
}

function showHideTag($termID, $catID) {
    $showArray = array();
    foreach (explode(',', get_term_meta((int) $termID, 'show_in_cats', true)) as $index => $element):
        $showArray[$index] = (int) $element;
    endforeach;
    return in_array($catID, $showArray) ? TRUE : FALSE;
}

function st_get_tag_terms($cats, $catOrIds) {
    $returnArray = array();
    foreach (st_get_category_ids($cats, $catOrIds) as $id):
        foreach (get_the_terms($id, 'product_tag') as $term):
            $term_name = $term->name;
            $term = $term->term_id;
            if (!in_array($term_name, $returnArray)):
                array_push($returnArray, $term_name);
            endif;
            unset($term);
        endforeach;
        unset($id);
    endforeach;
    sort($returnArray);
    return $returnArray;
}

function getProductsTags($cats, $catOrIds) {
    foreach (st_get_tag_terms($cats, $catOrIds) as $tag):
        echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding squaredFour no-margin margin-5">';
        echo "<p>{$tag}</p>";
        echo "<input type='checkbox' id='{$tag}' value='{$tag}' name='tag'/><label for='{$tag}'></label>";
        echo '</div>';
        unset($tag);
    endforeach;
}

/*
 * ----------------------------------|
 * Get product id's for archive page |
 * ----------------------------------|
 */

function st_get_args_for_sort_product_ids($metaArray, $cats) {
    $order_by = 'meta_value_num';
    if (!$metaArray[$price]):
        $price = 'ASC';
        $order_by = 'title';
    endif;
    if (isset($metaArray)):
        $meta_query = array(
            'relation' => 'AND',
            array(
                'key' => '_price',
                'value' => array($metaArray[$prices[0]], $metaArray[$prices[1]]),
                'compare' => 'BETWEEN',
                'type' => 'numeric'
            ),
            array(
                'key' => '_brand',
                'value' => $metaArray[$brands],
                'compare' => 'IN'
            ),
            array(
                'key' => 'st_size',
                'value' => $metaArray[$sizes],
                'compare' => 'IN'
            ),
            array(
                'key' => 'st_colour',
                'value' => $metaArray[$colours],
                'compare' => 'IN'
            )
        );
    else:
        $meta_query = array();
    endif;
    $query = new WP_Query(array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'fields' => 'ids',
        'orderby' => $order_by,
        'meta_key' => '_price',
        'post_status' => 'publish',
        'product_tag' => $metaArray[$tags],
        'order' => $price,
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'id',
                'terms' => $cats
            )
        ),
        'meta_query' => $meta_query
    ));
    return $query->posts;
}

function st_add_tracking_number_to_order() {

}

function st_addWholesalePriceToItemMeta($orderID) {
    global $woocommerce;
    $order = new WC_Order($orderID);
    $items = $order->get_items();
    foreach ($items as $index => $item):
        wc_add_order_item_meta((int) $index, '_wholesale_price', get_post_meta((int) $item["item_meta"]["_product_id"][0], '_wholesale_price', true));
        $_product = $_pf->get_product($item["product_id"]);
    endforeach;
}

function st_sort_product_ids($cats, $loadedArray, $metaArray) {
    $cleanArray = array();
    $arrayAfterParent = array();
    //$newLoadedProducts = array();
    $returnArray = array();
    foreach (st_get_args_for_sort_product_ids($metaArray, $cats)as $id):
        if (is_int($id)):
        //array_push($newLoadedProducts, $id);
        endif;
        if (st_not_in_array($id, $loadedArray)):
            array_push($cleanArray, $id);
        endif;
    endforeach;
    foreach ($cleanArray as $id):
        $id = (int) $id;
        if (!get_post_meta($id, '_is_child', true) && is_int($id)):
            if (!in_array($id, $arrayAfterParent)):
                array_push($arrayAfterParent, $id);
            endif;
        else:
            $parent_id = (int) st_get_products_parent($id);
            if (st_not_in_array($parent_id, $arrayAfterParent) && is_int($parent_id)):
                array_push($arrayAfterParent, $parent_id);
            else:
                if (!is_int($parent_id)):
                //echo $id;
                //st_var_dump($id);
                endif;
            endif;
        endif;
    endforeach;
    unset($cleanArray);
    array_push($returnArray, $arrayAfterParent);
    unset($arrayAfterParent);
    //array_push($returnArray, $newLoadedProducts);
    //unset($newLoadedProducts);
    return $returnArray;
}

/*
 * ----------------------------------------------------------|
 * Get products for archive page when they have been refined |
 * ----------------------------------------------------------|
 */

function st_get_products($post_args) {
    remove_all_filters('posts_orderby');
    global $post;
    $brand = $post_args['brand'];
    $price = $post_args['price'];
    $cat = $post_args['cat'];
    $offset = $post_args['offset'];
    if (!$price):
        $price = 'ASC';
    endif;

    if ($brand):
        $meta_query = array(
            array(
                'key' => '_brand',
                'value' => $brand,
                'compare' => 'like',
            )
        );
    else:
        $meta_query = array();
    endif;

    $args = array(
        'post_type' => 'product',
        'product_cat' => $cat,
        'orderby' => 'meta_value_num',
        'meta_key' => '_price',
        'order' => $price,
        'posts_per_page' => 10,
        'offset' => $offset,
        'suppress_filters' => true,
        'meta_query' => $meta_query
    );
    $loop = new WP_Query($args);
    if ($loop->have_posts()):
        while ($loop->have_posts()) : $loop->the_post();
            $post_id = $post->ID;
            ?>
            <div id="product_<?php echo $post_id ?>" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 archive-product-container">
                <input class="product_id" type="hidden" value="<?php echo $post_id ?>" />
                <div class="col-lg-3 col-md-3 col-sm-3 archive-product-image">
                    <?php the_post_thumbnail('medium') ?>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 archive-product-details">
                    <div class="col-lg-12 col-sm-12 archive-product-title">
                        <h4><?php the_title() ?></h4>
                    </div>
                    <div class="col-lg-12 col-md-6 col-sm-12 archive-product-description">
                        <?php the_excerpt() ?>
                    </div>
                    <div class="col-lg-12 col-sm-12 archive-product-price">
                        <p class="price">$<?php echo get_post_meta($post_id, '_price', true); ?>
                    </div>
                </div>
                <div id="mobile_product_<?php echo $post_id ?>" class="col-sm-12 archive-mobile-product-container"></div>
            </div>

            <?php
        endwhile;
    else:
        echo 'false';
    endif;
    wp_reset_postdata();
}

function st_get_products_parent($product_id) {
//    $parent_id = '';
//    $aa = get_post_meta($product_id, '_parent_id', true);
//    foreach ($aa as $id):
//        if (get_post_meta($id, '_is_parent', true) == 'true'):
//            $parent_id = $id;
//        endif;
//    endforeach;
//    return $parent_id;
    return get_post_meta($product_id, '_parent_id', true);
}

/*
 * --------------------------|
 * Custom email for new user |
 * --------------------------|
 */

add_action('plugins_loaded', 'new_user_notifiaction');

function new_user_notifiaction() {

    function wp_new_user_notification($user_id) {

    }

}

/*
 * Check a products price for the sort filter
 */

function st_check_price_filter($product_id, $price_array) {
    $return_boo = false;
    $price = (float) st_get_product_meta($product_id, '_price');
    foreach ($price_array as $value):
        $value = (float) $value;
        if ($value == 19.99):
            if ($price <= $value):
                return true;
            endif;
        endif;
        if ($value == 39.99):
            if ($price <= $value):
                return true;
            endif;
        endif;
        if ($value == 59.99):
            if ($price <= $value):
                return true;
            endif;
        endif;
        if ($value == 60.00):
            if ($price >= $value):
                return true;
            endif;
        endif;
    endforeach;
    return $return_boo;
}

function vibrators_size_search($id, $sizes) {
    $is_in_array = false;
    $product_size = st_get_product_meta($id, 'st_size');
    $substring = (int) substr($product_size, 0, 2);
    if ($substring[count($substring)] === '.' || $substring[count($substring)] === ' '):
        $substring = substr($substring, 0, 1);
    endif;
    if ($substring):
        foreach ($sizes as $size):
            if ($size === '2-6 inches'):
                if ($substring <= 5):
                    $is_in_array = true;
                endif;
            endif;
            if ($size === '6-10 inches'):
                if ($substring >= 6 && $substring <= 9):
                    $is_in_array = true;
                endif;
            endif;
            if ($size === '10-14 inches'):
                if ($substring >= 10 && $substring <= 13):
                    $is_in_array = true;
                endif;
            endif;
            if ($size === 'over 14 inches'):
                if ($substring >= 14):
                    $is_in_array = true;
                endif;
            endif;
        endforeach;
    endif;
    return $is_in_array;
}

function st_apparel_size_search($id, $sizes) {
    $is_in_array = false;
    $smallArray = array('small', 's/m');
    $mediumArray = array('medium', 's/m');
    $largeArray = array('large', 'l/xl');
    $xlargeArray = array('x large', 'l/xl');
    $plusArray = array('1x', '2xl', '3xl', '1x', '2x', '3x', '4x', 'queen');
    $product_size = strtolower(st_get_product_meta($id, 'st_size'));
    foreach ($sizes as $size):
        $size = strtolower($size);
        if ($size == 'one size' && $product_size == 'one size'):
            $is_in_array = true;
        endif;
        if ($size == 'small' && in_array($product_size, $smallArray, true)):
            $is_in_array = true;
        endif;
        if ($size == 'medium' && in_array($product_size, $mediumArray, true)):
            $is_in_array = true;
        endif;
        if ($size == 'large' && in_array($product_size, $largeArray, true)):
            $is_in_array = true;
        endif;
        if ($size == 'x large' && in_array($product_size, $xlargeArray, true)):
            $is_in_array = true;
        endif;
        if ($size == 'plus' && in_array($product_size, $plusArray, true)):
            $is_in_array = true;
        endif;
    endforeach;
    return $is_in_array;
}

/*
 * Get the product colour value
 */

function st_get_product_color($post_id) {
    return get_post_meta($post_id, 'st_colour', true);
}

/*
 * ---------------------------|
 * Get the product size value |
 * ---------------------------|
 */

function st_get_product_size($post_id) {
    return get_post_meta($post_id, 'st_size', true);
}

/*
 * ---------------------------|
 * Get the product meta value |
 * ---------------------------|
 */

function st_get_product_meta($post_id, $meta_value) {
    return get_post_meta($post_id, $meta_value, true);
}

/*
 * Get Array of products brands
 */

function st_get_products_metas($idArray, $meta_value) {
    $cleanArray = array();
    foreach ($idArray as $id):
        $meta = st_get_product_meta($id, $meta_value);
        if (st_not_in_array($meta, $cleanArray) && $meta != ''):
            array_push($cleanArray, $meta);
        endif;
    endforeach;
    return $cleanArray;
}

/*
 * Get the products cat slug from id
 */

function st_get_cat_slug_from_id($product_id) {
    $term_array = get_the_terms($product_id, 'product_cat');
    $term_object = $term_array[0];
    return $term_object->slug;
}

/*
 * Get the products related cat slug from id
 */

function st_get_related_cat_slug_from_id($product_id) {
    $post_meta = get_post_meta($product_id, '_select_show_related_products_from_cat', true);
    if (!$post_meta):
        $post_meta = 25;
    endif;
    $term_array = get_term_by('id', (int) $post_meta, 'product_cat', 'ARRAY_A');
    return $term_array['slug'];
}

function st_get_cat_id_from_slug($cat_slug) {
    $category = get_term_by('slug', $cat_slug, 'product_cat', 'ARRAY_A');
    return $category["term_id"];
}

/*
 * Count the number of products in a category
 */

function st_get_number_of_products_in_category($cat_slug, $post_id) {	
	global $wpdb;
    $wpdb->flush();
    $rowcount = $wpdb->get_var("SELECT COUNT(*) FROM wp_posts
					INNER JOIN wp_term_relationships ON wp_posts.ID = wp_term_relationships.object_id
					INNER JOIN wp_term_taxonomy ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id AND wp_term_taxonomy.taxonomy = 'product_cat'
					INNER JOIN wp_terms ON wp_terms.term_id = wp_term_taxonomy.term_id AND wp_terms.slug = '".$cat_slug."' 
					WHERE wp_posts.post_type = 'product' AND wp_posts.post_status = 'publish'");	    
    
	return intval($rowcount);
}

function st_get_full_archive_product_callback() {
    include_once 'partials/full-product-overlay.php';
    wp_die();
}

/*
 * Add item to cart by ajax
 */

function st_add_to_cart_callback() {
    $quantity = strip_tags($_POST['quantity']);
    $id = strip_tags($_POST['ID']);
    $result = WC()->cart->add_to_cart($id, $quantity);
    if ($result):
        echo WC()->cart->get_cart_contents_count();
    endif;
    wp_die();
}

function st_populate_cart_overlay_callback() {
    echo st_get_cart_contents();
    wp_die();
}

function st_get_the_post_thumbnail($post_id) {
    $image = get_post_meta($post_id, 'image_link_thumb', true);
    if (!$image || $image == ''):
        $image = get_post_meta($post_id, 'main_image_link', true);
    endif;
    return $image;
}

function st_get_the_post_full_image($post_id, $image_size = 'full') {
    $_product = wc_get_product($post_id);
    $image = $_product->get_image($image_size);
    if (st_is_placeholder_image($image)):
        $image = get_post_meta($post_id, 'main_image_link', true);
    endif;
    return $image;
}

function st_is_placeholder_image($image_url) {
    return (strpos($image_url, 'placeholder')) ? true : false;
}

function image_tag_class($class) {
    $class .= ' main-image';
    return $class;
}

add_filter('get_image_tag_class', 'image_tag_class');

/*
 * ----------------------------------------|
 * Product container for the products page |
 * ----------------------------------------|
 */

function st_archive_product_container($post_id, $product_number, $list_number = 0) {
    if (get_post_meta($post_id, '_price', true) && st_check_price_vs_retail($post_id)):
        require_once 'functions/reviews_class.php';
        require_once 'ajax_functions.php';
        $stars = reviews_class::getStarRating($post_id);
        global $post;
        $post = get_post((int) $post_id);
        ?>
        <div id="product_<?php echo $post_id ?>" class="col-lg-3 col-md-3 col-sm-4 col-xs-6 archive-product-container <?php echo ($list_number > 11) ? 'hidden' : '' ?>">            
            <div class="archive-product-container-inner">
                <a href="<?php the_guid($post_id) ?>">
                    <div class="col-lg-12 col-md-12 col-sm-12 archive-product-image">
                        <?php
                        if (has_post_thumbnail($post)):
                            echo get_the_post_thumbnail($post, 'thumb');
                        else:
                            ?>
                            <img class="attachment-thumb size-thumb wp-post-image <?php echo ($list_number > 11) ? 'hidden' : '' ?>" src="<?php echo st_get_the_post_thumbnail($post_id) ?>" data-src="<?php echo st_get_the_post_thumbnail($post_id) ?>"/>
                        <?php endif;
                        ?>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 archive-product-details">
                        <div class="col-lg-12 col-sm-12 archive-product-title">
                            <h4><?php the_title() ?></h4>
                            <?php /* <div class="stars">
                                <?php if ($stars): ?>
                                    <?php for ($i = 0; $i < $stars; $i++): ?>
                                        <input class="star star-<?php echo $i + 1 ?>" type="radio" name="star"/>
                                        <label class="star star-<?php
                                        echo $i + 1;
                                        echo ($i == 0) ? ' first-star' : ''
                                        ?>" for="star-5-<?php echo $i + 1 ?>"></label>
                                            <?php
                                        endfor;
                                    else:
                                        ?>
                                    <input class="star-blank first-star"/>
                                    <label class="star-blank first-star"></label>
                                    <input class="star-blank"/>
                                    <label class="star-blank"></label>
                                    <input class="star-blank"/>
                                    <label class="star-blank"></label>
                                    <input class="star-blank"/>
                                    <label class="star-blank"></label>
                                    <input class="star-blank"/>
                                    <label class="star-blank"></label>
                                <?php endif; ?>
                            </div> */ ?>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 archive-product-description">
                            <?php
    //                        $the_excerpt = get_the_excerpt();
    //                        if (strpos($the_excerpt, '&nbsp;') !== false):
    //                            $the_excerpt = str_replace('&nbsp;', '', $the_excerpt);
    //                        endif;
                            //echo $the_excerpt;
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding product-price-button-container">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 archive-product-price">
                            <?php $retail_price = get_post_meta($post_id, '_retail_price', true); ?>
                            <p class="retail_price"><?php echo ($retail_price) ? 'Don\'t pay $' . number_format((float) $retail_price, 2) : '' ?></p>
                            <p class="price">Now $<?php echo number_format((float) get_post_meta($post_id, '_price', true), 2); ?></p>
                        </div>                    
                    </div>
                </a>
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 no-padding">
                    <div class="col-sm-12 no-padding">
                        <button id="<?php echo $post_id ?>" class="btn archive-product-container-button add-to-cart-ajax" type="button" onclick="addToCartAjax(this);">Add to Cart</button>                            
                    </div>
                    <div class="col-sm-12 no-padding">    
                        <button id="<?php echo $post_id ?>" type="button" class="btn archive-product-container-button buy-now" onclick="addToCartAjax(this,'<?php echo WC_Cart::get_checkout_url(); ?>')">Buy Now</button>
                    </div>
                </div>                
            </div>
        </div>
        <div id="mobile_product_<?php echo $post_id ?>" class="col-sm-12 archive-mobile-product-container"></div>
        <?php
        $product_number++;
    endif;
    return $product_number;
}

function st_check_price_vs_retail($post_id) {
    return ((float) get_post_meta($post_id, '_price', true) >= (float) get_post_meta($post_id, '_wholesale_price', true)) ? true : false;
}

function st_archive_product_container_ajax($post_id, $offset) {
    if (get_post_meta($post_id, '_price', true) && st_check_price_vs_retail($post_id)):
        global $post;
        $post = get_post((int) $post_id);
        ?>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 archive-product-container">
            <input type="hidden" name='product_offset' value="<?php echo $offset ?>"/>
            <div class="archive-product-container-inner">
                <a href="<?php the_guid($post_id) ?>">
                    <div class="col-lg-12 col-md-12 col-sm-12 archive-product-image">
                        <?php
                        if (has_post_thumbnail($post)):
                            echo get_the_post_thumbnail($post, 'thumb');
                        else:
                            ?>
                            <img class="attachment-thumb size-thumb wp-post-image" src="<?php echo st_get_the_post_thumbnail($post_id) ?>" data-src="<?php echo st_get_the_post_thumbnail($post_id) ?>"/>
                        <?php endif;
                        ?>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 archive-product-details">
                        <div class="col-lg-12 col-sm-12 archive-product-title">
                            <h4><?php the_title() ?></h4>
                            <?php /* <div class="stars">
                                <?php if ($stars): ?>
                                    <?php for ($i = 0; $i < $stars; $i++): ?>
                                        <input class="star star-<?php echo $i + 1 ?>" type="radio" name="star"/>
                                        <label class="star star-<?php
                                        echo $i + 1;
                                        echo ($i == 0) ? ' first-star' : ''
                                        ?>" for="star-5-<?php echo $i + 1 ?>"></label>
                                            <?php
                                        endfor;
                                    else:
                                        ?>
                                    <input class="star-blank first-star"/>
                                    <label class="star-blank first-star"></label>
                                    <input class="star-blank"/>
                                    <label class="star-blank"></label>
                                    <input class="star-blank"/>
                                    <label class="star-blank"></label>
                                    <input class="star-blank"/>
                                    <label class="star-blank"></label>
                                    <input class="star-blank"/>
                                    <label class="star-blank"></label>
                                <?php endif; ?>
                            </div> */?>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 archive-product-description">
                            <?php
    //                        $the_excerpt = get_the_excerpt();
    //                        if (strpos($the_excerpt, '<p>&nbsp;</p>') !== false):
    //                            $the_excerpt = str_replace('<p>&nbsp;</p>', '', $the_excerpt);
    //                        endif;
    //                        echo $the_excerpt;
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding product-price-button-container">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 archive-product-price">
                            <?php $retail_price = get_post_meta($post_id, '_retail_price', true); ?>
                            <p class="retail_price"><?php echo ($retail_price) ? 'Don\'t pay $' . number_format((float) $retail_price, 2) : '' ?></p>
                            <p class="price">Now $<?php echo number_format((float) get_post_meta($post_id, '_price', true), 2); ?></p>
                        </div>                    
                    </div>
                </a>
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 no-padding">
                    <div class="col-sm-12 no-padding">    
                        <button id="<?php echo $post_id ?>" class="btn archive-product-container-button add-to-cart-ajax" type="button" onclick="addToCartAjax(this);">Add to Cart</button> 
                    </div>
                    <div class="col-sm-12 no-padding">    
                        <button id="<?php echo $post_id ?>" type="button" class="btn archive-product-container-button buy-now" onclick="addToCartAjax(this,'<?php echo WC_Cart::get_checkout_url(); ?>')">Buy Now</button>
                    </div>
                </div>                
            </div>
        </div>
        <?php
    endif;
}

function st_archive_product_container_old($post_id, $product_number) {
    global $post;
    $post = get_post((int) $post_id);
    if ($product_number == 0):
        ?><div class="mobile-row">
    <?php endif; ?>
        <div id="product_<?php echo $post_id ?>" class="col-lg-12 col-md-6 col-sm-6 col-xs-6 archive-product-container">
            <input class="product_id" type="hidden" value="<?php echo $post_id ?>" />
            <div class="col-lg-3 col-md-12 col-sm-6 archive-product-image">
                <?php the_post_thumbnail('thumb') ?>
            </div>
            <div class="col-lg-9 col-md-12 col-sm-6 archive-product-details">
                <div class="col-lg-12 col-sm-12 archive-product-title">
                    <h4><?php the_title() ?></h4>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 archive-product-description">
                    <?php the_excerpt() ?>
                </div>
                <div class="col-lg-6 col-sm-12 archive-product-price">
                    <p class="price">$<?php echo get_post_meta($post_id, '_price', true); ?></p>
                </div>
            </div>
        </div>
        <div id="mobile_product_<?php echo $post_id ?>" class="col-sm-12 archive-mobile-product-container"></div>
        <?php if ($product_number == 1): ?></div><?php
    endif;
    if ($product_number == 1):
        $product_number = 0;
    else:
        $product_number++;
    endif;
    return $product_number;
}

/*
 * Sort products for new archive page
 */

function st_sort_products_new_callback() {
    $refine_array = array();
    $refine_array['brand'] = $_POST['brand'];
    $refine_array['price'] = $_POST['price'];
    $refine_array['cat'] = $_POST['cat'];
    $refine_array['offset'] = 0;
    st_get_products($refine_array);
    wp_die();
}

function st_new_product_photo($post_id, $hi_res_image_path, $image) {
    $awspath = '';
    update_post_meta($post_id, '_low_rez_image_link', $image);
    update_post_meta($post_id, '_high_rez_image_link', $hi_res_image_path);
}

function st_import_product_callback() {
    if (!empty($_POST['custom_attr'])):
        $custom_attr = $_POST['custom_attr'];
        $colour = $custom_attr["st_colour"];
        $size = $custom_attr["st_size"];
    endif;
    $custom_price = (float) $_POST['manual_price_input'];
    $custom_price_option = (int) $_POST['price_markup_option'];
    $excluded_product = $_POST['excluded_product'];
    $manaul_markup = (int) $_POST['manaul_markup'];
    $category = $_POST['category'];
    $related_category = $_POST['related_category'];
    $category = explode('|', $category);
    $catsArray = array();
    foreach ($category as $cat):
        array_push($catsArray, (int) $cat);
    endforeach;
    $code = $_POST['code'];
    $name = $_POST['name'];
    $s_desc = $_POST['s_desc'];
    $l_desc = $_POST['l_desc'];
    $wholesale_price = $_POST['price'];
    $brand = $_POST['brand'];
    $cat = $_POST['cat'];
    $thumb = $_POST['thumb'];
    $image = $_POST['image'];
    $keywords = $_POST['keywords'];
    $barcode = $_POST['barcode'];
    $imageName = explode('/', $image);
    $imageName = $imageName[count($imageName) - 1];
    $hi_res_image_path = '/' . 'hi_res' . '/' . $imageName;
    if ($excluded_product):
        delete_excluded_product_from_database((int) $code);
    endif;
    if (!empty($manaul_markup)):
        $product_price = number_format(round($wholesale_price + (($wholesale_price / 100) * $manaul_markup), 0) - 0.01, 2);
    elseif (!empty($custom_price)):
        $product_price = $custom_price;
    else:
        $product_price = number_format(round($wholesale_price + (($wholesale_price / 100) * 20), 0) - 0.01, 2);
    endif;
    $post = array(
        'post_status' => "draft",
        'post_title' => $name,
        'post_type' => "product",
        'post_content' => $l_desc,
        'comment_status' => 'closed',
        'post_excerpt' => limit_text($s_desc, 10)
    );
    $post_id = wp_insert_post($post);
    if ($post_id):
        if (!empty($_POST['parent_product_id'])):
            $clean_array = array();
            array_push($clean_array, (int) $post_id);
            $parent_id = (int) $_POST['parent_product_id'];
            update_post_meta($parent_id, '_is_parent', 'true');
            update_post_meta($post_id, '_is_child', 'true');
            array_push($clean_array, $parent_id);
            $products = get_post_meta($parent_id, '_associated_products');
            foreach ($products as $product):
                if (is_array($product)):
                    foreach ($product as $product1):
                        if (!in_array((int) $product1, $clean_array)):
                            array_push($clean_array, (int) $product1);
                        endif;
                    endforeach;
                endif;
            endforeach;
            foreach ($clean_array as $product):
                delete_post_meta($product, '_associated_products');
                update_post_meta($product, '_associated_products', $clean_array);
            endforeach;
        endif;
        //wp_set_object_terms($post_id, 'variable', 'product_type');
        update_post_meta($post_id, '_sku', $barcode);
        update_post_meta($post_id, '_brand', $brand);
        update_post_meta($post_id, '_wholesale_price', $wholesale_price);
        update_post_meta($post_id, '_code', $code);
        update_post_meta($post_id, '_regular_price', $product_price);
        update_post_meta($post_id, '_price', $product_price);
        if (!empty($manaul_markup)):
            update_post_meta($post_id, '_price_markup', $manaul_markup);
        else:
            update_post_meta($post_id, '_price_markup', 20);
        endif;
        if (!empty($custom_price_option)):
            update_post_meta($post_id, '_select_price_save', $custom_price_option);
        else:
            update_post_meta($post_id, '_select_price_save', 0);
        endif;
        $attachment = array(
            'post_mime_type' => 'image/jpeg',
            'post_title' => sanitize_file_name($code),
            'post_content' => '',
            'post_status' => 'inherit'
        );
        $attach_id = wp_insert_attachment($attachment, false, $post_id);
        if (!empty($custom_attr)):
            update_post_meta($post_id, 'st_colour', $colour);
            update_post_meta($post_id, 'st_size', $size);
        endif;
        update_post_meta($attach_id, '_wp_attached_file', 'low-res/' . $imageName);
        update_post_meta($post_id, '_thumbnail_id', $attach_id);
        update_post_meta($post_id, '_update_automaticaly', true);
        update_post_meta($post_id, '_select_show_related_products_from_cat', $related_category);
        $imagePath = explode('.jpg', $imageName);
        $imagePath = $imagePath[0];
        $image_array = array();
        array_push($image_array, $imagePath . '.jpg');
        array_push($image_array, $imagePath . '-150x150.jpg');
        array_push($image_array, $imagePath . '-300x300.jpg');
        array_push($image_array, $imagePath . '-600x600.jpg');
        update_post_meta($post_id, '_image_high_res_link', Aws_Wrapper::aw_get_image_path($imageName, 'high-res'));
        update_post_meta($post_id, '_image_low_res_link', Aws_Wrapper::aw_get_image_path($imageName, 'low-res'));
        wp_set_object_terms($post_id, $catsArray, 'product_cat');
        Aws_Wrapper::aw_move_array_images($image_array, 'temp', 'low-res');
        echo $post_id;
    else:
        echo false;
    endif;
    wp_die();
}

/*
 * Get excluded products from the database
 */

function st_get_excluded_products() {
    global $wpdb;
    $exclude_products = array();
    $wpdb->flush();
    $result1 = $wpdb->query("SELECT `product_id` FROM `excluded_products`");
    $result = $wpdb->last_result;
    foreach ($result as $row):
        array_push($exclude_products, $row->product_id);
        unset($row);
    endforeach;
    return $exclude_products;
}

function delete_excluded_product_from_database($barcode) {
    global $wpdb;
    $wpdb->flush();
    $wpdb->query("DELETE FROM `excluded_products` WHERE `product_id` = " . $barcode);
}

/*
 * Insert excluded products to the database
 */

function st_put_excluded_product_callback() {
    global $wpdb;
    $wpdb->flush();
    $barcode = $_POST['code'];
    $result = $wpdb->query("INSERT INTO `admin_sextoys`.`excluded_products` (`index`, `product_id`) VALUES (NULL, '" . $barcode . "')");
    echo $result;
    wp_die();
}

function st_delete_attachment_from_aws($postid) {
    $code = get_post_meta($postid, '_code', true);
    Aws_Wrapper::aw_delete_object($code, 'low-res');
}

add_action('delete_attachment', 'st_delete_attachment_from_aws');

add_action('before_delete_post', 'st_delete_attachment_with_post');

function st_delete_attachment_with_post($postid) {

    global $post_type;
    if ($post_type != 'my_custom_post_type')
        return;

    $code = get_post_meta($postid, '_code', true);
    Aws_Wrapper::aw_delete_object($code, 'low-res');
}

function st_drill_down_array($array) {
    foreach ($array as $element):
        if (is_array($element)):
            return st_drill_down_array($element);
        else:
            return $element;
        endif;
    endforeach;
}

/*
 * -----------------------------------|
 * Get products children and siblings |
 * ---------------------------------- |
 */

function st_get_varible_products($post_id) {
    $clean_array = array();
    $parent_id = get_post_meta($post_id, '_parent', true);
    if ($parent_id):
        $parent_children = get_post_meta($parent_id, '_children');
    else:
        $parent_children = get_post_meta($post_id, '_children');
    endif;
    return $clean_array;
}

/*
 * -----------------------------------|
 * Get products for header search box |
 * -----------------------------------|
 */

function st_get_searched_products_callback() {
    $cat = '';
    if (isset($_POST['cat'])):
        $cat = $_POST['cat'];
    endif;
    $cleanArray = array();
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'post_status' => 'publish',
    );
    $results = new WP_Query($args);
    $results = $results->get_posts();
    foreach ($results as $result):
        if ($result->ID):
            $cleanArray[$result->ID] = array(
                'post_title' => $result->post_title,
                'guid' => $result->guid
            );
        endif;
    endforeach;
    wp_reset_postdata();
    $cleanArray = json_encode($cleanArray);
    echo $cleanArray;
    wp_die();
}

/*
 * ------------------------------------------|
 * Check to see if category has any children |
 * ------------------------------------------|
 */

function st_cat_has_children($cat_id) {
    return (get_categories(array('taxonomy' => 'product_cat', 'parent' => $cat_id))) ? true : false;
}

/*
 * ------------------------|
 * Get categories children |
 * ------------------------|
 */

function st_cat_get_children($cat_id) {
    $i = 99;
    $returnArray = array();
    $children = get_categories(array('taxonomy' => 'product_cat', 'parent' => $cat_id));
    if ($children):
        foreach ($children as $index => $child):
            $order = (int) st_get_category_meta($child->term_id, 'cat_order');
            if (!$order) {
                $order = $i;
                $i--;
            }
            $returnArray[$order] = $child;
        endforeach;
    endif;
    ksort($returnArray);
    return ($returnArray) ? $returnArray : false;
}

/*
 * --------------------------------|
 * Get categories top level parent |
 * --------------------------------|
 */

function st_cat_top_level_parent($cat_id) {
    $cat_obj = get_term($cat_id, 'product_cat');
    $parent_cat = '';
    if ($cat_obj->parent):
        while ($cat_obj->parent):
            if ($cat_obj->parent):
                $parent_cat = $cat_obj->parent;
            endif;
            $cat_obj = get_term($cat_obj->parent, 'product_cat');
        endwhile;
    else:
        $parent_cat = $cat_id;
    endif;
    return $parent_cat;
}

/*
 * -------------------------------------------------
 * Get background image for product page
 * -------------------------------------------------
 */

function st_get_product_background_image($cat_id) {
    return st_get_category_image(st_cat_top_level_parent($cat_id), 'full');
}

/*
 * -------------------------------------------------
 * Get category object
 * -------------------------------------------------
 */

function st_get_cat_object($cat_id) {
    return get_term($cat_id, 'product_cat');
}

/*
 * -------------------------------------------------
 * Get category link
 * -------------------------------------------------
 */

function st_get_category_guid($cat_id) {
    return get_term_link($cat_id, 'product_cat');
}

/*
 * -------------------------------------------------
 * Is category subcat of parent
 * -------------------------------------------------
 */

function st_is_category_parent_subcat($cat_object) {
    return ($cat_object->parent == st_cat_top_level_parent($cat_object->term_id)) ? true : false;
}

/*
 * -------------------------------------------------
 * Get a parent subcat object
 * -------------------------------------------------
 */

function st_get_parent_subcat($cat_object) {
    $parent_cat = st_cat_top_level_parent($cat_object->term_id);
    $main_cats = st_cat_get_children($parent_cat);
    $cat = '';
    while ($cat != $parent_cat):
        $cat_object = st_get_cat_object($cat_object->term_id);
        $cat = $cat_object->term_id;
        if (in_array($cat, $main_cats)):
            break;
        endif;
    endwhile;
}

function st_get_all_children($cat_id) {
    $clean_array = array();
    $sub_cat_children = st_cat_get_children($cat_id);
    foreach ($sub_cat_children as $child):
        array_push($clean_array, $child->term_id);
        foreach (st_cat_get_children($child->term_id) as $sub_cat):
            array_push($clean_array, $sub_cat->term_id);
            foreach (st_cat_get_children($sub_cat->term_id) as $sub_sub_cat):
                array_push($clean_array, $sub_sub_cat->term_id);
            endforeach;
        endforeach;
    endforeach;
    return $clean_array;
}

/*
 * View all the products from a category
 */

function st_view_all_products_from_cat($parent_cat) {
    $clean_array = array();
    $all_cats = get_all_categories();
    foreach ($all_cats as $cat):
        if ($cat->parent == $parent_cat && !in_array($cat, $clean_array)):
            array_push($clean_array, $cat);
        endif;
        foreach ($all_cats as $subcat):
            foreach ($clean_array as $parentCat):
                if ($subcat->parent == $parentCat->term_id && !in_array($subcat, $clean_array)):
                    array_push($clean_array, $subcat);
                endif;
            endforeach;
        endforeach;
    endforeach;
    return $clean_array;
}

/*
 * Get all children category ids from parent
 */

function st_get_all_subcats_ids($parent_cat) {
    $clean_array = array();
    $cats = st_view_all_products_from_cat($parent_cat);
    foreach ($cats as $cat):
        array_push($clean_array, $cat->term_id);
    endforeach;
    return $clean_array;
}

/*
 * Get total price for cart
 */

function st_get_cart_total() {
    global $woocommerce;
    return $woocommerce->cart->get_cart_total();
}

/*
 * Get total items for cart ajax
 */

function st_get_cart_total_callback() {
    echo number_of_items_in_cart();
    wp_die();
}

/*
 * Get total price for cart ajax
 */

function st_get_cart_total_price_callback() {
    echo st_get_cart_total();
    wp_die();
}

function st_get_all_products_ids() {
    $loop = new WP_Query(array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'fields' => 'ids',
    ));
    return $loop->posts;
}

function st_get_all_products_ids_if_same_as_parent($parent_id) {
    $loop = new WP_Query(array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'fields' => 'ids',
        'meta_query' => array(
            array(
                'key' => '_parent_id',
                'value' => $parent_id,
                'compare' => '=',
            ),
        ),
    ));
    return $loop->posts;
}

function st_get_associated_products($product_id) {
    $clean_array = array();
    $parent_id = 0;
    if (get_post_meta($product_id, '_is_parent', true)):
        $parent_id = (int) $product_id;
        array_push($clean_array, $parent_id);
    else:
        if (get_post_meta($product_id, '_is_child', true)):
            $parent_id = (int) get_post_meta($product_id, '_parent_id', true);
            array_push($clean_array, $parent_id);
        endif;
    endif;
    if ($parent_id):
        foreach (st_get_all_products_ids_if_same_as_parent($parent_id) as $product) :
            if ((int) get_post_meta($product, '_parent_id', true) === $parent_id):
                array_push($clean_array, $product);
            endif;
            unset($product);
        endforeach;
    endif;
    return $clean_array;
}

/*
 * Get a products custom attribute
 */

function st_get_product_attribute($_product_id, $attribute) {
    return get_post_meta($_product_id, 'st_' . $attribute, true);
}

/*
 * Override exsiting woocomerce function to display achive-page
 */

function woocommerce_content() {

    if (is_singular('product')) {

        while (have_posts()) : the_post();

            wc_get_template_part('content', 'single-product');

        endwhile;
    } else {
        woocommerce_get_template('archive-product.php');
    }
}

/*
 * Get image with html tags
 */

function st_get_products_gallery_image($attchment_id) {
    $image_url = get_post_meta($attchment_id, '_wp_attached_file', true);
    $image_string = "<img class='gallery-image' data-id='{$attchment_id}' src='" . st_get_aws_low_res_bucket_url() . $image_url . " alt='{$image_url}'/>";
    return $image_string;
}

/*
 * Get products gallery images ids
 */

function st_get_products_gallery_ids($product_id) {
    $id_string = get_post_meta($product_id, '_product_image_gallery', true);
    $id_array = explode(',', $id_string);
    return $id_array;
}

/*
 * Get the url for aws bucket
 */

function st_get_aws_low_res_bucket_url() {
    return "https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/";
}

/*
 * Get products youtube link
 */

function st_get_product_youtube_link($product_id) {
    return get_post_meta($product_id, '_youtube_video', true);
}

/*
 * Get a users display name
 */

function st_get_user_display_name() {
    global $current_user;
    $nice_name = '';
    if ($current_user):
        $current_user = $current_user->data;
        $nice_name = $current_user->display_name;
    endif;
    return $nice_name;
}

/*
 * Remove fields from woocomerce checkout page
 */

add_filter('woocommerce_checkout_fields', 'custom_override_checkout_fields');

function custom_override_checkout_fields($fields) {
    // Remove company field
    unset($fields['billing']['billing_company']);
    unset($fields['shipping']['shipping_company']);

    // Rename fields

    $fields['billing']['billing_phone']['label'] = 'Mobile phone number';
    $fields['billing']['billing_address_1']['placeholder'] = 'PO Box number, Apartment, suite, unit etc.';
    $fields['billing']['billing_address_2']['placeholder'] = 'Street Name';
    $fields['shipping']['shipping_address_1']['placeholder'] = 'PO Box number, Apartment, suite, unit etc.';
    $fields['shipping']['shipping_address_2']['placeholder'] = 'Street Name';

    $fields['billing']['billing_title'] = array(
        'label' => __('Title', 'woocommerce'),
        'placeholder' => _x('', 'placeholder', 'woocommerce'),
        'required' => false,
        'clear' => false,
        'type' => 'select',
        'class' => array('col-lg-12'),
        'options' => array(
            'Mr' => __('Mr', 'woocommerce'),
            'Mrs' => __('Mrs', 'woocommerce'),
            'Ms' => __('Ms', 'woocommerce'),
            'Miss' => __('Miss', 'woocommerce'),
            'Sir' => __('Sir', 'woocommerce'),
            'Dr' => __('Dr', 'woocommerce')
        )
    );

    $fields['shipping']['shipping_title'] = array(
        'label' => __('Title', 'woocommerce'),
        'placeholder' => _x('', 'placeholder', 'woocommerce'),
        'required' => false,
        'clear' => false,
        'type' => 'select',
        'options' => array(
            'Mr' => __('Mr', 'woocommerce'),
            'Mrs' => __('Mrs', 'woocommerce'),
            'Ms' => __('Ms', 'woocommerce'),
            'Miss' => __('Miss', 'woocommerce'),
            'Sir' => __('Sir', 'woocommerce'),
            'Dr' => __('Dr', 'woocommerce')
        )
    );

    $fields['billing'] = st_array_last_to_first($fields['billing']);
    $fields['shipping'] = st_array_last_to_first($fields['shipping']);

    return $fields;
}

function st_array_last_to_first($array) {
    $lastvalue = end($array);
    $lastkey = key($array);

    $arr1 = array($lastkey => $lastvalue);

    array_pop($array);

    return array_merge($arr1, $array);
}

add_action('woocommerce_checkout_update_order_meta', 'st_customise_checkout_field_update_order_meta');

function st_customise_checkout_field_update_order_meta($order_id) {
    if (!empty($_POST['billing_title'])) {
        update_post_meta($order_id, 'billing_title', sanitize_text_field($_POST['billing_title']));
    }
    if (!empty($_POST['shipping_title'])) {
        update_post_meta($order_id, 'shipping_title', sanitize_text_field($_POST['shipping_title']));
    }
}

/*
 * Get subscribed users
 */

function st_get_subcribed_users() {
    global $wpdb;
    $clean_array = array();
    $ids = $wpdb->get_results("SELECT customer_id FROM email_subscribers");
    foreach ($ids as $id) {
        array_push($clean_array, (int) $id->customer_id);
    }
    return $clean_array;
}

/*
 * Check if subscribed user
 */

function st_is_subscribed_user($customer_id) {
    return get_user_meta($customer_id, 'subscribed', true);
}

/*
 * Ajax function for my-account subscrible toggle switch
 */

function st_change_subscribe_status_callback() {
    echo update_user_meta((int) strip_tags($_POST['customer_id']), 'subscribed', strip_tags($_POST['status']));
    wp_die();
}

/*
 * Get Address
 */

function st_get_address($userID, $addressName) {
    $addressArray = array();
    $addressArray["address1"] = get_user_meta($userID, $addressName . "_address_1", true);
    $addressArray["address2"] = get_user_meta($userID, $addressName . "_address_2", true);
    $addressArray["city"] = get_user_meta($userID, $addressName . "_city", true);
    $addressArray["firstName"] = get_user_meta($userID, $addressName . "_first_name", true);
    $addressArray["lastName"] = get_user_meta($userID, $addressName . "_last_name", true);
    $addressArray["postcode"] = get_user_meta($userID, $addressName . "_postcode", true);
    $addressArray["state"] = get_user_meta($userID, $addressName . "_state", true);
    return $addressArray;
}

/*
 * Get address from address book callback
 */

function st_get_address_callback() {
    $userID = strip_tags($_POST['user_id']);
    $address_name = strip_tags($_POST['address_name']);
    echo json_encode(st_get_address($userID, $address_name));
    wp_die();
}

/*
 * Send contact email ajax function
 */

function st_get_email_class() {
    require_once 'emails/email_templates.php';
    return new email_templates();
}

function st_send_contact_email_callback() {
    $data = array(
        'first_name' => strip_tags($_POST['firstName']),
        'surname' => strip_tags($_POST['surname']),
        'email' => strip_tags($_POST['contactEmail']),
        'number' => strip_tags($_POST['contactNumber']),
        'problem' => (int) strip_tags($_POST['contactProblemSelect']),
        'message' => strip_tags($_POST['details'])
    );
    require_once 'emails/email_templates.php';
    $emails = new email_templates();
    $emails->send_internal_mail_content($data);
    echo $emails->send_no_reply_email($data['email']);
    wp_die();
}

function st_is_third_tier_cat($cat_id, $parent_id) {
    $parent_object = st_get_cat_object($cat_id->parent);
    return is_wp_error(st_get_cat_object($parent_object->parent)) ? false : true;
}

function st_convert_memory($size) {
    $unit = array('b', 'kb', 'mb', 'gb', 'tb', 'pb');
    return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
}

function st_delivery_information() {
    ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-delivery-information-new no-padding">
        <div class="col-lg-12 col-md-12 col-sm-8 col-xs-8 col-md-offset-0 col-sm-offset-4 col-xs-offset-2 product-delivery-information-new-wrapper">
            <div class="product_shipping-example hidden">
                <img src="https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/ww_shipping_example_image.jpg"/>
                <img class="shipping-down-arrow" src="<?php echo get_template_directory_uri() ?>/images/hover-down-triangle.png" alt=""/>
            </div>
            <i class="fa fa-truck" aria-hidden="true"></i>
            <p>Discreet delivery Australia Wide</p>
            <i class="fa fa-caret-down show-dropdown-info" aria-hidden="true"></i>
            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 drop-down-text">
                <p class="hidden">All products are <a class="shipping-packaging-link">discreetly packaged</a> with no direct mention of the nature of the products inside. We deliver within approximately 3-10 business days. We ship only within Australia. </p>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-8 col-xs-8 col-md-offset-0 col-sm-offset-4 col-xs-offset-2 product-delivery-information-new-wrapper">
            <i class="fa fa-lock" aria-hidden="true"></i>
            <p>100% Secure Payment</p>
            <i class="fa fa-caret-down show-dropdown-info" aria-hidden="true"></i>
            <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 drop-down-text">
                <p class="hidden">The Me & Mrs. Jones website uses 128-bit encryption to protect all personal information you may submit online. To safeguard your security, Me & Mrs. Jones does not store any credit card details. Me & Mrs. Jones will never disclose, sell or transfer any personal information provided by our clients to a third party, except as might be required by law.</p>
            </div>
        </div>
    </div>
    <?php
}

function st_is_size_chart_for_brand($brand) {
    $brand = strtolower(str_replace(' ', '', $brand));
    return (file_exists(get_template_directory() . '/partials/sizeCharts/' . $brand . '.min.html')) ? true : false;
}

function st_get_size_chart_for_brand($brand) {
    $brand = strtolower(str_replace(' ', '', $brand));
    include get_template_directory() . '/partials/sizeCharts/' . $brand . '.min.html';
}

/*
 * --------------------------
 * Get all products for pages
 * --------------------------
 */

function st_get_related_cats_for_template_page($slug, $parent = null) {
    $cats = get_all_categories();
    $catArray = array();
    foreach ($cats as $cat):
        $cat_parent = get_term($cat->parent);
        if (isset($cat_parent->name)):
            $cat_parent = strtolower($cat_parent->name);
            $patterns = array();
            $patterns[0] = '/&amp;/';
            $patterns[1] = '/&/';
            $patterns[2] = '/-/';
            $patterns[3] = '/,/';
            $patterns[4] = '/ /';
            $catName = strtolower(preg_replace($patterns, '', $cat->name));
            $slugName = strtolower(preg_replace($patterns, '', $slug));
            unset($patterns);
            if ($slugName == $catName):
                if ($parent && $slug == 'accessories'):
                    if ($cat_parent == $parent):
                        array_push($catArray, $cat->term_id);
                        $subCats = st_cat_get_children($cat->term_id);
                        foreach ($subCats as $subCat):
                            array_push($catArray, $subCat->term_id);
                        endforeach;
                    endif;
                else:
                    array_push($catArray, $cat->term_id);
                    if (empty($parent)):
                        $subCats = st_cat_get_children($cat->term_id);
                        foreach ($subCats as $subCat):
                            array_push($catArray, $subCat->term_id);
                        endforeach;
                    endif;
                endif;
            endif;
        endif;
    endforeach;
    return $catArray;
}

function st_get_all_products_for_template($slug, $offsets, $gender = null, $parent = null, $excluded_products = null) {
    $catArray = st_get_related_cats_for_template_page($slug, $parent);
    if ($gender):
        $catArray = st_filter_for_gender($gender, $catArray);
    endif;
    $cleanArray = array();
    $cleanArray[0] = array();
    $args = array(
        'posts_per_page' => (int) $offsets,
        'post_status' => 'publish',
        'post_type' => 'product',
        'fields' => 'ids',
        'offset' => 0,
        'orderby' => 'rand',
        'order' => 'ASC',
        'meta_query' => array(
            'relation' => 'OR',
            array(
                'key' => '_is_parent',
                'value' => 1,
                'compare' => 'LIKE',
            ),
            array(
                'key' => '_is_child',
                'compare' => 'NOT EXISTS',
            )
        ),
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'id',
                'terms' => $catArray
            ),
        )
    );
    if (isset($excluded_products) && !empty($excluded_products)):
        $args['post__not_in'] = $excluded_products;
        $cleanArray[0] = $excluded_products;
    endif;
    $products = new WP_Query($args);
    $products = $products->posts;
    unset($args);
    if (isset($excluded_products) && !empty($excluded_products)):
        for ($i = 0; $i < 15; $i++):
            if (!isset($cleanArray[0][$i]) && empty($cleanArray[0][$i])):
                $first_value = reset($products);
                $first_key = key($products);
                $cleanArray[0][$i] = $first_value;
                unset($products[$first_key]);
            endif;
        endfor;
        ksort($cleanArray[0]);
    endif;
    unset($excluded_products);
    foreach ($products as $id):
        array_push($cleanArray[0], $id);
        unset($id);
    endforeach;
    $cleanArray[1] = $catArray;
    return $cleanArray;
}

function st_filter_for_gender($gender, $cats) {
    $cleanArray = array();
    $gender = st_return_gender_cat_id($gender);
    foreach ($cats as $index => $cat):
        if ($gender == st_cat_top_level_parent($cat)):
            array_push($cleanArray, $cat);
        endif;
    endforeach;
    return $cleanArray;
}

/*
 * -----------------------------------------|
 * Load more products for the products page |
 * -----------------------------------------|
 */

function st_get_more_products_callback() {
    $offset = (int) $_POST['offset'];
    $numberOfProducts = (int) $_POST['numberOfProducts'];
    if ($_POST['is_search'] == 'false'):
        $cats = st_get_related_cats_for_template_page(htmlentities($_POST['cats']), htmlentities($_POST['parent']));
    else:
        $cats = explode(',', htmlentities($_POST['cats']));
    endif;
    if (!empty($_POST['gender'])):
        $cats = st_filter_for_gender(htmlentities($_POST['gender']), $cats);
    endif;
    if (!empty($_POST['refined']) && (boolean) $_POST['refined']):
        $sort_by = htmlentities($_POST['selected']);
        $prices = explode(',', htmlentities($_POST['prices']));
        $brands = explode(',', htmlentities($_POST['brands']));
        if (in_array('all', $brands)):
            $brands = array();
        else:
            foreach ($brands as $index => $brand):
                $brands[$index] = trim(ucwords(str_replace('&amp;', '&', $brand)));
            endforeach;
        endif;
        $sizes = explode(',', strtolower(htmlentities($_POST['sizes'])));
        if (in_array('all', $sizes)):
            $sizes = array();
        else:
            $sizes = st_get_custom_sizes($sizes, $cats, htmlentities($_POST['slug']));
        endif;
        $colours = explode(',', htmlentities($_POST['colours']));
        if (strlen($_POST['tags']) > 0):
            $tags = explode(',', htmlentities($_POST['tags']));
        endif;
        $colours = (in_array('all', $colours)) ? array() : st_get_colour_meta_for_sort($cats, $colours);
        $percificsArray = array('relation' => 'AND');
        if (count($brands) > 0):
            array_push($percificsArray, array(
                'key' => '_brand',
                'value' => $brands,
                'compare' => 'IN'
            ));
        endif;
        if (count($colours) > 0):
            array_push($percificsArray, array(
                'key' => 'st_colour',
                'value' => $colours,
                'compare' => 'IN',
            ));
        endif;
        if (count($sizes) > 0):
            array_push($percificsArray, array(
                'key' => 'st_size',
                'value' => $sizes,
                'compare' => 'IN',
            ));
        endif;
        $meta_query = array();
        if (count($percificsArray) > 1):
            array_push($meta_query, $percificsArray);
        endif;
        array_push($meta_query, st_return_price_meta_array($prices));
        switch ((int) $_POST['selected']):
            case 0:
                $order = 'DESC';
                $orderby = 'meta_value_num';
                $meta_key = '_price';
                break;
            case 1:
                $order = 'ASC';
                $orderby = 'meta_value_num';
                $meta_key = '_price';
                break;
            case 2:
                $order = 'DESC';
                $orderby = 'title';
                break;
            case 3:
                $order = 'ASC';
                $orderby = 'title';
                break;
        endswitch;
    else:
        $meta_query = array(
            'relation' => 'OR',
            array(
                'key' => '_is_parent',
                'value' => 1,
                'compare' => 'LIKE',
            ),
            array(
                'key' => '_is_child',
                'compare' => 'NOT EXISTS',
            )
        );
        $orderby = 'rand';
        $order = 'ASC';
    endif;
    $cleanArray = array();
    $taxQuery = array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'id',
            'terms' => $cats
        )
    );
    if (!empty($tags)):
        $tagQuery = array(
            'taxonomy' => 'product_tag',
            'field' => 'name',
            'terms' => $tags
        );
        array_push($taxQuery, $tagQuery);
        $taxQuery['relation'] = 'AND';
    endif;
    while (count($cleanArray) <= $numberOfProducts):
        $args = array(
            'posts_per_page' => $numberOfProducts,
            'post_status' => 'publish',
            'post_type' => 'product',
            'fields' => 'ids',
            'offset' => $offset,
            'orderby' => $orderby,
            'order' => $order,
            'meta_key' => $meta_key,
            'meta_query' => $meta_query,
            'tax_query' => $taxQuery
        );
        if ($_POST['is_search'] == 'true'):
            $args['post__in'] = explode(',', htmlentities($_POST['archive_loaded_products']));
        endif;
        if (isset($_POST['featured_products']) && !empty($_POST['featured_products'])):
            $args['post__not_in'] = explode(',', htmlentities($_POST['featured_products']));
        endif;
        //wp_mail('matt@chillidee.com.au', 'Query Json', json_encode($args));
        $products = new WP_Query($args);
        if ($products && count($products->posts) > 0):
            foreach ($products->posts as $index => $id):
                if (get_post_meta($id, '_is_child', true) && st_not_in_array((int) $id, $cleanArray)):
                    array_push($cleanArray, (int) get_post_meta($id, '_parent_id', true));
                else:
                    if (st_not_in_array((int) $id, $cleanArray)):
                        array_push($cleanArray, (int) $id);
                    endif;
                endif;
            endforeach;
            $cleanArray = array_unique($cleanArray);
        else:
            break;
        endif;
        $offset = $numberOfProducts + $offset;
    endwhile;
    if (count($cleanArray) > 0):
        foreach ($cleanArray as $id):
            st_archive_product_container_ajax($id, $offset);
            unset($id);
        endforeach;
        $returnArray = json_encode($returnArray);
    else:
        echo false;
    endif;
    wp_die();
}

function st_return_gender_cat_id($gender) {
    if (isset($gender)):
        switch ($gender):
            case 'her':
                $gender = 9;
                break;
            case 'him':
                $gender = 10;
                break;
            case 'us':
                $gender = 24;
                break;
        endswitch;
    endif;
    return $gender;
}

function st_get_custom_sizes($sizes, $cats, $slug) {
    foreach ($sizes as $index => $size):
        $sizes[$index] = ucwords(str_replace('\\', '', $size));
    endforeach;
    if (st_is_vibrator_cat_id($cats)):
        $sizes = st_get_vibrator_size_meta_for_sort($cats, $sizes);
    endif;
    if (st_is_costume_cat_id($cats, $slug)):
        $sizes = st_get_costume_size_meta_for_sort($cats, $sizes);
    endif;
    if (st_is_condom_cat_id($cats)):
        $sizes = st_get_condom_size_meta_for_sort($cats, $sizes);
    endif;
    if (st_is_lube_cat_id($cats)):
        $sizes = st_get_lube_size_meta_for_sort($cats, $sizes);
    endif;
    if (st_is_apparel_cat_id($cats, $slug)):
        $sizes = st_get_apparel_size_meta_for_sort($cats, $sizes);
    endif;
    if (st_is_mens_range($cats, $slug)):
        $sizes = st_get_mens_range_size_meta_for_sort($cats, $sizes);
    endif;
    if (st_is_fetishwear_cat_id($cats, $slug)):
        $sizes = st_get_apparel_size_meta_for_sort($cats, $sizes);
    endif;
    return $sizes;
}

function st_is_vibrator_cat_id($cats) {
    $result = false;
    $vibratorArray = array(19, 25, 130, 35, 132, 36, 134, 34, 137, 37, 38, 100, 105, 106, 107, 108, 111, 104, 126, 130, 131, 132, 136, 208, 138, 111);
    foreach ($cats as $cat):
        if (in_array($cat, $vibratorArray)):
            $result = true;
        endif;
    endforeach;
    return $result;
}

function st_is_condom_cat_id($cats) {
    $result = false;
    $cleanArray = array(173, 175, 153);
    foreach ($cats as $cat):
        if (in_array($cat, $cleanArray)):
            $result = true;
        endif;
    endforeach;
    return $result;
}

function st_is_costume_cat_id($cats, $slug) {
    $result = false;
    $custumeArray = array(67, 119);
    foreach ($cats as $cat):
        if (in_array($cat, $custumeArray) && $slug != 'apparel'):
            $result = true;
        endif;
    endforeach;
    return $result;
}

function st_is_fetishwear_cat_id($cats, $slug) {
    $result = false;
    $custumeArray = array(321, 20, 39, 40, 46, 41, 45, 42, 43, 44, 41, 101, 112, 113, 117, 116, 114, 115, 321, 127, 141, 143, 140, 148, 145, 146, 147);
    $slugArray = array('fetish', 'fetish-wear');
    foreach ($cats as $cat):
        if (in_array($cat, $custumeArray) && in_array($slug, $slugArray)):
            $result = true;
        endif;
    endforeach;
    return $result;
}

function st_is_apparel_cat_id($cats, $slug) {
    $result = false;
    $custumeArray = array(70, 120, 66, 172, 63, 22, 102);
    $slugArray = array('apparel', 'accessories', 'lingerie', 'bridal', 'club-wear');
    foreach ($cats as $cat):
        if (in_array($cat, $custumeArray) && in_array($slug, $slugArray)):
            $result = true;
        endif;
    endforeach;
    return $result;
}

function st_is_lube_cat_id($cats) {
    $result = false;
    $custumeArray = array(26, 25, 155);
    $slugArray = array('lubes-gels');
    foreach ($cats as $cat):
        if (in_array($cat, $custumeArray) && in_array($slug, $slugArray)):
            $result = true;
        endif;
    endforeach;
    return $result;
}

function st_is_mens_range($cats, $slug) {
    $result = false;
    $custumeArray = array(118);
    foreach ($cats as $cat):
        if (in_array($cat, $custumeArray) && $slug != 'apparel'):
            $result = true;
        endif;
    endforeach;
    return $result;
}

function st_get_all_size_meta_for_sort($post_cat_id_array) {
    $cleanArray = array();
    foreach (st_get_product_meta_from_cat_array($post_cat_id_array, 'st_size') as $index => $size):
        $cleanArray[$index] = (str_replace('\\', '', $size));
    endforeach;
    return $cleanArray;
}

function st_get_vibrator_size_meta_for_sort($post_cat_id_array, $sizes) {
    $cleanArray = array();
    foreach (st_get_product_meta_from_cat_array($post_cat_id_array, 'st_size') as $size):
        if (in_array('all', $sizes)):
            array_push($cleanArray, $size);
        else:
            foreach ($sizes as $_size):
                switch ($_size):
                    case '1':
                        if ((int) $size >= 1 && (int) $size <= 5 || $size === 'small' || $size === 'no size'):
                            array_push($cleanArray, $size);
                        endif;
                        break;
                    case '2':
                        if ((int) $size >= 6 && (int) $size <= 9 || $size === 'medium' || $size === 'no size'):
                            array_push($cleanArray, $size);
                        endif;
                        break;
                    case '3':
                        if ((int) $size >= 10 || $size === 'large' || $size === 'no size' || $size === 'xl'):
                            array_push($cleanArray, $size);
                        endif;
                        break;
                    case '4':
                        if ((int) $size == 0 || strpos(strtolower($size), 'set') !== false || strpos(strtolower($size), 'kit') !== false):
                            array_push($cleanArray, $size);
                        endif;
                        break;
                endswitch;
            endforeach;
        endif;
    endforeach;
    return $cleanArray;
}

function st_get_condom_size_meta_for_sort($post_cat_id_array, $sizes) {
    $cleanArray = array();
    foreach (st_get_product_meta_from_cat_array($post_cat_id_array, 'st_size') as $size):
        if (in_array('all', $sizes)):
            array_push($cleanArray, $size);
        else:
            foreach ($sizes as $_size):
                switch ($_size):
                    case '1':
                        if ((int) $size >= 1 && (int) $size <= 5 || $size === 'small' || $size === 'no size'):
                            array_push($cleanArray, $size);
                        endif;
                        break;
                    case '2':
                        if ((int) $size >= 6 && (int) $size <= 9 || $size === 'medium' || $size === 'no size'):
                            array_push($cleanArray, $size);
                        endif;
                        break;
                    case '3':
                        if ((int) $size >= 10 || $size === 'large' || $size === 'no size' || $size === 'xl'):
                            array_push($cleanArray, $size);
                        endif;
                        break;
                endswitch;
            endforeach;
        endif;
    endforeach;
    return $cleanArray;
}

function st_get_apparel_size_meta_for_sort($post_cat_id_array, $sizes) {
    $xsmallArray = array('x/s', 'xs', 'xs/s');
    $smallArray = array('small', 's/m', 's');
    $mediumArray = array('medium', 's/m', 'm');
    $largeArray = array('large', 'l/xl', 'l');
    $xlargeArray = array('x large', 'l/xl', 'xl');
    $plusArray = array('xxl', '1x', '2xl', '3xl', '1x', '2x', '3x', '4x', 'queen');
    $osArray = array('o/s', 'one size', '');
    $cleanArray = array();
    foreach (st_get_product_meta_from_cat_array($post_cat_id_array, 'st_size') as $size):
        foreach ($sizes as $_size):
            switch ($_size):
                case '1':
                    if (in_array(strtolower($size), $xsmallArray)):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '2':
                    if (in_array(strtolower($size), $smallArray)):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '3':
                    if (in_array(strtolower($size), $mediumArray)):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '4':
                    if (in_array(strtolower($size), $largeArray)):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '5':
                    if (in_array(strtolower($size), $xlargeArray)):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '6':
                    if (in_array(strtolower($size), $plusArray)):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '7':
                    if (in_array(strtolower($size), $osArray)):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '8':
                    if ((int) $size >= 32 && (int) $size <= 36):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '9':
                    if ((int) $size >= 38 && (int) $size <= 44):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '10':
                    if ((int) $size > 5 && (int) $size <= 16):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '11':
                    if (strpos($size, 'kit') != false || strpos($size, 'set') != false):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '12':
                    array_push($cleanArray, $size);
                    break;
            endswitch;
        endforeach;
    endforeach;
    if (count($cleanArray) == 0):
        $cleanArray = $sizes;
    endif;
    return $cleanArray;
}

function st_get_mens_range_size_meta_for_sort($post_cat_id_array, $sizes) {
    $xsmallArray = array('x/s', 'xs', 'xs/s');
    $smallArray = array('small', 's/m', 's');
    $mediumArray = array('medium', 's/m', 'm');
    $largeArray = array('large', 'l/xl', 'l');
    $xlargeArray = array('x large', 'l/xl', 'xl');
    $plusArray = array('xxl', '1x', '2xl', '3xl', '1x', '2x', '3x', '4x', 'queen', 'xx/l', 'xxl/xxxl');
    $osArray = array('o/s', 'one size', '');
    $cleanArray = array();
    foreach (st_get_product_meta_from_cat_array($post_cat_id_array, 'st_size') as $size):
        foreach ($sizes as $_size):
            switch ($_size):
                case '1':
                    if (in_array(strtolower($size), $xsmallArray)):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '2':
                    if (in_array(strtolower($size), $smallArray)):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '3':
                    if (in_array(strtolower($size), $mediumArray)):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '4':
                    if (in_array(strtolower($size), $largeArray)):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '5':
                    if (in_array(strtolower($size), $xlargeArray)):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '6':
                    if (in_array(strtolower($size), $plusArray)):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '7':
                    if (in_array(strtolower($size), $osArray)):
                        array_push($cleanArray, $size);
                    endif;
                    break;
            endswitch;
        endforeach;
    endforeach;
    if (count($cleanArray) == 0):
        $cleanArray = $sizes;
    endif;
    return $cleanArray;
}

function st_get_costume_size_meta_for_sort($post_cat_id_array, $sizes) {
    $cleanArray = array();
    foreach (st_get_product_meta_from_cat_array($post_cat_id_array, 'st_size') as $size):
        foreach ($sizes as $_size):
            switch ((int) $_size):
                case '1':
                    if ((int) $size <= 9 || (int) $size == ''):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '2':
                    if ((int) $size >= 10):
                        array_push($cleanArray, $size);
                    endif;
                    break;
            endswitch;
        endforeach;
    endforeach;
    return $cleanArray;
}

function st_get_lube_size_meta_for_sort($post_cat_id_array, $sizes) {
    $cleanArray = array();
    foreach (st_get_product_meta_from_cat_array($post_cat_id_array, 'st_size') as $size):
        foreach ($sizes as $_size):
            switch ((int) $_size):
                case '1':
                    if ((int) $size < 100 && (int) $size != 0):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '2':
                    if ((int) $size >= 100 && $size <= 200):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '3':
                    if ((int) $size > 200):
                        array_push($cleanArray, $size);
                    endif;
                    break;
                case '4':
                    if ((int) $size == 0 || $size == ''):
                        array_push($cleanArray, $size);
                    endif;
                    break;
            endswitch;
        endforeach;
    endforeach;
    return $cleanArray;
}

function st_get_colour_meta_for_sort($post_cat_id_array, $colourArray) {
    $cleanArray = array();
    $colorArray = st_get_product_meta_from_cat_array($post_cat_id_array, 'st_colour');
    foreach ($colorArray as $colour):
        foreach ($colourArray as $_colour):
            if (strpos($_colour, $colour) !== false):
                array_push($cleanArray, $colour);
            endif;
        endforeach;
    endforeach;
    if (count($cleanArray) == 0):
        $cleanArray = $colourArray;
    endif;
    return $cleanArray;
}

function st_return_price_meta_array($prices) {
    $priceArray = array('relation' => 'OR');
    if (in_array('49.99', $prices)):
        $priceMeta = array(
            'key' => '_price',
            'value' => '49.99',
            'compare' => '<',
            'type' => 'NUMERIC'
        );
        array_push($priceArray, $priceMeta);
    endif;
    if (in_array('99.99', $prices)):
        $priceMeta = array(
            'key' => '_price',
            'value' => array('50.00', '99.99'),
            'compare' => 'BETWEEN',
            'type' => 'NUMERIC'
        );
        array_push($priceArray, $priceMeta);
    endif;
    if (in_array('100.00', $prices)):
        $priceMeta = array(
            'key' => '_price',
            'value' => '100.00',
            'compare' => '>',
            'type' => 'NUMERIC'
        );
        array_push($priceArray, $priceMeta);
    endif;
    return $priceArray;
}

function st_get_size_array($slug, $parent_slug) {
    $returnArray = false;
    switch ($slug):
        case 'vibrators':
        case 'dongs-dildos-strap-ons':
        case 'bullets-balls-eggs':
        case 'anal':
        case 'toys':
        case 'sleeves-extensions':
            $returnArray = array(1 => 'Up to 6 inches', 2 => '6 inches to 10 inches', 3 => '10 inches and up');
            break;
        case 'costumes':
        case 'club-wear':
        case 'bridal':
        case 'lingerie':
        case 'apparel':
        case 'fetish-wear':
        case 'fetish':
            $returnArray = array(1 => 'XS', 2 => 'S', 3 => 'M', 4 => 'L', 5 => 'XL', 6 => 'Plus', 7 => 'One Size');
            break;
        case 'love-dolls':
        case 'swings-slings':
        case 'sex-furniture':
        case 'accessories':
        case 'nipple-stimulators':
        case 'fifty-shades-of-grey-inspired':
        case 'whips-paddles-ticklers':
        case 'cuffs-restraints-gags':
        case 'blindfolds-hoods':
        case 'cock-rings':
        case 'bullets-balls-eggs':
        case 'pumps':
        case 'essentials':
        case 'gifts-games':
        case 'sex-games':
        case 'naughty-novelties':
        case 'couples-kits':
        case 'bucks-night-products':
        case 'hens-night-products':
            $returnArray = true;
            break;
        case 'condoms':
            $returnArray = array(1 => 'up to 10 pack', 2 => '10 pack and up');
            break;
        case 'lubes-gels':
            $returnArray = array(1 => 'up to 100ml', 2 => '100ml to 200ml', 3 => '200ml and up', 4 => 'Other');
            break;
        case 'mens-range':
            $returnArray = array(1 => 'XS', 2 => 'S', 3 => 'M', 4 => 'L', 5 => 'XL', 6 => 'XXL+', 7 => 'One Size');
            break;
    endswitch;
    if ($slug == 'sleeves-extensions'):
        $returnArray[4] = 'Sets';
    endif;
    if ($slug == 'accessories'):
        if ($parent_slug == 'apparel'):
            $returnArray = array(1 => 'XS', 2 => 'S', 3 => 'M', 4 => 'L', 5 => 'XL', 6 => 'Plus', 7 => 'One Size');
        endif;
    endif;
    if ($slug == 'lingerie' || $slug == 'apparel'):
        $returnArray[8] = '32-36';
        $returnArray[9] = '38-44';
    endif;
    if ($slug == 'apparel'):
        $returnArray[10] = 'shoes';
    endif;
    if ($slug == 'fetish-wear' || $slug == 'fetish'):
        $returnArray[11] = 'set';
        $returnArray[12] = 'other';
    endif;
    return $returnArray;
}

function st_get_colour_array($slug, $parent_slug) {
    $returnArray = true;
    $noShowArraySlug = array(
        'essentials', 'gifts-games', 'sex-games', 'naughty-novelties', 'couples-kits', 'bucks-night-products', 'hens-night-products'
    );
    if ($parent_slug == 'essentials' || in_array($slug, $noShowArraySlug)):
        $returnArray = false;
    endif;
//    switch ($slug):
//        case 'sleeves-extensions':
//            $returnArray = array(1 => 'Up to 6 inches', 2 => '6 inches to 10 inches', 3 => '10 inches and up');
//            break;
//    endswitch;
    return $returnArray;
}

function st_get_random_image_background($default) {
    $backgroundURL = '';
    if (!$default):
        $ramdomNumber = rand(1, 10);
    endif;
    switch ($ramdomNumber):
        case 1:
            $backgroundURL = 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/for-her-background.jpg';
            break;
        case 2:
            $backgroundURL = 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-background-for-him.jpg';
            break;
        case 3:
            $backgroundURL = 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-background-us.jpg';
            break;
        case 4:
            $backgroundURL = 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/for-her-product-background.jpg';
            break;
        case 5:
            $backgroundURL = 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/products-background-for-him.jpg';
            break;
        case 6:
            $backgroundURL = 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/for-us-product-background.jpg';
            break;
        case 7:
            $backgroundURL = 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/products/Backgroun-MMJ-Guide.jpg';
            break;
        case 8:
            $backgroundURL = 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/Returns_Exchange_BG.jpg';
            break;
        case 9:
            $backgroundURL = 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/account-background.jpg';
            break;
        case 99:
            $backgroundURL = 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-background.jpg';
            break;
        default :
            $backgroundURL = 'https://s3-ap-southeast-2.amazonaws.com/meandmrsjones/images/site_images/home-background.jpg';
            break;
    endswitch;
    return $backgroundURL;
}

/*
 * -----------------------------|
 * Add wholesale price to order |
 * -----------------------------|
 */

add_action('woocommerce_add_order_item_meta', 'st_add_wholesale_price_to_order', 10, 3);

function st_add_wholesale_price_to_order($item_id, $values, $cart_item_key) {
    $wholesale_price = get_post_meta($values['product_id'], '_wholesale_price', true);
    $size = get_post_meta($values['product_id'], 'st_size', true);
    $colour = get_post_meta($values['product_id'], 'st_colour', true);
    wc_add_order_item_meta($item_id, '_wholesale', $wholesale_price);
    if ($size):
        wc_add_order_item_meta($item_id, 'Size', $size);
    endif;
    if ($colour):
        wc_add_order_item_meta($item_id, 'Colour', $colour);
    endif;
}

/*
 * -------------------------|
 * Add email after checkout |
 * -------------------------|
 */

add_action('woocommerce_checkout_update_user_meta', 'st_checkout_email_signup', 10, 2);

function st_checkout_email_signup($customer_id, $posted) {
    if (isset($_POST['marketing'])):
        global $wpdb;
        $email = htmlentities($_POST['billing_email']);
        $phone = htmlentities($_POST['billing_phone']);
        $customer_id = ($customer_id) ? $customer_id : 0;
        $inDB = $wpdb->query("SELECT * FROM  `email_subscribers` WHERE `email` LIKE  '" . $email . "'");
        if (!$inDB):
            echo $wpdb->query("INSERT INTO `" . DB_NAME . "`.`email_subscribers` (`id`,`customer_id`, `email`,`phone`) VALUES (NULL,'" . $customer_id . "', '" . $email . "','" . $phone . "')");
        endif;
    endif;
}

if (!wp_next_scheduled('st_cache_pages')) {
    wp_schedule_event(time(), 'daily', 'st_cache_pages');
}

function st_cache_pages() {
    require_once get_template_directory() . '/functions/cache_functions.php';
    new cache_functions();
}

add_action('manage_product_posts_custom_column', 'st_product_column_wholesale_price', 10, 2);

function st_product_column_wholesale_price($column, $postid) {
    if ($column == '_wholesale_price') {
        echo number_format(get_post_meta($postid, '_wholesale_price', true), 2);
    }
    if ($column == '_size') {
        echo get_post_meta($postid, 'st_size', true);
    }
    if ($column == '_colour') {
        echo get_post_meta($postid, 'st_colour', true);
    }
    if ($column == 'markup') {
        echo get_post_meta($postid, '_markup_precentage', true) . '%';
    }
}

add_filter('manage_edit-product_columns', 'st_show_product_order', 1);

function st_show_product_order($columns) {
    $newColumns = array();
    foreach ($columns as $key => $column):
        if ($key == 'price') {
            $newColumns['_wholesale_price'] = __('Wholesale Price');
            $newColumns['markup'] = __('Markup');
            $newColumns['_colour'] = __('Colour');
            $newColumns['_size'] = __('Size');
        }
        $newColumns[$key] = $column;
    endforeach;
    return $newColumns;
}

function st_selectively_enqueue_admin_script($hook) {
    if ('edit.php' != $hook) {
        return;
    }
    wp_enqueue_script('st_admin_products_script', get_template_directory_uri() . '/js/admin.js', array(), '1.0');
}

/*
 * -----------------------|
 *  Send order to vendors |
 * -----------------------|
 */

add_action('woocommerce_payment_complete_order_status', 'st_send_vendor_orders', 10, 2);

function st_send_vendor_orders($order_status, $order_id) {
    $order = wc_get_order($order_id);
    st_order_created($order);
    return $order_status;
}

function st_order_created($order) {
    $customer = array();
    $vendors = array();
    $billingAddress = array(
        'first_name' => $order->billing_first_name,
        'last_name' => $order->billing_last_name,
        'address_1' => $order->billing_address_1,
        'address_2' => $order->billing_address_2,
        'city' => $order->billing_city,
        'state' => $order->billing_state,
        'postcode' => $order->billing_postcode,
        'country' => $order->billing_country,
        'phone' => $order->billing_phone,
        'email' => $order->billing_email
    );
    $customer['billingAddress'] = $billingAddress;
    $billingAddress = array(
        'first_name' => $order->shipping_first_name,
        'last_name' => $order->shipping_last_name,
        'address_1' => $order->shipping_address_1,
        'address_2' => $order->shipping_address_2,
        'city' => $order->shipping_city,
        'state' => $order->shipping_state,
        'postcode' => $order->shipping_postcode,
        'country' => $order->shipping_country,
        'phone' => $order->shipping_phone
    );
    $customer['shippingAddress'] = $billingAddress;
    $customer['details'] = array(
        'order_number' => $order->id
    );
    $items = $order->get_items();
    foreach ($items as $index => $item):
        $wholeseller = get_post_meta($item['product_id'], 'vendor', true);
        $customer['vendors'][$wholeseller][$item['product_id']] = $item;
    endforeach;
    st_sendVendorEmails($customer);
}

function st_createOrderTextFile($vendor, $data) {
    $file = get_template_directory() . '/orders/2417.' . $data['details']['order_number'] . '.txt';
    $content = "001\t2417\t" . $data['shippingAddress']['first_name'] . " " . $data['shippingAddress']['last_name'] . "\tMe & Mrs Jones\tinfo@meandmrsjones.com.au\t1300 780 182\t" . $data['shippingAddress']['address_1'] . "," . $data['shippingAddress']['address_2'] . "\t" . $data['shippingAddress']['city'] . "\t" . $data['shippingAddress']['state'] . "\t" . $data['shippingAddress']['postcode'] . "\tDrop Ship\t" . $data['details']['order_number'] . "\t2417\r\n";
    foreach ($data['vendors'] as $_vendor => $item):
        if ($_vendor == $vendor):
            foreach ($item as $item_id => $_item):
				try {
					$item_quantity = $_item->get_quantity();
				} catch (Exception $e) {}				
                $content .= "002\t" . get_post_meta($_item['product_id'], '_code', true) . "\t" . $_item['name'] . "\t\t\t" . $item_quantity . "\t\r\n";
            endforeach;
        endif;
    endforeach;
    $content .= "003\tAcct\t\t\t\t\t\t\t\r\n";
    $content .= "999\r";
    file_put_contents($file, $content);
    return $file;
}

function st_createSimpleOrderTextFile($vendor, $data) {
    $vendorTrim = strtolower(str_replace(' ', '', $vendor));
    $file = get_template_directory() . '/orders/' . $vendorTrim . "-" . $data['details']['order_number'] . '.txt';
    $content = "New Order From MMJ\r\n\r\n";
    $content .= "Order number " . $data['details']['order_number'] . "\r\n\r\n";
    $content .= "Customer Details\r\n\r\n";
    $content .= "Billing Address\r\n\r\n";
    $content .= "{$data['billingAddress']['first_name']} {$data['billingAddress']['last_name']}\r\n{$data['billingAddress']['email']}\r\n{$data['billingAddress']['address_1']}\r\n {$data['billingAddress']['address_2']}\r\n {$data['billingAddress']['city']}\r\n {$data['billingAddress']['state']}\r\n {$data['billingAddress']['postcode']}\r\n\r\n";
    $content .= "Shipping Address\r\n\r\n";
    $content .= "{$data['shippingAddress']['first_name']} {$data['shippingAddress']['last_name']}\r\n {$data['shippingAddress']['address_1']}\r\n {$data['shippingAddress']['address_2']}\r\n {$data['shippingAddress']['city']}\r\n {$data['shippingAddress']['state']}\r\n {$data['shippingAddress']['postcode']}\r\n\r\n";
    $content .= "Item Details\r\n\r\n";
    foreach ($data['vendors'] as $_vendor => $item):
        if ($_vendor == $vendor):
            foreach ($item as $_item):
                $content .= "Code: " . get_post_meta($_item['product_id'], '_code', true) . ", Name: " . $_item['name'] . ", Size: " . get_post_meta($_item['product_id'], 'st_size', true) . ", Colour: " . get_post_meta($_item['product_id'], 'st_colour', true) . ", Quantity: " . $_item['item_meta']['_qty'][0] . ", Barcode: " . get_post_meta($_item['product_id'], '_barcode', true) . "\r\n";
            endforeach;
        endif;
    endforeach;
    file_put_contents($file, $content);
    return $file;
}

function st_sendVendorEmails($data) {
    $email_address = 'matt@chillidee.com.au';
    $subject = 'MMJ Order Placed';
    $headers = 'From: MMJ <info@meandmrsjones.com.au>' . "\r\n";
    $file = '';
    foreach ($data['vendors'] as $vendor => $items):
        switch ($vendor):
            case 'Windsor Wholeseller':
                $email_address = array('weborders@windsorwholesale.com.au', 'info@meandmrsjones.com.au');
                $file = st_createOrderTextFile($vendor, $data);
                if (!st_ftp_windsor_order($file)):
                    wp_mail($email_address, $subject, 'Please view attached order', $headers, $file);
                endif;
                break;
            case 'Wrapped Secrets':
                $email_address = array('admin@wrappedsecrets.com.au', 'des@wrappedsecrets.com.au', 'wstracking@meandmrsjones.com.au', 'info@meandmrsjones.com.au');
                $file = st_createSimpleOrderTextFile($vendor, $data);
                wp_mail($email_address, $subject, 'Please view attached order', $headers, $file);
                break;
            default:
                $file = st_createSimpleOrderTextFile($vendor, $data);
                wp_mail('info@meandmrsjones.co.au', 'ALERT! Order not sent to vendor', 'ALERT! Order not sent to vendor', $headers, $file);
                break;
        endswitch;
    endforeach;
}

function st_ftp_windsor_order($orderFile) {
    $result = false;
    $ftp_server = "windsorftp.dyndns.info";
    $ftp_user_name = "2417";
    $ftp_user_pass = "AP_2303";
    $conn_id = ftp_connect($ftp_server);
    $result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
    if ($result):
        $result = ftp_chdir($conn_id, '/webninja/');
        if ($result):
            $result = ftp_put($conn_id, basename($orderFile), $orderFile, FTP_ASCII);
        endif;
    endif;
    return $result;
}

add_action('admin_enqueue_scripts', 'st_selectively_enqueue_admin_script');

apply_filters('wp_mail_from', 'info@meandmrsjones.com.au');
apply_filters('wp_mail_from_name', 'Me & Mrs Jones');

/*
 * ----------------------------------------------|
 * Make sure scheduled posts have been made live |
 * ----------------------------------------------|
 */

function pubScheduledPost() {
    global $wpdb;
    $dateFormat = "Y-m-d H:i:s";
    $now = gmdate($dateFormat);
    $sql = "Select ID from $wpdb->posts where post_status='future' and post_date_gmt<'$now'";
    $resulto = $wpdb->get_results($sql);
    foreach ($resulto as $thisarr) {
        $changeID = $thisarr->ID;
        wp_publish_post($changeID);
    }
}

add_action("wp_head", "pubScheduledPost");

/*
 * ------------------------|
 * Add Text to order email |
 * ------------------------|
 */

add_action('woocommerce_email_after_order_table', 'st_add_text_to_order_email');

function st_add_text_to_order_email() {
    echo '<p style="margin:10px 0">Please note some orders will arrive in multiple parcels</p>';
}

function add_post_formats_filter_to_product_admin() {
    global $post_type;
    if ($post_type == 'product') {
        echo '<select name="filter_stock_product_admin">';
        echo '<option value="-1">All Stock';
        echo '</option>';
        echo '<option value="1" ';
        echo (isset($_GET['filter_stock_product_admin']) && !empty($_GET['filter_stock_product_admin']) && $_GET['filter_stock_product_admin'] === '1') ? 'selected' : '';
        echo '>In Stock</option>';
        echo '<option value="2" ';
        echo (isset($_GET['filter_stock_product_admin']) && !empty($_GET['filter_stock_product_admin']) && $_GET['filter_stock_product_admin'] === '2') ? 'selected' : '';
        echo '>Out of Stock</option>';
        echo '</select>';

        echo '<select name="filter_markups_product_admin">';
        echo '<option value="-1">All Markups';
        echo '</option>';
        echo '<option value="1" ';
        echo (isset($_GET['filter_markups_product_admin']) && !empty($_GET['filter_markups_product_admin']) && $_GET['filter_markups_product_admin'] === '1') ? 'selected' : '';
        echo '>Below 50%</option>';
        echo '<option value="2" ';
        echo (isset($_GET['filter_markups_product_admin']) && !empty($_GET['filter_markups_product_admin']) && $_GET['filter_markups_product_admin'] === '2') ? 'selected' : '';
        echo '>Below 100%</option>';
        echo '</select>';
    }
}

add_action('restrict_manage_posts', 'add_post_formats_filter_to_product_admin');

function add_post_format_filter_to_products($query) {
    global $post_type, $pagenow;

    if ($pagenow == 'edit.php' && $post_type == 'product') {
        if (isset($_GET['filter_stock_product_admin'])) {
            switch ($_GET['filter_stock_product_admin']):
                case '1':
                    $query->set('meta_query', array(
                        array(
                            'key' => '_stock_status',
                            'value' => 'instock',
                        ),
                    ));
                    break;
                case '2':
                    $query->set('meta_query', array(
                        array(
                            'key' => '_stock_status',
                            'value' => 'outofstock',
                        ),
                    ));
                    break;
            endswitch;
        }
        if (isset($_GET['filter_markups_product_admin'])) {
            switch ($_GET['filter_markups_product_admin']):
                case '1':
                    $query->set('meta_query', array(
                        array(
                            'key' => '_markup_precentage',
                            'value' => '50',
                            'compare' => '<=',
                            'type' => 'NUMERIC'
                        ),
                    ));
                    break;
                case '2':
                    $query->set('meta_query', array(
                        array(
                            'key' => '_markup_precentage',
                            'value' => '100',
                            'compare' => '<=',
                            'type' => 'NUMERIC'
                        ),
                    ));
                    break;
            endswitch;
        }
    }
}

add_action('pre_get_posts', 'add_post_format_filter_to_products');


/*
 * ------------------------------------|
 * Make columns sortable in admin area |
 * ------------------------------------|
 */

add_filter('manage_edit-product_sortable_columns', 'sortable_product_columns');

function sortable_product_columns($columns) {
    $custom = array(
        'is_in_stock' => 'is_in_stock',
        '_wholesale_price' => 'wholesale_price',
        'markup' => 'markup'
    );
    return wp_parse_args($custom, $columns);
}

add_filter('request', 'sortable_product_columns_handler');

function sortable_product_columns_handler($vars) {
    if (isset($vars['orderby']) && 'wholesale_price' == $vars['orderby']) {
        $vars = array_merge($vars, array(
            'meta_key' => '_wholesale_price',
            'orderby' => 'meta_value_num'
        ));
    }
    if (isset($vars['orderby']) && 'is_in_stock' == $vars['orderby']) {
        $vars = array_merge($vars, array(
            'meta_key' => '_stock_status',
            'orderby' => 'meta_value'
        ));
    }
    if (isset($vars['orderby']) && 'markup' == $vars['orderby']) {
        $vars = array_merge($vars, array(
            'meta_key' => '_markup_precentage',
            'orderby' => 'meta_value_num'
        ));
    }
    return $vars;
}

/*
 * ---------------------------------|
 * Remove columns from product list |
 * ---------------------------------|
 */

add_filter('manage_edit-product_columns', 'removeProductColumns');

function removeProductColumns($columns) {
    unset($columns['featured']);
    unset($columns['product_type']);
    unset($columns['wpseo-score-readability']);
    unset($columns['wpseo-score']);
    return $columns;
}

function st_get_product_reviews_callback() {
    require_once 'functions/reviews_class.php';
    $post_id = strip_tags($_POST['post_id']);
    $reviews = reviews_class::getReviewsTextOnly($post_id);
    $cleanArray = array();
    foreach ($reviews as $review):
        array_push($cleanArray, json_decode($review->comment_content));
    endforeach;
    echo (isset($cleanArray) && !empty($cleanArray)) ? json_encode($cleanArray) : json_encode(array('error' => 'Error Loading Reviews'));
    wp_die();
}

function st_add_featured_to_edit_page() {
    require_once WP_PLUGIN_DIR . '/warehouse/includes/warehouse_product_edit_extra_functions.php';
    warehouse_product_edit_extra_functions::add_featured_meta_box();
}

add_action('admin_init', 'st_add_featured_to_edit_page');

require_once 'ajax_functions.php';

function cp_change_admin_featured_product_image( $content, $post_id ) {
	
	$post = get_post($post_id); 
	
	if ( 'product' == get_post_type($post) ) {
		$content = str_replace( 'TB_iframe=1"', 'TB_iframe=1&amp;width=85%&amp;height=90%"', $content);
		$content = str_replace( 'Set product image', '<img width="266" height="266" src="'.get_post_meta($post_id, 'main_image_link', true).'" class="attachment-266x266 size-266x266" alt="">Only available on Amazon S3', $content );		
	}
	return $content;
}
add_filter( 'admin_post_thumbnail_html', 'cp_change_admin_featured_product_image', 10, 2 );

function cp_change_admin_product_image_list( $column, $postid ) {
    if ( $column == 'thumb' ) {
		   if (!has_post_thumbnail($postid)){
				$elem_id = uniqid();
				echo '<img width="150" height="150" src="'.get_post_meta($postid, 'main_image_link', true).'" class="attachment-thumbnail size-thumbnail wp-post-image '.$elem_id.'" alt="">';
				echo '<script>jQuery(".'.$elem_id.'").siblings().first().html("");jQuery(".'.$elem_id.'").appendTo(jQuery(".'.$elem_id.'").siblings().first());</script>';
		   }
    }
}

add_action( 'manage_product_posts_custom_column', 'cp_change_admin_product_image_list', 10, 2 );